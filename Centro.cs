﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace WSZFK
{
    public class Centro
    {
        public string IdCentro { get; set; }
        public string Descripcion { get; set; }
        public string IdEmpresa { get; set; }
        public string CodCentroCarrefour { get; set; }

        public static Centro DevolverCentro(string IdCentro, string Descripcion, string CodCentroCarrefour )
        {
            Centro Cen = new Centro();
            try
            {
                string SQL = string.Empty;

                if (!string.IsNullOrEmpty(CodCentroCarrefour))
                    SQL = "select top 1 * from centro where CodCentroCarrefour='" + CodCentroCarrefour + "'";
                else
                {
                    if (!string.IsNullOrEmpty(Descripcion))
                    {
                        SQL = "select top 1 * from centro where Descripcion='" + Descripcion + "'";
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(IdCentro))
                        {
                            SQL = "select top 1 * from centro where IdCentro='" + IdCentro + "'";
                        }
                    }
                }
                if (!string.IsNullOrEmpty(SQL))
                {
                    DataTable datos = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                    if (datos.Rows.Count > 0)
                    {
                        Cen.IdCentro  = datos.Rows[0]["IdCentro"].ToString();
                        Cen.Descripcion  = datos.Rows[0]["Descripcion"].ToString();
                        Cen.IdEmpresa = datos.Rows[0]["IdEmpresa"].ToString();
                        CodCentroCarrefour= datos.Rows[0]["CodCentroCarrefour"].ToString();

                        return Cen;
                    }
                    else
                        return null;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {

                Trazas t = new Trazas("DevolverDocumento", "Documento.cs", -1, ex.Message, ex.ToString());
                return null;
            }
        }

        public static Boolean ExisteCentro(Centro Cen)
        {
            try
            {
                if (!string.IsNullOrEmpty(Cen.CodCentroCarrefour ) && !string.IsNullOrEmpty(Cen.Descripcion))
                {
                    string SQL = "select * from Centro where CodCentroCarrefour='" + Cen.CodCentroCarrefour + "' and Descripcion='" + Cen.Descripcion  + "'";
                    DataTable DT = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                    if (DT.Rows.Count > 0)
                        return true;
                    else
                        return false;

                }
                return false;

            }
            catch (Exception e)
            {
                return false;
            }
        }
        public static string GuardarCentroenBBDD(Centro Cen)
        {
            try
            {
                string SQL, Res = string.Empty;
                if (!ExisteCentro(Cen))
                {
                    SQL = "Insert into Centro (Descripcion,IdEmpresa,CodCentroCarrefour) values ('" + Cen.Descripcion + "'," + Cen.IdEmpresa + ",'" + Cen.CodCentroCarrefour + "')";
                    Res = DatabaseConnection.executeScalarString(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                }
                return Res;
            }
            catch (Exception E)
            {
                return "";
            }
        }
    }
}