﻿using System;
using System.Collections.Generic;
using System.IO;

namespace WSZFK
{
    internal class SvcEmail : IDisposable
    {
        public int idEmail { get; set; }
        private int idServer = -1;

        internal bool tieneHTML = false;

        public class cCC
        {

            public List<string> emails = new List<string>();

            public cCC()
            {

            }

            public void Add(string email)
            {
                emails.Add(email);
            }

            public int Count()
            {
                return emails.Count;
            }

        }

        public class cCCO
        {

            public List<string> emails = new List<string>();

            public cCCO()
            {

            }

            public void Add(string email)
            {
                emails.Add(email);
            }

            public int Count()
            {
                return emails.Count;
            }

        }

        public cAttachments Attachment = new cAttachments();
        public cHost Host = new cHost();
        public cFrom From = new cFrom();
        public cCC CC = new cCC();
        public cCCO CCO = new cCCO();

        public class cAttachments
        {

            public List<Document> _att = new List<Document>();

            public cAttachments()
            {

            }

            public void Add(Document doc)
            {
                _att.Add(doc);
            }

            public int Count()
            {
                return _att.Count;
            }

        }

        internal int saveResult(int intento, string respuesta, bool ok)
        {
            int resp = -1;
            try
            {

                respuesta = Utilities.tratarParam(respuesta);


                resp =
                    DatabaseConnection.executeNonQueryInt(string.Format(
                        @"INSERT INTO intentos (idemail, numIntento, fechaIntento, resultado, correcto) 
                        VALUES
                        ('{0}', '{1}', GETDATE(), '{2}', '{3}')",
                        idEmail, intento, respuesta, ok),
                        System.Data.CommandType.Text,
                        ConnectionString.emails
                        );




            }
            catch (Exception e)
            {
                Trazas t = new Trazas("saveResult", "SvcEmail", -1, e.Message, e.ToString());
            }
            return resp;
        }

        internal class cHost
        {
            internal string host = string.Empty;
            internal int port = 0;
            internal string user = string.Empty;
            internal string pass = string.Empty;
            internal bool startTLS = false;

            public void Set(string _host, int _port, string _user, string _pass, bool _startTLS)
            {

                host = _host;
                user = _user;
                pass = _pass;
                startTLS = _startTLS;
                port = _port;

            }

            public cHost()
            {

            }


        }

        internal string To { get; set; }

        internal class cFrom
        {
            internal string direccionEmailFrom = string.Empty;
            internal string nombreFrom = string.Empty;

            public void Set(string _direccionEmailFrom, string _nombreFrom)
            {
                direccionEmailFrom = _direccionEmailFrom;
                nombreFrom = _nombreFrom;
            }

            public cFrom()
            {

            }
        }

        internal string Subject { get; set; }

        internal string Body { get; set; }

        internal int idEstado { get; set; }

        /// <summary>
        /// Clase para crear documentos que se adjuntaran al email
        /// </summary>
        internal class Document
        {

            public string name { get; set; }
            public byte[] file { get; set; }
            public string mimeType { get; set; }

            public Document(string _name, byte[] _file, string _mimeType)
            {
                name = _name;
                file = _file;
                mimeType = _mimeType;
            }

        }

        internal void Send(int idEmpresa)
        {
            // Vamos a insertar el email completo
            createEmailServer(idEmpresa);
            createEmailHeader();
            createEmailCCs();
            createEmailCCOs();
            createEmailAttachments();
        }

        private void createEmailIntento(int intento, bool? correcto)
        {
            try
            {


                int resp =
                    DatabaseConnection.executeNonQueryInt(string.Format(
                        @"INSERT INTO intentos (idemail, numIntento, fechaIntento, resultado, correcto) 
                        VALUES
                        ('{0}', '{1}', '{2}', GETDATE(), '{3}')",
                        idEmail, intento, correcto),
                        System.Data.CommandType.Text,
                        ConnectionString.emails
                        );




            }
            catch (Exception e)
            {
                Trazas t = new Trazas("createEmailIntento", "SvcEmail", -1, e.Message, e.ToString());
            }
        }

        private void createEmailServer(int idEmpresa)
        {
            try
            {
                idServer =
                     DatabaseConnection.executeScalarInt(string.Format(
                         @"SELECT TOP 1 id FROM servers 
                        WHERE host = '{0}' and usuario = '{1}' and pass = '{2}' and port = '{3}' and startTLS = '{4}' and idEmpresa = '{5}'",
                         Host.host, Host.user, Host.pass, Host.port, Host.startTLS, idEmpresa),
                         System.Data.CommandType.Text,
                         ConnectionString.emails
                         );


                if (idServer <= 0)
                {

                    idServer =
                        DatabaseConnection.executeScalarInt(string.Format(
                            @"INSERT INTO servers (nombre, descripcion, host, port, usuario, pass, startTLS, idEmpresa) 
                        VALUES
                        ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}'); select SCOPE_IDENTITY();",
                            Host.host, Host.host, Host.host, Host.port, Host.user, Host.pass, Host.startTLS, idEmpresa),
                            System.Data.CommandType.Text,
                            ConnectionString.emails
                            );

                }


            }
            catch (Exception e)
            {
                Trazas t = new Trazas("createEmailServer", "SvcEmail", -1, e.Message, e.ToString());
            }
        }

        private void createEmailHeader()
        {
            try
            {
                Subject = Subject.Replace("'", "''");
                Body = Body.Replace("'", "''");
                From.nombreFrom = From.nombreFrom.Replace("'", "''");

                idEmail =
                    DatabaseConnection.executeScalarInt(string.Format(
                            @"INSERT INTO emails (idServer, emisor, receptor, asunto, cuerpo, nombreFrom, tieneCC, tieneCCO, tieneAdjuntos, tieneHTML, idEstado, entorno) 
                        VALUES
                        ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}'); select SCOPE_IDENTITY();",
                            idServer, From.direccionEmailFrom, To, Subject, Body, From.nombreFrom,
                            CC.Count() > 0, CCO.Count() > 0, Attachment.Count() > 0, tieneHTML, idEstado, "WSZFK"),
                            System.Data.CommandType.Text,
                            ConnectionString.emails
                            );


            }
            catch (Exception e)
            {
                Trazas t = new Trazas("createEmailHeader", "SvcEmail", -1, e.Message, e.ToString());
            }
        }

        private void createEmailCCs()
        {
            try
            {
                for (int i = 0; i < CC.Count(); i++)
                {

                    try
                    {
                        int resp =
                            DatabaseConnection.executeNonQueryInt(string.Format(
                                    @"INSERT INTO CCs (idEmail, email) 
                        VALUES
                        ('{0}', '{1}'); select SCOPE_IDENTITY();",
                                    idEmail, CC.emails[i]),
                                    System.Data.CommandType.Text,
                                    ConnectionString.emails
                                    );

                    }
                    catch (Exception ex)
                    {
                        Trazas t = new Trazas("createEmailCCsIn", "SvcEmail", -1, ex.Message, ex.ToString());
                    }

                }
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("createEmailCCs", "SvcEmail", -1, e.Message, e.ToString());
            }
        }

        private void createEmailCCOs()
        {
            try
            {
                for (int i = 0; i < CCO.Count(); i++)
                {

                    try
                    {
                        int resp =
                            DatabaseConnection.executeNonQueryInt(string.Format(
                                    @"INSERT INTO CCOs (idEmail, email) 
                        VALUES
                        ('{0}', '{1}'); select SCOPE_IDENTITY();",
                                    idEmail, CCO.emails[i]),
                                    System.Data.CommandType.Text,
                                    ConnectionString.emails
                                    );

                    }
                    catch (Exception ex)
                    {
                        Trazas t = new Trazas("createEmailCCOsIn", "SvcEmail", -1, ex.Message, ex.ToString());
                    }

                }
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("createEmailCCOs", "SvcEmail", -1, e.Message, e.ToString());
            }
        }

        /*
        internal static string getRutaAdjuntosEmails(string idAdjunto)
        {
            string path = "0000000000".Substring(idAdjunto.Length) + idAdjunto;
            if (Convert.ToInt32(idAdjunto) < 10) idAdjunto = "0" + idAdjunto;

            string pathString = WebConfigurationManager.AppSettings["ruta"] + "adjuntosEmails/" + path.Substring(0, 2) + "/" + path.Substring(2, 2) + "/" +
                    path.Substring(4, 2) + "/" + idAdjunto.Substring(idAdjunto.Length - 2, 2) + ".d";


            return pathString;
        }
        */

        private void createEmailAttachments()
        {
            try
            {
                for (int i = 0; i < Attachment.Count(); i++)
                {

                    try
                    {
                        int idAdjunto =
                            DatabaseConnection.executeScalarInt(string.Format(
                                    @"INSERT INTO adjuntos (idEmail, nombre, mimeType) 
                        VALUES
                        ('{0}', '{1}', '{2}'); select SCOPE_IDENTITY();",
                                    idEmail, Attachment._att[i].name, Attachment._att[i].mimeType),
                                    System.Data.CommandType.Text,
                                    ConnectionString.emails
                                    );

                        if (idAdjunto > 0)
                        {
                            // El fichero se ha insertado correctamente en la base de datos, lo guardamos
                            string ruta = Utilities.getRutaAdjuntosEmails(idAdjunto + "");

                            File.WriteAllBytes(ruta, Attachment._att[i].file);

                        }

                    }
                    catch (Exception ex)
                    {
                        Trazas t = new Trazas("createEmailAttachmentsIn", "SvcEmail", -1, ex.Message, ex.ToString());
                    }

                }
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("createEmailAttachments", "SvcEmail", -1, e.Message, e.ToString());
            }
        }

        public void Dispose()
        {
            // Aquí borramos limpiamos todo
        }
    }
}
