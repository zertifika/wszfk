﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web.Security;
using System.Data;
using System.Threading.Tasks;
using Limilabs.Client.SMTP;
using Limilabs.Mail;
using Limilabs.Mail.Headers;
using System.Web.Configuration;
using System.IO;

namespace WSZFK
{
    public class Utilities
    {
        #region Generales
        public static String getParam(HttpContext c, String s)
        {
            string parametro = tratarParam(c.Request[s]);
            return WebUtility.HtmlEncode(parametro);
        }


        public static string tratarParam(string s)
        {
            if (string.IsNullOrEmpty(s)) return null;

            if (s.Contains("--")) s = s.Replace("--", "");
            if (s.Contains("'")) s = s.Replace("'", "");
            return s;
        }

        

        #endregion

        #region Seguridad y criptografía
        public static String GetSHA1(String data)
        {
            return FormsAuthentication.HashPasswordForStoringInConfigFile(data, "SHA1");
        }

        public static int comprobarValidezUsuario(string tokenUsuario)
        {
            try
            {
                if (tokenUsuario.Contains(';'))
                    tokenUsuario = tokenUsuario.Split(';')[0];

                String query = "EXEC getPass " + tokenUsuario.Split('I')[0];
                String pass = DatabaseConnection.executeScalar(query, CommandType.Text,  ConnectionString.CadenaConexionZDocsSinProvider).ToString();

                if (Utilities.GetSHA1(pass) == tokenUsuario.Split('I')[1])
                {
                    return 1;
                }
                return -1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }
        #endregion

        #region enviarMail
        public static string enviarMail2(string direccionEmailTo, string direccionEmailFrom,
            string asunto, string contenido, string nombreDocumento = "", byte[] documento = null, string Host = "mail.zertifika.com", string User = "info@zertifika.com",
            string Pass = "r3dM#z41#z41#z41", string nombreFrom = "", string mailCCO = "", List<string> mailCCs = null, int idEstado = -1, int Port = 587, int idEmpresa = -1)
        {
            bool startTLS = false;
            try
            {

#if DEBUG
                direccionEmailTo = "bcmail@zertifika.com";
                Host = "mail.zertifika.com";
                User = "info@zertifika.com";
                Pass = "r3dM#z41#z41#z41";
                mailCCs = null;
                mailCCO = string.Empty;
                // Seguidores usuarios
                // List<string> seguidores = BasicoFlujo.getSeguidoresUsuario(direccionEmailTo);
#endif

                Trazas tt = new Trazas("BasicoFlujo", "enviarMail2", -1, "Sin error", "Se va a enviar mail a " + direccionEmailTo);

                // Insertamos en el proceso

                SvcEmail em = new SvcEmail();

                direccionEmailTo = direccionEmailTo.Trim();
                direccionEmailFrom = direccionEmailFrom.Trim();

                try
                {
                    startTLS = !(Host.ToLower().Contains("zertifika"));

                    if ((Host.Equals("192.168.131.50")))
                    {
                        direccionEmailFrom = "info@fnmt.es";
                        startTLS = true;
                    }

                    em.Host.Set(Host, Port, User, Pass, startTLS);

                    em.To = direccionEmailTo;
                    em.From.Set(direccionEmailFrom, nombreFrom);
                    em.Subject = asunto;
                    em.Body = contenido;
                    em.idEstado = idEstado;
                    if (!string.IsNullOrEmpty(nombreDocumento) && documento != null)
                    {
                        SvcEmail.Document doc = new SvcEmail.Document(nombreDocumento, documento, "application/pdf");
                        em.Attachment.Add(doc);
                    }

                    //em.CC.Add("josearamz@gmail.com");
                    //em.CCO.Add("jramirez@zertifika.com");
                    //em.Attachment.Add(new SvcEmail.Document("pp.pdf", File.ReadAllBytes("c:/temp/pp.pdf"), "application/pdf"));

                    em.tieneHTML = true;
                    if (!string.IsNullOrEmpty(mailCCO)) em.CCO.Add(mailCCO.Trim());

                    // Añadimos BCC de seguidores
                    //if (seguidores.Count > 0)
                    //{
                    //    foreach (string s in seguidores)
                    //    {
                    //        em.CCO.Add(s);
                    //    }
                    //}


                    // Añadimos CCs
                    if (mailCCs != null && mailCCs.Count > 0)
                    {
                        foreach (string s in mailCCs)
                        {
                            em.CC.Add(s.Trim());
                        }
                    }

                    em.Send(idEmpresa);

                }
                catch (Exception e)
                {
                    Trazas tt2 = new Trazas("BasicoFlujo", "Error en servicio emails", -1, e.Message, e.ToString());
                }

                Task t = new Task(() =>
                {


                    //            SmtpClient client = new SmtpClient("smtp.office365.com", 587);
                    //            client.EnableSsl = true;
                    //            client.Credentials = new System.Net.NetworkCredential(User, Pass);
                    //            MailAddress from = new MailAddress(direccionEmailFrom, nombreFrom, System.Text.Encoding.UTF8);
                    //            MailAddress to = new MailAddress("jramirez@zertifika.com");
                    //            MailMessage message = new MailMessage(from, to);
                    //            message.Body = contenido;
                    //            message.BodyEncoding = System.Text.Encoding.UTF8;
                    //            message.Subject = asunto;
                    //            message.SubjectEncoding = System.Text.Encoding.UTF8;
                    //            // Set the method that is called back when the send operation ends.
                    //            client.SendCompleted += new SendCompletedEventHandler(SendCompletedCallback);
                    //            ServicePointManager.ServerCertificateValidationCallback =
                    //(sender, certificate, chain, sslPolicyErrors) => true;
                    //            // The userState can be any object that allows your callback 
                    //            // method to identify this send operation.
                    //            // For this example, I am passing the message itself
                    //            client.TargetName = "STARTTLS/smtp.office365.com";
                    //            client.SendAsync(message, message);
                        
                    using (Smtp smtp = new Smtp())
                    {
                        try
                        {
                            smtp.Connect(Host, Port);
                            if (!Host.ToLower().Contains("zertifika") && !direccionEmailFrom.ToLower().Contains("fnmt.es"))
                                smtp.StartTLS();


                            if (!direccionEmailFrom.ToLower().Contains("fnmt.es"))
                            {
                                smtp.UseBestLogin(User, Pass);
                            }

                            MailBuilder builder = new MailBuilder();

                            builder.Html = contenido;
                            builder.From.Add(new MailBox(direccionEmailFrom, nombreFrom));
                            builder.To.Add(new MailBox(direccionEmailTo));

                            builder.Subject = asunto;

                            if (!string.IsNullOrEmpty(mailCCO))
                            {
                                builder.Bcc.Add(new MailBox(mailCCO.Trim()));
                            }

                            // Añadimos BCC de seguidores
                            //if (seguidores.Count > 0)
                            //{
                            //    foreach (string s in seguidores)
                            //    {
                            //        builder.Bcc.Add(new MailBox(s));
                            //    }
                            //}

                            // Añadimos CCs
                            if (mailCCs != null && mailCCs.Count > 0)
                            {
                                foreach (string s in mailCCs)
                                {
                                    builder.Cc.Add(new MailBox(s.Trim()));
                                }
                            }

                            // si es de estrella ponemos como copia oculta a nic
                            if (User.Contains("@estrellagalicia.es"))
                            {
                                builder.Bcc.Add(new MailBox("nic@estrellagalicia.es"));
                            }

                            builder.Bcc.Add(new MailBox("bcmail@zertifika.com"));

                            //builder.Bcc.Add(new MailBox("jramirez@zertifika.com"));
                            //builder.Bcc.Add(new MailBox("jramirez@zertifika.com"));
                            //builder.Cc.Add(new MailBox("jaime.maceiras@bizlayer.com"));
                            if (documento != null)
                            {
                                builder.AddAttachment(documento).FileName = nombreDocumento;
                            }

                            IMail email = builder.Create();

                            SendMessageResult smr = smtp.SendMessage(email);

                            Trazas tt3 = new Trazas("BasicoFlujo SMR", "enviarMail info", -1, direccionEmailTo + "( " + asunto + ") : " + smr.ToString(), "");

                            smtp.Close();

                            em.saveResult(0, smr.ToString(), smr.Status == SendMessageStatus.Success);

                        }
                        catch (Exception e)
                        {
                            Trazas tt2 = new Trazas("BasicoFlujo", "enviarMail ERROR", -1, e.Message, e.ToString());
                            em.saveResult(0, e.Message, false);
                        }
                    }
                });

                t.Start();

                // t.Wait();

                return "1";

            }
            catch (Exception ex)
            {
                new Trazas("enviarEmail2", "BasicoFlujo", -1, ex.Message, ex.ToString());
                return "-1";
            }
        }

        internal static string getRutaAdjuntosEmails(string idAdjunto)
        {
            string path = "0000000000".Substring(idAdjunto.Length) + idAdjunto;
            if (Convert.ToInt32(idAdjunto) < 10) idAdjunto = "0" + idAdjunto;

            string pathString = WebConfigurationManager.AppSettings["ruta"] + "adjuntosEmails/" + path.Substring(0, 2) + "/" + path.Substring(2, 2) + "/" +
                    path.Substring(4, 2) + "/" + idAdjunto.Substring(idAdjunto.Length - 2, 2) + ".d";

            Directory.CreateDirectory(WebConfigurationManager.AppSettings["ruta"] + "adjuntosEmails/" + path.Substring(0, 2) + "/" + path.Substring(2, 2) + "/" +
                    path.Substring(4, 2) + "/");

            return pathString;
        }
        #endregion
    }
}