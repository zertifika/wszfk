﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace WSZFK
{
    public class Documento
    {
        public string Id { get; set; }
        public  string Nombre { get; set; }
        public  string FechaCreacion { get; set; }
        public string IdUsuario { get; set; }
        public Boolean Activo { get; set; }
        //  public virtual string IdAnteriorDocumento { get; set; }
        public string Formato { get; set; }
        public  string NombreOriginal { get; set; }
        public  string CodCentroCarrefour { get; set; }
        public string IdEmpresa { get; set; }
        public  string FechaUltimaModicacion { get; set; }
        public  Boolean EsActualizacion { get; set; }
        public string Version { get; set; }

        public static Documento DevolverDocumento(string IdDocumento, string Nombre, string token = "", string idUsuario = "")
        {
            Documento Doc = new Documento();
            try
            {
                string IdUsuario = "";
                string SQL = "";


                if (!string.IsNullOrEmpty(IdDocumento))
                    SQL = "select top 1 * from Documentos doc left join documentosusuarios dusu on doc.iddocumento=dusu.iddocumento where doc.iddocumento=" + IdDocumento;
                else
                    SQL = "select * from Documentos doc,documentosusuarios dusu where (doc.activo='True') and doc.iddocumento=dusu.iddocumento and doc.nombre='" + Nombre + "' and dusu.idusuario='" + IdUsuario + "'";


                DataTable datos = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                if (datos.Rows.Count > 0)
                {
                    Doc.Id = datos.Rows[0]["iddocumento"].ToString();
                    Doc.Nombre = datos.Rows[0]["docOriginal"].ToString();
                    Doc.FechaCreacion = datos.Rows[0]["fechaCreacion"].ToString();

                    Doc.IdUsuario = datos.Rows[0]["idusuario"].ToString(); //Usuario.DevolverUsuario(datos.Rows[0]["idusuario"].ToString(), ""); 
                    Doc.Activo = (datos.Rows[0]["Activo"].ToString() == "1" ? true : false);
                    Doc.Formato = datos.Rows[0]["formato"].ToString();
                    Doc.NombreOriginal = datos.Rows[0]["NombreOriginal"].ToString();
                    if (!DBNull.Value.Equals(datos.Rows[0]["Idcentro"].ToString()))
                        Doc.CodCentroCarrefour = DevolverCodCentroCarrefour(datos.Rows[0]["Idcentro"].ToString());

                    Doc.IdEmpresa = datos.Rows[0]["idempresa"].ToString();
                    Doc.FechaUltimaModicacion = datos.Rows[0]["FechaUltimaModificacion"].ToString();
                    Doc.EsActualizacion = (datos.Rows[0]["Esactualizacion"].ToString() == "1" ? true : false);
                    Doc.Version = datos.Rows[0]["version"].ToString();

                    return Doc;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {

                Trazas t = new Trazas("DevolverDocumento", "Documento.cs", -1, ex.Message, ex.ToString());
                return null;
            }
        }
        public static string DevolverCodCentroCarrefour(string IdCentro)
        {
            try
            {
                string SQL = "Select CodCentroCarrefour from centro where Idcentro=" + IdCentro;

                System.Data.DataTable DT = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                string Resul = string.Empty;
                if (DT.Rows.Count > 0)
                {
                    Resul = DT.Rows[0]["CodcentroCarrefour"].ToString();
                }
                return Resul;
            }
            catch (Exception e)
            {
                return "";
            }
        }
        public static string DevolverIdCentro(string CodCentroCarrefour)
        {
            try
            {
                string SQL = "Select IdCentro from centro where CodcentroCarrefour=" + CodCentroCarrefour;

                System.Data.DataTable DT = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                string Resul = string.Empty;
                if (DT.Rows.Count > 0)
                {
                    return DT.Rows[0]["Idcentro"].ToString();
                }
                else
                   return "-1";
            }
            catch (Exception e)
            {
                return "";
            }
        }
    }
}