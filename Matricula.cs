﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace WSZFK
{
    public class Matricula
    {
        public string Centro { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Dni { get; set; }
        public string Texto { get; set; }
        public string Fecha { get; set; }
        public string CodCentroCarrefour { get; set; }

        public static Boolean ExisteMatricula(Matricula Mat)
        {
            try
            {
                if (!string.IsNullOrEmpty(Mat.CodCentroCarrefour) && !string.IsNullOrEmpty(Mat.Descripcion))
                {
                    string SQL = "select * from matriculas where [Codigo Centro]='" + Mat.CodCentroCarrefour + "' and Matricula='" + Mat.Descripcion + "'";
                    DataTable DT = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                    if (DT.Rows.Count > 0)
                        return true;
                    else
                        return false;

                }
                return false;

            }
            catch (Exception e)
            {
                return false;
            }
        }
        public static Matricula DevolverMatricula(string Descripcion, string Dni, string CodCentroCarrefour)
        {
            Matricula Mat = new Matricula();
            try
            {
                string SQL = string.Empty;

                if (!string.IsNullOrEmpty(CodCentroCarrefour))
                    SQL = "select top 1 * from matriculas where [Codigo Centro]='" + CodCentroCarrefour + "'";
                else
                {
                    if (!string.IsNullOrEmpty(Descripcion))
                    {
                        SQL = "select top 1 * from matriculas where Matricula='" + Descripcion + "'";
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(Dni))
                        {
                            SQL = "select top 1 * from Matriculas where Dni='" + Dni + "'";
                        }
                    }
                }
                if (!string.IsNullOrEmpty(SQL))
                {
                    DataTable datos = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                    if (datos.Rows.Count > 0)
                    {
                        Mat.Centro  = datos.Rows[0]["Centro"].ToString();
                        Mat.Nombre = datos.Rows[0]["Nombre"].ToString();
                        Mat.Descripcion = datos.Rows[0]["Matricula"].ToString();
                        Mat.Dni = datos.Rows[0]["Dni"].ToString();
                        Mat.Texto = datos.Rows[0]["Texto"].ToString();
                        Mat.Fecha = datos.Rows[0]["Fecha"].ToString();
                        Mat.CodCentroCarrefour = datos.Rows[0]["Codigo Centro"].ToString();
                        return Mat;
                    }
                    else
                        return null;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {

                Trazas t = new Trazas("DevolverDocumento", "Documento.cs", -1, ex.Message, ex.ToString());
                return null;
            }
        }
        public static string GuardarMatriculaenBBDD(Matricula Mat)
        {
            try {
                string SQL, Res = string.Empty;
                if (!ExisteMatricula(Mat))
                {
                    SQL = "Insert into Matriculas (Centro,Nombre,Matricula,Dni,Texto,Fecha,[Codigo Centro]) values ('" + Mat.Centro + "','" + Mat.Nombre + "','" + Mat.Descripcion + "','" + Mat.Dni + "','" + Mat.Texto + "','" + Mat.Fecha + "','" + Mat.CodCentroCarrefour + "')";
                    Res = DatabaseConnection.executeScalarString(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                }
                return Res;
            }
            catch (Exception E)
            {
                return "";
            }
        }
    }
}