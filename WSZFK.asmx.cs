﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Data.OleDb;
using System.Data;
//using WS = WSZFK.GetMyCompanyUsers;
using WSFich = WSZFK.wszfkap;
using WSAlta.Encryption;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Net.Mail;
using System.Collections;
using System.Linq;
using System.Xml;
using System.Web.Services;
using System.Web.Services.Protocols;
using Indexacion.Validacion;
using System.Globalization;
using Newtonsoft.Json;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.Web.Configuration;
using System.Text;
using System.Web.Security;
using System.Security.Cryptography;
using System.Web.Services.Configuration;
using WSZFK.FNMT;


namespace WSZFK
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]


    public class Service1 : System.Web.Services.WebService
    {
        string CadenaConexion = "Provider=SQLNCLI11;Server=tcp:wtpmh0e15c.database.windows.net;Database=zertifika;Uid=ZertifikaAdmin@wtpmh0e15c.database.windows.net;Pwd=zdbB!NCTjl$Lp3NPMWqZ;Encrypt=True;Connection Timeout=3000";
        //public const string ConnectionString.CadenaConexionZDocs = "Provider=SQLNCLI11;Server=tcp:192.168.1.95;Database=SignesDemos;Uid=edoc;Pwd=edoc;Encrypt=True;Connection Timeout=30";
        //public const string ConnectionString.CadenaConexionZDocs = "Provider=SQLNCLI11;Server=tcp:192.168.1.95;Database=SignesDemos;Uid=edoc;Pwd=edoc;Encrypt=True;Connection Timeout=30";

        //public const string ConnectionString.CadenaConexionZDocs = "Server=tcp:192.168.1.95;Database=SignesPro;Uid=edoc;Pwd=edoc;Encrypt=True;Connection Timeout=30";
        // public const string ConnectionString.CadenaConexionZDocs = "Provider=SQLNCLI11;Server=SMET-ZTK01;Database=Signes;Database=signes;Uid=sa;Pwd=Zertifika;Encrypt=True;Connection Timeout=3000;TrustServerCertificate=True";     


        public string Directorio = WebConfigurationManager.AppSettings["RutaVolumenes"];
        public string RutaInicial = WebConfigurationManager.AppSettings["RutaInicial"];

        //public const string ConnectionString.CadenaConexionZDocs = "Provider=SQLNCLI11;Server=tcp:192.168.1.95;Database=SignesDemos;Uid=edoc;Pwd=edoc;Encrypt=True;Connection Timeout=30";
        public const string PaginaLogin = "index.aspx";
        public const string PaginaDocsCompartidos = "DocumentosCompartidos.aspx";
        //  public const string RutaInicial = "62.43.192.130:83/ZDocs/";        
        public const string IdUsuarioMaestro = "5421";// 5;// 5421;
        //5421 ES EL USUARIO MAESTRO DE CARREFOUR EN LOCAL, EL 4701 EN LA FABRICA

        public const double IdUsuarioGarvin = 5421;
        public const double IdEmpresaCarrefour = 9;// 1;// 9;
        public double IdUsuarioFormacion = Convert.ToDouble(WebConfigurationManager.AppSettings["UsuarioFormacion"]);
        public AuthenticationHeader AuthenticationInformation;

        //  WSZFK.Service1 wsprueba = new WSZFK.Service1();
        public class AuthenticationHeader : SoapHeader
        {
            public string UserName;
            public string Password;
        }

        //[WebMethod]
        //public string ExisteUsuario(string Usuario, string Password)
        //{
        //    WS.UserInfo _info = new WS.UserInfo();
        //    try
        //    {

        //        _info.Mail = Usuario;
        //        _info.Pwd = Password;


        //        WS.WSAccessPoint _cliente = new WS.WSAccessPoint();
        //        string kk = _cliente.GetCategories(_info);


        //    }
        //    catch (Exception ex)
        //    {
        //        string error = ex.Message;
        //        if (error == "Usuario/Password Incorrectos")
        //        {
        //            return "Usuario/Password Incorrectos";
        //        }

        //        if (error == "Usuario no encontrado")
        //        {
        //            return "Usuario no encontrado";
        //        }


        //        return idUsuario(Usuario);
        //    }

        //    return idUsuario(Usuario);

        //}
        [WebMethod]
        public byte[] DescargaDocZDocs(string Usuario, string Password, string IdDocumento)
        {

            try
            {
                byte[] PDF = null;

                string IdUsuario = LoginCorrectoNew(Usuario, Password);

                if (IdUsuario != "-1")
                {

                    //MISMO CODIGO QUE EN WSZFK
                    //    string Directorio = "C:/Inetpub/wwwroot/signesProVolumenes/";

                    string Ruta = "00000000";
                    Ruta = Ruta.Substring(0, 8 - IdDocumento.Length) + IdDocumento;
                    string pathString = Directorio + Ruta.Substring(0, 2) + "/" + Ruta.Substring(2, 2) + "/" + Ruta.Substring(4, 2) + "/";// +Ruta.Substring(6, 2) + "/";
                    String formato = "pdf";


                    if (System.IO.File.Exists(pathString + Ruta.Substring(6, 2) + "." + formato))
                    {
                        PDF = File.ReadAllBytes(pathString + Ruta.Substring(6, 2) + "." + formato);
                        return PDF;
                    }
                }



                // Si llega aquí es porque no se ha autenticado correctamente
                Context.Response.StatusCode = 403;
                return null;

            }
            catch (Exception e)
            {
                return null;
            }
        }

        //[WebMethod]
        //public byte[] DescargaDoc(string Usuario, string Password, string Documento)
        //{
        //    WS.UserInfo _info = new WS.UserInfo();
        //    try
        //    {

        //        _info.Mail = Usuario;
        //        _info.Pwd = Password;

        //        WS.WSAccessPoint _cliente = new WS.WSAccessPoint();
        //        byte[] kk = _cliente.DownloadDocument(_info, Documento);

        //        // Aqui tenemos que generar el evento de documento visualizador por el usuario del correo.
        //        string vUsuario = idUsuario(Usuario);

        //        string leido = GenerarEventoLectura(vUsuario, Documento);

        //        return kk;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //}


        [WebMethod]
        public void encriptaClavesUser(string clave, int idUsuario)
        {
            string query = string.Empty;
            try
            {
                if (clave == "Zertifika1234")
                {


                    query = "SELECT TOP 1 Password FROM usuarios WHERE idUsuario =" + idUsuario;


                    string password = DatabaseConnection.executeScalarString(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                    //System.Security.Cryptography.SHA1 alg = System.Security.Cryptography.SHA512.Create();
                    //byte[] result = alg.ComputeHash(Encoding.UTF8.GetBytes(password));
                    //string hash = Convert.ToBase64String(result);

                    if (!string.IsNullOrEmpty(password))
                    {

                        int resp = -1;

                        query = "UPDATE Usuarios SET PasswordE = '" + GetSHA1(password) + "' WHERE idUsuario = " + idUsuario;
                        resp = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                        query = "UPDATE Usuarios SET PasswordE2 = '" + GetSHA512(password) + "' WHERE idUsuario = " + idUsuario;
                        resp = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                    }

                }

            }
            catch (Exception e)
            {

            }

        }

        public static String GetSHA1(String data)
        {
            return FormsAuthentication.HashPasswordForStoringInConfigFile(data, "SHA1");
        }
        public static string GetSHA512(string data)
        {
            try
            {

                byte[] hash = Encoding.UTF8.GetBytes(data);

                using (SHA512 shaM = new SHA512Managed())
                {
                    hash = shaM.ComputeHash(hash);
                }
                return Convert.ToBase64String(hash);
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("GetSHA512", "WorkFlow", -1, e.ToString() + ". " + data, e.InnerException + "");
                return "";
            }
        }


        //[WebMethod]
        //public byte[] DescargaDocBackup(string Usuario, string Password, string Documento)
        //{
        //    WS.UserInfo _info = new WS.UserInfo();
        //    try
        //    {

        //        _info.Mail = Usuario;
        //        _info.Pwd = Password;

        //        WS.WSAccessPoint _cliente = new WS.WSAccessPoint();
        //        byte[] kk = _cliente.DownloadDocument(_info, Documento);

        //        return kk;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //}


        [WebMethod]
        public string ExisteUsuarioDNI(string DNI)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select * from users where DNI='" + DNI + "'";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return "True"; // ds.Tables[0].Rows[0].ItemArray[7].ToString();


                }
                else
                {
                    return "False";
                }


            }
            catch (OleDbException ex)
            {
                return ex.ToString();
            }


        }
        [WebMethod]
        public string ExisteEmpresaZDocs(string Cif)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);

                string SQL = "select IdEmpresa from empresa where Cif='" + Cif + "'";
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();
                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);
                //hola
                if (ds.Tables[0].Rows.Count > 0)
                    return ds.Tables[0].Rows[0].ItemArray[0].ToString();
                else
                    return "";

            }
            catch (OleDbException ex)
            {
                return ex.ToString();
            }

        }
        [WebMethod]
        public string ExisteUsuarioZDocs(string EMail)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);

                string SQL = "select IdUsuario from usuarios where eMail='" + EMail + "'";
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();
                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                    return ds.Tables[0].Rows[0].ItemArray[0].ToString();
                else
                    return "";

            }
            catch (OleDbException ex)
            {
                return ex.ToString();
            }


        }
        [WebMethod]
        public string ExisteUsuarioEmpresaZDocs(string Idusuario, string IdEmpresa)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);

                string SQL = "select * from UsuarioEmpresa where IdUsuario=" + Idusuario + " and IdEmpresa=" + IdEmpresa;
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();
                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                    return ds.Tables[0].Rows[0].ItemArray[0].ToString();
                else
                    return "";

            }
            catch (OleDbException ex)
            {
                return ex.ToString();
            }


        }
        [WebMethod]
        public string ExisteUsuarioAnywhere(string Mail)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select uniqueid from users where eMail='" + Mail + "'";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].Rows[0].ItemArray[0].ToString(); // ds.Tables[0].Rows[0].ItemArray[36].ToString();


                }
                else
                {
                    return "";
                }


            }
            catch (OleDbException ex)
            {
                return ex.ToString();
            }


        }
        [WebMethod]
        public string ExisteUsuarioMail(string Mail)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select * from users where eMail='" + Mail + "'";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return "True"; // ds.Tables[0].Rows[0].ItemArray[36].ToString();


                }
                else
                {
                    return "False";
                }


            }
            catch (OleDbException ex)
            {
                return ex.ToString();
            }


        }

        [WebMethod]
        public string EmpleadosEmpresaCIF(string CIF)
        {
            try
            {

                // Primero buscamos idEmpresa
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select * from Companies where IdentifierValue='" + CIF + "'";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {

                    string IdCompañia = ds.Tables[0].Rows[0].ItemArray[0].ToString();

                    SQL = "select * from users where CompanyUniqueId='" + IdCompañia + "'";
                    vComando.CommandText = SQL;

                    ds.Tables.Clear();

                    da.Fill(ds);

                    string Usuarios = "";


                    for (int i = 0; i < ds.Tables[0].Rows.Count - 1; i++)
                    {
                        Usuarios += "|" + ds.Tables[0].Rows[i].ItemArray[36].ToString();
                    }
                    return Usuarios;

                }
                else
                {
                    return "";
                }


            }
            catch (OleDbException ex)
            {
                return ex.ToString();
            }


        }

        [WebMethod]
        public String JerarquiaEtiquetas(string Padre, string Usuario)
        {



            Padre = idEtiqueta(Usuario, Padre);

            if (Padre == "")
            {
                return "";
            }

            OleDbConnection vCon = new OleDbConnection(CadenaConexion);


            string SQL = "SELECT Name from Tags where exists (select * from TagsJerarquia where idpadre='" + Padre + "' and idHijo=UniqueId) and  not exists (select * from TagsJerarquia where idPadre=UniqueId) order by name";

            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Etiquetas = "";

            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                Etiquetas += "|" + ds.Tables[0].Rows[i].ItemArray[0].ToString();

            }
            return Etiquetas;


        }

        [WebMethod]
        public String JerarquiaEtiquetasConHijos(string Padre, string Usuario)
        {



            Padre = idEtiqueta(Usuario, Padre);

            if (Padre == "")
            {
                return "";
            }

            OleDbConnection vCon = new OleDbConnection(CadenaConexion);

            string SQL = "SELECT Name from Tags where exists (select * from TagsJerarquia where idpadre='" + Padre + "' and idHijo=UniqueId) and  exists (select * from TagsJerarquia where idPadre=UniqueId) order by name";

            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Etiquetas = "";

            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                Etiquetas += "|" + ds.Tables[0].Rows[i].ItemArray[0].ToString();

            }
            return Etiquetas;


        }

        private String idEtiqueta(string Usuario, string Etiqueta)
        {


            Usuario = idUsuario(Usuario);

            OleDbConnection vCon = new OleDbConnection(CadenaConexion);


            string SQL = "SELECT * from Tags where useruniqueid='" + Usuario + "' and name='" + Etiqueta + "'";

            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Etiquetas = "";

            if (ds.Tables[0].Rows.Count > 0)
            {
                Etiquetas = ds.Tables[0].Rows[0].ItemArray[0].ToString();
            }
            else
            {
                Etiquetas = "";
            }

            return Etiquetas;

        }

        [WebMethod]
        public String EtiquetasRaiz(string Usuario)
        {


            Usuario = idUsuario(Usuario);

            OleDbConnection vCon = new OleDbConnection(CadenaConexion);


            string SQL = "SELECT Name from Tags where useruniqueid='" + Usuario + "' and not exists (select * from TagsJerarquia where idhijo=UniqueId) and not exists (select * from TagsJerarquia where idPadre=UniqueId) order by name desc";

            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Etiquetas = "";

            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                Etiquetas += "|" + ds.Tables[0].Rows[i].ItemArray[0].ToString();

            }
            return Etiquetas;


        }

        [WebMethod]
        public String EtiquetasRaizConHijos(string Usuario)
        {


            Usuario = idUsuario(Usuario);

            OleDbConnection vCon = new OleDbConnection(CadenaConexion);


            string SQL = "SELECT Name from Tags where useruniqueid='" + Usuario + "' and not exists (select * from TagsJerarquia where idhijo=UniqueId) and  exists (select * from TagsJerarquia where idPadre=UniqueId) order by name desc";

            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Etiquetas = "";

            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                Etiquetas += "|" + ds.Tables[0].Rows[i].ItemArray[0].ToString();

            }
            return Etiquetas;


        }

        [WebMethod]
        public String EtiquetasDocumento(string Documento)
        {
            OleDbConnection vCon = new OleDbConnection(CadenaConexion);


            string SQL = "SELECT Name,UniqueId from Tags where exists (select * from DocumentTags where documentuniqueid='" + Documento + "' and TagUniqueId=UniqueId)";



            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Etiquetas = "";

            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                Etiquetas += "|" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[1].ToString();

            }
            return Etiquetas;


        }


        // Mirar el tag de la etiqueta de documentos compartiro por empresa.
        private string TagEtiqueta(string Usuario, string Tipo)
        {
            string SQL = "select * from tags where UserUniqueID='" + Usuario + "' and Name='" + Tipo + "'";

            OleDbConnection vCon = new OleDbConnection(CadenaConexion);

            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                return ds.Tables[0].Rows[0]["UniqueId"].ToString();
            }
            else
                return "";
        }

        private string NombreUsuario(string Usuario, ref string Compañia)
        {
            string SQL = "select * from users where uniqueid='" + Usuario + "'";

            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);

                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    Compañia = ds.Tables[0].Rows[0]["CompanyUniqueId"].ToString();
                    return ds.Tables[0].Rows[0]["CompleteName"].ToString();
                }
                else
                {
                    return "";
                }

            }
            catch (OleDbException ex)
            {
                return "";
            }


        }

        public string DescripcionDocumento(string Documento)
        {
            string SQL = "select * from documents where uniqueid='" + Documento + "'";

            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);

                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].Rows[0]["Name"].ToString();
                }
                else
                {
                    return "";
                }

            }
            catch (OleDbException ex)
            {
                return "";
            }

        }

        public string DescripcionCompañia(string Compañia)
        {
            string SQL = "select * from companies where uniqueid='" + Compañia + "'";

            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);

                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].Rows[0]["Name"].ToString();
                }
                else
                {
                    return "";
                }

            }
            catch (OleDbException ex)
            {
                return "";
            }

        }
        [WebMethod]
        public string GenerarEventoLectura(string Usuario, string Documento)
        {

            // Buscamos los datos necesarios para hacer el insert en el socialevents
            // SocialEvents: UniqueId, TimeStamp = '634834758123100104', CreationDate, SchemaVersion='1.0', Type='6', Source='2', ActionUserUniqueId, ActionUserDescription, ActionCompanyUniqueId, ActionCompanyDescription, DocumentUniqueId, DocumentDescription

            string Compañia = "";
            string Nombre = NombreUsuario(Usuario, ref Compañia);

            Guid valueid = Guid.NewGuid();

            string SQL = "insert into Socialevents (UniqueId, TimeStamp, CreationDate, SchemaVersion, Type, Source, ActionUserUniqueId, ActionUserDescription, ActionCompanyUniqueId,ActionCompanyDescription,  DocumentUniqueId, DocumentDescription)";
            SQL += " values ('" + valueid + "','" + DevolverUltimoTimestamp() + "','" + DateTime.Now.Month + "/" + DateTime.Now.Day + "/" + DateTime.Now.Year + " " + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second + "','1.0','6','2','";
            SQL += Usuario + "','" + Nombre + "','" + Compañia + "','" + DescripcionCompañia(Compañia) + "','" + Documento + "','" + DescripcionDocumento(Documento) + "') ";

            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                vComando.ExecuteNonQuery();
                return "OK";
            }
            catch (OleDbException ex)
            {
                return ex.Message;
            }

        }

        public string DevolverUltimoTimestamp()
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);
                string Sql = "select top(10) Timestamp from Socialevents order by Creationdate desc ";
                OleDbCommand vComando = new OleDbCommand(Sql, vCon);
                vCon.Open();
                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].Rows[0].ItemArray[0].ToString();
                }
                else
                {
                    return "";
                }

            }
            catch (OleDbException ex)
            {
                return "";
            }
        }

        private string BuscarEtiquetaUsuario(string Usuario, string Etiqueta)
        {
            OleDbConnection vCon = new OleDbConnection(CadenaConexion);

            string SQL = "select * from TAGS where useruniqueid='" + Usuario + "' and name='" + Etiqueta + "'";

            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                return ds.Tables[0].Rows[0]["UniqueId"].ToString();

            }
            else
            {
                return "";
            }

        }

        private int NumeroEtiquetasDocumento(string Documento)
        {
            string SQL = "select max(DocumentTagsId) as vTotal from DocumentTags where DocumentUniqueId='" + Documento + "'";
            OleDbConnection vCon = new OleDbConnection(CadenaConexion);
            Guid UniqueId = new Guid(Documento);

            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();
            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                try
                {
                    int Total = Convert.ToInt32(ds.Tables[0].Rows[0]["vTotal"].ToString());
                    Total += 1;
                    return Total;
                }
                catch (Exception ex)
                {
                    return 0;
                }


            }
            else
            {
                return 0;
            }

        }

        [WebMethod]
        public string AnadirEtiquetaDocumento(string Usuario, string Documento, string Etiqueta)
        {
            Usuario = idUsuario(Usuario);

            string vidEtiqueta = BuscarEtiquetaUsuario(Usuario, Etiqueta);
            if (vidEtiqueta != "")
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                int vNumEtiquetas = NumeroEtiquetasDocumento(Documento);

                string SQL = "insert into DocumentTags (DocumentUniqueID,TagUniqueId,DocumentTagsId) values (";
                SQL += "'" + Documento + "','" + vidEtiqueta + "'," + vNumEtiquetas + ")";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                vComando.ExecuteNonQuery();



            }

            return "";
        }

        [WebMethod]
        public string getTagDocumento(string Usuario, string Clave, string idDoc)
        {
            try
            {
                string query = "SELECT TOP (1) tag.Name " +
                               "FROM     Documents AS doc INNER JOIN " +
                               "DocumentTags AS dt ON dt.DocumentUniqueId = doc.UniqueId INNER JOIN " +
                               "Tags AS tag ON tag.UniqueId = dt.TagUniqueId " +
                               "WHERE  (tag.Name LIKE '%compartidos%' OR tag.Name LIKE '%propios%' " +
                               "OR tag.Name LIKE '%empresa%') AND (doc.UniqueId = '" + idDoc + "')";

                OleDbConnection vCon = new OleDbConnection(CadenaConexion);
                OleDbCommand vComando = new OleDbCommand(query, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;

                return vComando.ExecuteScalar().ToString();

            }
            catch (Exception e)
            {
                return "-1";
            }
        }




        [WebMethod]
        public string DocumentosNuevosCompartidos2(string Usuario)
        {
            OleDbConnection vCon = new OleDbConnection(CadenaConexion);

            Usuario = idUsuario(Usuario);

            string vTagEtiquetaEmpresa = TagEtiqueta(Usuario, "Documentos de empresa");
            // string vTagEtiquetaObsoletos = TagEtiqueta(Usuario, "Documentos obsoletos");
            string vTagEtiquetaPapelera = TagEtiqueta(Usuario, "Papelera");

            string SQL = "SELECT  Name,CreationDate, PreviewMimeType, UniqueId, DossierUniqueId  FROM Documents docs WHERE docs.name<>'NotificationsManual' and docs.uploader<>'" + Usuario + "' and (exists";
            SQL += " (SELECT DossierUniqueId FROM DossierCompanyDatas WHERE dossieruniqueid=docs.dossieruniqueid and (UserUniqueId ='" + Usuario + "') )) and not exists (select *";
            SQL += " from tags,documenttags where documenttags.documentuniqueid=docs.uniqueid and documenttags.taguniqueid=tags.uniqueid and tags.name='Documentos Obsoletos' ) and not exists (select * from SocialEvents where";
            SQL += " ActionUserUniqueId='" + Usuario + "' and documentuniqueid=docs.uniqueid and (type=6 or type=16)) and not exists (select * FROM DossierDocNotShareds where userid='" + Usuario + "' and";
            SQL += " DossierDocNotShareds.documentid=docs.uniqueid) AND ( exists (SELECT t.UniqueId FROM Tags AS t INNER JOIN DocumentTags AS d ON t.UniqueId = d.TagUniqueId WHERE d.documentuniqueid=docs.uniqueid and t.useruniqueid='" + Usuario + "' and (t.uniqueid =";
            SQL += "'" + vTagEtiquetaEmpresa + "'   )) )and not exists (select * from tags,documenttags where tags.useruniqueid='" + Usuario + "' and documenttags.documentuniqueid=docs.uniqueid and documenttags.taguniqueid=tags.uniqueid and";
            SQL += " tags.uniqueid='" + vTagEtiquetaPapelera + "' )and not exists (select * from tags,documenttags where tags.useruniqueid=docs.useruniqueid and documenttags.documentuniqueid=docs.uniqueid and documenttags.taguniqueid=tags.uniqueid and tags.Name='Papelera' ) UNION ";

            SQL += " (select Name,docs.CreationDate, PreviewMimeType, docs.UniqueId, DossierUniqueId from DocShareds sha,documents docs where docs.uniqueid=sha.documentid and not exists (select dtag.documentuniqueid from Documenttags dtag,tags where dtag.documentuniqueid=docs.uniqueid and dtag.taguniqueid=tags.uniqueid and tags.name='Documentos Obsoletos') and";
            SQL += " docs.uploader<>'" + Usuario + "' and sha.userid='" + Usuario + "' and not exists (select * from socialevents where (type=6 or type=16) and documentuniqueid=sha.documentid and actionuseruniqueid='" + Usuario + "') and not";
            SQL += " exists (select * FROM DossierDocNotShareds where userid='" + Usuario + "' and DossierDocNotShareds.documentid=docs.uniqueid) AND ( exists (SELECT t.UniqueId FROM Tags AS t INNER JOIN DocumentTags AS d ON t.UniqueId = d.TagUniqueId WHERE d.documentuniqueid=docs.uniqueid and";
            SQL += " t.useruniqueid='" + Usuario + "' and (t.uniqueid ='" + vTagEtiquetaEmpresa + "'   )) )and not exists (select dtag.documentuniqueid from Documenttags dtag,tags where dtag.taguniqueid=tags.uniqueid and dtag.documentuniqueid=docs.uniqueid and tags.useruniqueid='" + Usuario + "' and";
            SQL += " tags.uniqueid='" + vTagEtiquetaPapelera + "')and not exists (select dtag.documentuniqueid from Documenttags dtag,tags where dtag.taguniqueid=tags.uniqueid and dtag.documentuniqueid=docs.uniqueid and tags.useruniqueid=docs.useruniqueid and tags.Name='Papelera')) order by docs.creationdate desc";


            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Documentos = "";

            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                Documentos += "|" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[1].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[2].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[3].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[4].ToString();

            }



            return Documentos;


        }

        [WebMethod]
        public string DocumentosNuevosCompartidosNombre2(string Usuario, string Nombre)
        {
            OleDbConnection vCon = new OleDbConnection(CadenaConexion);

            Usuario = idUsuario(Usuario);

            string vTagEtiquetaEmpresa = TagEtiqueta(Usuario, "Documentos de empresa");
            // string vTagEtiquetaObsoletos = TagEtiqueta(Usuario, "Documentos obsoletos");
            string vTagEtiquetaPapelera = TagEtiqueta(Usuario, "Papelera");

            string SQL = "SELECT  Name,CreationDate, PreviewMimeType, UniqueId  FROM Documents docs WHERE docs.name like '%" + Nombre + "%' and docs.uploader<>'" + Usuario + "' and (exists";
            SQL += " (SELECT DossierUniqueId FROM DossierCompanyDatas WHERE dossieruniqueid=docs.dossieruniqueid and (UserUniqueId ='" + Usuario + "') )) and not exists (select *";
            SQL += " from tags,documenttags where documenttags.documentuniqueid=docs.uniqueid and documenttags.taguniqueid=tags.uniqueid and tags.name='Documentos Obsoletos' ) and not exists (select * from SocialEvents where";
            SQL += " ActionUserUniqueId='" + Usuario + "' and documentuniqueid=docs.uniqueid and (type=6 or type=16)) and not exists (select * FROM DossierDocNotShareds where userid='" + Usuario + "' and";
            SQL += " DossierDocNotShareds.documentid=docs.uniqueid) AND ( exists (SELECT t.UniqueId FROM Tags AS t INNER JOIN DocumentTags AS d ON t.UniqueId = d.TagUniqueId WHERE d.documentuniqueid=docs.uniqueid and t.useruniqueid='" + Usuario + "' and (t.uniqueid =";
            SQL += "'" + vTagEtiquetaEmpresa + "'   )) )and not exists (select * from tags,documenttags where tags.useruniqueid='" + Usuario + "' and documenttags.documentuniqueid=docs.uniqueid and documenttags.taguniqueid=tags.uniqueid and";
            SQL += " tags.uniqueid='" + vTagEtiquetaPapelera + "' )and not exists (select * from tags,documenttags where tags.useruniqueid=docs.useruniqueid and documenttags.documentuniqueid=docs.uniqueid and documenttags.taguniqueid=tags.uniqueid and tags.Name='Papelera' ) UNION ";

            SQL += " (select Name,docs.CreationDate, PreviewMimeType, docs.UniqueId  from DocShareds sha,documents docs where docs.name like '%" + Nombre + "%' and docs.uniqueid=sha.documentid and not exists (select dtag.documentuniqueid from Documenttags dtag,tags where dtag.documentuniqueid=docs.uniqueid and dtag.taguniqueid=tags.uniqueid and tags.name='Documentos Obsoletos') and";
            SQL += " docs.uploader<>'" + Usuario + "' and sha.userid='" + Usuario + "' and not exists (select * from socialevents where (type=6 or type=16) and documentuniqueid=sha.documentid and actionuseruniqueid='" + Usuario + "') and not";
            SQL += " exists (select * FROM DossierDocNotShareds where userid='" + Usuario + "' and DossierDocNotShareds.documentid=docs.uniqueid) AND ( exists (SELECT t.UniqueId FROM Tags AS t INNER JOIN DocumentTags AS d ON t.UniqueId = d.TagUniqueId WHERE d.documentuniqueid=docs.uniqueid and";
            SQL += " t.useruniqueid='" + Usuario + "' and (t.uniqueid ='" + vTagEtiquetaEmpresa + "'   )) )and not exists (select dtag.documentuniqueid from Documenttags dtag,tags where dtag.taguniqueid=tags.uniqueid and dtag.documentuniqueid=docs.uniqueid and tags.useruniqueid='" + Usuario + "' and";
            SQL += " tags.uniqueid='" + vTagEtiquetaPapelera + "')and not exists (select dtag.documentuniqueid from Documenttags dtag,tags where dtag.taguniqueid=tags.uniqueid and dtag.documentuniqueid=docs.uniqueid and tags.useruniqueid=docs.useruniqueid and tags.Name='Papelera')) order by docs.creationdate desc";


            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Documentos = "";

            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                Documentos += "|" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[1].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[2].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[3].ToString();

            }
            return Documentos;

        }

        public Double DevolverNumeroRegistroQuery(string Query)
        {
            OleDbConnection vCon = new OleDbConnection(CadenaConexion);

            OleDbCommand vComando = new OleDbCommand(Query, vCon);
            vCon.Open();
            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                try
                {
                    double Total = Convert.ToDouble(ds.Tables[0].Rows[0].ItemArray[0].ToString());
                    return Total;
                }
                catch (Exception ex)
                {
                    return 0;
                }

            }
            else
            {
                return 0;
            }
        }
        [WebMethod]
        public string DevolverResumenDocs(string Iduser, string Origen, string Ruta)
        { //ACTUALIZADO 22/04/2015
            string html = "";

            String SQL = "";
            OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
            try
            {
                Ruta = "http://192.168.1.197/Zdocs/";
                SQL = "SELECT top(25) doc.iddocumento, doc.docOriginal, doc.idusuariopropietario, docusu.idusuariocomparte FROM  Documentos AS doc LEFT OUTER JOIN documentosusuarios AS docusu ON doc.iddocumento = docusu.iddocumento WHERE (doc.activo='True') and (docusu.idusuario = '" + Iduser + "') AND (doc.idusuariopropietario <> '" + Iduser + "') order by docusu.fechaInclusion desc";
                //SQL = "SELECT doc.IdDocumento,doc.descripcion,doc.idusuariopropietario FROM Documentos AS doc LEFT OUTER JOIN documentosusuarios AS docusu ON doc.iddocumento = docusu.iddocumento WHERE (docusu.idusuario = '" + Iduser + "') AND (docusu.LecturaObligatoria = 'True')";
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();
                OleDbDataAdapter da = new OleDbDataAdapter();
                da.SelectCommand = vComando;
                DataSet ds = new DataSet();
                da.SelectCommand = vComando;
                da.Fill(ds);
                html += "<div id='titmecomparten'><p>Me comparten &nbsp&nbsp";
                Double NumRegistros = DevolverNumeroRegistroQuery("SELECT count(*) FROM  Documentos AS doc LEFT OUTER JOIN documentosusuarios AS docusu ON doc.iddocumento = docusu.iddocumento WHERE (doc.activo='True') and (docusu.idusuario = '" + Iduser + "') AND (doc.idusuariopropietario <> '" + Iduser + "') ");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    html += "<a href='javascript:$(\"#divmecomparten\").toggle()'>" + NumRegistros + "</a></p> ";
                    html += "<div id='divmecomparten' style='display:none; height:150px; overflow: scroll'>";
                    html += "<ul>";
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        if (ds.Tables[0].Rows[i].ItemArray[1].ToString().Length <= 35)
                            html += "<li><a style='margin-left:55px' title='" + ds.Tables[0].Rows[i].ItemArray[1].ToString() + "' href=javascript:mostrarDoc('visor/web/viewer.html?idDoc=" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "');>" + ds.Tables[0].Rows[i].ItemArray[1].ToString() + "</a></li>";
                        else
                            html += "<li><a style='margin-left:55px' title='" + ds.Tables[0].Rows[i].ItemArray[1].ToString() + "' href=javascript:mostrarDoc('visor/web/viewer.html?idDoc=" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "');>" + ds.Tables[0].Rows[i].ItemArray[1].ToString().Substring(0, 34) + "</a></li>";
                    }
                    html += "</ul>";
                    html += "</div>";

                }
                else
                    html += "0</br>";

                html += "</div>";

                SQL = "SELECT top(25) doc.iddocumento, doc.docOriginal,docusu.fechainclusion FROM Documentos AS doc LEFT OUTER JOIN documentosusuarios AS docusu ON doc.iddocumento = docusu.iddocumento WHERE (doc.activo='True') and (docusu.idusuario <> '" + Iduser + "') AND (doc.idusuariopropietario = '" + Iduser + "') GROUP BY doc.iddocumento, doc.docOriginal,docusu.fechainclusion order by docusu.fechainclusion desc";
                vComando = new OleDbCommand(SQL, vCon);
                //OleDbDataAdapter da2 = new OleDbDataAdapter();                
                da.SelectCommand = vComando;
                ds.Clear();
                da.Fill(ds);
                html += "<div id='tityocomparto'><p>Compartidos por mi &nbsp&nbsp";
                NumRegistros = DevolverNumeroRegistroQuery("SELECT count(*) FROM Documentos AS doc LEFT OUTER JOIN documentosusuarios AS docusu ON doc.iddocumento = docusu.iddocumento WHERE (doc.activo='True') and (docusu.idusuario <> '" + Iduser + "') AND (doc.idusuariopropietario = '" + Iduser + "')");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    html += "<a href='javascript:$(\"#divyocomparto\").toggle()'>" + NumRegistros + "</a></p>";
                    html += "<div id='divyocomparto' style='display:none; height:150px; overflow: scroll'>";
                    html += "<ul>";
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        if (ds.Tables[0].Rows[i].ItemArray[1].ToString().Length <= 35)
                            //html += "<li><div id='pop" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "'  data-toggle='popover' onclick='javascript:MostrarPop2(\"" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "\")' title='Compatido con: ' data-content='<h1>Prueba</h1>' >" + ds.Tables[0].Rows[i].ItemArray[1].ToString() + "</div></li>";
                            html += "<li><a style='margin-left:55px' href='#' class='emergente' id='pop" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "'  data-toggle='popover' data-html='true' onclick='javascript:MostrarPop(\"" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "\")' title='Compatido con: ' data-content='' >" + ds.Tables[0].Rows[i].ItemArray[1].ToString() + "</a></li>";
                        else
                            html += "<li><a style='margin-left:55px' href='#' class='emergente' id='pop" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "' data-toggle='popover' data-html='true' onclick='javascript:MostrarPop(\"" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "\")' title='Compatido con: ' data-content='' >" + ds.Tables[0].Rows[i].ItemArray[1].ToString().Substring(0, 34) + "</a></li>";
                    }
                    html += "</ul>";
                    html += "</div>";
                }
                else
                    html += "0</br>";

                html += "</div>";
                // DOCUMENTOS SUBIDOS POR MI
                SQL = "SELECT top(25) doc.iddocumento, doc.docOriginal,docusu.FechaInclusion FROM Documentos AS doc LEFT OUTER JOIN documentosusuarios AS docusu ON doc.iddocumento = docusu.iddocumento WHERE (doc.activo='True') and (docusu.idusuario = '" + Iduser + "') AND (doc.idusuariouploader = '" + Iduser + "') GROUP BY doc.iddocumento, doc.docOriginal,docusu.fechainclusion order by docusu.fechainclusion desc";
                vComando = new OleDbCommand(SQL, vCon);
                da = new OleDbDataAdapter();
                da.SelectCommand = vComando;
                ds.Clear();
                da.Fill(ds);
                html += "<div id='tityosubo'><p>Subidos por mi &nbsp&nbsp";
                NumRegistros = DevolverNumeroRegistroQuery("SELECT count(*) FROM Documentos AS doc LEFT OUTER JOIN documentosusuarios AS docusu ON doc.iddocumento = docusu.iddocumento WHERE (doc.activo='True') and (docusu.idusuario = '" + Iduser + "') AND (doc.idusuariouploader = '" + Iduser + "') ");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    html += "<a href='javascript:$(\"#divyosubo\").toggle()'>" + NumRegistros + "</a></p>";
                    html += "<div id='divyosubo' style='display:none; height:150px; overflow: scroll'>";
                    html += "<ul>";
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        if (ds.Tables[0].Rows[i].ItemArray[1].ToString().Length <= 35)
                        {
                            if (Origen.ToUpper() == "PORTADA")
                                html += "<li><a style='margin-left:55px'  title='" + ds.Tables[0].Rows[i].ItemArray[1].ToString() + "' href= '" + Ruta + "visor/web/viewer.html?idDoc=" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "'  id='pop" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "' >" + ds.Tables[0].Rows[i].ItemArray[1].ToString() + "</a></li>";
                            else
                                html += "<li><a style='margin-left:55px'  title='" + ds.Tables[0].Rows[i].ItemArray[1].ToString() + "' href=javascript:mostrarDoc('visor/web/viewer.html?idDoc=" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "');  id='pop" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "' >" + ds.Tables[0].Rows[i].ItemArray[1].ToString() + "</a></li>";
                        }
                        else
                        {
                            if (Origen.ToUpper() == "PORTADA")
                                html += "<li><a style='margin-left:55px' title='" + ds.Tables[0].Rows[i].ItemArray[1].ToString() + "' href='" + Ruta + "visor/web/viewer.html?idDoc=" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "'  id='pop" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "' >" + ds.Tables[0].Rows[i].ItemArray[1].ToString().Substring(0, 34) + "</a></li>";
                            else
                                html += "<li><a style='margin-left:55px' title='" + ds.Tables[0].Rows[i].ItemArray[1].ToString() + "' href=javascript:mostrarDoc('visor/web/viewer.html?idDoc=" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "');  id='pop" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "' >" + ds.Tables[0].Rows[i].ItemArray[1].ToString().Substring(0, 34) + "</a></li>";
                        }
                    }
                    html += "</ul>";
                    html += "</div>";
                    //return html;
                }
                else
                    html += "0</br>";

                html += "</div>";
                //*****
                //OBLIGADA LECTURA
                Boolean Primero = true;
                SQL = "SELECT top(25) doc.IdDocumento,doc.docOriginal,doc.idusuariopropietario,docusu.visualizado FROM Documentos AS doc LEFT OUTER JOIN documentosusuarios AS docusu ON doc.iddocumento = docusu.iddocumento WHERE (doc.activo='True') and (docusu.idusuario = '" + Iduser + "') AND (docusu.LecturaObligatoria = 'True') order by docusu.FechaInclusion desc, docusu.visualizado";
                vComando = new OleDbCommand(SQL, vCon);
                da = new OleDbDataAdapter();
                da.SelectCommand = vComando;
                ds.Clear();
                da.Fill(ds);
                html += "<div id='obligadalectura'><p>Obligada Lectura &nbsp&nbsp";
                NumRegistros = DevolverNumeroRegistroQuery("SELECTcount(*) FROM Documentos AS doc LEFT OUTER JOIN documentosusuarios AS docusu ON doc.iddocumento = docusu.iddocumento WHERE (doc.activo='True') and (docusu.idusuario = '" + Iduser + "') AND (docusu.LecturaObligatoria = 'True')");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    html += "<a href='javascript:$(\"#divobligadalectura\").toggle()'>" + NumRegistros + "</a></p>";
                    html += "<div id='divobligadalectura' style='display:none; height:150px; overflow: scroll'>";

                    html += "<ul>";
                    html += "<p style='margin-left:0px'>No Visualizados</p> ";

                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        if (ds.Tables[0].Rows[i].ItemArray[3].ToString() == "False")
                            if (Origen.ToUpper() == "PORTADA")
                                html += "<li><a style='margin-left:55px' href='" + Ruta + "visor/web/viewer.html?idDoc=" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "'>" + ds.Tables[0].Rows[i].ItemArray[1].ToString() + "</a></li>";
                            else
                                html += "<li><a style='margin-left:55px' href=javascript:mostrarDoc('visor/web/viewer.html?idDoc=" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "');>" + ds.Tables[0].Rows[i].ItemArray[1].ToString() + "</a></li>";
                        else
                        {
                            if (Primero)
                            {
                                html += "<p style='margin-left:0px'>Ya Visualizados</p> ";
                                Primero = false;
                            }
                            if (Origen.ToUpper() == "PORTADA")
                                html += "<li><a style='margin-left:55px' href='" + Ruta + "visor/web/viewer.html?idDoc=" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "'>" + ds.Tables[0].Rows[i].ItemArray[1].ToString() + "</a></li>";
                            else
                                html += "<li><a style='margin-left:55px' href=javascript:mostrarDoc('visor/web/viewer.html?idDoc=" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "');>" + ds.Tables[0].Rows[i].ItemArray[1].ToString() + "</a></li>";
                        }
                    }
                    html += "</ul>";
                }
                html += "</div>";

                html += "</div>";

                //********
                return html;
            }
            catch (Exception e)
            {
                return "";
            }
        }
        [WebMethod]
        public string DevolverValoresGraficoDocumentos(string IdUsuario)
        {
            string respuesta = "", respuesta1 = "", respuesta2 = "";
            CultureInfo ci = new CultureInfo("Es-Es");

            string SQL = "SELECT COUNT(dusu.iddocumento) , dusu.idusuario, YEAR(dusu.FechaInclusion) AS YeaR, MONTH(dusu.FechaInclusion) AS Mes FROM Documentos AS doc LEFT OUTER JOIN documentosusuarios AS dusu ON doc.iddocumento = dusu.iddocumento WHERE (doc.activo='True') and (dusu.idusuario = " + IdUsuario + ") GROUP BY dusu.idusuario, YEAR(dusu.FechaInclusion), MONTH(dusu.FechaInclusion) ORDER BY  year desc , mes ";

            OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
            try
            {
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();
                OleDbDataAdapter da = new OleDbDataAdapter();
                respuesta += "[";

                DataSet ds = new DataSet();
                da.SelectCommand = vComando;
                da.Fill(ds);

                int NumValores = 0;
                int UltimoMes = 0;
                if (ds.Tables[0].Rows.Count > 0)
                {

                    for (int i = (ds.Tables[0].Rows.Count >= 12 ? 12 : ds.Tables[0].Rows.Count - 1); i >= 0; i--)
                    {
                        //respuesta += "{ y: '" + ci.DateTimeFormat.GetMonthName(Convert.ToInt16(ds.Tables[0].Rows[i].ItemArray[3].ToString())) + " " + ds.Tables[0].Rows[i].ItemArray[2].ToString() + "', a : " + ds.Tables[0].Rows[i].ItemArray[0].ToString() + " },";
                        respuesta1 += "{ y: '" + ci.DateTimeFormat.GetMonthName(Convert.ToInt16(ds.Tables[0].Rows[i].ItemArray[3].ToString())).Substring(0, 3) + "', a : " + ds.Tables[0].Rows[i].ItemArray[0].ToString() + " },";
                        NumValores++;
                        UltimoMes = Convert.ToInt16(ds.Tables[0].Rows[i].ItemArray[3].ToString());
                    }

                    UltimoMes--;
                    if (UltimoMes < 1)
                        UltimoMes = 12;
                    for (int i = NumValores; i < 12; i++)
                    {
                        respuesta2 = "{ y: '" + ci.DateTimeFormat.GetMonthName(UltimoMes).Substring(0, 3) + "', a : " + 0 + " }," + respuesta2;
                        UltimoMes--;
                        if (UltimoMes < 1)
                            UltimoMes = 12;
                    }

                    if (!string.IsNullOrEmpty(respuesta2))
                        respuesta = respuesta + respuesta2 + respuesta1.Substring(0, respuesta1.Length - 1);
                    else
                        respuesta = respuesta + respuesta1.Substring(0, respuesta1.Length - 1);
                    respuesta += "]";
                    return respuesta;
                }
                else return "[]";
            }
            catch (Exception e)
            {
                return "";
            }

        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        [WebMethod]
        public Boolean TieneZFlow(string Idusuario)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string SQL = "select tokenZFlow from usuarios where idUsuario='" + Idusuario + "'";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0 && !string.IsNullOrEmpty(ds.Tables[0].Rows[0].ItemArray[0].ToString()))
                    return true;
                else
                    return false;

            }
            catch (OleDbException ex)
            {
                return false;
            }
        }
        [WebMethod]
        public Boolean TieneZdocs(string Idusuario)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string SQL = "select tokenZDocs from usuarios where idUsuario='" + Idusuario + "'";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0 && !string.IsNullOrEmpty(ds.Tables[0].Rows[0].ItemArray[0].ToString()))
                    return true;
                else
                    return false;

            }
            catch (OleDbException ex)
            {
                return false;
            }
        }
        [WebMethod]
        public Boolean ExisteUsuarioPorId(string Idusuario)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string SQL = "select * from usuarios where idUsuario='" + Idusuario + "'";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                    return true;
                else
                    return false;

            }
            catch (OleDbException ex)
            {
                return false;
            }
        }
        [WebMethod]
        public string AuditoriaDocumento(string Usuario, string Pass, string IdDoc, string IdExp, string IdForm)
        {
            string respuesta = "";
            try
            {
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string idUsuario = LoginCorrectoNew(Usuario, Pass);
                if (idUsuario == "-1")
                {
                    return "-1";
                }

                string Consulta1 = "";
                //if (string.IsNullOrEmpty(IdExp))
                //    Consulta1="SELECT soc.fecha, usu.nombre + ' ' + usu.apellidos,emp.nombre,soc.comentarios,usu2.Nombre + ' ' + usu2.Apellidos AS Con,soc.email from eventos soc left join usuarios USU on soc.idusuario=usu.idusuario left join empresa emp on soc.idempresa=emp.idempresa left join Usuarios AS usu2 ON usu2.idUsuario = soc.idusuariocolateral where soc.iddoc='" + IdDoc + "'";// order by soc.fecha desc";
                //else
                //    Consulta1 = "SELECT soc.fecha, usu.nombre + ' ' + usu.apellidos,emp.nombre,soc.comentarios,usu2.Nombre + ' ' + usu2.Apellidos AS Con,soc.email from eventos soc left join usuarios USU on soc.idusuario=usu.idusuario left join empresa emp on soc.idempresa=emp.idempresa left join Usuarios AS usu2 ON usu2.idUsuario = soc.idusuariocolateral where soc.idexpediente='" + IdExp + "'";// order by soc.fecha desc";

                if (!string.IsNullOrEmpty(IdDoc))
                    Consulta1 = "SELECT soc.fecha, usu.nombre + ' ' + usu.apellidos,emp.nombre,soc.comentarios,usu2.Nombre + ' ' + usu2.Apellidos AS Con,soc.email from eventos soc left join usuarios USU on soc.idusuario=usu.idusuario left join empresa emp on soc.idempresa=emp.idempresa left join Usuarios AS usu2 ON usu2.idUsuario = soc.idusuariocolateral where soc.iddoc='" + IdDoc + "'";// order by soc.fecha desc";
                else if (!string.IsNullOrEmpty(IdExp))
                    Consulta1 = "SELECT soc.fecha, usu.nombre + ' ' + usu.apellidos,emp.nombre,soc.comentarios,usu2.Nombre + ' ' + usu2.Apellidos AS Con,soc.email from eventos soc left join usuarios USU on soc.idusuario=usu.idusuario left join empresa emp on soc.idempresa=emp.idempresa left join Usuarios AS usu2 ON usu2.idUsuario = soc.idusuariocolateral where soc.idexpediente='" + IdExp + "'";// order by soc.fecha desc";
                else
                    Consulta1 = "SELECT soc.fecha, usu.nombre + ' ' + usu.apellidos,emp.nombre,soc.comentarios,usu2.Nombre + ' ' + usu2.Apellidos AS Con,soc.email from eventos soc left join usuarios USU on soc.idusuario=usu.idusuario left join empresa emp on soc.idempresa=emp.idempresa left join Usuarios AS usu2 ON usu2.idUsuario = soc.idusuariocolateral where soc.idFormulario='" + IdForm + "'";// order by soc.fecha desc";

                OleDbCommand vComando = new OleDbCommand(Consulta1, vCon);
                vCon.Open();
                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);
                vCon.Close();

                List<String> lista = new List<String>();
                string aux = "";
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    if (!string.IsNullOrEmpty(dr[4].ToString()) && (dr[4].ToString() != "-1"))
                        aux = "|" + dr[0] + "^" + dr[1] + "^" + dr[2] + "^" + dr[3] + "^" + dr[4];
                    else
                        if (!string.IsNullOrEmpty(dr[5].ToString()) && (dr[5].ToString() != "-1"))
                        aux = "|" + dr[0] + "^" + dr[1] + "^" + dr[2] + "^" + dr[3] + "^" + dr[5];
                    else
                        aux = "|" + dr[0] + "^" + dr[1] + "^" + dr[2] + "^" + dr[3] + "^" + "";
                    if (!lista.Contains(aux))
                    {
                        lista.Add(aux);
                    }
                }


                // Finalmente montamos la respuesta
                foreach (string s in lista)
                {
                    respuesta += s;
                }
                return respuesta;


            }
            catch (Exception e)
            {
                return -1 + "";
            }

        }
        //*****
        public string DevolverTiposInferiores(string IdTipo)
        {
            OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
            string Consulta = "Select IdTipoDocumento from tipoDocumento where padre=" + IdTipo;
            OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);
            string resp = "";

            if (ds.Tables[0].Rows.Count > 0)
                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i].ItemArray[0].ToString()))
                        resp += ds.Tables[0].Rows[i].ItemArray[0].ToString() + ";";
                }

            else
                return "";
            return resp;

        }

        public string DevolverTiposInferioresFormularios(string IdTipo)
        {
            OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
            string Consulta = "Select IdTipoFormulario from tipoFormulario where padre=" + IdTipo;
            OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);
            string resp = "";

            if (ds.Tables[0].Rows.Count > 0)
                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i].ItemArray[0].ToString()))
                        resp += ds.Tables[0].Rows[i].ItemArray[0].ToString() + ";";
                }

            else
                return "";
            return resp;

        }
        public string MontarConsultaDesdeHasta(Boolean Fecha)
        {
          //  string Hoy = DateTime.Now.Date.ToString("dd/MM/yyyy");// +" 0:00:00";//.ToString().Substring(0,10);
            string html = "";
            if (!Fecha)
                html = " and ( (dusu.desde<=Getdate() and dusu.hasta>=Getdate() ) or (dusu.desde is null and dusu.hasta is null) or (dusu.desde is null and dusu.hasta >= Getdate()) or ( dusu.desde<= GetDate() and dusu.hasta is null) ) ";
            else
                html = " and ( (dusu.desde is not null or dusu.hasta is not null) ) ";
            return html;

        }




        [WebMethod(Description = @"devuelve los documentos de un usuario segun el tipo de documento (-1 raiz). El parametro html si contiene 1 solo devolvera los archivos que tengan html
            si contiene 0 solo los que no tienen html, si no contiene valor o otro serà indiferente. Tabla, es para definir si es un formulario o un documento ")]
        public string BuscarArchivos(string Usuario, string Pass, string Nombre, string idempresa, string tipoArchivo, string tabla, string html)
        {
            DataTable respuesta = new DataTable();
            // string respuesta2 = "";
            tipo tipoAux;
            var consultaAux = string.Empty;
            var consultaHtml = string.Empty;
            string Consulta1 = "";
            DateTime Hoy = DateTime.Now;

            try
            {
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string idUsuario = LoginCorrectoNew(Usuario, Pass);
                if (idUsuario == "-1")
                {
                    return "";
                }


                switch (html)
                {
                    case "0":
                        consultaHtml = " and arch.tieneHtml=0";
                        break;
                    case "1":
                        consultaHtml = " and arch.tieneHtml=1";
                        break;
                    default:
                        consultaHtml = string.Empty;
                        break;
                }

                if (!string.IsNullOrEmpty(tipoArchivo))
                {
                    tipoAux = devolverArbolTipo(idUsuario, idempresa, Convert.ToInt32(tipoArchivo), tabla);
                    var stringTipos = devolverTiposString(tipoAux);


                    foreach (var idTipo in stringTipos.Split('|'))
                    {
                        if (idTipo != "" && idTipo != tipoArchivo)
                        {
                            consultaAux += " OR archTip.idTipo" + tabla + "= " + idTipo;
                        }
                        else if (idTipo == tipoArchivo)
                        {
                            consultaAux += " AND ( archTip.idTipo" + tabla + " = " + idTipo;
                        }
                    }
                    consultaAux += ")";
                }

                if (tabla.ToLower() == "documento")
                {// query para documentos
                    #region documentos


                    if (string.IsNullOrEmpty(idempresa))
                        Consulta1 = "SELECT arch.docOriginal as Nombre, arch.fechaCreacion, arch.iddocumento, dusu.carpetadocumento,emp.nombre,arch.idusuariopropietario,dusu.LecturaObligatoria,arch.Compartido,dusu.Visualizado  FROM Usuarios AS usu LEFT OUTER JOIN DocumentosUsuarios AS dusu ON usu.idusuario = dusu.idusuario INNER JOIN Documentos AS arch ON arch.iddocumento = dusu.iddocumento left join empresa emp on emp.idempresa=arch.idempresa left join TiposDocumento archTip on arch.Iddocumento=archTip.iddoc WHERE (arch.activo='True') and (dusu.idusuario = '" + idUsuario + "') and arch.activo='True' AND (arch.docoriginal <> 'NotificationsManual') ";
                    else
                        //QUITO LA LIMITACION DE QUE SOLO SE VEAN LOS DOCUMENTOS DE TU EMPRESA
                        Consulta1 = "SELECT arch.docOriginal as Nombre, arch.fechaCreacion, arch.iddocumento, dusu.carpetadocumento,emp.nombre,arch.idusuariopropietario,dusu.LecturaObligatoria,arch.Compartido,dusu.Visualizado  FROM Usuarios AS usu LEFT OUTER JOIN DocumentosUsuarios AS dusu ON usu.idusuario = dusu.idusuario INNER JOIN Documentos AS arch ON arch.iddocumento = dusu.iddocumento left join empresa emp on emp.idempresa=arch.idempresa left join TiposDocumento archTip on arch.Iddocumento=archTip.iddoc WHERE (arch.activo='True') and (dusu.idusuario = '" + idUsuario + "') and arch.activo='True' and (arch.idempresa='" + idempresa + "') AND (arch.docOriginal <> 'NotificationsManual')";

                    if (!string.IsNullOrEmpty(Nombre))
                    {
                        Consulta1 += " and arch.docoriginal like '%" + Nombre + "%'";
                    }
                    Consulta1 += consultaAux;
                    Consulta1 += consultaHtml;
                    Consulta1 += @"  GROUP by arch.idDocumento,arch.docOriginal,arch.FechaCreacion,dusu.carpetadocumento,emp.Nombre,arch.idusuariopropietario
 ,dusu.LecturaObligatoria
 ,arch.Compartido
 ,dusu.Visualizado order by dusu.LecturaObligatoria,arch.fechacreacion";
                    #endregion documentos
                }
                else if (tabla.ToLower() == "formulario")
                {

                    Consulta1 = @"SELECT  * FROM Formularios AS arch
                                  INNER JOIN TiposFormulario as archTip
                                     ON arch.idFormulario = archTip.idFormulario
                                  WHERE (EXISTS (SELECT *
                                    FROM Formularios AS arch
                                    WHERE idUsuario = " + idUsuario + @" and arch.activo=1)
                                 OR EXISTS (SELECT *
                                    FROM FormulariosUsuarios 
                                    WHERE FormulariosUsuarios.idFormulario = arch.idFormulario
                                    AND idUsuario = " + idUsuario + @")
                                   )
                                 " + consultaHtml + consultaAux;
                    if (!string.IsNullOrEmpty(Nombre))
                    {
                        Consulta1 += " and arch.Nombre like '%" + Nombre + "%'";
                    }

                    //falta añadir condiciones 
                }
                respuesta = DatabaseConnection.executeNonQueryDT(Consulta1, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);



                if (respuesta.Rows.Count > 0)
                {

                    return DataTableToJSON(respuesta);
                }
                else
                {
                    return "";
                }

            }
            catch (Exception e)
            {
                return "";
            }

        }



        internal string DataTableToJSON(DataTable dt)
        {
            try
            {
                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;
                foreach (DataRow dr in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    rows.Add(row);
                }
                return serializer.Serialize(rows);

            }
            catch (Exception e)
            {
                Trazas t = new Trazas("DataTableToJSON", "WorkFlow", -1, e.ToString(), e.InnerException + "");
                return "[]";
            }
        }

        /// <summary>
        /// devuelve todos los tipos involucrados separados por el character '|'
        /// </summary>
        /// <param name="tipoAux"></param>
        /// <returns></returns>
        private string devolverTiposString(tipo tipoAux)
        {

            var resp = string.Empty;

            try
            {
                resp = tipoAux.id + "|";
                if (tipoAux.hijosTipo != null)
                {
                    foreach (tipo t in tipoAux.hijosTipo)
                    {
                        resp += devolverTiposString(t);
                    }
                }

                return resp;

            }
            catch (Exception e)
            {
                Trazas t = new Trazas("contruirConsultaTipos", "WSZFK.asmx", -1, e.Message, e.ToString());
                return null;
            }
        }

        public class tipo
        {

            public string id { get; set; }
            public string nombre { get; set; }
            public List<tipo> hijosTipo { get; set; }

            public tipo()
            {
                this.hijosTipo = new List<tipo>();
            }

        }
        /// <summary>
        /// Devolvermos el tipo con todo su arbol de descendencia
        /// </summary>
        /// <param name="idUsuario"></param>
        /// <param name="tipoArchivo"></param>
        /// <param name="tabla"> documento,formulario</param>
        /// <returns></returns>
        private tipo devolverArbolTipo(string idUsuario, string idEmpresa, int tipoArchivo, string tabla)
        {
            try
            {
                var query = string.Empty;
                var tipos = new Dictionary<string, tipo>();
                var tablaQuery = string.Empty;
                DataTable dt;

                switch (tabla.ToLower())
                {
                    case "documento":
                        tablaQuery = "documento";
                        break;
                    case "formulario":
                        tablaQuery = "formulario";
                        break;
                    default:
                        Trazas t = new Trazas("devolverArbolTipo", "wszfk.asmx", -1, "No se ha insertado un nombre de tabla correcto", "No se ha insertado un nombre de tabla correcto");
                        return null;
                        break;
                }

                if (tipoArchivo != -1)
                {
                    //recogemos descripcion del tipo i lo añadimos al diccionario
                    query = "select Descripcion from Tipo" + tablaQuery + " where idTipo" + tablaQuery + " =" + tipoArchivo;

                    //buscamos todos los tipos del usuario i empezamos a montar su arbol
                    dt = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                    tipos.Add(tipoArchivo.ToString(), new tipo { id = tipoArchivo.ToString(), nombre = dt.Rows[0]["Descripcion"].ToString() });
                }
                else
                {
                    tipos.Add(tipoArchivo.ToString(), new tipo { id = tipoArchivo.ToString(), nombre = "Raíz" });
                }


                query = "select idTipo" + tablaQuery + ",Descripcion,Padre from Tipo" + tablaQuery + " where Padre =" + tipoArchivo + " and (IdUsuario=" + idUsuario + " or idEmpresa=" + idEmpresa + ")";
                dt = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                foreach (DataRow row in dt.Rows)
                {
                    //creamos el sub arbol de hijos
                    tipos.Add(row["idTipo" + tablaQuery].ToString(), new tipo
                    {
                        id = row["idTipo" + tablaQuery].ToString(),
                        nombre = row["Descripcion"].ToString(),
                        hijosTipo = devolverHijosTipo(idUsuario, row["idTipo" + tablaQuery].ToString(), tablaQuery)
                    });
                    //añadimos el padre de la raiz
                    tipos[tipoArchivo.ToString()].hijosTipo.Add(tipos[row["idTipo" + tablaQuery].ToString()]);

                }

                return tipos[tipoArchivo.ToString()];
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("devolverHijosTipo", "WSZFK.asmx", -1, e.Message, e.ToString());
                return null;
            }
        }
        /// <summary>
        /// devuelve una lista con los hijos de un tipoDocumento
        /// </summary>
        /// <param name="tipoArchivo"></param>
        /// <param name="tabla">Documentos,Formularios</param>
        /// <returns></returns>
        private List<tipo> devolverHijosTipo(string idUsuario, string tipoArchivo, string tabla)
        {
            try
            {   //recogemos descripcion del tipo i lo añadimos al diccionario
                var query = "select idTipo" + tabla + ",Descripcion,Padre from Tipo" + tabla + " where Padre =" + tipoArchivo;
                var tipos = new Dictionary<string, tipo>();
                var dt = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                var listaHijos = new List<tipo>();
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        listaHijos.Add(new tipo
                        {
                            id = row["idTipo" + tabla].ToString(),
                            nombre = row["Descripcion"].ToString(),
                            hijosTipo = devolverHijosTipo(idUsuario, row["idTipo" + tabla].ToString(), tabla)
                        });

                    }

                    return listaHijos;
                }

                return null;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("devolverHijosTipo", "WSZFK.asmx", -1, e.Message, e.ToString());
                return null;
            }
        }


        //[WebMethod (Description ="Devuelve los documentos que tienen html si html=true o los que no tienen html=false")]
        //public DataTable BuscarDocumentos(string Usuario, string Pass, string Nombre, string idempresa, string tipoDocumento, bool html)
        //{

        /// <summary>
        /// busca los archivos que se pueden cargar en el editor en linea(que tengan el archivo en formato html)
        /// </summary>
        /// <param name="Usuario"></param>
        /// <param name="Pass"></param>
        /// <param name="Nombre fichero"></param>
        /// <param name="idempresa"></param>
        /// <param name="tabla"></param>
        /// <param name="tipoArchivo">tag, etiqueta, tipo</param>
        /// <returns></returns>
        [WebMethod]
        public string BuscarArchivosCargarHtml(string Usuario, string Pass, string Nombre, string idempresa, string tabla, string tipoArchivo)
        {
            string respuesta = "";
            string respuesta2 = "";
            try
            {
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string idUsuario = LoginCorrectoNew(Usuario, Pass);
                if (idUsuario == "-1")
                {
                    return "-1";
                }

                string Consulta1 = "";
                DateTime Hoy = DateTime.Now;
                if (tabla.ToLower() == "documentos")
                {
                    if (string.IsNullOrEmpty(idempresa))//      2                  3                  4         5 
                        Consulta1 = "SELECT docs.iddocumento, docs.docOriginal as Nombre, docs.fechaCreacion, emp.nombre,docs.Compartido  FROM Usuarios AS usu LEFT OUTER JOIN DocumentosUsuarios AS dusu ON usu.idusuario = dusu.idusuario INNER JOIN Documentos AS docs ON docs.iddocumento = dusu.iddocumento left join empresa emp on emp.idempresa=docs.idempresa left join TiposDocumento doctip on docs.Iddocumento=doctip.iddoc WHERE (docs.activo='True') and (dusu.idusuario = '" + idUsuario + "')  AND (docs.tieneHtml='true')";
                    else
                        //QUITO LA LIMITACION DE QUE SOLO SE VEAN LOS DOCUMENTOS DE TU EMPRESA
                        Consulta1 = "SELECT docs.iddocumento, docs.docOriginal as Nombre, docs.fechaCreacion, emp.nombre,docs.Compartido  FROM Usuarios AS usu LEFT OUTER JOIN DocumentosUsuarios AS dusu ON usu.idusuario = dusu.idusuario INNER JOIN Documentos AS docs ON docs.iddocumento = dusu.iddocumento left join empresa emp on emp.idempresa=docs.idempresa left join TiposDocumento doctip on docs.Iddocumento=doctip.iddoc WHERE (docs.activo='True') and (dusu.idusuario = '" + idUsuario + "')  and (docs.idempresa='" + idempresa + "')  AND (docs.tieneHtml='true')";

                    if (tipoArchivo != "")
                    {
                        string Jerarquia = DevolverConsultaJerarquia(tipoArchivo);
                        Consulta1 += "AND ((doctip.idTipoDocumento='" + tipoArchivo + "')";
                        if (!string.IsNullOrEmpty(Jerarquia))
                        {
                            Consulta1 += Jerarquia;

                        }
                        Consulta1 += ")";
                    }
                    if (!string.IsNullOrEmpty(Nombre))
                        Consulta1 += " and docs.docoriginal like '%" + Nombre + "%'";
                    Consulta1 += " order by dusu.LecturaObligatoria,docs.fechacreacion";

                }
                else if (tabla.ToLower() == "formularios")
                {
                    if (string.IsNullOrEmpty(idempresa))
                        Consulta1 = "Select form.idformulario,form.Nombre,form.FechaCreacion,emp.nombre,form.Compartido FROM  Usuarios AS usu left join formularios AS form ON form.idusuario = usu.idusuario inner join empresa emp on emp.idempresa=form.idempresa left join TiposFormulario tipform on form.Idformulario=tipform.idformulario WHERE (form.activo='True') and (form.idusuario = '" + idUsuario + "') AND  form.tieneHtml = 'true'  ";
                    else
                        Consulta1 = "SELECT form.idformulario,form.Nombre, form.fechaCreacion,emp.nombre,form.Compartido FROM Usuarios AS usu LEFT join formularios AS form ON form.idusuario = usu.idusuario inner join empresa emp on emp.idempresa=form.idempresa left join TiposFormulario tipform on form.Idformulario=tipform.idformulario WHERE (form.activo='True') and (form.idusuario = '" + idUsuario + "') and (form.idempresa='" + idempresa + "')AND form.tieneHtml = 'true' ";


                    if (!string.IsNullOrEmpty(tipoArchivo))
                    {
                        string Jerarquia = DevolverTiposInferioresFormularios(tipoArchivo);
                        if (string.IsNullOrEmpty(Jerarquia))
                            Consulta1 += " and (tipform.idtipoformulario='" + tipoArchivo + "' and tipform.Activo='True') ";
                        else
                        {
                            //Consulta1 += " and ";
                            // Boolean Primero = true;
                            Consulta1 += " and ( (tipform.idtipoformulario='" + tipoArchivo + "' and tipform.Activo='True') ";
                            string[] Hijos = Jerarquia.Split(';');
                            foreach (string Nodo in Hijos)
                            {
                                if (!string.IsNullOrEmpty(Nodo))
                                    Consulta1 += DevolverConsultaJerarquiaFormularios(Nodo);
                            }
                            Consulta1 += ")";
                        }
                    }
                }


                OleDbCommand vComando = new OleDbCommand(Consulta1, vCon);
                vCon.Open();
                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;

                da.Fill(ds);
                vCon.Close();

                List<String> lista = new List<String>();
                string aux = "";
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    aux = "|" + dr[0] + "^" + dr[1] + "^" + " " + "^" + dr[2] + "^" + dr[3] + "^" + dr[4];
                    if (!lista.Contains(aux))
                    {
                        lista.Add(aux);
                        respuesta += aux;
                    }
                }
                return respuesta;

            }
            catch (Exception e)
            {
                return -1 + "";
            }

        }
        [WebMethod]
        public string DevolverDocumentosCompartidos(string Usuario, string Pass, string Nombre, string tag, string Vistos, string idempresa, string Carpeta, string Consulta)
        {
            string respuesta = "";
            try
            {
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string idUsuario = LoginCorrectoNew(Usuario, Pass);
                if (idUsuario == "-1")
                {
                    return "-1";
                }

                string Consulta1 = "";
                DateTime Hoy = DateTime.Now;
                if (string.IsNullOrEmpty(idempresa))
                    Consulta1 = "SELECT docs.docOriginal as Nombre, docs.fechaCreacion, docs.iddocumento, dusu.carpetadocumento,emp.nombre,docs.idusuariopropietario,dusu.LecturaObligatoria,docs.Compartido,dusu.Visualizado  FROM Usuarios AS usu LEFT OUTER JOIN DocumentosUsuarios AS dusu ON usu.idusuario = dusu.idusuario INNER JOIN Documentos AS docs ON docs.iddocumento = dusu.iddocumento left join empresa emp on emp.idempresa=docs.idempresa left join TiposDocumento doctip on docs.Iddocumento=doctip.iddoc WHERE (dusu.idusuarioComparte<>'" + idUsuario + "') and (docs.activo='True') and (dusu.idusuario = '" + idUsuario + "') and docs.activo='True' ";
                else
                    //QUITO LA LIMITACION DE QUE SOLO SE VEAN LOS DOCUMENTOS DE TU EMPRESA
                    Consulta1 = "SELECT docs.docOriginal as Nombre, docs.fechaCreacion, docs.iddocumento, dusu.carpetadocumento,emp.nombre,docs.idusuariopropietario,dusu.LecturaObligatoria,docs.Compartido,dusu.Visualizado  FROM Usuarios AS usu LEFT OUTER JOIN DocumentosUsuarios AS dusu ON usu.idusuario = dusu.idusuario INNER JOIN Documentos AS docs ON docs.iddocumento = dusu.iddocumento left join empresa emp on emp.idempresa=docs.idempresa left join TiposDocumento doctip on docs.Iddocumento=doctip.iddoc WHERE (dusu.idusuarioComparte<>'" + idUsuario + "') and (docs.activo='True') and (dusu.idusuario = '" + idUsuario + "') and docs.activo='True' and (docs.idempresa='" + idempresa + "') ";

                if (!string.IsNullOrEmpty(tag))
                {
                    string Jerarquia = DevolverTiposInferiores(tag);
                    if (string.IsNullOrEmpty(Jerarquia))
                        Consulta1 += " and (doctip.idtipodocumento='" + tag + "' and doctip.Activo='True') ";
                    else
                    {
                        //Consulta1 += " and ";
                        // Boolean Primero = true;
                        Consulta1 += " and ( (doctip.idtipodocumento='" + tag + "' and doctip.Activo='True') ";
                        string[] Hijos = Jerarquia.Split(';');
                        foreach (string Nodo in Hijos)
                        {
                            if (!string.IsNullOrEmpty(Nodo))
                                Consulta1 += DevolverConsultaJerarquia(Nodo);
                        }
                        Consulta1 += ")";
                    }
                }
                if (!string.IsNullOrEmpty(Vistos))
                    Consulta1 += " and dusu.Visualizado='" + Vistos + "' and dusu.descargado='" + Vistos + "'";

                if (!string.IsNullOrEmpty(Consulta))
                    Consulta1 += " and exists (" + Consulta + " and tipdoc.iddoc=docs.iddocumento)";

                Consulta1 += " order by dusu.LecturaObligatoria,docs.fechacreacion";


                OleDbCommand vComando = new OleDbCommand(Consulta1, vCon);
                vCon.Open();
                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);
                vCon.Close();

                List<String> lista = new List<String>();
                string aux = "";
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    aux = "|" + dr[0] + "^" + dr[1] + "^" + " " + "^" + dr[2] + "^" + dr[3] + "^" + dr[4] + "^" + dr[5] + "^" + dr[6] + "^" + dr[7] + "^" + dr[8];
                    if (!lista.Contains(aux))
                    {
                        lista.Add(aux);
                        respuesta += aux;
                    }
                }
                return respuesta;

            }
            catch (Exception e)
            {
                Trazas t = new Trazas("DevolverDocumentosCompartidos", "WSZFK.asmx", -1, e.Message, e.ToString());
                return -1 + "";
            }

        }

        [WebMethod]
        public string DocumentosNombreNew(string Usuario, string Pass, string Nombre, string tag, string Vistos, string idempresa, string Carpeta, string Consulta, Boolean SoloconFecha)
        {
            string respuesta = "";
            string respuesta2 = "";
            try
            {
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string idUsuario = LoginCorrectoNew(Usuario, Pass);
                if (idUsuario == "-1")
                {
                    return "-1";
                }

                string Consulta1 = "";
                DateTime Hoy = DateTime.Now;
                if (string.IsNullOrEmpty(idempresa))
                    Consulta1 = "SELECT docs.docOriginal as Nombre, docs.fechaCreacion, docs.iddocumento, dusu.carpetadocumento,emp.nombre,docs.idusuariopropietario,dusu.LecturaObligatoria,docs.Compartido,dusu.Visualizado  FROM Usuarios AS usu LEFT OUTER JOIN DocumentosUsuarios AS dusu ON usu.idusuario = dusu.idusuario INNER JOIN Documentos AS docs ON docs.iddocumento = dusu.iddocumento left join empresa emp on emp.idempresa=docs.idempresa left join TiposDocumento doctip on docs.Iddocumento=doctip.iddoc WHERE (docs.activo='True') and (dusu.idusuario = '" + idUsuario + "') and docs.activo='True' AND (docs.docoriginal <> 'NotificationsManual') ";
                else
                    //QUITO LA LIMITACION DE QUE SOLO SE VEAN LOS DOCUMENTOS DE TU EMPRESA
                    Consulta1 = "SELECT docs.docOriginal as Nombre, docs.fechaCreacion, docs.iddocumento, dusu.carpetadocumento,emp.nombre,docs.idusuariopropietario,dusu.LecturaObligatoria,docs.Compartido,dusu.Visualizado  FROM Usuarios AS usu LEFT OUTER JOIN DocumentosUsuarios AS dusu ON usu.idusuario = dusu.idusuario INNER JOIN Documentos AS docs ON docs.iddocumento = dusu.iddocumento left join empresa emp on emp.idempresa=docs.idempresa left join TiposDocumento doctip on docs.Iddocumento=doctip.iddoc WHERE (docs.activo='True') and (dusu.idusuario = '" + idUsuario + "') and docs.activo='True' and (docs.idempresa='" + idempresa + "') AND (docs.docOriginal <> 'NotificationsManual')";
                //Consulta1 = "SELECT docs.docOriginal as Nombre, docs.fechaCreacion, docs.iddocumento, dusu.carpetadocumento,emp.nombre,docs.idusuariopropietario,dusu.LecturaObligatoria,docs.Compartido,dusu.Visualizado  FROM Usuarios AS usu LEFT OUTER JOIN DocumentosUsuarios AS dusu ON usu.idusuario = dusu.idusuario INNER JOIN Documentos AS docs ON docs.iddocumento = dusu.iddocumento left join empresa emp on emp.idempresa=docs.idempresa left join TiposDocumento doctip on docs.Iddocumento=doctip.iddoc WHERE (dusu.idusuario = '" + idUsuario + "') and (docs.docOriginal <> 'NotificationsManual') ";

                Consulta1 += MontarConsultaDesdeHasta(SoloconFecha);

                //string Consulta1="Select docs.Nombre, docs.FechaSubida, docs.PreviewMimeType, docs.iddocumento,docs.carpetadocumento from usuarios as usu left join DocumentosUsuarios as docs on usu.idusuario=docs.idusuario where docs.nombre<>'NotificationsManual' and docs.idusuario='" + idUsuario + "' ";                
                if (!string.IsNullOrEmpty(Carpeta))
                    Consulta1 += " and dusu.carpetadocumento='" + Carpeta + "'";
                if (!string.IsNullOrEmpty(tag))
                {
                    string Jerarquia = DevolverTiposInferiores(tag);
                    if (string.IsNullOrEmpty(Jerarquia))
                        Consulta1 += " and (doctip.idtipodocumento='" + tag + "' and doctip.Activo='True') ";
                    else
                    {
                        //Consulta1 += " and ";
                        // Boolean Primero = true;
                        Consulta1 += " and ( (doctip.idtipodocumento='" + tag + "' and doctip.Activo='True') ";
                        string[] Hijos = Jerarquia.Split(';');
                        foreach (string Nodo in Hijos)
                        {
                            if (!string.IsNullOrEmpty(Nodo))
                                Consulta1 += DevolverConsultaJerarquia(Nodo);
                        }
                        Consulta1 += ")";
                    }
                }
                if (!string.IsNullOrEmpty(Vistos))
                    Consulta1 += " and dusu.Visualizado='" + Vistos + "' and dusu.descargado='" + Vistos + "'";

                if (!string.IsNullOrEmpty(Consulta))
                    Consulta1 += " and exists (" + Consulta + " and tipdoc.iddoc=docs.iddocumento)";

                Consulta1 += " order by dusu.LecturaObligatoria,docs.fechacreacion";


                OleDbCommand vComando = new OleDbCommand(Consulta1, vCon);
                vCon.Open();
                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);
                vCon.Close();

                List<String> lista = new List<String>();
                string aux = "";
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    aux = "|" + dr[0] + "^" + dr[1] + "^" + " " + "^" + dr[2] + "^" + dr[3] + "^" + dr[4] + "^" + dr[5] + "^" + dr[6] + "^" + dr[7] + "^" + dr[8];
                    if (!lista.Contains(aux))
                    {
                        lista.Add(aux);
                        respuesta += aux;
                    }
                }
                return respuesta;

            }
            catch (Exception e)
            {
                Trazas t = new Trazas("DocumentosNombreNew", "WSZFK.asmx", -1, e.Message, e.ToString());
                return -1 + "";
            }

        }

        [WebMethod]
        public string DevolverFormulariosUsuario(string Usuario, string Pass, string tag, string idempresa, string Consulta)
        {
            string respuesta = "";

            try
            {
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string idUsuario = LoginCorrectoNew(Usuario, Pass);
                if (idUsuario == "-1")
                {
                    return "-1";
                }

                string Consulta1 = "";
                DateTime Hoy = DateTime.Now;
                if (string.IsNullOrEmpty(idempresa))
                    // Consulta1 = "Select form.idformulario,form.Nombre,form.FechaCreacion,emp.nombre,form.Compartido FROM  Usuarios AS usu left join formularios AS form ON form.idusuario = usu.idusuario inner join empresa emp on emp.idempresa=form.idempresa left join TiposFormulario tipform on form.Idformulario=tipform.idformulario WHERE (form.activo='True') and (form.idusuario = '" + idUsuario + "')  ";
                    Consulta1 = @"SELECT form.idformulario,form.Nombre, form.fechaCreacion,form.Compartido FROM Formularios as form
                                  WHERE EXISTS (SELECT * FROM FormulariosUsuarios WHERE FormulariosUsuarios.idFormulario = form.idFormulario AND idUsuario = " + idUsuario + @")
                                  or ( form.idUsuario=" + idUsuario;
                else
                    //Consulta1 = "SELECT form.idformulario,form.Nombre, form.fechaCreacion,emp.nombre,form.Compartido FROM Usuarios AS usu LEFT join formularios AS form ON form.idusuario = usu.idusuario inner join empresa emp on emp.idempresa=form.idempresa left join TiposFormulario tipform on form.Idformulario=tipform.idformulario WHERE (form.activo='True') and (form.idusuario = '" + idUsuario + "') and (form.idempresa='" + idempresa + "') ";
                    Consulta1 = @"SELECT form.idformulario,form.Nombre, form.fechaCreacion,form.Compartido FROM Formularios as form
                                  WHERE EXISTS (SELECT * FROM FormulariosUsuarios WHERE FormulariosUsuarios.idFormulario = form.idFormulario AND idUsuario =" + idUsuario + @")
                                  or (form.idempresa=" + idempresa + " and form.idUsuario=" + idUsuario;

                if (!string.IsNullOrEmpty(tag))
                {
                    Consulta1 += "and idFormulario in ( SELECT idFormulario FROM TiposFormulario tipform where ";
                    string Jerarquia = DevolverTiposInferioresFormularios(tag);
                    if (string.IsNullOrEmpty(Jerarquia))
                        Consulta1 += " (tipform.idtipoformulario='" + tag + "' and tipform.Activo='True') ";
                    else
                    {
                        //Consulta1 += " and ";
                        // Boolean Primero = true;
                        Consulta1 += " ((tipform.idtipoformulario='" + tag + "' and tipform.Activo='True') ";
                        string[] Hijos = Jerarquia.Split(';');
                        foreach (string Nodo in Hijos)
                        {
                            if (!string.IsNullOrEmpty(Nodo))
                                Consulta1 += DevolverConsultaJerarquiaFormularios(Nodo);
                        }
                        Consulta1 += ")";
                    }
                    Consulta1 += ")";
                }
                //****************************************

                if (!string.IsNullOrEmpty(Consulta))
                    Consulta1 += " and exists (" + Consulta + " and tipform.idformulario=form.idformulario)";

                Consulta1 += " ) and form.Activo=1 order by form.fechacreacion,form.nombre";


                OleDbCommand vComando = new OleDbCommand(Consulta1, vCon);
                vCon.Open();
                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);
                string nombreEmpresa = DevolverEmpresaPrincipal(idUsuario, idempresa).Split('|')[0];

                List<String> lista = new List<String>();
                string aux = "";
                foreach (DataRow dr in ds.Tables[0].Rows)
                {   // idformulario, Nombre,  FechaCreacion ,emp.nombre,  form.Compartido
                    //aux = "|" + dr[0] + "^" + dr[1] + "^" + dr[2] + "^" + dr[3] + "^" + dr[4];
                    aux = "|" + dr[0] + "^" + dr[1] + "^" + dr[2] + "^" + nombreEmpresa + "^" + dr[3];
                    if (!lista.Contains(aux))
                    {
                        lista.Add(aux);
                        respuesta += aux;
                    }
                }
                vCon.Close();
                return respuesta;

            }
            catch (Exception e)
            {
                Trazas t = new Trazas("DevolverFormulariosUsuario", "WSZFK.asmx", -1, e.Message, e.ToString());
                return -1 + "";
            }

        }

        //[WebMethod]
        //public string DevolverFormulariosUsuario(string EmailUsuario, string Pass, string idempresa)
        //{
        //    string respuesta = "";
        //    try
        //    {
        //        OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
        //        string idUsuario = LoginCorrectoNew(EmailUsuario, Pass);
        //        if (idUsuario == "-1")
        //        {
        //            return "-1";
        //        }

        //        string Consulta1 = "";
        //        DateTime Hoy = DateTime.Now;
        //        if (string.IsNullOrEmpty(idempresa))
        //            Consulta1 = "SELECT f.Nombre, f.FechaCreacion, f.idformulario from formularios f where f.idusuario=" + DevolveridUsuario(EmailUsuario);
        //        else
        //            Consulta1 = "SELECT f.Nombre, f.FechaCreacion, f.idformulario from formularios f where f.idempresa=" + idempresa + " and f.idusuario=" + DevolveridUsuario(EmailUsuario);
        //        Consulta1 += " order by f.nombre,f.FechaCreacion";

        //        OleDbCommand vComando = new OleDbCommand(Consulta1, vCon);
        //        vCon.Open();
        //        OleDbDataAdapter da = new OleDbDataAdapter();
        //        DataSet ds = new DataSet();

        //        da.SelectCommand = vComando;
        //        da.Fill(ds);


        //        List<String> lista = new List<String>();
        //        string aux = "";
        //        foreach (DataRow dr in ds.Tables[0].Rows)
        //        {
        //            aux = "|" + dr[0] + "^" + dr[1] + "^" + dr[2];
        //            if (!lista.Contains(aux))
        //            {
        //                lista.Add(aux);
        //                respuesta += aux;
        //            }
        //        }

        //        vCon.Close();

        //        return respuesta;


        //    }
        //    catch (Exception e)
        //    {
        //        return -1 + "";
        //    }

        //}

        public string DevolverConsultaJerarquiaExpedientes(string tag)
        {
            string Jerarquia = DevolverTiposInferiores(tag);
            string Consulta = "";
            Consulta += " or (tipdoc.idtipodocumento='" + tag + "' and tipdoc.Activo='true' and tipdoc.idDoc = docusu.iddocumento )";
            string[] Hijos = Jerarquia.Split(';');
            foreach (string Nodo in Hijos)
            {
                if (!string.IsNullOrEmpty(Nodo))
                {
                    Consulta += " or (tipdoc.idtipodocumento='" + Nodo + "' and tipdoc.Activo='true' and tipdoc.idDoc = docusu.iddocumento) ";
                    string Jerarquia2 = DevolverTiposInferiores(Nodo);
                    if (!string.IsNullOrEmpty(Jerarquia2))
                        DevolverConsultaJerarquiaExpedientes(Nodo);
                }
            }
            return Consulta;
        }
        public string DevolverConsultaJerarquia(string tag)
        {
            string Jerarquia = DevolverTiposInferiores(tag);
            string Consulta = "";
            Consulta += " or (doctip.idtipodocumento='" + tag + "' and doctip.Activo='true' )";
            string[] Hijos = Jerarquia.Split(';');
            foreach (string Nodo in Hijos)
            {
                if (!string.IsNullOrEmpty(Nodo))
                {
                    Consulta += " or (doctip.idtipodocumento='" + Nodo + "' and doctip.Activo='true') ";
                    string Jerarquia2 = DevolverTiposInferiores(Nodo);
                    if (!string.IsNullOrEmpty(Jerarquia2))
                        DevolverConsultaJerarquia(Nodo);
                }
            }
            return Consulta;
        }
        public string DevolverConsultaJerarquiaFormularios(string tag)
        {
            string Jerarquia = DevolverTiposInferioresFormularios(tag);
            string Consulta = "";
            Consulta += " or (tipform.idtipoformulario='" + tag + "' and tipform.Activo='true')";
            string[] Hijos = Jerarquia.Split(';');
            foreach (string Nodo in Hijos)
            {
                if (!string.IsNullOrEmpty(Nodo))
                {
                    Consulta += " or (tipform.idtipoformulario='" + Nodo + "' and tipform.Activo='true') ";
                    string Jerarquia2 = DevolverTiposInferiores(Nodo);
                    if (!string.IsNullOrEmpty(Jerarquia2))
                        DevolverConsultaJerarquiaFormularios(Nodo);
                }
            }
            return Consulta;
        }
        [WebMethod]
        public string DocumentosNombreGuid(string Guid)
        {
            string respuesta = "";
            try
            {
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                //string idUsuario = LoginCorrectoNew(Usuario, Pass);
                //if (idUsuario == "-1")
                //{
                //    return "-1";
                //}
                string Tipo = TipoGuidCompartido(Guid);
                string Consulta1 = "";

                if (Tipo == "Documento")
                    Consulta1 = "SELECT docs.docOriginal, comp.fechacomparticion, docs.iddocumento, docs.idusuariopropietario,comp.iddoc  FROM  DocsCompartidos AS comp left join Documentos docs ON docs.iddocumento = comp.iddoc where  comp.IdGuid='" + Guid + "'  ";
                else
                    if (Tipo == "Expediente")
                    Consulta1 = " SELECT docs.docOriginal, comp.fechaComparticion, docs.idDocumento, docs.idusuariopropietario, comp.IdExpediente FROM DocsCompartidos AS comp LEFT OUTER JOIN DocumentosExpedientes AS docexp ON docexp.idexpediente = comp.idExpediente left join Documentos AS docs ON docs.idDocumento = docexp.iddocumento WHERE (comp.idGuid = '" + Guid + "') ";
                //Consulta1 = "SELECT docs.docOriginal, comp.fechacomparticion, docs.iddocumento, docs.idusuariopropietario,comp.idexpediente FROM  DocsCompartidos AS comp left join Documentos docs ON docs.iddocumento = comp.iddoc left join documentosexpedientes docexp on docexp.iddocumento=docs.iddocumento where comp.IdGuid='" + Guid + "'  ";


                Consulta1 += " order by comp.fechacomparticion,docs.docOriginal";


                OleDbCommand vComando = new OleDbCommand(Consulta1, vCon);
                vCon.Open();
                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);
                vCon.Close();

                List<String> lista = new List<String>();
                string aux = "";
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    if (!string.IsNullOrEmpty((dr[2].ToString())))
                    {
                        aux = "|" + dr[0] + "^" + dr[1] + "^" + dr[2] + "^" + dr[3] + "^" + dr[4];
                        if (!lista.Contains(aux))
                        {
                            lista.Add(aux);
                        }
                    }
                }


                // Finalmente montamos la respuesta
                foreach (string s in lista)
                {
                    respuesta += s;
                }
                return respuesta;


            }
            catch (Exception e)
            {
                return -1 + "";
            }

        }
        //***
        public string TipoGuidCompartido(string Guid)
        {
            string SQL = "select iddoc,idexpediente from docscompartidos where idguid='" + Guid + "'";

            OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
                if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0].ItemArray[0].ToString()) && ds.Tables[0].Rows[0].ItemArray[0].ToString() != "-1")
                    return "Documento";
                else
                    if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0].ItemArray[1].ToString()))
                    return "Expediente";
                else
                    return "-1";
            else
                return "-1";

        }

        [WebMethod]
        public string CrearNuevoExpedienteZDocs(string Nombre, string idusuario, string OldUniqueid)
        {
            string SQL = "insert into Expedientes (descripcion,idusuario,activo,fecha,OldUniqueid) values ('" + Nombre + "','" + idusuario + "','True',GETDATE()," + (string.IsNullOrEmpty(OldUniqueid) ? "NULL" : "'" + OldUniqueid + "'") + "); select SCOPE_IDENTITY();";

            string resp = DatabaseConnection.executeScalarString(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);// vComando.ExecuteScalar().ToString();
            return resp;
        }
        [WebMethod]
        public string CrearNuevoExpedienteUsuarioZDocs(string idexpediente, string idusuario)
        {
            string SQL = "insert into expedientesusuarios (idexpediente,idusuario) values ('" + idexpediente + "','" + idusuario + "')";

            string resp = DatabaseConnection.executeScalarString(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);// vComando.ExecuteNonQuery().ToString();
            return resp;
        }

        [WebMethod]
        [SoapHeader("AuthenticationInformation")]
        public Byte[] DescargarFicheroZDocsPro(string IdDocumento)
        {
            try
            {
                if (AuthenticationInformation != null)
                {

                    string IdUsuario = LoginCorrectoNew(AuthenticationInformation.UserName, AuthenticationInformation.Password);
                    if (IdUsuario != "-1")
                    {

                        string Directorio = "/Inetpub/wwwroot/signesProVolumenes/";

                        String Fec = DateTime.Today.Year.ToString() + DateTime.Today.Month.ToString() + DateTime.Today.Day.ToString();
                        string Fecha = string.Format("{0:yyyyMMdd}", DateTime.Today);

                        string Ruta = "00000000";
                        Ruta = Ruta.Substring(0, 8 - IdDocumento.Length) + IdDocumento;
                        string pathString = Directorio + Ruta.Substring(0, 2) + "/" + Ruta.Substring(2, 2) + "/" + Ruta.Substring(4, 2) + "/";// +Ruta.Substring(6, 2) + "/";
                        String formato = "pdf";
                        //System.IO.Directory.CreateDirectory(pathString);
                        //EnviarCorreo("lpueyo@zertifika.com", "La ruta es: " + pathString, "", "");
                        //File.WriteAllBytes(pathString + Ruta.Substring(6, 2) + "." + formato, Fichero);
                        if (System.IO.File.Exists(pathString + Ruta.Substring(6, 2) + "." + formato))
                        {
                            Byte[] PDF = File.ReadAllBytes(pathString + Ruta.Substring(6, 2) + "." + formato);
                            return PDF;
                        }
                        else
                        {

                            return null; EnviarCorreo("lpueyo@zertifika.com", "No encuentra el fichero", "", "");
                        }
                    }
                    else
                        return null;
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                return null;
            }
        }
        [WebMethod]
        public Byte[] DescargarFicheroZDocsProAndres(string IdDocumento, string Usuario, string Password)
        {
            try
            {


                string IdUsuario = LoginCorrectoNew(Usuario, Password);
                if (IdUsuario != "-1")
                {

                    string Directorio = "/Inetpub/wwwroot/signesProVolumenes/";

                    String Fec = DateTime.Today.Year.ToString() + DateTime.Today.Month.ToString() + DateTime.Today.Day.ToString();
                    string Fecha = string.Format("{0:yyyyMMdd}", DateTime.Today);

                    string Ruta = "00000000";
                    Ruta = Ruta.Substring(0, 8 - IdDocumento.Length) + IdDocumento;
                    string pathString = Directorio + Ruta.Substring(0, 2) + "/" + Ruta.Substring(2, 2) + "/" + Ruta.Substring(4, 2) + "/";// +Ruta.Substring(6, 2) + "/";
                    String formato = "pdf";
                    //System.IO.Directory.CreateDirectory(pathString);
                    //EnviarCorreo("lpueyo@zertifika.com", "La ruta es: " + pathString, "", "");
                    //File.WriteAllBytes(pathString + Ruta.Substring(6, 2) + "." + formato, Fichero);
                    if (System.IO.File.Exists(pathString + Ruta.Substring(6, 2) + "." + formato))
                    {
                        Byte[] PDF = File.ReadAllBytes(pathString + Ruta.Substring(6, 2) + "." + formato);
                        return PDF;
                    }
                    else
                    {

                        return null; //EnviarCorreo("lpueyo@zertifika.com", "No encuentra el fichero", "", "");
                    }
                }
                else
                    return null;

            }
            catch (Exception e)
            {
                return null;
            }
        }
        [WebMethod]
        public string ReemplazarFichero(Byte[] Fichero, string pathString)
        {
            try
            {
                if (File.Exists(pathString))
                {
                    File.WriteAllBytes(pathString, Fichero);
                }
                return "1";
            }
            catch
            {
                return "-1";
            }
        }
        [WebMethod]
        public string SubirFichero(Byte[] Fichero, string pathString, string NomFichero)
        {
            try
            {
                string Directorio = "/Inetpub/wwwroot/signesProVolumenes/";
                System.IO.Directory.CreateDirectory(Directorio + pathString);
                File.WriteAllBytes(Directorio + pathString + NomFichero, Fichero);
                return "1";
            }
            catch
            {
                return "-1";
            }
        }
        public Boolean EsExtensionValida(string Extension,string idusuario)
        {
            try
            {
                switch (Extension.ToUpper())
                {
                    case "PDF":
                    case "XLS":
                    case "XLSX":
                    case "DOC":
                    case "DOCX":
                    case "TXT":
                        return true;
                    default:
                        return false;

                }
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("EsExtensionValida", "ZDocs", Convert.ToInt16(idusuario), e.ToString(), e.InnerException + "");
                return false;
            }
        }
        [WebMethod]
        public string SubirFicheroZDocsPro(byte[] Fichero, string NomFichero, string idusuario, string idExpediente, string idempresa)
        {
            string resultado = "";
            try
            {

                string Directorio = WebConfigurationManager.AppSettings["RutaVolumenes"];
                if (Fichero != null)
                {
                    String Fec = DateTime.Today.Year.ToString() + DateTime.Today.Month.ToString() + DateTime.Today.Day.ToString();
                    string Fecha = string.Format("{0:yyyyMMdd}", DateTime.Today);

                    string ObligadaLectura = "False";
                    byte[] kk = null;
                    String[] aux = null;
                    String formato = string.Empty;
                    if (NomFichero.ToString().Substring(NomFichero.Length - 5).Contains('.')  )
                    {
                        if (EsExtensionValida(aux[aux.Length - 1], idusuario))
                        {
                            aux = NomFichero.Split('.');
                            formato = aux[aux.Length - 1];
                        }
                        else
                            formato = "pdf";
                    }
                    else
                    {
                        formato = "pdf";
                    }

                    //String[] aux = NomFichero.Split('.');
                    //String formato = aux[aux.Length - 1];
                    string err = "";

                    string idDoc = insertarDocEnBBDD(NomFichero, idusuario, formato, idempresa, "", "");
                    // Trazas t = new Trazas("SubirDocumento", "ZDocs-Demos", Convert.ToInt16(idusuario),"Insertado Doc en BDDD, Id: " + idDoc,"Por ahora bien");
                    string Ruta = "00000000";
                    Ruta = Ruta.Substring(0, 8 - idDoc.Length) + idDoc;
                    string pathString = Directorio + Ruta.Substring(0, 2) + "/" + Ruta.Substring(2, 2) + "/" + Ruta.Substring(4, 2) + "/";// +Ruta.Substring(6, 2) + "/";
                    System.IO.Directory.CreateDirectory(pathString);
                    // Trazas t = new Trazas("SubirDocumento", "ZDocs-Demos", Convert.ToInt16(idusuario), "La ruta es: " + pathString, "Antes de guardar fichero, hora: " + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second);
                    File.WriteAllBytes(pathString + Ruta.Substring(6, 2) + "." + formato, Fichero);
                    //  t = new Trazas("SubirDocumento", "ZDocs-Demos", Convert.ToInt16(idusuario), "Guardado fichero ", "Despues de guardar fichero, hora: " + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second);
                    if (!ExisteDocumentoUsuario(idDoc, idusuario))
                    {
                        err = InsertarDocumentoenDocumentosUsuario(idDoc, idusuario, "Documentos Propios", idusuario, ObligadaLectura, "", "", "", "", idempresa);
                    }

                    if (!string.IsNullOrEmpty(idExpediente))
                    {
                        if (!ExisteDocumentoExpediente(idDoc, idExpediente))
                        {
                            InsertarDocumentoenExpediente(idDoc, idExpediente);
                        }

                        CompartirDocumentoaUsuariosExpediente(idDoc, idExpediente, idusuario, ObligadaLectura, idempresa);

                    }

                    //QUITO LA ACTUALIZACION DE NOMBRE ORIGINAL, PARA QUE NO SE USE EN INDEXACION AL HACER UN SPLIT Y A CONSECUENCIA SE REPITAN LOS NOMBRES ORIGINALES
                    //ActualizarCampo("Documentos", "NombreOriginal", NomFichero, " IdDocumento=" + idDoc);

                    if (File.Exists(pathString + Ruta.Substring(6, 2) + "." + formato)) // + "." + formato  pathString + "/" + Fecha.Substring(6, 2) + "." + formato))                    
                        return idDoc;
                    else
                    {
                        return "-1";
                    }

                }
                return resultado;
            }
            catch (Exception e)
            {

                //string Fichero = "";
                Trazas t = new Trazas("SubirDocumento", "ZDocs-Demos", Convert.ToInt16(idusuario), e.ToString(), e.InnerException + "");
                //return "-1";
                return "error";
            }
        }




        /*
        [WebMethod]
        public string crearExpedienteConFicheroLocal(string Usuario, string Clave, string NombreExpediente, int NivelComparticion, string pathDocumento)
        {
            try
            {
                string idUsuario = LoginCorrecto(Usuario, Clave);
                if (!idUsuario.Equals("-1"))
                {
                    string dossierId = AltaExpediente(NombreExpediente, NivelComparticion, idUsuario);
                    if (!dossierId.Equals("-1"))
                    {
                        // Inserción del fichero 
                        Byte[] kk = File.ReadAllBytes(pathDocumento);
                        
                        string nombreFichero = Path.GetFileName(pathDocumento);
                        Guid g = Guid.NewGuid();
                        
                        /*  
                         * No es necesario, el service ya genera esta fila. Lo mantengo para cuando migremos. 
                         * 
                        string queryInsercionDocumento = "INSERT INTO Documents (UniqueId, Timestamp, CreationDate, SchemaVersion, Name, ViewOnly, PreviewHash, " +
                            "PreviewMimeType, OnlyMeCanChangeVisi, DocumentType, Pending, Private, UserUniqueId, Uploader, IsRead, IsDownload) " +
                            "VALUES ('" + g + "', '" + DevolverUltimoTimestamp() + "', GETDATE(), '1.0', '" +  nombreFichero + "'False', NULL, " +
                            "'text/html','False',4,'False','False','" + idUsuario + "','" + idUsuario + "','S','N')";
                         * *\


                        WSZFK.wszfkap.UserInfo _user = new WSZFK.wszfkap.UserInfo();
                        _user.Mail = Usuario;
                        _user.Pwd = Clave;

                        WSFich.FileObject _ftx = new WSFich.FileObject();
                        _ftx.FileName=nombreFichero;
                        _ftx.MimeType = getMymeTypeByExtension(pathDocumento);
                        _ftx.File=kk;

                        string[] vusuarios = new string[1];
                        vusuarios[0] = idUsuario;
                        Dictionary<string, string> _fields = new Dictionary<string, string>();
                        string _sFields = DictionaryToJson(_fields);
                        WSFich.WSDocumentAccessPoint _cliente = new WSFich.WSDocumentAccessPoint();
                        string docType = "0";
                        string docId = _cliente.AddDocumentTypified(_user, _ftx, dossierId, false , false, docType, _sFields, vusuarios);
                        docId = docId.Split('\"')[1];
                       
                        // Insertamos la etiqueta al documento 
                        int resp = insertarEtiquetaAlExpediente(idUsuario, dossierId, "Documentos Propios");
                        // Finalmente insertamos trazas 
                        InsertarTrazasNuevoExpediente(idUsuario, dossierId, NombreExpediente);
                        InsertarTrazasNuevoDocumento(idUsuario, docId, nombreFichero);

                        return "1";
                        
                    }
                }

                return "-1";
            }
            catch (Exception e)
            {
                return "-1";
            }
        }
    */

        #region Métodos de trazas



        #endregion





        [WebMethod]
        public string getTrazasDoc(string Usuario, string Clave, string idDoc)
        {
            string resultado = "";
            try
            {

                return resultado;
            }
            catch (Exception e)
            {
                return "-1";
            }
        }

        [WebMethod]
        public string NumeroDocumentosNuevosUsuario(string Usuario, string Desde, string Hasta)
        {
            OleDbConnection vCon = new OleDbConnection(CadenaConexion);

            Usuario = idUsuario(Usuario);

            string SQL = "SELECT    count(*) as Documentos FROM  Documents AS doc WHERE not exists (select * from SocialEvents where ActionUserUniqueId='" + Usuario + "' and Type=6 and SocialEvents.DocumentUniqueId=Doc.UniqueId) and  EXISTS  (SELECT     UniqueId, Timestamp, CreationDate, SchemaVersion, LastModify, DocumentId, UserId, CompanyId, CreatorId, UserIdLastModify     FROM          DocShareds        WHERE      (doc.UniqueId = DocumentId) AND (UserId = '" + Usuario + "')) or UserUniqueId='" + Usuario + "'";


            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Documentos = "";

            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                Documentos += ds.Tables[0].Rows[i].ItemArray[0].ToString();

            }
            return Documentos;


        }


        [WebMethod]
        public string NumDocumentosUsuarioCompartidos(string Usuario)
        {
            OleDbConnection vCon = new OleDbConnection(CadenaConexion);

            Usuario = idUsuario(Usuario);

            string SQL = "SELECT count(*) from docshareds where UserId ='" + Usuario + "'";


            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Documentos = "";

            Documentos += ds.Tables[0].Rows[0].ItemArray[0].ToString();


            return Documentos;


        }


        [WebMethod]
        public string ContactosUsuario(string Usuario)
        {
            OleDbConnection vCon = new OleDbConnection(CadenaConexion);

            Usuario = idUsuario(Usuario);

            string SQL = "SELECT count(*) from UserConnections where UserUniqueId ='" + Usuario + "'";


            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Documentos = "";

            Documentos += ds.Tables[0].Rows[0].ItemArray[0].ToString();


            return Documentos;


        }

        [WebMethod]
        public string DatosUsuario(string Usuario, string Password)
        {

            OleDbConnection vCon = new OleDbConnection(CadenaConexion);




            Usuario = idUsuario(Usuario);


            string SQL = "SELECT * from users where UniqueId ='" + Usuario + "'";


            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Documentos = "";

            Documentos += ds.Tables[0].Rows[0].ItemArray[6].ToString() + "|" + ds.Tables[0].Rows[0].ItemArray[7].ToString() + "|" + ds.Tables[0].Rows[0].ItemArray[36].ToString();

            return Documentos;


        }


        [WebMethod]
        public string DevolverIdDistribuidorZDocs(string Usuario)
        {

            OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);

            string SQL = "SELECT iddistribuidor from usuarios where Idusuario ='" + Usuario + "'";


            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0].ItemArray[0].ToString()))
                    return ds.Tables[0].Rows[0].ItemArray[0].ToString();
                else
                    return "1";
            }

            return "";

        }
        //[WebMethod]
        //public string DevolverPassZDocs(string Usuario)
        //{
        //    OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);

        //    string SQL = "SELECT PasswordE,Password from usuarios where Idusuario ='" + Usuario + "'";


        //    OleDbCommand vComando = new OleDbCommand(SQL, vCon);
        //    vCon.Open();

        //    OleDbDataAdapter da = new OleDbDataAdapter();
        //    DataSet ds = new DataSet();

        //    da.SelectCommand = vComando;
        //    da.Fill(ds);


        //    if (ds.Tables[0].Rows.Count>0)
        //    {
        //        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0].ItemArray[0].ToString() ) )
        //            return ds.Tables[0].Rows[0].ItemArray[0].ToString();
        //        else
        //            return ds.Tables[0].Rows[0].ItemArray[1].ToString();

        //    }

        //    return "";

        //}

        private string DevolverIdCompañiaPrincipalUsuarioZDocs(string Usuario)
        {
            OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);

            string SQL = "SELECT IdEmpresa from usuarioEmpresa where Idusuario ='" + Usuario + "' and Principal='True'";


            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Documentos = "";

            Documentos += ds.Tables[0].Rows[0].ItemArray[0].ToString();


            return Documentos;

        }
        private string NombreUsuario(string Usuario)
        {
            OleDbConnection vCon = new OleDbConnection(CadenaConexion);


            string SQL = "SELECT * from users where UniqueId ='" + Usuario + "'";


            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Documentos = "";

            Documentos += ds.Tables[0].Rows[0].ItemArray[6].ToString();


            return Documentos;

        }



        private byte[] StrToByteArray(string str)
        {
            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            return encoding.GetBytes(str);
        }



        private string DevolveridUsuario(string Correo)
        {
            try
            {


                string SQL = "select top 1 IdUsuario from Usuarios where Email='" + Correo + "'";

                int idUsuario = DatabaseConnection.executeScalarInt(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                return idUsuario.ToString();
            }
            catch (OleDbException ex)
            {
                return ex.ToString();
            }
        }
        [WebMethod]
        public string DevolverIdUsuarioExpedienteAnywhere(string IdExpediente)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select UserUniqueid from Dossiers where Uniqueid='" + IdExpediente + "'";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].Rows[0].ItemArray[0].ToString();

                }
                else
                {
                    return "";
                }
            }
            catch (OleDbException ex)
            {
                return ex.ToString();
            }
        }
        private string idUsuario(string Correo)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select UniqueId from Users where Email='" + Correo + "'";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].Rows[0].ItemArray[0].ToString();

                }
                else
                {
                    return "False";
                }
            }
            catch (OleDbException ex)
            {
                return ex.ToString();
            }
        }

        [WebMethod]
        private string idEmpresaUsuario(string idUsuario)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select CompanyUniqueId from Users where UniqueId='" + idUsuario + "'";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].Rows[0].ItemArray[0].ToString();

                }
                else
                {
                    return "False";
                }
            }
            catch (OleDbException ex)
            {
                return ex.ToString();
            }
        }



        [WebMethod]
        public string UsuariosCias(string IdCia) //List<string>
        { //ACTUALIZADA

            string html = "";
            try
            {
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);

                String SQL = "select usu.Nombre,usu.Apellidos,usu.idusuario from usuarios usu inner join usuarioempresa usucia on usu.idusuario=usucia.idusuario inner join empresa cia on usucia.idempresa=cia.idempresa where usu.activo='S' and usu.baneado='False' and cia.activo='True' and cia.baneado='False' and usucia.idempresa='" + IdCia + "'";
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();
                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();
                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        html += "|" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[1].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[2].ToString();
                    }
                }

                return html;
            }
            catch
            { }
            return "-1";
        }
        [WebMethod]
        public string CompartirFormulario(string IdUsuarioComparte, string Clave, string IdFormulario, string IdUsuario, string Obligadalectura, string Desde, string Hasta, string Comentarios, string IdEmpresaUsuarioComparte)
        {
            try
            {
                //string IdUsuarioComparte=DevolveridUsuario(UsuarioComparte);                
                //if (SondelamismaEmpresa(IdUsuarioComparte,IdUsuario))
                //    Carpeta="Documentos de Empresa";
                //else
                //    Carpeta="Documentos Compartidos";
                if (!ExisteFormularioUsuario(IdFormulario, IdUsuario))
                    InsertarFormularioenFormulariosUsuario(IdFormulario, IdUsuario, IdUsuarioComparte, Obligadalectura, "", Desde, Hasta, Comentarios, IdEmpresaUsuarioComparte);

                InsertarEventoSocial("", IdUsuarioComparte, DevolverEmpresaPrincipal(IdUsuarioComparte, "").Split('|')[1], IdUsuario, DevolverEmpresaPrincipal(IdUsuario, "").Split('|')[1], "", "Formulario Compartido ", 20, "", "Formulario Compartido", IdFormulario);
                return "";
            }
            catch
            { return "error"; }
        }

        [WebMethod]
        public string CompartirDocumentoNew(string IdUsuarioComparte, string Clave, string DocumentId, string IdUsuario, string Obligadalectura, string Desde, string Hasta, string Comentarios, string IdEmpresaUsuarioComparte)
        {
            try
            {
                //string IdUsuarioComparte=DevolveridUsuario(UsuarioComparte);
                string Carpeta = "";
                //if (SondelamismaEmpresa(IdUsuarioComparte,IdUsuario))
                //    Carpeta="Documentos de Empresa";
                //else
                //    Carpeta="Documentos Compartidos";
                if (!ExisteDocumentoUsuario(DocumentId, IdUsuario))
                    InsertarDocumentoenDocumentosUsuario(DocumentId, IdUsuario, Carpeta, IdUsuarioComparte, Obligadalectura, "", Desde, Hasta, Comentarios, IdEmpresaUsuarioComparte);

                InsertarEventoSocial(DocumentId, IdUsuarioComparte, DevolverEmpresaPrincipal(IdUsuarioComparte, "").Split('|')[1], IdUsuario, DevolverEmpresaPrincipal(IdUsuario, "").Split('|')[1], "", "Documento Compartido ", 8, "", "Documento Compartido", "");
                return "";
            }
            catch
            { return "error"; }
        }



        private string TotalDocs()
        {
            OleDbConnection vCon = new OleDbConnection(CadenaConexion);

            string SQL = "select count(*) as TotalDocs from Documents";


            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                return ds.Tables[0].Rows[0].ItemArray[0].ToString();
            }
            else
            {
                return "0";
            }


        }

        private string TotalUsuarios()
        {
            OleDbConnection vCon = new OleDbConnection(CadenaConexion);

            string SQL = "select count(*) as TotalUsuarios from users";


            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                return ds.Tables[0].Rows[0].ItemArray[0].ToString();
            }
            else
            {
                return "0";
            }


        }

        private string TotalCompanies()
        {
            OleDbConnection vCon = new OleDbConnection(CadenaConexion);

            string SQL = "select count(*) as TotalEmpresas from companies";


            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                return ds.Tables[0].Rows[0].ItemArray[0].ToString();
            }
            else
            {
                return "0";
            }


        }



        [WebMethod]
        private string AccesoUsuarioCentro(string Centro, string Usuario, string Empresa)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select * from usuarioscentros where cif='" + Empresa + "' and DNI='" + Usuario + "'";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    SQL = "select * from usuarioscentros where cif='" + Empresa + "' and DNI='" + Usuario + "' and centro='" + Centro + "'";
                    OleDbCommand vComando2 = new OleDbCommand(SQL, vCon);


                    OleDbDataAdapter da2 = new OleDbDataAdapter();
                    DataSet ds2 = new DataSet();

                    da2.SelectCommand = vComando;
                    da2.Fill(ds2);
                    if (ds2.Tables[0].Rows.Count > 0)
                    {
                        return "True"; // ds.Tables[0].Rows[0].ItemArray[36].ToString();
                    }
                    else
                    {
                        return "False";
                    }



                }
                else
                {
                    return "True";
                }
            }
            catch (OleDbException ex)
            {
                return ex.ToString();
            }

        }

        [WebMethod]
        private string AltaUsuarioCentro(string Centro, string Usuario, string idUsuario, string Empresa)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                // string SQL = "select * from usuarioscentros where cif='" + Empresa + "' and DNI='" + Usuario + "'";

                Guid Id = Guid.NewGuid();
                string vId = Id.ToString();


                //add time clocked in/out in clock table
                string insertStatement = "INSERT INTO UsuariosCentros "
                                     + "(UniqueId,Cif, Dni, Centro,UserUniqueId) "
                                     + "VALUES ('" + vId + "','" + Empresa + "','" + Usuario + "','" + Centro + "','" + idUsuario + "')";

                OleDbCommand insertCommand = new OleDbCommand(insertStatement, vCon);

                /*  insertCommand.Parameters.Add("Cif", OleDbType.VarChar).Value = Empresa;
                  insertCommand.Parameters.Add("Dni", OleDbType.VarChar).Value = Usuario;
                  insertCommand.Parameters.Add("Centro", OleDbType.VarChar).Value = Centro;
                  insertCommand.Parameters.Add("UniqueId", OleDbType.VarChar).Value = vId;
                  insertCommand.Parameters.Add("UserUniqueId", OleDbType.VarChar).Value = vId; */

                vCon.Open();

                try
                {
                    int count = insertCommand.ExecuteNonQuery();
                }
                catch (OleDbException ex)
                {
                    return ex.Message;
                }
                return "True";

            }
            catch (OleDbException ex)
            {
                return ex.ToString();
            }

        }

        [WebMethod]
        private string BajaUsuarioCentro(string Centro, string Usuario, string idUsuario, string Empresa)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                // string SQL = "select * from usuarioscentros where cif='" + Empresa + "' and DNI='" + Usuario + "'";

                Guid Id = Guid.NewGuid();
                string vId = Id.ToString();


                //add time clocked in/out in clock table
                string deleteStatement = "delete from UsuariosCentros " + "where  (CIF='" + Empresa + "' and dni='" + Usuario + "' and Centro='" + Centro + "')";

                vCon.Open();
                OleDbCommand deleteCommand = vCon.CreateCommand();     // new OleDbCommand(deleteCommand , vCon);
                deleteCommand.CommandText = deleteStatement;



                try
                {
                    int count = deleteCommand.ExecuteNonQuery();
                }
                catch (OleDbException ex)
                {
                    return ex.Message;
                }
                return "True";

            }
            catch (OleDbException ex)
            {
                return ex.ToString();
            }

        }

        public string BuscarNombreEtiqueta(string Etiqueta, string Documento)
        {
            OleDbConnection vCon = new OleDbConnection(CadenaConexion);

            string SQL = "select * from tags where uniqueid='" + Etiqueta + "'";

            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                string NombreEtiqueta = ds.Tables[0].Rows[0].ItemArray[4].ToString();
                string vEtiqueta = BuscarCamposEtiqueta(Etiqueta, Documento);



                return NombreEtiqueta + "|" + vEtiqueta;


            }

            return "";
        }

        public string BuscarValorCampo(string Campo, string Documento)
        {
            string ValorCampo = "";
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select * from FieldValues where fielduniqueid='" + Campo + "' and documentuniqueid='" + Documento + "'";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {

                    return ds.Tables[0].Rows[0].ItemArray[4].ToString();

                }
            }
            catch
            {

                return "";

            }


            return "";
        }

        public string BuscarCamposEtiqueta(string Etiqueta, string Documento)
        {
            string CampoEtiqueta = "";
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select * from fields where taguniqueid='" + Etiqueta + "' order by Position";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        CampoEtiqueta += BuscarValorCampo(ds.Tables[0].Rows[i].ItemArray[0].ToString(), Documento) + "|";


                    }

                    return CampoEtiqueta;
                }
            }
            catch
            {
                return "";
            }



            return "";


        }

        [WebMethod]
        public string CamposEtiqueta(string Documento)
        {

            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select * from documenttags where documentuniqueid='" + Documento + "'";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    string Etiqueta = BuscarNombreEtiqueta(ds.Tables[0].Rows[0].ItemArray[1].ToString(), Documento);
                    return Etiqueta;
                }
                ;
            }
            catch
            {
                return "";
            }


            return "";

        }

        [WebMethod]
        private DataSet DocumentosPrevis(string Empresa, string Desde, string Hasta)
        {

            string DesdeFecha = Desde;
            string HastaFecha = Hasta;

            if (Desde != "")
            {
                DesdeFecha += " 00:00:00";
            }

            if (Hasta != "")
            {
                HastaFecha += " 23:59:59";
            }

            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select UniqueId,CreationDate,Name from documents where uploader='" + "6954a08f-2f52-4a08-960c-a0a000b2b0f5' ";

                if (Empresa != "")
                {
                    SQL += "and name like '" + Empresa + "%' ";
                }


                if (DesdeFecha != "")
                {
                    SQL += "and CreationDate>='" + string.Format("{0:yyyy-MM-dd H:mm:ss}", DateTime.Parse(DesdeFecha)) + "'";
                }
                if (HastaFecha != "")
                {
                    SQL += "and CreationDate<='" + string.Format("{0:yyyy-MM-dd H:mm:ss}", DateTime.Parse(HastaFecha)) + "'";
                }


                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                return ds;

            }
            catch (OleDbException ex)
            {
                return null;
            }

        }

        [WebMethod]
        private string UsuariosCIF(string Empresa)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select users.*,* from users,recibirdocumentosempresa where cifempresa='" + Empresa + "' and users.uniqueid<>'6954a08f-2f52-4a08-960c-a0a000b2b0f5' and users.uniqueid=recibirdocumentosempresa.useruniqueid and users.email not like '%mc@zertifika.com'";



                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    string vUsuarios = "";
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        vUsuarios += ds.Tables[0].Rows[i].ItemArray[0] + "^" + ds.Tables[0].Rows[i].ItemArray[6] + "^" + ds.Tables[0].Rows[i].ItemArray[36] + "|";

                    return vUsuarios;
                }
                else
                {
                    return "";
                }

            }
            catch (OleDbException ex)
            {
                return ex.ToString();
            }

        }

        [WebMethod]
        private string UsuariosCentros(string idUsuario)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select * from usuarioscentros  where useruniqueid='" + idUsuario + "' order by Centro";




                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    string vUsuarios = "";
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        vUsuarios += ds.Tables[0].Rows[i].ItemArray[3] + "|";

                    return vUsuarios;
                }
                else
                {
                    return "";
                }

            }
            catch (OleDbException ex)
            {
                return ex.ToString();
            }

        }

        [WebMethod]
        private string PosicionGlobal()
        {

            return TotalCompanies() + "|" + TotalUsuarios() + "|" + TotalDocs();


        }


        public static string DevolverRutaInicial()
        {
            int Numrutas = HttpContext.Current.Request.Url.ToString().Split('/').Count();

            //QUITO LAS DOS ULTIMAS PARTES DE LA RUTA, LA PARTE DEL PARAMETRO, MAS LA PAGINA DE FUNCIONES
            if (Numrutas > 4)
                return HttpContext.Current.Request.Url.ToString().Substring(0, HttpContext.Current.Request.Url.ToString().Length - (HttpContext.Current.Request.Url.ToString().Split('/')[Numrutas - 1].Length + HttpContext.Current.Request.Url.ToString().Split('/')[Numrutas - 2].Length + 1));
            else
                return HttpContext.Current.Request.Url.ToString().Substring(0, HttpContext.Current.Request.Url.ToString().Length - (HttpContext.Current.Request.Url.ToString().Split('/')[Numrutas - 1].Length));
        }
        public string CrearCuerpoInvitacionaDocumento(string Usuario, string IdAcceso, string NombreDoc, string Compañia)
        {
            try
            {
                //string tokenpass = Funciones.CrearTokenPassword(user.Uniqueid.ToString(), false);

                string body;
                string Clave = IdAcceso.Split('|')[1];
                IdAcceso = IdAcceso.Split('|')[0];
                string link = "<a href='" + RutaInicial + PaginaDocsCompartidos + "?Guid=" + IdAcceso + "'>" + "el siguiente enlace" + "</a>";

                string _htmlheader = "<table width='98%' style='width: 98%;'>" +
                                       "<tr>" +
                                         "<td width='100%' style='width: 100%; padding: 30pt;'>" +
                                             "<table style='font-family: arial; font-size: 12px; width: 631px;'>" +
                                                 "<tr><td style='background: none repeat scroll 0% 0% #1f497d; padding-left:25px; height:32px; font-size:23px; font-family: Helvetica;'><p>" +
                                                    "<span style='color: white; letter-spacing: -0.35pt;'>ZERTIFIKA<b><span style='color: white; font-size:9px;'> anywhere</span></b></p>" +
                                                 "</tr>" +
                                                 "<tr>" +
                                                     "<td valign='top' style='border-width: medium 1pt 1pt; border-style: none solid solid; border-color: #385D8A; background: white; padding: 25px;' colspan='2'>";


                string _htmlSignature = "<div style='font-size:13px; padding: 10px 0 0 25px;'>";
                _htmlSignature += "<b><label>" + "ZERTIFIKA, su nuevo servicio web de gestión de documentos:";


                _htmlSignature += "</label><a style='color:#005f98; text-decoration: none;' href='" + RutaInicial + PaginaDocsCompartidos + "'> ZERTIFIKA anywhere</b></a>";

                _htmlSignature += "</div>";

                _htmlSignature += "<div style='border-width: medium medium 1pt; border-style: none none solid; border-color: rgb(204, 204, 204); padding: 0cm;'><p><span style='font-size: 9pt;'>&nbsp;</span></p></div><p><span style='font-size: 9pt;'></span></p></div>";

                string _htmlfooter = _mailFooter() +
                                              "</td>" +
                                            "</tr>" +
                                         "</table></td></tr></table>";

                body = "El Usuario " + Usuario + " " + "perteneciente a la empresa" + " " + Compañia + ", le ha invitado a ver el documento: " + NombreDoc + " <br /><br />";
                body += "Si desea verlo, pulse " + link + "<br /><br/>";
                body += "La clave paa poder ver este documento es: " + Clave;
                return _htmlheader + body + _htmlSignature + _htmlfooter;

            }
            catch
            {
                return "error";
            }
        }
        public static string _mailFooter()
        {
            string _footer = "<div style='font-size:10px; color: gray;'>";
            _footer += "<label><b>" + "Navegadores recomendados: Firefox, Chrome, Safari e Internet Explorer a partir de la versión 8." + "</b></label><br />";

            return _footer;
        }
        public string CrearCuerpoCorreo(string Company, string Motivo)
        {
            string _htmlheader = "<table width='98%' style='width: 98%;'>" +
                                       "<tr>" +
                                         "<td width='100%' style='width: 100%; padding: 30pt;'>" +
                                             "<table style='font-family: arial; font-size: 12px; width: 631px;'>" +
                                                 "<tr><td style='background: none repeat scroll 0% 0% #1f497d; padding-left:25px; height:32px; font-size:23px; font-family: Helvetica;'><p>" +
                                                    "<span style='color: white; letter-spacing: -0.35pt;'>ZERTIFIKA<b><span style='color: white; font-size:9px;'> anywhere</span></b></p>" +
                                                 "</tr>" +
                                                 "<tr>" +
                                                     "<td valign='top' style='border-width: medium 1pt 1pt; border-style: none solid solid; border-color: #385D8A; background: white; padding: 25px;' colspan='2'>";


            string _htmlSignature = "<div style='font-size:13px; padding: 10px 0 0 25px;'>";
            _htmlSignature += "<b><label>" + "ZERTIFIKA, su nuevo servicio web de gestión de documentos:";

            //PREVIS
            if (Company == "Previs")
                _htmlSignature += "</label><a style='color:#005f98; text-decoration: none;' href='" + RutaInicial + PaginaLogin + "?Tema=Previs'> ZERTIFIKA anywhere</b></a>";
            else
                _htmlSignature += "</label><a style='color:#005f98; text-decoration: none;' href='" + RutaInicial + PaginaLogin + "'> ZERTIFIKA anywhere</b></a>";

            _htmlSignature += "</div>";

            _htmlSignature += "<div style='border-width: medium medium 1pt; border-style: none none solid; border-color: rgb(204, 204, 204); padding: 0cm;'><p><span style='font-size: 9pt;'>&nbsp;</span></p></div><p><span style='font-size: 9pt;'></span></p></div>";

            string _htmlfooter = _mailFooter() +
                                          "</td>" +
                                        "</tr>" +
                                     "</table></td></tr></table>";


            // return _htmlheader + "Previs le ha distribuido el documento: " + Doc + "</br></br>" + _htmlSignature + _htmlfooter;

            return _htmlheader + Motivo + "</br></br>" + _htmlSignature + _htmlfooter;
        }
        public static Boolean ExisteenListaExclusion(string Email)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string SQL = "select * from ListaExclusion where email='" + Email + "'";
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();
                da.SelectCommand = vComando;
                da.Fill(ds);

                // DataTable datos = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocs);

                if (ds.Tables[0].Rows.Count > 0)
                    return true;
                else
                    return false;
            }
            catch (OleDbException ex)
            {
                return false;
            }
        }
        [WebMethod]
        public string CompartirDocumentoEmail(string Destino, string IdDocumento, string NombreDoc, string Compañia, string IdUsuario, string IdEmpresa, string IdExpediente)
        { // Usuario Nombre + Apellidos
            try
            {
                string IdAcceso = "";
                if (!string.IsNullOrEmpty(IdDocumento))
                    IdAcceso = InsertarDocumentoenDocumentosCompartidos(IdDocumento, "", Destino, IdUsuario, "", "");
                else
                    IdAcceso = InsertarDocumentoenDocumentosCompartidos("", IdExpediente, Destino, IdUsuario, "", "");

                string Usuario = DevolverNombreUsuario(IdUsuario);
                if (IdAcceso != "error")
                {
                    if (!string.IsNullOrEmpty(IdDocumento))
                    {
                        InsertarEventoSocial(IdDocumento, IdUsuario, IdEmpresa, "", "", "", "Documento " + NombreDoc + " compartido", 8, Destino, "Documento Compartido");
                        EnviarCorreo(Destino, "Se le ha invitado a ver un documento", CrearCuerpoInvitacionaDocumento(Usuario, IdAcceso, NombreDoc, Compañia), "");
                        ActualizarCampo("Documentos", "Compartido", "True", " iddocumento='" + IdDocumento + "'");
                        return "";
                    }
                    else
                    {
                        InsertarEventoSocial("", IdUsuario, IdEmpresa, "", "", IdExpediente, "Expediente " + NombreDoc + " compartido", 9, Destino, "Expediente Compartido");
                        EnviarCorreo(Destino, "Se le ha invitado a ver un expediente", CrearCuerpoInvitacionaDocumento(Usuario, IdAcceso, NombreDoc, Compañia), "");
                        ActualizarCampo("Expedientes", "Compartido", "True", " idexpediente='" + IdExpediente + "'");
                        return "";
                    }
                }
                else
                    return "error";
            }
            catch
            {
                return "-1";
            }
        }
        public string ComprobarCorreo(string email)
        {
            string lista1 = "abcdefghijklmnopqrstuvwxyz";
            string lista2 = "-_.@";
            if (!email.Contains("@")) return "0";
            if (email.Length < 5) return "0";
            for (int i = 0; i < email.Length; i++)
            {
                if (!lista1.Contains(email[i]) && !lista2.Contains(email[i]))
                    return "0";
            }
            return "1";
        }
        public string InsertarInvitacion(string Email, string IdUsuario, string IdEmpresa, string Usuario)
        {  //USUARIO Son Nombre y Apellidos
            try
            {
                int resultado;

                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string IdToken = Guid.NewGuid().ToString();
                string Consulta = "Insert into Invitaciones ( fechaEmision,IdUsuarioEmisor,mailReceptor) values ";
                Consulta += "(GETDATE(),'" + IdUsuario + "','" + Email + "')";

                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                da.SelectCommand = vComando;
                resultado = vComando.ExecuteNonQuery();

                Correo.EnviarCorreo(Email, "El usuario " + Usuario + " le invita a ZDocs", CrearCuerpoCorreo("", "Invitacion a probar ZDocs"), "");
                InsertarEventoSocial(null, IdUsuario, IdEmpresa, null, null, null, "invitacion Enviada a " + Email, 7, "", "Invitación Enviada");

                vCon.Close();
                return "1";

            }
            catch { return "Error enviando la invitación"; }
        }
        [WebMethod]
        public string EnviarInvitacion(string Email, string IdUsuario, string IdEmpresa, string Usuario)
        { //USUARIO Son Nombre y Apellidos
            if (DevolveridUsuario(Email) != "0")
                return "El Usuario al que desea Invitar ya está dado de alta en la plataforma";
            if (ComprobarCorreo(Email) == "1")
                return InsertarInvitacion(Email, IdUsuario, IdEmpresa, Usuario);
            else
                return "la dirección de correo introducida no es valida";

        }
        [WebMethod]
        public string ActualizarCampo(string Tabla, string NomCampo, string ValorCampo, string Criterio)
        {

            try
            {
                int resultado;
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string Consulta = "update " + Tabla + " set [" + NomCampo + "] ='" + ValorCampo + "' where " + Criterio;

                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                da.SelectCommand = vComando;
                resultado = vComando.ExecuteNonQuery();

                vCon.Close();
                return "1";
            }
            catch { return "-1"; }
        }
        public static string EnviarCorreo(string Destino, string Asunto, string Texto, string Ruta)//,string Ruta2)
        {

            string Host = "mail.zertifika.com";
            string Origen = "info@zertifika.com";
            string Pass = "r3dM#z41#z41#z41";
            string User = "info@zertifika.com";
            if (!ExisteenListaExclusion(Destino))
            {

                MailMessage vCorreo = new MailMessage(Origen, Destino, Asunto, Texto);
                vCorreo.IsBodyHtml = true;
                //if (System.IO.File.Exists(Ruta))// && System.IO.File.Exists(Ruta2))
                //{
                //    Attachment vAdjunto = null;
                //    if (Ruta != null)
                //    {
                //        vAdjunto = new Attachment(Ruta);
                //        vCorreo.Attachments.Add(vAdjunto);
                //    }                

                SmtpClient vCliente = new SmtpClient(Host);
                vCliente.Credentials = new System.Net.NetworkCredential(User, Pass);

                try
                {
                    vCliente.Send(vCorreo);
                    return "";

                }
                catch
                {
                    return "\r\n" + Destino + DateTime.Now.ToString();
                }
            }
            else
                return "";
        }


        [WebMethod]
        public string DocumentosExpedienteNew(string emailUsuario, string Clave, string idExpediente, string leido, string Consulta, string Tag)
        {
            try
            {
                string idUsuario = LoginCorrectoNew(emailUsuario, Clave);
                if (!idUsuario.Equals("-1"))
                {
                    OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);

                    string SQL = "Select docusu.iddocumento,doc.docOriginal,doc.idusuariopropietario,doc.Compartido from documentosexpedientes docexp inner join Documentos doc on docexp.iddocumento=doc.iddocumento inner join DocumentosUsuarios docusu on docusu.iddocumento=doc.iddocumento where (doc.activo='True') and docexp.idexpediente='" + idExpediente + "' and docusu.idusuario='" + idUsuario + "' ";
                    if (leido == "false")
                        SQL += " and docusu.Visualizado='False' group by docusu.iddocumento,doc.docOriginal,doc.idusuariopropietario";

                    if (!string.IsNullOrEmpty(Consulta))
                        SQL += " and exists (" + Consulta + " and tipdoc.iddoc=docusu.iddocumento)";

                    if (!string.IsNullOrEmpty(Tag))
                    {
                        SQL += " and exists (SELECT tipdoc.iddoc from TiposDocumento tipdoc inner join documentosexpedientes docexp on tipdoc.iddoc=docexp.iddocumento where ( (tipdoc.idtipodocumento='" + Tag + "' and tipdoc.Activo='True' and tipdoc.idDoc = docusu.iddocumento) ";
                        //SQL += " and exists (SELECT tipdoc.iddoc from TiposDocumento tipdoc inner join documentosexpedientes docexp on tipdoc.iddoc=docexp.iddocumento where (  ";                    
                        string Jerarquia = DevolverTiposInferiores(Tag);
                        if (string.IsNullOrEmpty(Jerarquia))
                            //SQL += "  (tipdoc.idtipodocumento='" + Tag + "' and tipdoc.Activo='True' and tipdoc.idDoc = docusu.iddocumento) )";
                            SQL += " )";//  (tipdoc.idtipodocumento='" + Tag + "' and tipdoc.Activo='True' and tipdoc.idDoc = docusu.iddocumento) )";
                        else
                        {
                            //SQL += " or  (tipdoc.idtipodocumento='" + Tag + "' and tipdoc.Activo='True' and tipdoc.idDoc = docusu.iddocumento)";
                            string[] Hijos = Jerarquia.Split(';');
                            foreach (string Nodo in Hijos)
                            {
                                if (!string.IsNullOrEmpty(Nodo))
                                    SQL += DevolverConsultaJerarquiaExpedientes(Nodo);
                                //if (!string.IsNullOrEmpty(Nodo))
                                //   SQL += "or tipdoc.idtipodocumento='" + Nodo + "'" ;
                            }
                            SQL += ")";
                        }
                        SQL += ")";
                        // SQL += " AND (tipdoc.idDoc = docusu.iddocumento) )";
                    }

                    OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                    vCon.Open();

                    OleDbDataAdapter da = new OleDbDataAdapter();
                    DataSet ds = new DataSet();

                    da.SelectCommand = vComando;
                    da.Fill(ds);

                    string Documentos = "";

                    for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                    {
                        Documentos += "|" +
                            ds.Tables[0].Rows[i].ItemArray[0].ToString() + "^" +
                            ds.Tables[0].Rows[i].ItemArray[1].ToString() + "^";
                        if (ds.Tables[0].Rows[i].ItemArray[2].ToString() == idUsuario)
                            Documentos += "True";
                        else
                            Documentos += "False";
                        Documentos += "^" + ds.Tables[0].Rows[i].ItemArray[3].ToString();
                        //ds.Tables[0].Rows[i].ItemArray[2].ToString() + "^" +
                        //ds.Tables[0].Rows[i].ItemArray[3].ToString() + "^" +
                        //ds.Tables[0].Rows[i].ItemArray[4].ToString();

                    }
                    return Documentos;
                }
                else
                {
                    return "-1";
                }
            }
            catch (Exception e)
            {
                return "-1";
            }

        }


        [WebMethod]
        public string ExpedientesUsuarioNew(string Usuario, string NombreExpediente, string Etiqueta, Boolean Vistos)
        {
            try
            {


                Usuario = DevolveridUsuario(Usuario);

                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);

                //string vEtiqueta = idEtiquetaUsuario(Usuario, Etiqueta);
                //

                StringBuilder SQL = new StringBuilder();
                //if (Vistos==true)
                //    SQL = "SELECT exp.idexpediente,exp.descripcion from expedientes exp left join expedientesusuarios exusu on exp.Idexpediente=exusu.idexpediente where exusu.idusuario='" + Usuario + "' group by exp.idexpediente,exp.descripcion";
                //else
                //SQL = "SELECT exp.idexpediente,exp.descripcion,exp.idusuario,exp.compartido from expedientes exp left join expedientesusuarios exusu on exp.Idexpediente=exusu.idexpediente left join documentosexpedientes dcexp on dcexp.idexpediente=exp.idexpediente left join DocumentosUsuarios docusu on docusu.iddocumento=dcexp.iddocumento left join tiposdocumento dtip on dtip.iddoc=docusu.iddocumento where exusu.idusuario='" + Usuario + "'"; // and docusu.Visualizado='False' ";            
                //).Append(
                SQL.Append("SELECT exp.idexpediente, exp.descripcion, exp.idusuario, exp.Compartido,exusu.favorito  FROM Expedientes AS exp LEFT OUTER JOIN ExpedientesUsuarios AS exusu ON exp.idexpediente = exusu.idexpediente LEFT OUTER JOIN TiposExpediente AS etip ON etip.idExp = exusu.idexpediente WHERE exp.activo='True' and exusu.idusuario =").Append(Usuario);

                if (!string.IsNullOrEmpty(Etiqueta))
                    SQL.Append(" and etip.Activo='True' and etip.idtipoexpediente='").Append(Etiqueta).Append("'");
                //SQL += " and doctip.idtipodocumento='" + Etiqueta + "'";

                if (!string.IsNullOrEmpty(NombreExpediente))
                {
                    SQL.Append(" and exp.descripcion like '%").Append(NombreExpediente).Append("%'");
                }
                SQL.Append(" group by exp.idexpediente,exp.descripcion,exp.idusuario,exp.compartido,exusu.favorito");
                SQL.Append("  order by exp.descripcion desc");
                OleDbCommand vComando = new OleDbCommand(SQL.ToString(), vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                StringBuilder Expedientes = new StringBuilder();

                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    Expedientes.Append("|").Append(ds.Tables[0].Rows[i].ItemArray[0].ToString());
                    Expedientes.Append("^").Append(ds.Tables[0].Rows[i].ItemArray[1].ToString());
                    Expedientes.Append("^").Append(ds.Tables[0].Rows[i].ItemArray[2].ToString());
                    Expedientes.Append("^").Append(ds.Tables[0].Rows[i].ItemArray[3].ToString());
                    Expedientes.Append("^").Append(ds.Tables[0].Rows[i].ItemArray[4].ToString());
                }
                return Expedientes.ToString();
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("ExpedientesUsuarioNew", "WSZFK.asmx", -1, e.Message, e.ToString());
                return "-1";
            }


        }
        //


        [WebMethod]
        public string DocumentosSinExpediente(string Usuario, string IdExpediente)
        {

            Usuario = DevolveridUsuario(Usuario);

            OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);

            string SQL = "Select docs.iddocumento,docs.docOriginal from DocumentosUsuarios docusu left join documentos docs on docs.iddocumento=docusu.iddocumento where (docs.activo='True') and docusu.idusuario='" + Usuario + "' and not exists (select * from documentosexpedientes where iddocumento=docs.iddocumento and idexpediente='" + IdExpediente + "')   GROUP by docs.idDocumento,docs.docOriginal order by docs.docOriginal ";


            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();
            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Documentos = "";

            if (ds.Tables[0].Rows.Count > 0)
                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    Documentos += "|" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[1].ToString();

                }
            return Documentos;

        }


        [WebMethod]
        public string DevolverRutaDocumento(string Iddocumento)
        {
            string Ruta = "";
            //string Path = "/Inetpub/wwwroot/signesProVolumenes/";
            string Path = WebConfigurationManager.AppSettings["RutaVolumenes"].Split(':')[1];
            Ruta = "00000000";
            Ruta = Ruta.Substring(0, 8 - Iddocumento.Length) + Iddocumento;
            //string pathString = Funciones.Ruta + Ruta.Substring(0, 2) + "/" + Ruta.Substring(2, 2) + "/" + Ruta.Substring(4, 2) + "/" + idDoc + Doc.Formato;// +Ruta.Substring(6, 2) + "/";
            string pathString = Path + Ruta.Substring(0, 2) + "/" + Ruta.Substring(2, 2) + "/" + Ruta.Substring(4, 2) + "/" + Ruta.Substring(6, 2) + ".pdf";
            return pathString;

        }

        [WebMethod]
        public string RenombrarDocumento(string IdDocumento)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string Consulta = "select iddocumento,docoriginal from documentos where activo='True' and Iddocumento=" + IdDocumento;


                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    string Dni = "";
                    string Matricula = "";
                    string IdDoc = ds.Tables[0].Rows[0].ItemArray[0].ToString();
                    string NomActual = ds.Tables[0].Rows[0].ItemArray[1].ToString();
                    string IdTipo = DevolverIdTipoDocumento(IdUsuarioMaestro, IdDocumento).Split('|')[0];
                    string DesTipo = DevolverDescripcionTipo(IdTipo, "9", IdUsuarioMaestro);
                    if (IdTipo != "2" && IdTipo != "13" && IdTipo != "21" && IdTipo != "26" && IdTipo != "32" && IdTipo != "45" && IdTipo != "56" && IdTipo != "73")
                    {//LOS QUE NO CAMBIAMOS SON LOS TIPOS PRINCIPALES 2,3 , LOS QUE NO SON SUBTIPOS 2.3 pe
                        if (!string.IsNullOrEmpty(DesTipo))
                        {
                            DesTipo = DesTipo.Split('.')[2];
                            string Expediente = DevolverExpedienteDocumento(IdDocumento);

                            if (Expediente.Length > 10)
                                Dni = Expediente.Split('_')[1];
                            else
                                Dni = Expediente;
                            Matricula = DevolverMatricula(Dni, "Matricula");

                            if (NomActual.ToUpper() != DesTipo.ToUpper() + "-" + Matricula.ToUpper() + ".PDF")
                            {
                                if (string.IsNullOrEmpty(Matricula))
                                    ActualizarNombreDocumento(DesTipo + "-" + Dni + ".pdf", IdUsuarioMaestro, IdDocumento);
                                else
                                    ActualizarNombreDocumento(DesTipo + "-" + Matricula + ".pdf", IdUsuarioMaestro, IdDocumento);
                                ActualizarCampo("Documentos", "CamposActualizados", "True", " iddocumento=" + IdDocumento);
                            }
                        }
                        return "1";
                    }
                    else
                        return "-1";


                }
                else
                    return "-1";

            }
            catch { return "-1"; }
        }
        public int ActualizarNombreDocumento(string NombreBueno, string IdUsuario, string IdDocumento)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string SQL = "update Documentos set DocOriginal='" + NombreBueno + "' where idusuario=" + IdUsuario + " and IdDocumento=" + IdDocumento;
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                da.SelectCommand = vComando;
                int resultado = vComando.ExecuteNonQuery();
                return resultado;
            }

            catch { return -1; }
        }

        public int IndexacionActualizarDatosDocumento(string IdDocumento, Documento Doc, string IdUsuario)   //string NombreBueno, string IdUsuario, string IdDocumento)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string SQL = "update Documentos set DocOriginal='" + Doc.Nombre + "',NombreOriginal ='" + Doc.NombreOriginal + "',Activo='" + Doc.Activo + "', Idcentro=" + Documento.DevolverIdCentro(Doc.CodCentroCarrefour) + ",Esactualizacion='" + Doc.EsActualizacion + "',FechaUltimaModificacion='" + Doc.FechaUltimaModicacion + "',Version='" + Doc.Version + "' where idusuario=" + IdUsuario + " and IdDocumento=" + IdDocumento;
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                da.SelectCommand = vComando;
                int resultado = vComando.ExecuteNonQuery();
                return resultado;
            }

            catch (Exception e) { return -1; }
        }
        public string DevolverMatricula(string Dni, string Campo)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                //Guid Idtoken = Guid.NewGuid();
                string Consulta = "select Matricula,Texto from matriculas where Dni='" + Dni + "'";
                string Resul = "";

                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count == 1)
                    if (Campo.ToUpper() == "MATRICULA")
                        Resul = ds.Tables[0].Rows[0].ItemArray[0].ToString();
                    else
                        if (Campo.ToUpper() == "TEXTO")
                        Resul = ds.Tables[0].Rows[0].ItemArray[1].ToString();
                vCon.Close();
                return Resul;
            }
            catch { return "error"; }
        }
        public string DevolverExpedienteDocumento(string iddocumento)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                //Guid Idtoken = Guid.NewGuid();
                string Consulta = "select exp.descripcion from expedientes exp inner join documentosexpedientes docexp on docexp.idexpediente=exp.idexpediente where docexp.iddocumento=" + iddocumento;


                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count == 1)
                    return ds.Tables[0].Rows[0].ItemArray[0].ToString();


                vCon.Close();
                return "";
            }
            catch { return "error"; }
        }


        //Metodos de Dani, pendientes de un testeo exhaustivo

        [WebMethod]
        public List<string> getUsuersByEmail(string Email)
        {
            string query = "SELECT baneado, idUsuario, Nombre, Apellidos, email, fechaalta FROM Usuarios WHERE email LIKE '%" + Email + "%'";
            DataTable datos = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
            List<string> resultado = new List<string>();
            for (int i = 0; i < datos.Rows.Count; i++)
            {
                string Baneado = "NO BANEADO";
                if (datos.Rows[i].ItemArray[0].ToString().Equals("True"))
                {
                    Baneado = "BANEADO";
                }
                resultado.Add(Baneado + " - " + datos.Rows[i].ItemArray[1].ToString() + " - " + datos.Rows[i].ItemArray[2].ToString() + " " + datos.Rows[i].ItemArray[3].ToString()
                    + " - " + datos.Rows[i].ItemArray[4].ToString() + " - " + datos.Rows[i].ItemArray[5].ToString());
            }
            return resultado;
        }

        [WebMethod]
        public List<string> getDocumentsByEmail(string email)
        {
            string query = "SELECT idDocumento, docOriginal, Documentos.NombreOriginal, Documentos.activo from Documentos inner join Usuarios on Documentos.idUsuario = Usuarios.idUsuario where email = '" + email + "'";
            DataTable datos = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
            List<string> resultado = new List<string>();
            for (int i = 0; i < datos.Rows.Count; i++)
            {
                string Activo = "NO ACTIVO";
                if (datos.Rows[i].ItemArray[3].ToString().Equals("True"))
                {
                    Activo = "ACTIVO";
                }
                resultado.Add(Activo + " - " + datos.Rows[i].ItemArray[0].ToString() + " - " + datos.Rows[i].ItemArray[1].ToString() + " " + datos.Rows[i].ItemArray[2].ToString());
            }
            return resultado;
        }

        [WebMethod]
        public List<string> getUsuersByName(string Nombre)
        {

            string query = "SELECT baneado, idUsuario, Nombre, Apellidos, email, fechaalta FROM Usuarios WHERE Nombre LIKE '%" + Nombre + "%'";
            DataTable datos = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

            List<string> resultado = new List<string>();
            for (int i = 0; i < datos.Rows.Count; i++)
            {
                string Baneado = "NO BANEADO";
                if (datos.Rows[i].ItemArray[0].ToString().Equals("True"))
                {
                    Baneado = "BANEADO";
                }
                resultado.Add(Baneado + " - " + datos.Rows[i].ItemArray[1].ToString() + " - " + datos.Rows[i].ItemArray[2].ToString() + " " + datos.Rows[i].ItemArray[3].ToString()
                    + " - " + datos.Rows[i].ItemArray[4].ToString() + " - " + datos.Rows[i].ItemArray[5].ToString());
            }
            return resultado;
        }

        [WebMethod]
        public List<string> DevolverUsuarioPorDni(string Dni)
        {
            string SQL = " select Nombre,Apellidos,Email from usuarios where dni='" + Dni + "'";
            List<string> resul = new List<string>();
            System.Data.DataTable DT = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
            if (DT.Rows.Count > 0)
                resul.Add(DT.Rows[0]["Nombre"].ToString() + " " + DT.Rows[0]["Apellidos"].ToString() + " " + DT.Rows[0]["Email"].ToString());

            return resul;
        }

        [WebMethod]
        public List<string> DocumentosNombreICAIB(string Usuario, string Pass, string IdUsuarioBusqueda, string Nombre)
        {
            string respuesta = "";
            string respuesta2 = "";
            try
            {
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string idUsuario = LoginCorrectoNew(Usuario, Pass);
                if (idUsuario == "-1")
                {
                    return null;
                }
                if (idUsuario != "-1")
                {
                    string Consulta1 = "";
                    DateTime Hoy = DateTime.Now;

                    Consulta1 = "SELECT docs.docOriginal as Nombre, docs.fechaCreacion  FROM Usuarios AS usu LEFT OUTER JOIN DocumentosUsuarios AS dusu ON usu.idusuario = dusu.idusuario INNER JOIN Documentos AS docs ON docs.iddocumento = dusu.iddocumento left join TiposDocumento doctip on docs.Iddocumento=doctip.iddoc WHERE (docs.activo='True') and (dusu.idusuario = '" + IdUsuarioBusqueda + "') and docs.activo='True' AND (docs.docoriginal <> 'NotificationsManual') ";

                    if (!string.IsNullOrEmpty(Nombre))
                        Consulta1 += " and docs.docOriginal like '%" + Nombre + "%'";


                    Consulta1 += " order by docs.fechacreacion desc";


                    OleDbCommand vComando = new OleDbCommand(Consulta1, vCon);
                    vCon.Open();
                    OleDbDataAdapter da = new OleDbDataAdapter();
                    DataSet ds = new DataSet();

                    da.SelectCommand = vComando;
                    da.Fill(ds);
                    vCon.Close();

                    List<String> lista = new List<String>();
                    string aux = "";
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        aux = "|" + dr[0] + "^" + dr[1];
                        if (!lista.Contains(aux))
                        {
                            lista.Add(aux);
                            respuesta += aux;
                        }
                    }
                    return lista;// respuesta;
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                return null;
            }

        }







        #region Métodos traidos de otro webservice
        [WebMethod]
        public string LoginApp(string Usuario, string Clave)
        {
            try
            {// este no funciona porque no usamos encriptacion AES
                Encrypter Cripto = new Encrypter();

                string vAnterior = Cripto.AESEncrypt(Clave);
                string CadenaConexion = ConnectionString.CadenaConexionZDocsSinProvider;
                //if (ConnectionString.WorkFlow != null)
                //{
                //    CadenaConexion = ConnectionString.WorkFlow;
                //}
                //else
                //{
                //    CadenaConexion = ConnectionString.CadenaConexionZDocs;
                //}
                Clave = Utilities.tratarParam(Clave);
                Usuario = Utilities.tratarParam(Usuario);
                string SQL = "select idusuario,PasswordE from usuarios where email='" + Usuario + "'  and Password='" + Clave + "'";

                DataTable DT = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, CadenaConexion);

                if (DT.Rows.Count > 0)
                {
                    return DT.Rows[0]["IdUsuario"].ToString() + "I" + DT.Rows[0]["PasswordE"].ToString();
                }
                else
                {
                    SQL = "select idusuario,PasswordE from usuarios where email='" + Usuario + "' and PasswordE='" + Utilities.GetSHA1(Clave) + "'";


                    DT = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, CadenaConexion);

                    if (DT.Rows.Count > 0)
                    {
                        return DT.Rows[0]["IdUSuario"].ToString() + "I" + DT.Rows[0]["PasswordE"].ToString();

                    }

                    return "-1";
                }

            }
            catch (Exception e)
            {
                Trazas t = new Trazas("lOGINcORRECTOApp", "WSZFK", -1, e.ToString(), e.Message);
                return "-1";

            }
        }
        [WebMethod]
        public string LoginCorrectoNew2(string Usuario, string Clave)
        {
            try
            {// este no funciona porque no usamos encriptacion AES
                Encrypter Cripto = new Encrypter();

                string vAnterior = Cripto.AESEncrypt(Clave);
                string CadenaConexion = ConnectionString.CadenaConexionZDocsSinProvider;
                if (ConnectionString.WorkFlow != null)
                {
                    CadenaConexion = ConnectionString.CadenaConexionZDocsSinProvider;
                }
                Clave = Utilities.tratarParam(Clave);
                Usuario = Utilities.tratarParam(Usuario);
                string SQL = "select idusuario from usuarios where email='"+ Usuario  +"'  and Password='"+Clave + "'";

                DataTable DT = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, CadenaConexion);

                if (DT.Rows.Count > 0)
                {
                    return DT.Rows[0].ItemArray[0].ToString();
                }
                else
                {
                    SQL = "select idusuario from usuarios where email='" + Usuario + "' and PasswordE='" + Utilities.GetSHA1(Clave) + "'";


                    DT = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, CadenaConexion);

                    if (DT.Rows.Count > 0)
                    {
                        return DT.Rows[0].ItemArray[0].ToString();

                    }

                    return "-1";
                }

            }
            catch (Exception e)
            {
                Trazas t = new Trazas("lOGINcORRECTO", "WSZFK", -1, e.ToString(), e.Message);
                return "-1";

            }
        }
        [WebMethod]
        public string LoginCorrectoNew(string Usuario, string Clave)
        {
            try
            {// este no funciona porque no usamos encriptacion AES
                Encrypter Cripto = new Encrypter();
                OleDbConnection vCon;
                string vAnterior = Cripto.AESEncrypt(Clave);
                if (ConnectionString.WorkFlow != null)
                {
                    vCon = new OleDbConnection(ConnectionString.WorkFlow);
                }
                else
                {
                    vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                }
                vCon.Open();
                string SQL = "select idusuario from usuarios where email=? and Password=?";
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vComando.Parameters.Add("@Usuario", OleDbType.VarChar, 50).Value = Usuario;
                vComando.Parameters.Add("@vAnterior", OleDbType.VarChar, 50).Value = vAnterior;

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].Rows[0].ItemArray[0].ToString();
                }
                else
                {
                    SQL = "select idusuario from usuarios where email=? and PasswordE=?";
                    vComando = new OleDbCommand(SQL, vCon);
                    vComando.Parameters.Add("@Usuario", OleDbType.VarChar, 50).Value = Usuario;
                    vComando.Parameters.Add("@vAnterior", OleDbType.VarChar, 50).Value = Utilities.GetSHA1(Clave);
                    ds = new DataSet();

                    da.SelectCommand = vComando;
                    da.Fill(ds);

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        return ds.Tables[0].Rows[0].ItemArray[0].ToString();

                    }

                    return "-1";
                }

            }
            catch (Exception e)
            {
                Trazas t = new Trazas("lOGINcORRECTO", "WSZFK", -1, e.ToString(), e.Message);
                return "-1";

            }
        }


        /* Jose: Hice este método para que se usara desde la app pero mientras tanto mantengo como privado */

        [WebMethod]
        public Boolean ExisteDocumentoExpediente(string iddocumento, string idexpediente)
        {  //ACTUALIZADO 21/04/2015
            try
            {
                string SQL = "Select * from documentosexpedientes where iddocumento='" + iddocumento + "' and idexpediente='" + idexpediente + "'";
                DataTable DT = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                if (DT.Rows.Count > 0)
                    return true;
                else
                    return false;

            }
            catch (Exception e)
            {
                return false;
            }
        }
        [WebMethod]
        public Boolean TieneExpediente(string IdDocumento)
        {  //ACTUALIZADO 21/04/2015
            try
            {
                string SQL = "Select * from documentosexpedientes where iddocumento='" + IdDocumento + "'";
                DataTable DT = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                if (DT.Rows.Count > 0)
                    return true;
                else
                    return false;

            }
            catch (Exception e)
            {
                return false;
            }
        }
        [WebMethod]
        public Boolean ExisteFormularioUsuario(string IdFormulario, string idusuario)
        { //ACTUALIZADO 21/04/2015
            try
            {
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);

                string SQL = "Select * from FormulariosUsuarios where IdFormulario='" + IdFormulario + "' and idusuario='" + idusuario + "'";
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                    return true;
                else
                    return false;

            }
            catch (Exception e)
            {
                return false;
            }
        }

        [WebMethod]
        public Boolean ExisteDocumentoUsuario(string iddocumento, string idusuario)
        { //ACTUALIZADO 21/04/2015
            try
            {
                string SQL = "Select * from DocumentosUsuarios where iddocumento='" + iddocumento + "' and idusuario='" + idusuario + "'";
                DataTable DT = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                if (DT.Rows.Count > 0)
                    return true;
                else
                    return false;

            }
            catch (Exception e)
            {
                return false;
            }
        }
        [WebMethod]
        public string ExisteExpedienteZDocs(string IdExpedienteAnywhere, string idusuario)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);

                string SQL = "Select * from Expedientes where OldUniqueid='" + IdExpedienteAnywhere + "' and idusuario='" + idusuario + "' ";
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                    return ds.Tables[0].Rows[0].ItemArray[0].ToString();
                else
                    return "";

            }
            catch (Exception e)
            {
                return "";
            }
        }

        [WebMethod]
        public string ExisteDocumentoEnZDocs(string IdOldDocumento)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);

                string SQL = "Select IdDocumento from Documentos where activo='True' and OldUniqueid='" + IdOldDocumento + "'";
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                    return ds.Tables[0].Rows[0].ItemArray[0].ToString();
                else
                    return "";

            }
            catch (Exception e)
            {
                return "";
            }
        }

        [WebMethod]
        public string ExisteExpedienteUsuarioPorNombre(string idusuario, string Descripcion)
        {
            try
            {
               // OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);

                string SQL = "Select expusu.IdExpediente from ExpedientesUsuarios expusu inner join expedientes exp on exp.idexpediente=expusu.idexpediente where exp.activo='True' and expusu.idusuario='" + idusuario + "' and exp.descripcion like '%" + Descripcion + "%'";
                DataTable DT = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                if (DT.Rows.Count > 0)
                    return DT.Rows[0].ItemArray[0].ToString();
                else
                    return "";

            }
            catch (Exception e)
            {
                return "";
            }
        }
        [WebMethod]
        public string ExisteExpedienteUsuario(string idexpediente, string idusuario)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);

                string SQL = "Select idExpediente from ExpedientesUsuarios where idexpediente='" + idexpediente + "' and idusuario='" + idusuario + "'";
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                    return ds.Tables[0].Rows[0].ItemArray[0].ToString();
                else
                    return "";

            }
            catch (Exception e)
            {
                return "";
            }
        }
        [WebMethod]
        public Boolean ExisteExpediente(string Descripcion, string IdUsuario)
        {
            try
            {
                string SQL = "Select idExpediente from Expedientes where descripcion='" + Descripcion + "' and idusuario='" + IdUsuario + "'";
                DataTable DT = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                if (DT.Rows.Count > 0)
                    return true;
                else
                    return false;

            }
            catch (Exception e)
            {
                return false;
            }
        }
        [WebMethod]
        public string CompartirDocumentoaUsuariosExpediente(string IdDocumento, string IdExpediente, string IdUsuarioComparte, string Obligadalectura, string IdEmpresaUsuarioComparte)
        {
            try
            {
                int resultado;
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string Consulta = "Select * from ExpedientesUsuarios where idexpediente='" + IdExpediente + "'";

                DataTable DT = DatabaseConnection.executeNonQueryDT(Consulta, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                if (DT.Rows.Count > 0)
                {
                    for (int i = 0; i <= DT.Rows.Count; i++)
                    {
                        if (!ExisteDocumentoUsuario(IdDocumento, DT.Rows[i]["IdUsuario"].ToString()))
                        {
                            if (SondelamismaEmpresa(IdUsuarioComparte, DT.Rows[i]["IdUsuario"].ToString()))
                                InsertarDocumentoenDocumentosUsuario(IdDocumento, DT.Rows[i]["IdUsuario"].ToString(), "Documentos de Empresa", IdUsuarioComparte, Obligadalectura, "", "", "", "", IdEmpresaUsuarioComparte);
                            else
                                InsertarDocumentoenDocumentosUsuario(IdDocumento, DT.Rows[i]["IdUsuario"].ToString(), "Documentos Compartidos", IdUsuarioComparte, Obligadalectura, "", "", "", "", IdEmpresaUsuarioComparte);
                        }
                    }

                }
                else
                {
                    return "False";
                }
                return "";
            }
            catch { return "error"; }
        }
        //*******************************************************
        [WebMethod]
        public Boolean SondelamismaEmpresa(string idusuario1, string Idusuario2)
        { //ACTUALIZADO 21/04/2015
            try
            {
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string Consulta = "SELECT * FROM usuarioempresa ucia1 where idusuario='" + idusuario1 + "' and exists (select idempresa from usuarioempresa ucia2 where ucia2.idusuario='" + Idusuario2 + "' and ucia1.idempresa=ucia2.idempresa  )";
                DataTable DT = DatabaseConnection.executeNonQueryDT(Consulta, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                if (DT.Rows.Count > 0)
                    return true;
                else
                    return false;
            }
            catch { return false; }
        }
        [WebMethod]
        public string ActualizarCarpetaDocumento(string IdDocumento, string Idusuario, string Carpeta)
        {
            try
            {
                int resultado;
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string Consulta = "update DocumentosUsuarios set carpetadocumento='" + Carpeta + "' where iddocumento='" + IdDocumento + "' and idusuario='" + Idusuario + "'";

                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                da.SelectCommand = vComando;
                resultado = vComando.ExecuteNonQuery();
                // InsertarDocumentoenDocumentosUsuario(IdDocumento, IdUsuario, Carpeta, IdUsuarioComparte);

                vCon.Close();
                return "";
            }
            catch { return "error"; }
        }
        [WebMethod]
        public string ActualizarDocumentoVisuializado(string IdDocumento, string Idusuario)
        {//ACTUALIZADO
            try
            {
                int resultado;
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string Consulta = "update DocumentosUsuarios set Visualizado='True' where iddocumento='" + IdDocumento + "' and idusuario='" + Idusuario + "'";
                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                da.SelectCommand = vComando;
                resultado = vComando.ExecuteNonQuery();
                vCon.Close();
                return "";
            }
            catch { return "error"; }
        }
        [WebMethod]
        public string ActualizarDocumentoDescargado(string IdDocumento, string Idusuario)
        {//ACTUALIZADO
            try
            {
                int resultado;
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string Consulta = "update DocumentosUsuarios set Visualizado='True',Descargado='True' where iddocumento='" + IdDocumento + "' and idusuario='" + Idusuario + "'";
                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                da.SelectCommand = vComando;
                resultado = vComando.ExecuteNonQuery();
                vCon.Close();
                return "";
            }
            catch { return "error"; }
        }
        //*********************************************************************
        public static Boolean ExisteFormularioTipo(string IdFormulario, string IdTipoFormulario)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                //string SQL = "select * from DocumentosTipos where idtipodocumento='" + IdTipoDocumento + "' and iddocumento='" + IdDocumento + "'";
                string SQL = "select * from TiposFormulario where idtipoformulario='" + IdTipoFormulario + "' and idformulario='" + IdFormulario + "'";
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                da.SelectCommand = vComando;
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                    return true;
                else
                    return false;
            }
            catch (OleDbException ex)
            {
                return false;
            }
        }
        public static Boolean ExisteExpedienteTipo(string IdExpediente, string IdTipoExpediente)
        {
            try
            {
                string SQL = "select * from TiposExpediente where idtipoexpediente='" + IdTipoExpediente + "' and idexp='" + IdExpediente + "'";
                DataTable DT = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                if (DT.Rows.Count > 0)
                    return true;
                else
                    return false;
            }
            catch (OleDbException ex)
            {
                return false;
            }
        }
        public static Boolean ExisteDocumentoTipo(string IdDocumento, string IdTipoDocumento)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                //string SQL = "select * from DocumentosTipos where idtipodocumento='" + IdTipoDocumento + "' and iddocumento='" + IdDocumento + "'";
                string SQL = "select * from TiposDocumento where idtipodocumento='" + IdTipoDocumento + "' and iddoc='" + IdDocumento + "'";
                DataTable DT = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                if (DT.Rows.Count > 0)
                    return true;
                else
                    return false;
            }
            catch (OleDbException ex)
            {
                return false;
            }
        }
        public int DesactivarTiposAsignados(string Id, string Tabla, string TipoBueno)
        {
            OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
            string SQL = "";
            if (Tabla.ToUpper() == "DOCUMENTOS")
                SQL = "update tiposdocumento set activo='False' where iddoc='" + Id + "'";
            else
                if (Tabla.ToUpper() == "EXPEDIENTES")
                SQL = "update tiposExpediente set activo='False' where idexp='" + Id + "'";
            else
                    if (Tabla.ToUpper() == "FORMULARIOS")
                SQL = "update tiposFormulario set activo='False' where idformulario='" + Id + "'";

            if (!string.IsNullOrEmpty(TipoBueno))
                if (Tabla.ToUpper() == "DOCUMENTOS")
                    SQL += " and idtipodocumento<>" + TipoBueno;
                else
                    if (Tabla.ToUpper() == "EXPEDIENTES")
                    SQL += " and idtipoexpediente<>" + TipoBueno;
                else
                        if (Tabla.ToUpper() == "FORMULARIOS")
                    SQL += " and idtipoformulario<>" + TipoBueno;

            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            da.SelectCommand = vComando;
            int resultado = vComando.ExecuteNonQuery();
            return resultado;
        }

        [WebMethod]
        public string ActualizarDocumentosTipos(string IdTipo, string Id, string Iddato, string Tabla)
        {//ACTUALIZADO 21/04/2015
            OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
            string SQL = "";
            if (Tabla.ToUpper() == "EXPEDIENTES")
            {
                if (!string.IsNullOrEmpty(Iddato))
                    SQL = "update tiposExpediente set idMetadato='" + Iddato + "' where idtipoexpediente='" + IdTipo + "' and idexp='" + Id + "' and Activo='True'";
                else
                    if (ExisteExpedienteTipo(Id, IdTipo))
                {
                    DesactivarTiposAsignados(Id, "Expedientes", IdTipo);
                    SQL = "update tiposExpediente set activo='True' where idtipoexpediente='" + IdTipo + "' and idexp='" + Id + "'";
                }
                else
                {
                    DesactivarTiposAsignados(Id, "Expedientes", "");
                    SQL = "insert into tiposExpediente (idtipoexpediente,idexp,idMetadato) values ('" + IdTipo + "','" + Id + "',-1); select SCOPE_IDENTITY();";
                }
            }
            else
            {
                if (Tabla.ToUpper() == "DOCUMENTOS")
                {
                    if (!string.IsNullOrEmpty(Iddato))
                        SQL = "update tiposDocumento set idMetadato='" + Iddato + "' where idtipodocumento='" + IdTipo + "' and iddoc='" + Id + "' and Activo='True'";
                    else
                    {
                        if (ExisteDocumentoTipo(Id, IdTipo))
                        {
                            DesactivarTiposAsignados(Id, "Documentos", IdTipo);
                            SQL = "update tiposDocumento set activo='True' where idtipodocumento='" + IdTipo + "' and iddoc='" + Id + "'";
                        }
                        else
                        {
                            DesactivarTiposAsignados(Id, "Documentos", "");
                            SQL = "insert into tiposDocumento (idtipodocumento,iddoc,idMetadato) values ('" + IdTipo + "','" + Id + "',-1); select SCOPE_IDENTITY();";
                        }
                    }
                }
                else
                    if (Tabla.ToUpper() == "FORMULARIOS")
                {
                    if (!string.IsNullOrEmpty(Iddato))
                        SQL = "update tiposFormulario set idMetadato='" + Iddato + "' where idtipoFormulario='" + IdTipo + "' and idFormulario='" + Id + "' and Activo='True'";
                    else
                    {
                        if (ExisteFormularioTipo(Id, IdTipo))
                        {
                            DesactivarTiposAsignados(Id, "Formularios", IdTipo);
                            SQL = "update tiposFormulario set activo='True' where idtipoformulario='" + IdTipo + "' and idformulario='" + Id + "'";
                        }
                        else
                        {
                            DesactivarTiposAsignados(Id, "Formularios", "");
                            SQL = "insert into tiposFormulario (idtipoformulario,idformulario,idMetadato) values ('" + IdTipo + "','" + Id + "',-1); select SCOPE_IDENTITY();";
                        }
                    }
                }
            }

            int resultado = DatabaseConnection.executeScalarInt(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
            return resultado.ToString();
        }
        [WebMethod]
        public string insertarFormularioFlujo(string idForm, string idFlujo)
        {
            //('" + IdTipoDocumento + "','" + IdDocumento + "')
            string SQL = "INSERT INTO signesDemos.dbo.FormulariosFlujo (idFormulario, idFlujo) VALUES ('" + idForm + "','" + idFlujo + "')";
            int respuesta = -1;
            try
            {
                respuesta = DatabaseConnection.executeScalarInt(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                if (respuesta != -1)
                {
                    return "1";
                }
                else
                {
                    return "-1";
                }

            }
            catch
            {
                return "-1";
            }
        }

        [WebMethod]
        public string flujosFormularioUsuario(string idUsuario)
        {

            string SQL = "SELECT Flujo.idFlujo, Flujo.Descripcion from signesDemos.dbo.Flujo WHERE Activo='S' AND idUsuario=" + idUsuario;
            string respuesta = "";
            try
            {
                DataTable dt = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (i == 0)
                        {
                            respuesta += dt.Rows[i]["idFlujo"] + "#" + dt.Rows[i]["Descripcion"];
                        }
                        else
                        {
                            respuesta += "|" + dt.Rows[i]["idFlujo"] + "#" + dt.Rows[i]["Descripcion"];
                        }

                    }
                    return respuesta;
                }
                else
                {
                    return "-1";
                }

            }
            catch
            {
                return "-1";
            }
        }

        [WebMethod]
        public string InsertarLineaCamposNoWeb(string Tabla, string DesCampo, string ValorCampo, string IdTipoDocumento, string IdDocumento, string Tipo)
        {
            try
            {
                string resultado;
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string Consulta = "insert into " + Tabla + " ([" + DesCampo + "]) values ('" + ValorCampo + "'); select SCOPE_IDENTITY()";

                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                da.SelectCommand = vComando;
                resultado = vComando.ExecuteScalar().ToString();
                if (Tipo == "Expedientes")
                    ActualizarDocumentosTipos(IdTipoDocumento, IdDocumento, resultado, "Expedientes");
                else
                    ActualizarDocumentosTipos(IdTipoDocumento, IdDocumento, resultado, "Documentos");

                vCon.Close();
                return resultado;
            }
            catch { return "-1"; }
        }
        //[WebMethod]
        //public string InsertarLineaCamposForulario(string fields, string Tabla,  string IdDocumento, string Tipo)
        //{//
        //    try
        //    {
        //        Dictionary<string, string> _fields = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(fields);

        //        string resultado;
        //        OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
        //        string Consulta = "insert into " + Tabla;
        //        Boolean Primero = true;
        //        string sqlnombres = "IdMetadato", sqlvalores = "";
        //        foreach (KeyValuePair<string, string> field in _fields)
        //        {
        //            if (!Primero)
        //            {
        //                sqlnombres += ",";
        //                sqlvalores += ",";
        //            }                    
        //            sqlnombres += "[" + field.Key + "]";
        //            sqlvalores += "'" + field.Value + "'";

        //            Primero = false;
        //        }
        //        Consulta += " (" + sqlnombres + ") values (" + sqlvalores + "); select SCOPE_IDENTITY()";
        //        OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
        //        vCon.Open();

        //        OleDbDataAdapter da = new OleDbDataAdapter();
        //        da.SelectCommand = vComando;
        //        resultado = vComando.ExecuteScalar().ToString();
        //        if (Tipo == "Expedientes")
        //            ActualizarDocumentosTipos(IdTipoDocumento, IdDocumento, resultado, "Expedientes");
        //        else
        //            if (Tipo == "Documentos")
        //                ActualizarDocumentosTipos(IdTipoDocumento, IdDocumento, resultado, "Documentos");
        //            else
        //                if (Tipo.ToUpper() == "FORMULARIOS")
        //                    ActualizarDocumentosTipos(IdTipoDocumento, IdDocumento, resultado, "Formularios");
        //        //string SQL2="update Documentostipos set iddato='" + NewId + "' where idtipodocumento='" + IdTipoDocumento + "' and iddocumento='" + IdDocumento + "'";
        //        //vComando = new OleDbCommand(SQL2, vCon);                

        //        //resultado = vComando.ExecuteNonQuery();

        //        vCon.Close();
        //        return "1";
        //    }
        //    catch { return "-1"; }
        //}

        [WebMethod]
        public string InsertarLineaCampos(string fields, string Tabla, string IdTipoDocumento, string IdDocumento, string Tipo)
        {//
            try
            {
                Dictionary<string, string> _fields = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(fields);

                string resultado;
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string Consulta = "insert into " + Tabla;
                Boolean Primero = true;
                string sqlnombres = "", sqlvalores = "";
                foreach (KeyValuePair<string, string> field in _fields)
                {
                    if (!Primero)
                    {
                        sqlnombres += ",";
                        sqlvalores += ",";
                    }
                    //else
                    //{
                    //    sqlnombres += "IdMetadato,";
                    //    sqlvalores += "'" + NewId + "',";
                    //}
                    sqlnombres += "[" + field.Key + "]";
                    sqlvalores += "'" + field.Value + "'";
                    //Consulta += "[" + field.Key + "]=" + "'" + field.Value + "'";
                    Primero = false;
                }
                Consulta += " (" + sqlnombres + ") values (" + sqlvalores + "); select SCOPE_IDENTITY()";
                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                da.SelectCommand = vComando;
                resultado = vComando.ExecuteScalar().ToString();
                if (Tipo.ToUpper() == "EXPEDIENTES")
                    ActualizarDocumentosTipos(IdTipoDocumento, IdDocumento, resultado, "Expedientes");
                else
                    if (Tipo.ToUpper() == "DOCUMENTOS")
                    ActualizarDocumentosTipos(IdTipoDocumento, IdDocumento, resultado, "Documentos");
                else
                        if (Tipo.ToUpper() == "FORMULARIOS")
                    ActualizarDocumentosTipos(IdTipoDocumento, IdDocumento, resultado, "Formularios");
                //string SQL2="update Documentostipos set iddato='" + NewId + "' where idtipodocumento='" + IdTipoDocumento + "' and iddocumento='" + IdDocumento + "'";
                //vComando = new OleDbCommand(SQL2, vCon);                

                //resultado = vComando.ExecuteNonQuery();

                vCon.Close();
                return "1";
            }
            catch { return "-1"; }
        }
        [WebMethod]
        public string DevolverIdTipoDocumento(string IdUsuario, string IdDocumento)
        {  //DEVUELVE EL IDTIPO DE UN DOCUMENTO Y el IdDelMetadato activo
            try
            {
                if (!string.IsNullOrEmpty(IdDocumento))
                {
                    OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);

                    string SQL = "select idtipodocumento,idMetadato from Tiposdocumento where idDoc=" + IdDocumento + " and Activo='True'";

                    OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                    vCon.Open();
                    OleDbDataAdapter da = new OleDbDataAdapter();
                    DataSet ds = new DataSet();

                    da.SelectCommand = vComando;
                    da.Fill(ds);

                    if (ds.Tables[0].Rows.Count > 0)
                        return ds.Tables[0].Rows[0].ItemArray[0].ToString() + "|" + ds.Tables[0].Rows[0].ItemArray[1].ToString();
                    else
                        return "";

                }
                else
                    return "";
            }
            catch (OleDbException ex)
            {
                return "";
            }
        }



        [WebMethod]
        public string DevolverCamposComillasDobles(string IdTipoDocumento, string DescripcionTipo, string IdDocumento, string IdMetaDato)
        { //ACTUALIZADO 22/04/2015
            try
            {
                string NomTabla = "c00000000";
                NomTabla = NomTabla.Substring(0, 9 - IdTipoDocumento.Length) + IdTipoDocumento;
                string Tabla = "[" + NomTabla + "]";
                string SQL = "";
                if (!string.IsNullOrEmpty(IdMetaDato) && IdMetaDato != "-1")
                    SQL += "Select * from " + Tabla + " where idMetadato='" + IdMetaDato + "'";
                else
                    SQL = "exec sp_columns [" + NomTabla + "]";//"Select top(1) * from " + Tabla;
                Boolean Primero = true;
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();
                OleDbDataAdapter da = new OleDbDataAdapter();

                DataSet ds = new DataSet();
                da.SelectCommand = vComando;
                da.Fill(ds);
                string Resp = "\"Campos\" : [";
                if (!string.IsNullOrEmpty(IdMetaDato) && IdMetaDato != "-1")
                {
                    foreach (DataRow dtRow in ds.Tables[0].Rows)
                    {
                        int colum = ds.Tables[0].Columns.Count;
                        int i = 1;
                        foreach (DataColumn dc in ds.Tables[0].Columns)
                        {
                            if (dc.ToString().ToUpper() != "IDMETADATO")
                            {
                                var field1 = dtRow[dc].ToString();
                                if (!Primero)
                                    Resp += ",";
                                Resp += "{\"" + dc.ToString() + "\" : \"" + field1 + "\"}";
                                Primero = false;
                            }
                            i++;
                        }
                    }
                }
                Resp += "]";

                return Resp;
            }
            catch
            {
                return "";
            }
        }

        [WebMethod]
        public string DevolverCamposFormulario(string idFormulario, string idDocumento)
        {
            string res = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(idFormulario))
                {
                    string query = "select * from camposFormulario where idFormulario = '" + idFormulario + "'";
                    DataTable dtNombreCampos = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                    string[] campos = null;
                    if (dtNombreCampos.Rows.Count > 0)
                    {
                        DataTable dtValorCampos = null;
                        if (!String.IsNullOrEmpty(idDocumento))
                        {
                            string tabla = "df000000";
                            tabla = tabla.Substring(0, 9 - idFormulario.Length) + idFormulario;
                            query = "select top 1 * from " + tabla + " where idDocumento = " + idDocumento;
                            dtValorCampos = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                        }
                        campos = new string[dtNombreCampos.Rows.Count - 2]; // -1 por que el primero siempre será la columna idMetadato que no interesa
                        for (int i = 0; i < dtNombreCampos.Rows.Count; i++)
                        {
                            string idCampos = dtNombreCampos.Rows[i]["idCampo"].ToString();
                            string nombreCol = dtNombreCampos.Rows[i]["Nombre"].ToString();
                            string tipoCol = dtNombreCampos.Rows[i]["Tipo"].ToString();
                            string tam = dtNombreCampos.Rows[i]["Tamaño"].ToString();

                            res += idCampos + "^" + nombreCol + "^" + tipoCol + "^" + tam;

                            if (!String.IsNullOrEmpty(idDocumento))
                            {
                                string valorCol = dtValorCampos.Rows[0][nombreCol].ToString();
                                valorCol = valorCol.Trim();
                                res += "^" + valorCol;
                            }
                            res += "|";
                        }
                    }
                    else
                        res = "-1";
                }
                else
                    res = "-1";
                if (res.EndsWith("|")) res = res.Substring(0, res.Length - 1);
                return res;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("DevolverCamposFormulario", "WSZFK", -1, e.ToString(), e.Message);
                return "-1";
            }
        }

        [WebMethod]
        public string DevolverElementosListaFormulario(string idCamposFormulario)
        {
            string res = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(idCamposFormulario))
                {
                    string query = "select * from ElementosListaFormulario where idCamposFormulario = '" + idCamposFormulario + "' order by orden asc";
                    DataTable dtListaCampos = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                    if (dtListaCampos.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtListaCampos.Rows.Count; i++)
                        {
                            string nombreCol = dtListaCampos.Rows[i]["Descripcion"].ToString();
                            string orden = dtListaCampos.Rows[i]["Orden"].ToString();

                            res += nombreCol + "^" + orden;
                            res += "|";
                        }
                    }
                    else
                        res = "-1";
                }
                else
                    res = "-1";

                if (res.EndsWith("|")) res = res.Substring(0, res.Length - 1);
                return res;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("DevolverElementosListaFormulario", "WSZFK", -1, e.ToString(), e.Message);
                return "-1";
            }
        }

        //[WebMethod]
        //public string DevolverCamposFormulario(string idFormulario, string idDocumento)
        //{ 

        //    try
        //    {           
        //        string res = string.Empty;
        //        string tabla = "df000000";
        //        tabla = tabla.Substring(0, 9 - idFormulario.Length) + idFormulario;
        //        string query = "exec sp_columns[" + tabla + "]";
        //        DataTable dtNombreCampos = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
        //        string[] campos = null;
        //        if (dtNombreCampos.Rows.Count > 0)
        //        {
        //            DataTable dtValorCampos = null;
        //            if(!String.IsNullOrEmpty(idDocumento))
        //            {
        //                query = "select top 1 * from " + tabla + " where idDocumento = " + idDocumento;
        //                dtValorCampos = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
        //            }
        //            campos = new string[dtNombreCampos.Rows.Count - 2]; // -1 por que el primero siempre será la columna idMetadato que no interesa
        //            for (int i = 2; i < dtNombreCampos.Rows.Count; i++)
        //            {
        //                string nombreCol = dtNombreCampos.Rows[i]["COLUMN_NAME"].ToString();
        //                string tipoCol = dtNombreCampos.Rows[i]["TYPE_NAME"].ToString();

        //                res += nombreCol + "^" + tipoCol;
        //                if(!String.IsNullOrEmpty(idDocumento))
        //                {
        //                    string valorCol = dtValorCampos.Rows[0][nombreCol].ToString();
        //                    valorCol = valorCol.Trim();
        //                    res += "^" + valorCol;
        //                }
        //                res += "|";
        //            }
        //        }
        //        else
        //            res = "-1";
        //        if (res.EndsWith("|"))    res= res.Substring(0, res.Length - 1);
        //        return res;
        //    }
        //    catch (Exception e)
        //    {
        //        return "-1";
        //    }
        //}

        [WebMethod]
        public string DevolverCampos(string IdTipoDocumento, string DescripcionTipo, string IdDocumento, string IdMetaDato)
        { //ACTUALIZADO 22/04/2015
            try
            {
                string NomTabla = "c00000000";
                NomTabla = NomTabla.Substring(0, 9 - IdTipoDocumento.Length) + IdTipoDocumento;
                string Tabla = "[" + NomTabla + "]";
                string SQL = "";
                if (!string.IsNullOrEmpty(IdMetaDato) && IdMetaDato != "-1")
                    SQL += "Select * from " + Tabla + " where idMetadato='" + IdMetaDato + "'";
                else
                    SQL = "exec sp_columns [" + NomTabla + "]";//"Select top(1) * from " + Tabla;
                Boolean Primero = true;
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();
                OleDbDataAdapter da = new OleDbDataAdapter();

                DataSet ds = new DataSet();
                da.SelectCommand = vComando;
                da.Fill(ds);
                string Resp = "'Campos' : [";
                if (!string.IsNullOrEmpty(IdMetaDato) && IdMetaDato != "-1")
                {
                    foreach (DataRow dtRow in ds.Tables[0].Rows)
                    {
                        int colum = ds.Tables[0].Columns.Count;
                        int i = 1;
                        foreach (DataColumn dc in ds.Tables[0].Columns)
                        {
                            if (dc.ToString().ToUpper() != "IDMETADATO")
                            {
                                var field1 = dtRow[dc].ToString();
                                if (!Primero)
                                    Resp += ",";
                                Resp += "{'" + dc.ToString() + "' : '" + field1 + "'}";
                                Primero = false;
                            }
                            i++;
                        }
                    }
                }
                Resp += "]";
                //else
                //{
                //    int i = 0;
                //    for (int i2 = 0; i2 < ds.Tables[0].Rows.Count; i2++)
                //    {                     
                //        if (ds.Tables[0].Rows[i2].ItemArray[3].ToString().ToUpper() != "IDMETADATO")
                //        {                                                        
                //            Resp += ds.Tables[0].Rows[i2].ItemArray[3].ToString() + ":";
                //            Resp += "   <input type='textbox' nombre='txtFieldValue" + ds.Tables[0].Rows[i2].ItemArray[3].ToString() + "' id='txtFieldValue" + ds.Tables[0].Rows[i2].ItemArray[3].ToString().Replace(" ", "") + "' class='Luis' value='";                                                                            
                //        }
                //        i++;

                //    }
                //}                
                return Resp;
            }
            catch
            {
                return "";
            }
        }

        [WebMethod]
        public string DevolverDescripcionTipo(string IdTipo, string IdEmpresa, string IdUsuario)
        {  //ACTUALIZADO
            try
            {
                if (!string.IsNullOrEmpty(IdTipo))
                {
                    OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);

                    string SQL = "select Descripcion from Tipodocumento where IdTipoDocumento=" + IdTipo + " and Activo='True' ";

                    if (!string.IsNullOrEmpty(IdUsuario))
                        SQL += " and IdUsuario=" + IdUsuario;

                    if (!string.IsNullOrEmpty(IdEmpresa))
                        SQL += " and idEmpresa=" + IdEmpresa;
                    OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                    vCon.Open();
                    OleDbDataAdapter da = new OleDbDataAdapter();
                    DataSet ds = new DataSet();

                    da.SelectCommand = vComando;
                    da.Fill(ds);

                    if (ds.Tables[0].Rows.Count > 0)
                        return ds.Tables[0].Rows[0].ItemArray[0].ToString();
                    else
                        return "";

                }
                else
                    return "";
            }
            catch (OleDbException ex)
            {
                return "";
            }
        }
        [WebMethod]
        public string ActualizarMetadatodeDocumento(string IdDocumento, string NombreCampo, string ValorCampo, string IdUsuario)
        { //CUIDADO ACTUALIZA EL METADATO DEL TIPO ACTUAL,ASÍ QUE HAY QUE ASIGNARLE EL TIPO ANTES DE ACTUALIZAR EL METADATO
            try
            {
                string Tipo = DevolverIdTipoDocumento(IdUsuario, IdDocumento);
                string IdTipo = Tipo.Split('|')[0];
                string MetadatoActual = Tipo.Split('|')[1];
                string Tabla = "c";
                for (int i = 0; i < 8 - IdTipo.Length; i++)
                    Tabla += "0";

                Tabla += IdTipo;

                //  OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string SQL = "select IdMetadato from tiposdocumento where Iddoc='" + IdDocumento + "' and idtipodocumento='" + Convert.ToInt16(Tabla.Substring(1)) + "'";
                DataTable datos = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                string Consulta = "";
                if (datos.Rows.Count > 0)
                {
                    SQL = "update " + Tabla + "  set " + NombreCampo + "='" + ValorCampo + "'  where IdMetadato='" + datos.Rows[0]["IdMetaDato"] + "'";
                    int resp = DatabaseConnection.executeNonQueryInt(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                    return resp.ToString();
                }
                else
                    return "";

            }
            catch (OleDbException ex)
            {
                return "";
            }
        }

        [WebMethod]
        public string DevolverMetadatodeDocumento(string IdDocumento, string NombreCampo, string IdUsuario)
        {
            try
            {
                string Tipo = DevolverIdTipoDocumento(IdUsuario, IdDocumento);
                string IdTipo = Tipo.Split('|')[0];
                string MetadatoActual = Tipo.Split('|')[1];
                string Tabla = "c";
                for (int i = 0; i < 8 - IdTipo.Length; i++)
                    Tabla += "0";

                Tabla += IdTipo;

                //  OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string SQL = "select IdMetadato from tiposdocumento where Iddoc='" + IdDocumento + "' and idtipodocumento='" + Convert.ToInt16(Tabla.Substring(1)) + "'";
                DataTable datos = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                string Consulta = "";
                if (datos.Rows.Count > 0)
                {
                    Consulta = "select " + NombreCampo + " from " + Tabla + " where IdMetadato='" + datos.Rows[0]["IdMetadato"].ToString() + "'";
                    DataTable datos2 = DatabaseConnection.executeNonQueryDT(Consulta, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                    if (datos2.Rows.Count > 0)
                        return datos2.Rows[0]["" + NombreCampo + ""].ToString();
                    else
                        return "";
                }
                else
                    return "";

            }
            catch (OleDbException ex)
            {
                return "";
            }
        }
        [WebMethod]
        public string DevolverIdTipoSimilar(string Tipo, string IdUsuario, string Tabla, string IdEmpresa)
        {  //ACTUALIZADO
            try
            {
                if (!string.IsNullOrEmpty(Tipo))
                {
                    OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                    string SQL = "";
                    if (Tabla == "Expedientes")
                        SQL = "select idtipoexpediente from Tipoexpediente where descripcion like '%" + Tipo + "%' and (Idusuario='" + IdUsuario + "' or idempresa=" + IdEmpresa + ")";
                    else
                        SQL = "select idtipodocumento from Tipodocumento where descripcion like '%" + Tipo + "%' and (Idusuario='" + IdUsuario + "' or idempresa=" + IdEmpresa + ")";

                    OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                    vCon.Open();
                    OleDbDataAdapter da = new OleDbDataAdapter();
                    DataSet ds = new DataSet();

                    da.SelectCommand = vComando;
                    da.Fill(ds);

                    //DataTable datos = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocs);
                    if (ds.Tables[0].Rows.Count > 0)
                        return ds.Tables[0].Rows[0].ItemArray[0].ToString();
                    else
                        return "";

                    //if (datos.Rows.Count > 0)
                    //    if (Tabla == "Expedientes")
                    //        return datos.Rows[0]["idtipoExpediente"].ToString();
                    //    else
                    //        return datos.Rows[0]["idtipoDocumento"].ToString();
                    //else
                    //    return "";

                }
                else
                    return "";
            }
            catch (OleDbException ex)
            {
                return "";
            }
        }

        [WebMethod]
        public string DevolverIdTipo(string Tipo, string IdUsuario, string Tabla, string IdEmpresa)
        {  //ACTUALIZADO
            try
            {
                if (!string.IsNullOrEmpty(Tipo))
                {

                    OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                    string SQL = "";
                    if (Tabla == "Formulario")
                        SQL = "select idTipoFormulario from TipoFormulario where descripcion like '%" + Tipo + "%' and (Idusuario='" + IdUsuario + "' or idempresa=" + IdEmpresa + ")";
                    else
                    {
                        if (Tabla == "Expedientes")
                            SQL = "select idtipoexpediente from Tipoexpediente where descripcion like '%" + Tipo + "%' and (Idusuario='" + IdUsuario + "' or idempresa=" + IdEmpresa + ")";
                        else
                        {
                            if ((Tipo.ToUpper().Contains("VARIOS")) && (IdEmpresa == "9"))
                                SQL = "select idtipodocumento from Tipodocumento where descripcion = '10.VARIOS'' and (Idusuario='" + IdUsuario + "' or idempresa=" + IdEmpresa + ")";
                            else
                                SQL = "select idtipodocumento from Tipodocumento where descripcion like '%" + Tipo + "%' and (Idusuario='" + IdUsuario + "' or idempresa=" + IdEmpresa + ")";
                        }
                    }
                    OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                    vCon.Open();
                    OleDbDataAdapter da = new OleDbDataAdapter();
                    DataSet ds = new DataSet();

                    da.SelectCommand = vComando;
                    da.Fill(ds);

                    //DataTable datos = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocs);
                    if (ds.Tables[0].Rows.Count > 0)
                        return ds.Tables[0].Rows[0].ItemArray[0].ToString();
                    else
                        return "";

                    //if (datos.Rows.Count > 0)
                    //    if (Tabla == "Expedientes")
                    //        return datos.Rows[0]["idtipoExpediente"].ToString();
                    //    else
                    //        return datos.Rows[0]["idtipoDocumento"].ToString();
                    //else
                    //    return "";

                }
                else
                    return "";
            }
            catch (OleDbException ex)
            {
                return "";
            }
        }
        [WebMethod]
        public string ActualizarCampos(string IdDato, string[] Campos, string docid, string fields, string Tabla)
        {
            try
            {
                Dictionary<string, string> _fields = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(fields);

                int resultado;
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string Consulta = "update " + Tabla + " set ";
                Boolean Primero = true;
                foreach (KeyValuePair<string, string> field in _fields)
                {
                    if (!Primero)
                        Consulta += ",";
                    if (string.IsNullOrEmpty(field.Value)) //SI EL CAMPO ES NULO LO MANDO COMO TAL, NO '', YA QUE SINO EN LOS TIPO FECHA PONE EL 01/01/1900
                        Consulta += "[" + field.Key + "]=null" ;
                    else
                       Consulta += "[" + field.Key + "]=" + "'" + field.Value + "'";
                    Primero = false;
                }
                Consulta += " where idMetadato='" + IdDato + "'";
                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                da.SelectCommand = vComando;
                resultado = vComando.ExecuteNonQuery();

                vCon.Close();
                return "";
            }
            catch { return "error"; }
        }
        [WebMethod]
        public string CrearTablaCamposTipo(string IdTipoDocumento, string DescripcionTipo, string[] Campos, string Tabla)
        { //ACTUALIZADO 22/05/2015
            try
            {
                //string[] Campos1 = (JsonConvert.DeserializeObject(Campos, typeof(string[])) as string[]);
                int resultado;
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string NomTabla = "";
                if (Tabla.ToUpper() == "EXPEDIENTES")
                    NomTabla = "e00000000";
                else
                    if (Tabla.ToUpper() == "DOCUMENTOS")
                    NomTabla = "c00000000";
                else
                        if (Tabla.ToUpper() == "FORMULARIOS")
                    NomTabla = "df0000000";

                NomTabla = NomTabla.Substring(0, 9 - IdTipoDocumento.Length) + IdTipoDocumento;
                //string Consulta = "create Table [" + NomTabla + "] (IdMetaDato uniqueidentifier PRIMARY KEY,";
                string Consulta = "";
                if (Tabla.ToUpper() != "FORMULARIOS")
                    Consulta = "create Table [" + NomTabla + "] (IdMetaDato int IDENTITY (1,1) PRIMARY KEY,";
                else
                    Consulta = "create Table [" + NomTabla + "] (idCamposFormulario int IDENTITY (1,1) PRIMARY KEY,";

                string query = string.Empty;
                for (int i = 0; i < Campos.Length; i++)
                {
                    if (!string.IsNullOrEmpty(Campos[i].Split('|')[0]))
                    {
                        // 0: nombre del campo
                        // 1: tipo del campo
                        // 2: tamaño del campo
                        // 4: Lista de elementos si es un campo de tipo lista
                        Consulta += "[" + Campos[i].Split('|')[0] + "] ";
                        string Tipo = Campos[i].Split('|')[1];

                        string tam = string.Empty;

                        string[] lista = null;
                        if (Campos[i].Split('|').Length == 3)
                            tam = Campos[i].Split('|')[2];
                        else if (Campos[i].Split('|').Length == 4)
                            lista = Campos[i].Split('|')[3].Split('^');

                        switch (Tipo)
                        {
                            case "Email":
                                Consulta += " varchar(100)";
                                break;
                            case "DNI":
                                Consulta += " varchar(12)";
                                break;

                            case "Caracter":
                            case "varchar":
                            case "text":
                                if (!string.IsNullOrEmpty(tam))
                                    Consulta += "varchar(" + tam + ")";
                                else
                                {
                                    Consulta += " varchar(250)"; // Por defecto serán campos de 250 caracteres
                                    tam = "250";
                                }

                                Tipo = "Alfanumerico";
                                break;
                            case "Fecha":
                            case "date":
                            case "datetime":
                                Consulta += " datetime ";
                                Tipo = "Fecha";
                                break;
                            case "Numerico":
                                Consulta += "numeric(18,0)";
                                Tipo = "Numérico";
                                break;
                            case "Lista":
                                if (Tabla.ToUpper().Equals("FORMULARIOS"))
                                {
                                    Consulta += "numeric(18,0)";
                                    Tipo = "Lista";
                                }
                                else if (Tabla.ToUpper().Equals("DOCUMENTOS"))
                                {
                                    Consulta += " varchar(255)";
                                    Tipo = "Lista";
                                }
                                break;
                        }
                        if (i < Campos.Length - 1)  //SI HAY MAS CAMPOS
                            if (HayMasCampos(Campos, i))  // (!string.IsNullOrEmpty(Campos[i+1].Split('|')[0])) //SI EL SIGUIENTE CAMPO NO ESTA VACIO
                                Consulta += ",";

                        if (Tabla.ToUpper().Equals("FORMULARIOS"))
                        {
                            query = "insert into camposFormulario (idFormulario, Nombre, Tipo, Tamaño)  values (" + IdTipoDocumento + ",'" + Campos[i].Split('|')[0] + "','" + Tipo + "'," + tam + "); select SCOPE_IDENTITY();";

                            int res = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                            if (res == -1)
                            {
                                Trazas t = new Trazas("CrearTablaCamposTipo", "WSZFK", -1, query, "");
                            }
                            else if (Tipo.Equals("Lista"))
                            {
                                query = "insert into ElementosListaFormulario (idCamposFormulario, Descripcion, Orden)  values ";
                                for (int j = 0; j < lista.Length; j++)
                                {
                                    query += "(" + res + ",'" + lista[j] + "'," + j + ")";

                                    if (j < lista.Length - 1) query += ",";
                                }

                                query += ";";


                                res = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                                if (res == -1)
                                {
                                    Trazas t = new Trazas("CrearTablaCamposTipo", "WSZFK", -1, query, "");
                                }
                            }
                        }
                        else if (Tabla.ToUpper().Equals("DOCUMENTOS"))
                        {
                            //Ahora insertaremos los metadatos en la tabla Metadatos
                            string queryMeta = string.Empty;    //tabla que coincide com c0000..,tipo si lista alfanumerico....
                            queryMeta = "insert into Metadatos (idTipoDocumento, Nombre, Tipo,Tamaño)  values ('" + IdTipoDocumento + "','" + Campos[i].Split('|')[0] + "','" + Tipo + "',255); select SCOPE_IDENTITY();";
                            var idMeta = DatabaseConnection.executeScalar(queryMeta, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                            if (idMeta.Equals(-1))
                            {
                                Trazas t = new Trazas("insert into Metadatos METODO CrearTablaCamposTipo", "WSZFK", -1, query, "");
                            }
                            else if (Tipo.Equals("Lista"))
                            {
                                queryMeta = "insert into ElementosListaMetadatos (idMeta,Descripcion,Orden,Title) Values";
                                //insertamos en elementoslistametadatos 
                                for (int x = 0; x < lista.Length; x++)
                                {
                                    queryMeta += "('" + idMeta + "','" + lista[x] + "','" + x + "','')";
                                    if (x < lista.Length - 1) queryMeta += ",";
                                }
                                queryMeta += ";";
                                var idElementosListaMetadatos = DatabaseConnection.executeNonQueryInt(queryMeta, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                                if (idElementosListaMetadatos == -1)
                                {
                                    Trazas t = new Trazas("insert into Metadatos METODO CrearTablaCamposTipo", "WSZFK", -1, query, "");

                                }


                            }
                        }

                    }
                }

                Consulta += ")";

                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                da.SelectCommand = vComando;
                resultado = vComando.ExecuteNonQuery();
                // InsertarDocumentoenDocumentosUsuario(IdDocumento, IdUsuario, Carpeta, IdUsuarioComparte);

                vCon.Close();
                return "";
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("CreatTablaCamposTipos", "WSZFK", -1, e.Message, e.ToString());
                return "error" + e.ToString();
            }
        }
        public Boolean HayMasCampos(string[] Cadena, int Indice)
        {
            Boolean Encontrado = false;
            int i = Indice + 1;
            do
            {
                if (!string.IsNullOrEmpty(Cadena[i].Split('|')[0]))
                    Encontrado = true;
                i++;
            }
            while (!Encontrado && i < Cadena.Length);

            return Encontrado;
        }
        [WebMethod]
        public string CrearMetadato(string IdMetadato, string IdTipoDocumento, string Nombre, string Tipo, int Tamaño)
        {
            try
            {
                int resultado;
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string Consulta = "insert into Metadatos (idmetadato,idtipodocumento,Nombre,Tipo,Tamaño) values ('" + IdMetadato + "','" + IdTipoDocumento + "','" + Nombre + "','" + Tipo + "'," + Tamaño + ")";

                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                da.SelectCommand = vComando;
                resultado = vComando.ExecuteNonQuery();

                vCon.Close();
                return "";
            }
            catch { return "error"; }
        }
        [WebMethod]
        public void EscribirEnPdf(string IdDocumento, string Texto)
        {
            string Fichero = string.Empty;
            //  string Directorio = "C:/inetpub/wwwroot/signesProVolumenes/";
            string Ruta = "00000000";
            Ruta = Ruta.Substring(0, 8 - IdDocumento.Length) + IdDocumento;
            //string pathString = Directorio + Ruta.Substring(0, 2) + "/" + Ruta.Substring(2, 2) + "/" + Ruta.Substring(4, 2) + "/" + Ruta.Substring(6, 2) + "/";
            Fichero = Directorio + Ruta.Substring(0, 2) + "/" + Ruta.Substring(2, 2) + "/" + Ruta.Substring(4, 2) + "/" + Ruta.Substring(6, 2) + ".pdf";
            /*

                    Aquí tendrías que poner tu aportación para sacar la ruta del fichero que irá en la variable fichero.

            */



            iTextSharp.text.Document document = new iTextSharp.text.Document();

            PdfReader reader = new PdfReader(Fichero);

            MemoryStream _ms = new MemoryStream();

            using (MemoryStream memorystream = new MemoryStream())
            {

                PdfStamper pdfStamper = new PdfStamper(reader, memorystream);
                for (int i = 1; i <= reader.NumberOfPages; i++)
                {
                    iTextSharp.text.Rectangle size = reader.GetPageSizeWithRotation(i);

                    PdfContentByte cb = pdfStamper.GetOverContent(i);

                    BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cb.SetColorFill(BaseColor.DARK_GRAY);
                    cb.SetFontAndSize(bf, 8);
                    cb.BeginText();

                    cb.SetTextMatrix(10, size.Top - 20);


                    cb.ShowText(Texto);

                    cb.EndText();


                }
                pdfStamper.Close();
                reader.Close();

                byte[] ms = memorystream.ToArray();
                memorystream.Close();
                // string Destino = @"c:\temp\destino.pdf";
                FileStream file = new FileStream(Fichero, FileMode.Create, FileAccess.Write);
                file.Write(ms, 0, ms.Length);
                file.Close();
                // memorystream.Close();
            }
        }
        [WebMethod]
        public string DevolverListaTipos(string Idusuario, string IdEmpresa, string Padre)
        {//ACTUALIZADO
            try
            {
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                String SQL = "select tip.idtipodocumento,tip.descripcion,tip.Padre,tip.idempresa from TipoDocumento tip where (tip.idusuario='" + Idusuario + "' or idempresa='" + IdEmpresa + "') and tip.activo='True' and Padre=" + Padre + " group by tip.idtipodocumento,tip.descripcion,tip.Padre,tip.idempresa,tip.activo order by tip.idempresa,tip.descripcion ";
                string Hijos = "";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Hijos += ds.Tables[0].Rows[i]["idtipodocumento"].ToString();
                        Hijos += "^";
                        Hijos += ds.Tables[0].Rows[i]["Descripcion"].ToString().Replace("\r\n", "");
                        Hijos += "^";
                        if (string.IsNullOrEmpty(ds.Tables[0].Rows[i]["idempresa"].ToString()))
                            Hijos += "U";
                        else
                            Hijos += "E";
                        //U INDICAMOS QUE ES DE USUARIO, E QUE ES DE EMPRESA
                        Hijos += "|";

                    }
                }
                vCon.Close();
                return Hijos;
            }
            catch (OleDbException ex)
            {
                return null;
            }
        }
        [WebMethod]
        public string CrearNotificacion(string Descripcion, string IdUsuarioEmisor, string IdTipo, string IdUsuarioReceptor)
        {
            // TIPOS
            // 7 - DOCUMENTO COMPARTIDO
            // 8 - EXPEDIENTE COMPARTIDO
            try
            {
                int resultado;
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string Consulta = "insert into Notificacion (Descripcion,IdUsuarioEmisor,FechaCreacion,Tipo,IdUsuarioReceptor,IdFlujo,IdFicha,IdEstado) values ";
                Consulta += "('" + Descripcion + "','" + IdUsuarioEmisor + "',GETDATE(),'" + IdTipo + "'," + IdUsuarioReceptor + ",-1,-1,-1)";

                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                da.SelectCommand = vComando;
                resultado = vComando.ExecuteNonQuery();

                vCon.Close();
                return "";
            }
            catch { return "error"; }
        }
        [WebMethod]
        public string CrearDocumentosTipos(string IdTipoDocumento, string IdDocumento)
        {
            try
            {
                int resultado;
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string Consulta = "insert into DocumentosTipos (idtipodocumento,iddocumento) values ('" + IdTipoDocumento + "','" + IdDocumento + "')";

                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                da.SelectCommand = vComando;
                resultado = vComando.ExecuteNonQuery();
                // InsertarDocumentoenDocumentosUsuario(IdDocumento, IdUsuario, Carpeta, IdUsuarioComparte);

                vCon.Close();
                return "";
            }
            catch { return "error"; }
        }
        [WebMethod]
        public string ExisteTipoDocumentoZDocs(string Idusuario, string Descripcion, string IdEmpresa)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);

                string SQL = "";
                if (string.IsNullOrEmpty(IdEmpresa))
                    SQL = "select * from Tipodocumento where IdUsuario=" + Idusuario + " and Descripcion='" + Descripcion + "'";
                else
                    SQL = "select * from Tipodocumento where (IdUsuario=" + Idusuario + " or Idempresa=" + IdEmpresa + ") and Descripcion='" + Descripcion + "'";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();
                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                    return ds.Tables[0].Rows[0].ItemArray[0].ToString();
                else
                    return "";

            }
            catch (OleDbException ex)
            {
                return ex.ToString();
            }


        }
        [WebMethod]
        public string CrearArboldeTipos(string ListaNodos, string IdEmpresa, string IdUsuario, string Campos)
        {
            try
            {
                string IdTipo = string.Empty; string Padre = "-1"; string NuevoTipo = string.Empty;
                string[] Nodos = ListaNodos.Split('|');
                if (Nodos.Count() > 0)
                {
                    for (int i = 0; i < Nodos.Count(); i++)
                    {
                        IdTipo = ExisteTipoDocumentoZDocs(IdUsuario, Nodos[i], IdEmpresa);
                        if (string.IsNullOrEmpty(IdTipo))
                        {
                            if (!string.IsNullOrEmpty(NuevoTipo))
                                Padre = NuevoTipo;
                            NuevoTipo = CrearTipoDocumento(Nodos[i], IdEmpresa, false, IdUsuario, Padre, "Documentos");
                            string[] _campos = Campos.Split('^');
                            //      (JsonConvert.DeserializeObject(Campos, typeof(string[])) as string[]);
                            CrearTablaCamposTipo(NuevoTipo, Nodos[i], _campos, "Documentos");
                        }
                        Padre = IdTipo;
                    }
                    // return IdTipo;
                }
                return "";
            }
            catch (Exception e)
            {
                return "Error creando árbol de tipos, " + e.Message;
            }
        }


        [WebMethod]
        public string CrearTipoDocumento(string Descripcion, string IdEmpresa, Boolean TieneCampos, string Idusuario, string Padre, string Tabla)
        { //ACTUALIZADO 22/05/2015
            try
            {
                if (string.IsNullOrEmpty(Padre))
                    Padre = "-1";
                string resultado;
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string Consulta = "";
                if (Tabla.ToUpper() == "EXPEDIENTES")
                    Consulta = "insert into TipoExpediente (descripcion,activo,IdEmpresa,TieneCampos,Idusuario,Padre) values ('" + Descripcion + "','True'," + (string.IsNullOrEmpty(IdEmpresa) ? "NULL" : "'" + IdEmpresa + "'") + ",'" + (TieneCampos ? "True" : "False") + "','" + Idusuario + "'," + Padre + ");select SCOPE_IDENTITY()";// SELECT SCOPE_IDENTITY() AS [SCOPE_IDENTITY]";//  select SCOPE_IDENTITY()";
                else
                    if (Tabla.ToUpper() == "DOCUMENTOS")
                    Consulta = "insert into TipoDocumento (descripcion,activo,IdEmpresa,TieneCampos,Idusuario,Padre) values ('" + Descripcion + "','True'," + (string.IsNullOrEmpty(IdEmpresa) ? "NULL" : "'" + IdEmpresa + "'") + ",'" + (TieneCampos ? "True" : "False") + "','" + Idusuario + "'," + Padre + ");select SCOPE_IDENTITY()";// SELECT SCOPE_IDENTITY() AS [SCOPE_IDENTITY]";// select SCOPE_IDENTITY()";
                else
                        if (Tabla.ToUpper() == "FORMULARIOS")
                    Consulta = "insert into TipoFormulario (Descripcion,activo,Idusuario,idempresa,Padre) values ('" + Descripcion + "','True'," + Idusuario + "," + (string.IsNullOrEmpty(IdEmpresa) ? "NULL" : "'" + IdEmpresa + "'") + "," + Padre + "); select SCOPE_IDENTITY()";// SELECT SCOPE_IDENTITY() AS [SCOPE_IDENTITY]"; // select SCOPE_IDENTITY()";

                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                da.SelectCommand = vComando;
                resultado = vComando.ExecuteScalar().ToString();
                // InsertarDocumentoenDocumentosUsuario(IdDocumento, IdUsuario, Carpeta, IdUsuarioComparte);

                vCon.Close();
                return resultado;
            }
            catch { return "error"; }
        }
        [WebMethod]
        public string InsertarDocumentoenExpedienteIdUsuario(string idDocumento, string idExpediente, string IdUsuario)
        {
            idDocumento = Utilities.tratarParam(idDocumento);
            idExpediente = Utilities.tratarParam(idExpediente);
            try
            {
                //ELIMINAMOS EL DOCUMENTO DE LOS EXPEDIENTES A LOS QUE PERTENEZCA, SALVO EN EL QUE QUERAMOS MANTENERLO
                EliminarDocumentodeExpediente(idDocumento, idExpediente);
                string Consulta = "insert into documentosexpedientes (iddocumento,idexpediente,idusuario) values ('" + idDocumento + "','" + idExpediente + "','" + IdUsuario + "')";
                return DatabaseConnection.executeNonQueryInt(Consulta, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider).ToString();
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("InsertarDocumentoExpedienteIdUsuario", "WSZFK", -1, e.ToString(), e.Message);
                return "-1";
            }
        }
        [WebMethod]
        public string InsertarDocumentoenExpediente(string idDocumento, string idExpediente)
        {
            idDocumento = Utilities.tratarParam(idDocumento);
            idExpediente = Utilities.tratarParam(idExpediente);
            try
            {
                //ELIMINAMOS EL DOCUMENTO DE LOS EXPEDIENTES A LOS QUE PERTENEZCA, SALVO EN EL QUE QUERAMOS MANTENERLO
                //EliminarDocumentodeExpediente(idDocumento, idExpediente);
                //string Consulta = "insert into documentosexpedientes (iddocumento,idexpediente) values ('" + idDocumento + "','" + idExpediente + "')";
                //return DatabaseConnection.executeNonQueryInt(Consulta, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider).ToString();
                return InsertarDocumentoenExpedienteIdUsuario(idDocumento, idExpediente, "-1");
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("InsertarDocumentoExpediente", "WSZFK", -1, e.ToString(), e.Message);
                return "-1";
            }
        }
        [WebMethod]
        public string EliminarDocumentodeExpediente(string idDocumento, string idExpedienteAMantener)
        {//ELIMINAMOS EL DOCUMENTO DE LOS EXPEDIENTES A LOS QUE PERTENEZCA, SALVO EN EL QUE QUERAMOS MANTENERLO
            idDocumento = Utilities.tratarParam(idDocumento);
            idExpedienteAMantener = Utilities.tratarParam(idExpedienteAMantener);

            idDocumento = Utilities.tratarParam(idDocumento);
            idExpedienteAMantener = Utilities.tratarParam(idExpedienteAMantener);
            try
            {
                string Consulta = "delete from documentosexpedientes where iddocumento='" + idDocumento + "' and IdExpediente<>'" + idExpedienteAMantener + "'";
                return DatabaseConnection.executeNonQueryInt(Consulta, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider).ToString();
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("EliminarDocumentodeExpediente", "WSZFK", -1, e.ToString(), e.Message);
                return "-1";
            }
        }
        [WebMethod]
        public string CrearEmpresaZDocs(string Nombre, string Cif, string Calle, string Poblacion, string Provincia, string Pais, string Cp, string Email, string Telefono, string Web)
        {
            try
            {
                int resultado;
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string Consulta = "Insert into Empresa (Nombre,Descripcion,Cif,Calle,poblacion,provincia,pais,cp,email,telefono,web,fechacreacion) values ('" + Nombre + "','" + Nombre + "','" + Cif + "','" + Calle + "','" + Poblacion + "','" + Provincia + "','" + Pais + "','" + Cp + "','" + Email + "','" + Telefono + "','" + Web + "',GETDATE() ); select SCOPE_IDENTITY();";

                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                da.SelectCommand = vComando;
                string res = vComando.ExecuteScalar().ToString();

                vCon.Close();
                return res;
            }
            catch (Exception ex) {
                Trazas t = new Trazas("CrearEmpresaZDocs", "WSZFK", -1, ex.Message, ex.ToString());
                return "-1";
            }
        }


        [WebMethod]
        public string insertarFormularioEnBBDD(string descripcion, string idUsuario, string idempresa, string Fecha)
        { //ACTUALIZADO 21/04/2015
            string SQL = "";
            string resul = "";
            OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
            try
            {
                if (string.IsNullOrEmpty(idempresa))
                    idempresa = DevolverIdCompañiaPrincipalUsuarioZDocs(idUsuario);
                if (string.IsNullOrEmpty(Fecha))
                {
                    SQL = "INSERT INTO Formularios ( FechaCreacion, idUsuario,Nombre,activo,idempresa) VALUES (" +
                        " GETDATE()," + idUsuario + ",'" + descripcion + "','True','" + idempresa + "') ; select SCOPE_IDENTITY();";
                }
                else
                {
                    SQL = "INSERT INTO Formularios ( FechaCreacion, idUsuario,Nombre,activo,idempresa) VALUES (" +
                        "'" + Fecha + "'," + idUsuario + ",'" + descripcion + "','True','" + idempresa + "'); select SCOPE_IDENTITY();";
                }
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();
                resul = vComando.ExecuteScalar().ToString();

                return resul;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        [WebMethod]
        public string insertarFormularioEnBBDDHtml(string descripcion, string idUsuario, string idempresa, string Fecha)
        { //ACTUALIZADO 21/04/2015
            string SQL = "";
            string resul = "";
            OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
            try
            {
                if (string.IsNullOrEmpty(idempresa))
                    idempresa = DevolverIdCompañiaPrincipalUsuarioZDocs(idUsuario);
                if (string.IsNullOrEmpty(Fecha))
                {
                    SQL = "INSERT INTO Formularios ( FechaCreacion, idUsuario,Nombre,activo,idempresa,tieneHtml) VALUES (" +
                        " GETDATE()," + idUsuario + ",'" + descripcion + "','True','" + idempresa + "','True') ; select SCOPE_IDENTITY();";
                }
                else
                {
                    SQL = "INSERT INTO Formularios ( FechaCreacion, idUsuario,Nombre,activo,idempresa,tieneHtml) VALUES (" +
                        "'" + Fecha + "'," + idUsuario + ",'" + descripcion + "','True','" + idempresa + "','True'); select SCOPE_IDENTITY();";
                }
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();
                resul = vComando.ExecuteScalar().ToString();

                return resul;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        [WebMethod]
        public string ReservarIdDocumento(int NumIds, string idUsuario, string IdEmpresa)
        { //ACTUALIZADO 21/04/2015
            string SQL = "select * from usuarios";
            string resul = "";
            OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
            try
            {
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();
                for (int i = 0; i < NumIds; i++)
                {
                    SQL = "INSERT INTO Documentos ( FechaCreacion, idUsuariopropietario,idusuariouploader,version,activo,idempresa,idusuario) VALUES (" +
                         " GETDATE()," + idUsuario + "," + idUsuario + ",'1','False','" + IdEmpresa + "'," + idUsuario + ") ; select SCOPE_IDENTITY();";
                    vComando = new OleDbCommand(SQL, vCon);

                    resul += vComando.ExecuteScalar().ToString() + "|";

                }



                return resul;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        [WebMethod]
        public string insertarDocEnBBDD(string descripcion, string idUsuario, string formato, string idempresa, string Fecha, string OldIdDocumento)
        { //ACTUALIZADO 21/04/2015
            string query = string.Empty;
            try
            {
                if (string.IsNullOrEmpty(idempresa))
                    idempresa = DevolverIdCompañiaPrincipalUsuarioZDocs(idUsuario);

                if (string.IsNullOrEmpty(Fecha))
                {
                    query = "INSERT INTO Documentos ( FechaCreacion, idUsuariopropietario,idusuariouploader,docOriginal, version,activo,formato,idempresa,idusuario,OldUniqueid) VALUES (" +
                        " GETDATE()," + idUsuario + "," + idUsuario + ", '" + descripcion + "','1','True','" + formato + "','" + idempresa + "'," + idUsuario + "," + (string.IsNullOrEmpty(OldIdDocumento) ? "NULL" : "'" + OldIdDocumento + "'") + ") ; select SCOPE_IDENTITY();";
                }
                else
                {
                    query = "INSERT INTO Documentos ( FechaCreacion, idUsuariopropietario,idusuariouploader,docOriginal, version,activo,formato,idempresa,idusuario,OldUniqueid) VALUES (" +
                        "'" + Fecha + "'," + idUsuario + "," + idUsuario + ", '" + descripcion + "','1','True','" + formato + "','" + idempresa + "'," + idUsuario + "," + (string.IsNullOrEmpty(OldIdDocumento) ? "NULL" : "'" + OldIdDocumento + "'") + "); select SCOPE_IDENTITY();";
                }

                return DatabaseConnection.executeScalarString(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider).ToString();

            }
            catch (Exception e)
            {
                Trazas t = new Trazas("insertarDocEnBBDD", "WSZFK", Convert.ToInt32(idUsuario), e.ToString(), e.Message);
                return "-1";
            }
        }

        [WebMethod]
        public string insertarDocEnBBDDHtml(string descripcion, string idUsuario, string formato, string idempresa, string Fecha, string OldIdDocumento)
        { //ACTUALIZADO 21/04/2015
            string SQL = "";
            string resul = "";
            OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
            try
            {
                if (string.IsNullOrEmpty(idempresa))
                    idempresa = DevolverIdCompañiaPrincipalUsuarioZDocs(idUsuario);
                if (string.IsNullOrEmpty(Fecha))
                {
                    SQL = "INSERT INTO Documentos ( FechaCreacion, idUsuariopropietario,idusuariouploader,docOriginal, version,activo,formato,idempresa,idusuario,OldUniqueid,tieneHtml) VALUES (" +
                        " GETDATE()," + idUsuario + "," + idUsuario + ", '" + descripcion + "','1','True','" + formato + "','" + idempresa + "'," + idUsuario + "," + (string.IsNullOrEmpty(OldIdDocumento) ? "NULL" : "'" + OldIdDocumento + "'") + ",'true') ; select SCOPE_IDENTITY();";
                }
                else
                {
                    SQL = "INSERT INTO Documentos ( FechaCreacion, idUsuariopropietario,idusuariouploader,docOriginal, version,activo,formato,idempresa,idusuario,OldUniqueid,tieneHtml) VALUES (" +
                        "'" + Fecha + "'," + idUsuario + "," + idUsuario + ", '" + descripcion + "','1','True','" + formato + "','" + idempresa + "'," + idUsuario + "," + (string.IsNullOrEmpty(OldIdDocumento) ? "NULL" : "'" + OldIdDocumento + "'") + ",'true'); select SCOPE_IDENTITY();";
                }
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();
                resul = vComando.ExecuteScalar().ToString();

                return resul;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        [WebMethod]
        public string InsertarEventoSocial(string IdDocumento, string IdUsuario, string idEmpresa, string IdUsuario2, string IdEmpresa2, string Idexpediente, string Comentarios, int TipoEvento, string Email, string DescripcionEvento, string IdFormulario = "")
        {
            try
            {
                int resultado;
                // int y = EventoSocial.DevolverTipoSocialEvent(tipo);
                //MONTAR INSERT
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);

                string Consulta = "Insert into Eventos (iddoc,fecha,idusuario,idempresa,idusuariocolateral,idempresacolateral,idexpediente,comentarios,idtipo,Email,Titulo,Texto,IdFormulario) values (";

                Consulta += string.IsNullOrEmpty(IdDocumento) ? "NULL" : "'" + IdDocumento + "'";
                Consulta += ",GETDATE(),";
                Consulta += string.IsNullOrEmpty(IdUsuario) ? "-1" : "'" + IdUsuario + "'";
                Consulta += ",";
                Consulta += string.IsNullOrEmpty(idEmpresa) ? "-1" : "'" + idEmpresa + "'";
                Consulta += ",";
                Consulta += string.IsNullOrEmpty(IdUsuario2) ? "NULL" : "'" + IdUsuario2 + "'";
                Consulta += ",";
                Consulta += string.IsNullOrEmpty(IdEmpresa2) ? "NULL" : "'" + IdEmpresa2 + "'";
                Consulta += ",";
                Consulta += string.IsNullOrEmpty(Idexpediente) ? "NULL" : "'" + Idexpediente + "'";

                Consulta += ",";
                Consulta += string.IsNullOrEmpty(Comentarios) ? "NULL" : "'" + Comentarios + "'," + TipoEvento;
                //Consulta += ",'" + Inicializarvariable(Comentarios) + "'," + EventoSocial.DevolverTipoSocialEvent(tipo);
                Consulta += ",";
                Consulta += string.IsNullOrEmpty(Email) ? "NULL" : "'" + Email + "'";

                Consulta += ",";
                //Consulta += "'" + EventoSocial.DevolverDescripcionEvento(tipo) + "','" + Inicializarvariable(Comentarios) + "')";
                Consulta += "'" + DescripcionEvento + "'";
                Consulta += ",";
                Consulta += string.IsNullOrEmpty(Comentarios) ? "NULL" : "'" + Comentarios + "'";
                Consulta += ",";
                Consulta += string.IsNullOrEmpty(IdFormulario) ? "NULL" : "'" + IdFormulario + "'";
                Consulta += ")";

                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                da.SelectCommand = vComando;
                resultado = vComando.ExecuteNonQuery();

                vCon.Close();
                return "";
            }
            catch { return "error"; }
        }

        [WebMethod]
        public string SubirFicheroZDocs(string NombreFichero, byte[] Fichero, string IdUsuario, string EmailUsuario, string NombreUsuario, string Apellidos, string Password, string Dni, string IdDistribuidor, string IdEmpresa, string NombreEmpresa, string Cif, string IdUsuarioComparte, string ObligadaLectura, string Fecha, string OldIdDocumento)
        {
            try
            {

                Boolean UsuarioNuevo = false;
                if (!string.IsNullOrEmpty(IdUsuario) || !string.IsNullOrEmpty(EmailUsuario))
                {
                    if (string.IsNullOrEmpty(IdUsuario))
                        IdUsuario = ExisteUsuarioZDocs(EmailUsuario);
                    if (string.IsNullOrEmpty(IdUsuario))
                    {
                        Guid TokenTemporal = Guid.NewGuid();
                        IdUsuario = CrearUsuarioZDocs(NombreUsuario, Apellidos, EmailUsuario, "", Password, Dni, IdDistribuidor, TokenTemporal.ToString(), "", "");
                        UsuarioNuevo = true;
                    }
                    if (string.IsNullOrEmpty(IdEmpresa))
                    {
                        if (!string.IsNullOrEmpty(Cif))
                            IdEmpresa = ExisteEmpresaZDocs(Cif);
                        else
                            IdEmpresa = "";
                        if (string.IsNullOrEmpty(IdEmpresa))
                        {
                            if (UsuarioNuevo)
                                IdEmpresa = CrearEmpresaZDocs(NombreEmpresa, Cif, "", "", "", "", "", "", "", "");
                            else
                                IdEmpresa = DevolverIdCompañiaPrincipalUsuarioZDocs(IdUsuario);
                        }
                    }
                    if (UsuarioNuevo)
                        CrearUsuarioEmpresaZDocs(IdUsuario, IdEmpresa, false, true);

                    string IdDoc = insertarDocEnBBDD(NombreFichero, IdUsuario, NombreFichero.Substring(NombreFichero.Length - 3), IdEmpresa, Fecha, OldIdDocumento);



                    string Ruta = "00000000";
                    Ruta = Ruta.Substring(0, 8 - IdDoc.Length) + IdDoc;
                    //string pathString = Directorio + Ruta.Substring(0, 2) + "/" + Ruta.Substring(2, 2) + "/" + Ruta.Substring(4, 2) + "/" + Ruta.Substring(6, 2) + "/";
                    string pathString = Directorio + Ruta.Substring(0, 2) + "/" + Ruta.Substring(2, 2) + "/" + Ruta.Substring(4, 2) + "/";// +Ruta.Substring(6, 2) + "/";

                    System.IO.Directory.CreateDirectory(pathString);
                    if (string.IsNullOrEmpty(IdEmpresa))
                        IdEmpresa = DevolverIdCompañiaPrincipalUsuarioZDocs(IdUsuario);

                    String[] aux = NombreFichero.Split('.');
                    String formato = aux[aux.Length - 1];
                    //File.WriteAllBytes(pathString + "/" + IdDoc, Fichero);
                    File.WriteAllBytes(pathString + Ruta.Substring(6, 2) + "." + formato, Fichero);

                    InsertarDocumentoenDocumentosUsuario(IdDoc, IdUsuario, "", IdUsuarioComparte, ObligadaLectura, Fecha, "", "", "", "");

                    return IdDoc;
                }
                else
                {
                    return "El id o el Email son Imprescindibles";
                }

            }
            catch (Exception ex) {
                Trazas t = new Trazas("SubirFicheroZDocs", "WSZFK", -1, ex.Message, ex.ToString());
                return "-1";
            }
        }

        [WebMethod]
        public string CrearUsuarioZDocs(string Nombre, string Apellidos, string Email, string PuestoTrabajo, string Password, string Dni, string IdDistribuidor, string TokenTemporal, string FechaAlta, string PasswordEncriptada)
        {
            try
            {
                int resultado;
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string Consulta = "";
                if (string.IsNullOrEmpty(IdDistribuidor))
                {
                    if (string.IsNullOrEmpty(FechaAlta))
                        Consulta = "Insert into Usuarios (Nombre,Apellidos,Email,PuestoTrabajo,Password,Dni,fechaalta,idtipoUsuario,PasswordE) values ('" + Nombre + "','" + Apellidos + "','" + Email + "','" + PuestoTrabajo + "','" + Password + "','" + Dni + "',GETDATE(),2,'" + PasswordEncriptada + "');Select SCOPE_IDENTITY()";
                    else
                        Consulta = "Insert into Usuarios (Nombre,Apellidos,Email,PuestoTrabajo,Password,Dni,fechaalta,idtipoUsuario,PasswordE) values ('" + Nombre + "','" + Apellidos + "','" + Email + "','" + PuestoTrabajo + "','" + Password + "','" + Dni + "','" + FechaAlta + "',2,'" + PasswordEncriptada + "');Select SCOPE_IDENTITY()";
                }
                else
                {
                    if (string.IsNullOrEmpty(FechaAlta))
                        Consulta = "Insert into Usuarios (Nombre,Apellidos,Email,PuestoTrabajo,Password,Dni,Iddistribuidor,fechaalta,idtipoUsuario,PasswordE) values ('" + Nombre + "','" + Apellidos + "','" + Email + "','" + PuestoTrabajo + "','" + Password + "','" + Dni + "','" + IdDistribuidor + "',GETDATE(),2,'" + PasswordEncriptada + "');Select SCOPE_IDENTITY()";
                    else
                        Consulta = "Insert into Usuarios (Nombre,Apellidos,Email,PuestoTrabajo,Password,Dni,Iddistribuidor,fechaalta,idtipoUsuario,PasswordE) values ('" + Nombre + "','" + Apellidos + "','" + Email + "','" + PuestoTrabajo + "','" + Password + "','" + Dni + "','" + IdDistribuidor + "','" + FechaAlta + "',2,'" + PasswordEncriptada + "');Select SCOPE_IDENTITY()";
                }
                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                da.SelectCommand = vComando;
                string res = vComando.ExecuteScalar().ToString();

                vCon.Close();
                return res;
            }
            catch (Exception ex) {
                Trazas t = new Trazas("CrearUsuarioZDocs", "WSZFK", -1, ex.Message, ex.ToString());
                return "-1";
            }
        }

        [WebMethod]
        public string AutocompleteUserCompanies(string IdEmpresa) //List<string>
        { //ACTUALIZADA
            List<string> _parties = new List<string>();

            string html = "";
            try
            {

                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);

                String SQL = "select usu.Nombre,usu.Apellidos,usu.idusuario from usuarios usu inner join usuarioempresa usucia on usu.idusuario=usucia.idusuario inner join empresa cia on usucia.idempresa=cia.idempresa where usu.activo='S' and usu.baneado='False' and cia.activo='True' and cia.baneado='False' and usucia.idempresa=" + IdEmpresa;
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();
                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();
                da.SelectCommand = vComando;
                da.Fill(ds);
                html = "[ ";
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        html += "{\"label\": \"" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + " " + ds.Tables[0].Rows[i].ItemArray[1].ToString() + "\", \"value\": \"" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + " " + ds.Tables[0].Rows[i].ItemArray[1].ToString() + "\" ,\"id\": \"" + ds.Tables[0].Rows[i].ItemArray[2].ToString() + "\" },";
                    }
                }
                html = html.Substring(0, html.Length - 1);
                html += " ]";
                return html;
            }
            catch
            { }
            return "";
        }
        [WebMethod]
        public string DevolverIdUsuarioToken(string Token)
        {
            try
            {
                string IdUsuario = "";
                if (Token.Contains("="))
                    IdUsuario = Token.Split('=')[1].Substring(0, Token.Split('=')[1].IndexOf("I"));
                else
                    IdUsuario = Token.Substring(0, Token.IndexOf("I"));
                return IdUsuario;
            }
            catch
            { return ""; }
        }
        [WebMethod]
        public string DevolverIdEmpresa(string IdUsuario)
        {
            string SQL = " select em.idempresa from empresa em inner join UsuarioEmpresa ue on ue.idempresa=em.idempresa where ue.Principal=1 and ue.idusuario=" + IdUsuario;
            System.Data.DataTable DT = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
            if (DT.Rows.Count > 0)
                return DT.Rows[0]["IdEmpresa"].ToString();
            else
                return "";
        }
        [WebMethod]
        public string DevolverIdEmpresaporDescripcion(string Descripcion)
        {
            string SQL = " select idempresa from empresa where descripcion='" + Descripcion + "'";
            System.Data.DataTable DT = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
            if (DT.Rows.Count > 0)
                return DT.Rows[0]["IdEmpresa"].ToString();
            else
                return "";
        }
        /// <summary>
        /// devuelve el nombre de la empresa i su identificador separado por el caracter '|'
        /// </summary>
        /// <param name="IdUsuario"></param>
        /// <param name="IdEmpresa"></param>
        /// <returns></returns>
        [WebMethod(Description = "devuelve el nombre de la empresa i su identificador separado por el caracter '|'")]
        public string DevolverEmpresaPrincipal(string IdUsuario, string IdEmpresa)
        {
            try
            {

                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string Consulta = "";
                if (string.IsNullOrEmpty(IdEmpresa) || IdEmpresa == "undefined" || IdEmpresa == "idempresa=")
                    Consulta = "Select emp.nombre,emp.idempresa from empresa emp left join usuarioempresa usuemp on emp.idempresa=usuemp.idempresa where usuemp.principal='True' and usuemp.idusuario='" + IdUsuario + "' ";
                else
                    Consulta = "Select emp.nombre,emp.idempresa from empresa emp left join usuarioempresa usuemp on emp.idempresa=usuemp.idempresa where usuemp.idempresa='" + IdEmpresa + "' and usuemp.idusuario='" + IdUsuario + "' ";

                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();
                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (ds.Tables[0].Rows[0].ItemArray[0].ToString().Length > 25)
                        return ds.Tables[0].Rows[0].ItemArray[0].ToString().Substring(0, 25) + "|" + ds.Tables[0].Rows[0].ItemArray[1].ToString();
                    else
                        return ds.Tables[0].Rows[0].ItemArray[0].ToString() + "|" + ds.Tables[0].Rows[0].ItemArray[1].ToString();
                }
                else
                {
                    return "";
                }

                return "";
            }
            catch { return "error"; }
        }
        //***
        public string DevolverNombreUsuario(string IdUsuario)
        {
            try
            {

                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string Consulta = "Select nombre,apellidos from usuarios where idusuario='" + IdUsuario + "' ";


                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();
                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                    return ds.Tables[0].Rows[0].ItemArray[0].ToString() + " " + ds.Tables[0].Rows[0].ItemArray[1].ToString();
                else
                    return "";

                return "";
            }
            catch { return "error"; }
        }
        public static Boolean EstaCompartidoelExpediente(string Idexpediente, string Idusuario)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string SQL = "";
                if (!string.IsNullOrEmpty(Idusuario))
                    SQL = "select * from expedientesusuarios where idexpediente='" + Idexpediente + "' and idusuario='" + Idusuario + "'";
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();
                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();
                da.SelectCommand = vComando;
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        [WebMethod]
        public string CompartirExpedienteNew(string IdExpediente, string IdUsuario, string IdUsuarioComparte, string Comentario, string IdEmpresaUsuarioComparte)
        {
            try
            {
                if (!EstaCompartidoelExpediente(IdExpediente, IdUsuario))
                {
                    int resultado;
                    string resp = "";
                    string ObligadaLectura = "False";
                    OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                    string Consulta = "Insert into ExpedientesUsuarios (idexpediente,idusuario,idusuariocomparte,Comentarios) values ('" + IdExpediente + "','" + IdUsuario + "','" + IdUsuarioComparte + "','" + Comentario + "'); SElect SCOPE_IDENTITY()";
                    OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                    vCon.Open();
                    ActualizarCampo("Expedientes", "Compartido", "True", " idexpediente='" + IdExpediente + "'");

                    string Resul = vComando.ExecuteScalar().ToString();
                    InsertarEventoSocial("", IdUsuario, DevolverEmpresaPrincipal(IdUsuario, ""), IdUsuarioComparte, DevolverEmpresaPrincipal(IdUsuarioComparte, ""), IdExpediente, "Expediente Compartido ", 9, "", "Expediente Compartido");

                    Consulta = "Select * from documentosexpedientes where idexpediente='" + IdExpediente + "'";
                    DataTable datos = DatabaseConnection.executeNonQueryDT(Consulta, CommandType.Text, ConnectionString.CadenaConexionZDocs);
                    if (datos.Rows.Count > 0)
                        return "";
                    else
                        return datos.Rows[0]["tokentemporal"].ToString();


                    if (datos.Rows.Count > 0)
                    {
                        for (int i = 0; i <= datos.Rows.Count; i++)
                        {
                            if (!ExisteDocumentoUsuario(datos.Rows[0]["IdDocumento"].ToString(), IdUsuario))
                            {
                                if (SondelamismaEmpresa(IdUsuarioComparte, IdUsuario))
                                    resp = InsertarDocumentoenDocumentosUsuario(datos.Rows[0]["IdDocumento"].ToString(), IdUsuario, "Documentos de Empresa", IdUsuarioComparte, ObligadaLectura, "", "", "", "", IdEmpresaUsuarioComparte);
                                else
                                    resp = InsertarDocumentoenDocumentosUsuario(datos.Rows[0]["IdDocumento"].ToString(), IdUsuario, "Documentos Compartidos", IdUsuarioComparte, ObligadaLectura, "", "", "", "", IdEmpresaUsuarioComparte);
                            }
                            ActualizarCampo("Documentos", "Compartido", "True", " iddocumento='" + datos.Rows[0]["IdDocumento"].ToString() + "'");
                        }

                    }
                    vCon.Close();
                    return "";
                }
                else
                {
                    return "MANDRIL";
                }
            }
            catch { return "error"; }
        }
        [WebMethod]
        public string CrearUsuarioEmpresaZDocs(string IdUsuario, string IdEmpresa, Boolean Administrador, Boolean Principal)
        {
            try
            {
                int resultado;
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string Consulta = "Insert into usuarioempresa (idusuario,Idempresa,Administrador,Principal) values ('" + IdUsuario + "','" + IdEmpresa + "'," + (Administrador ? "'True'" : "'False'") + "," + (Principal ? "'True'" : "'False'") + ")";

                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                da.SelectCommand = vComando;
                resultado = vComando.ExecuteNonQuery();

                vCon.Close();
                return "1";
            }
            catch (Exception ex) {
                Trazas t = new Trazas("CrearUsuarioEmpresaZDocs", "WSZFK", -1, ex.Message, ex.ToString());
                return "-1";
            }
        }
        [WebMethod]
        public string ActualizarDocumentoUsuario(string IdDocumento, string IdUsuario, string Obligadalectura, string Desde, string Hasta, string Comentarios)
        {
            try
            {
                int resultado;
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string Consulta = "";

                Consulta = "Update DocumentosUsuarios set LecturaObligatoria='" + Obligadalectura + "',Desde=" + (string.IsNullOrEmpty(Desde) ? "NULL" : "'" + Desde + "'") + ",Hasta=" + (string.IsNullOrEmpty(Hasta) ? "NULL" : "'" + Hasta + "'") + ",Comentarios=" + (string.IsNullOrEmpty(Comentarios) ? "NULL" : "'" + Comentarios + "'") + " where Iddocumento=" + IdDocumento + " and idusuario=" + IdUsuario;
                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                da.SelectCommand = vComando;
                string res = vComando.ExecuteScalar().ToString();

                vCon.Close();
                return res;
            }
            catch (Exception ex) {
                Trazas t = new Trazas("ActualizarDocumentoUsuario", "WSZFK", -1, ex.Message, ex.ToString());
                return "-1";
            }
        }
        [WebMethod]
        public string InsertarFormularioenFormulariosUsuario(string IdFormulario, string IdUsuario, string IdUsuarioComparte, string Obligadalectura, string Fecha, string Desde, string Hasta, string Comentarios, string IdEmpresaUsuarioComparte)
        { //ACTUALIZADO 21/04/2015
            try
            {
                int resultado;
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string Consulta = "Insert into FormulariosUsuarios (idFormulario,idusuario,idUsuarioComparte,LecturaObligatoria,FechaInclusion,Desde,Hasta,Comentarios,IdEmpresaUsuarioComparte) values ('" + IdFormulario + "','" + IdUsuario + "'," + (string.IsNullOrEmpty(IdUsuarioComparte) ? "NULL" : IdUsuarioComparte) + ",'" + Obligadalectura + "'," + (string.IsNullOrEmpty(Fecha) ? "GETDATE()" : "'" + Fecha + "'") + "," + (string.IsNullOrEmpty(Desde) ? "NULL" : "'" + Desde + "'") + "," + (string.IsNullOrEmpty(Hasta) ? "NULL" : "'" + Hasta + "'") + "," + (string.IsNullOrEmpty(Comentarios) ? "NULL" : "'" + Comentarios + "'") + "," + (string.IsNullOrEmpty(IdEmpresaUsuarioComparte) ? "NULL" : IdEmpresaUsuarioComparte) + ")";

                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                da.SelectCommand = vComando;
                resultado = vComando.ExecuteNonQuery();


                vCon.Close();
                return "";
            }
            catch (Exception e)
            { return e.Message; }
        }

        [WebMethod]
        public string InsertarDocumentoenDocumentosUsuario(string IdDocumento, string IdUsuario, string Carpeta, string IdUsuarioComparte, string Obligadalectura, string Fecha, string Desde, string Hasta, string Comentarios, string IdEmpresaUsuarioComparte)
        { //ACTUALIZADO 21/04/2015
            try
            {
                int resultado;
                string Consulta = "Insert into DocumentosUsuarios (iddocumento,idusuario,carpetaDocumento,idUsuarioComparte,LecturaObligatoria,FechaInclusion,Desde,Hasta,Comentarios,IdEmpresaUsuarioComparte) values ('" + IdDocumento + "','" + IdUsuario + "','" + Carpeta + "'," + (string.IsNullOrEmpty(IdUsuarioComparte) ? "NULL" : IdUsuarioComparte) + ",'" + Obligadalectura + "'," + (string.IsNullOrEmpty(Fecha) ? "GETDATE()" : "'" + Fecha + "'") + "," + (string.IsNullOrEmpty(Desde) ? "NULL" : "'" + Desde + "'") + "," + (string.IsNullOrEmpty(Hasta) ? "NULL" : "'" + Hasta + "'") + "," + (string.IsNullOrEmpty(Comentarios) ? "NULL" : "'" + Comentarios + "'") + "," + (string.IsNullOrEmpty(IdEmpresaUsuarioComparte) ? "NULL" : IdEmpresaUsuarioComparte) + ")";

                resultado = DatabaseConnection.executeScalarInt(Consulta, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                return "";
            }
            catch (Exception e)
            { return e.Message; }
        }
        [WebMethod]
        public string InsertarDocumentoenDocumentosCompartidos(string IdDocumento, string IdExpediente, string Email, string IdUsuarioComparte, string NombreUsuario, string EmpresaUsuario)
        { //ACTUALIZADO
            try
            {
                int resultado;

                Guid IdAcceso = Guid.NewGuid();
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string Consulta = "";
                Random rnd = new Random();
                Double Clave = rnd.NextDouble();
                String ClaveStr = Clave.ToString().Substring(Clave.ToString().Length - 4);
                ClaveStr = ClaveStr.Replace(".0", "");
                if (string.IsNullOrEmpty(IdExpediente))
                    Consulta = "Insert into DocsCompartidos (Email,idGuid,Nombre,Empresa,Iddoc,Idexpediente,idusuario,FECHAComparticion,Activo,DocPrincipal,Clave) values ('" + Email + "','" + IdAcceso + "','" + NombreUsuario + "','" + EmpresaUsuario + "','" + IdDocumento + "',NULL,'" + IdUsuarioComparte + "',GETDATE(),'True','True'," + ClaveStr + ")";
                else
                    Consulta = "Insert into DocsCompartidos (Email,idGuid,Nombre,Empresa,Iddoc,Idexpediente,idusuario,FECHAComparticion,Activo,DocPrincipal,Clave) values ('" + Email + "','" + IdAcceso + "','" + NombreUsuario + "','" + EmpresaUsuario + "',-1,'" + IdExpediente + "','" + IdUsuarioComparte + "',GETDATE(),'True','True'," + ClaveStr + ")";

                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                da.SelectCommand = vComando;
                resultado = vComando.ExecuteNonQuery();

                vCon.Close();
                return IdAcceso.ToString() + "|" + ClaveStr;
            }
            catch { return "error"; }
        }
        //[WebMethod]
        //public string CompartirExpedienteNew(string IdExpediente, string IdUsuario, string IdUsuarioComparte,string Obligadalectura)
        //{
        //    try
        //    {
        //        int resultado;
        //        OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
        //        string Consulta = "Insert into ExpedientesUsuarios (idexpediente,idusuario,idusuariocomparte) values ('" + IdExpediente + "','" + IdUsuario + "','" + IdUsuarioComparte + "')";

        //        OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
        //        vCon.Open();

        //        OleDbDataAdapter da = new OleDbDataAdapter();
        //        da.SelectCommand = vComando;
        //        resultado = vComando.ExecuteNonQuery();

        //        Consulta = "Select * from documentosexpedientes where idexpediente='" + IdExpediente + "'";
        //        vComando = new OleDbCommand(Consulta, vCon);                                
        //        DataSet ds = new DataSet();
        //        da.SelectCommand = vComando;
        //        da.Fill(ds);
        //        if (ds.Tables[0].Rows.Count > 0)
        //        {
        //            for (int i = 0; i <= ds.Tables[0].Rows.Count; i++)
        //            {
        //                if (!ExisteDocumentoUsuario( ds.Tables[0].Rows[0].ItemArray[0].ToString(),IdUsuario))
        //                {
        //                    if (SondelamismaEmpresa(IdUsuarioComparte, IdUsuario))
        //                        InsertarDocumentoenDocumentosUsuario( ds.Tables[0].Rows[0].ItemArray[0].ToString(),IdUsuario, "Documentos de Empresa", IdUsuarioComparte,Obligadalectura);
        //                    else
        //                        InsertarDocumentoenDocumentosUsuario( ds.Tables[0].Rows[0].ItemArray[1].ToString(), IdUsuario, "Documentos Compartidos", IdUsuarioComparte,Obligadalectura);
        //                }
        //            }

        //        }
        //        vCon.Close();
        //        return "";
        //    }
        //    catch { return "error"; }
        //}


        [WebMethod]
        public string ActualizaFormularioEnBBDD(string descripcion, string idUsuario, string idempresa, string Fecha, string idActualiza)
        { //ACTUALIZADO 13/04/2016
            string SQL = "";
            int resp = -1;
            int resp2 = -1;

            try
            {
                if (string.IsNullOrEmpty(idempresa))
                    idempresa = DevolverIdCompañiaPrincipalUsuarioZDocs(idUsuario);
                if (string.IsNullOrEmpty(Fecha))
                {
                    SQL = "INSERT INTO Formularios ( FechaCreacion, idUsuario,Nombre,activo,idempresa,idFormularioAnterior) VALUES (" +
                        " GETDATE()," + idUsuario + ",'" + descripcion + "','True','" + idempresa + "','" + idActualiza + "') ; select SCOPE_IDENTITY();";
                }
                else
                {
                    SQL = "INSERT INTO Formularios ( FechaCreacion, idUsuario,Nombre,activo,idempresa,idFormularioAnterior) VALUES (" +
                        "'" + Fecha + "'," + idUsuario + ",'" + descripcion + "','True','" + idempresa + "','" + idActualiza + "'); select SCOPE_IDENTITY();";
                }
                resp = DatabaseConnection.executeScalarInt(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                if (resp != -1)
                {
                    SQL = "UPDATE Formularios SET Activo='false' WHERE idFormulario ='" + idActualiza + "'";
                    resp2 = DatabaseConnection.executeScalarInt(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                }

                if (resp2 != -1)
                {
                    return resp.ToString();
                }
                return "-1";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        #endregion

        #region Métodos privados

        private int insertarEtiquetaAlExpediente(string idUsuario, string dossierId, string nombreEtiqueta)
        {
            string idEtiqueta = TagEtiqueta(idUsuario, nombreEtiqueta);

            string queryAsignarCompartido = "DECLARE @idAux int; " +
                        "SET @idAux = (SELECT TOP 1 (DossierTagsId + 1) " +
                        "FROM      DossierTags " +
                        "WHERE   DossierUniqueId = '" + dossierId + "' ORDER BY DossierTagsId DESC) " +
                        "IF @idAux is NULL " +
                        "INSERT INTO DossierTags (DossierUniqueId, TagUniqueId, DossierTagsId) " +
                        "VALUES ('" + dossierId + "','" + idEtiqueta + "', 0 ) " +
                        "ELSE " +
                        "INSERT INTO DossierTags (DossierUniqueId, TagUniqueId, DossierTagsId) " +
                        "VALUES ('" + dossierId + "','" + idEtiqueta + "', @idAux ) ";

            OleDbConnection vCon = new OleDbConnection(CadenaConexion);
            vCon.Open();
            OleDbCommand vComando = new OleDbCommand(queryAsignarCompartido, vCon);

            int resultado = vComando.ExecuteNonQuery();
            vCon.Close();

            return resultado;
        }

        private int insertarEtiquetaAlDocumento(string idUsuario, string docId, string nombreEtiqueta)
        {
            string idEtiqueta = TagEtiqueta(idUsuario, nombreEtiqueta);

            string queryAsignarCompartido = "DECLARE @idAux int; " +
                        "SET @idAux = (SELECT TOP 1 (DocumentTagsId + 1) " +
                        "FROM      DocumentTags " +
                        "WHERE   DocumentUniqueId = '" + docId + "' ORDER BY DocumentTagsId DESC) " +
                        "IF @idAux is NULL " +
                        "INSERT INTO DocumentTags (DocumentUniqueId, TagUniqueId, DocumentTagsId) " +
                        "VALUES ('" + docId + "','" + idEtiqueta + "', 0 ) " +
                        "ELSE " +
                        "INSERT INTO DocumentTags (DocumentUniqueId, TagUniqueId, DocumentTagsId) " +
                        "VALUES ('" + docId + "','" + idEtiqueta + "', @idAux ) ";

            OleDbConnection vCon = new OleDbConnection(CadenaConexion);
            vCon.Open();
            OleDbCommand vComando = new OleDbCommand(queryAsignarCompartido, vCon);

            int resultado = vComando.ExecuteNonQuery();
            vCon.Close();

            return resultado;
        }

        private string getMymeTypeByExtension(string DocName)
        {
            string ext = Path.GetExtension(DocName).ToLower();
            string mime;
            switch (ext)
            {
                case ".doc":
                case ".docx":
                    mime = "application/doc";
                    break;
                case ".xls":
                case ".xlsx":
                    mime = "application/xls";
                    break;
                case ".ppt":
                case ".pptx":
                case ".pps":
                case ".ppsx":
                    mime = "application/ppt";
                    break;
                case ".tiff":
                case ".tif":
                    mime = "image/tiff";
                    break;
                case ".bmp":
                    mime = "image/bmp";
                    break;
                case ".gif":
                    mime = "image/gif";
                    break;
                case ".jpg":
                case ".jpeg":
                    mime = "image/jpeg";
                    break;
                case ".pdf":
                    mime = "application/pdf";
                    break;
                case ".png":
                    mime = "image/png";
                    break;
                case ".zip":
                case ".rar":
                    mime = "application/zip";
                    break;
                case ".txt":
                    mime = "text/plain";
                    break;
                case ".htm":
                case ".html":
                case ".xml":
                    mime = "text/html";
                    break;
                default:
                    mime = "application/default";
                    break;
            }
            return mime;
        }

        private static string DictionaryToJson(Dictionary<string, string> dict)
        {
            string json = string.Empty;
            string _dic = string.Join(",", dict.Select(
                d => string.Format(" \"{0}\": \"{1}\"", d.Key, d.Value)).ToArray());
            json = "{ " + _dic + " }";
            return json;
        }

        private string getMailUsuario(string idUsuario)
        {
            try
            {
                String resultado = "-1";
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);
                String SQLGetCompanyId = "SELECT Email FROM Users WHERE (UniqueId = '" + idUsuario + "')";

                OleDbCommand vComando = new OleDbCommand(SQLGetCompanyId, vCon);

                vComando = new OleDbCommand(SQLGetCompanyId, vCon);
                vCon.Open();
                resultado = vComando.ExecuteScalar().ToString();
                vCon.Close();
                return resultado;
            }
            catch (Exception e)
            {
                return "-1";
            }
        }

        /*private int insertarTrazas(string usuario )
        {
            try
            {
                Guid g = new Guid();
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);
                vCon.Open();

                string SQL = "insert into Socialevents (UniqueId, TimeStamp, CreationDate, SchemaVersion, Type, Source, ActionUserUniqueId, ActionUserDescription, ActionCompanyUniqueId,ActionCompanyDescription,  DocumentUniqueId, DocumentDescription)";
                SQL += " values ('" + g + "','" + DevolverUltimoTimestamp() + "','" + DateTime.Now.Month + "/" + DateTime.Now.Day + "/" + DateTime.Now.Year + " " + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second + "','1.0','6','2','";
                SQL += usuario + "','" + Nombre + "','" + Compañia + "','" + DescripcionCompañia(Compañia) + "','" + Documento + "','" + DescripcionDocumento(Documento) + "') ";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                int resp = vComando.ExecuteNonQuery();
                if (resp > 0)
                {
                    return 1;
                }
                else
                {
                    return -1;
                }

                
                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }*/

        #endregion

        #region INDEXACION

        [WebMethod]
        public DataSet IndexacionCargaInicial(string token)
        {
            DataSet dsExpTipoCentro = new DataSet();

            if (Utilities.comprobarValidezUsuario(token) > 0)
            {
                dsExpTipoCentro.Tables.Add(IndexacionCargarExpedientes("", token));
                dsExpTipoCentro.Tables.Add(IndexacionCargarTiposDocumento(token));
                dsExpTipoCentro.Tables.Add(IndexacionCargarCentros(token));
            }

            dsExpTipoCentro.DataSetName = "ExpTiposCentros";
            return dsExpTipoCentro;
        }
        [WebMethod]
        public DataTable IndexacionCargarExpedientes(string nombreExpediente, string token)
        {
            nombreExpediente = Utilities.tratarParam(nombreExpediente);
            token = Utilities.tratarParam(token);
            DataTable dtExp = new DataTable();

            if (Utilities.comprobarValidezUsuario(token) > 0)
            {
                string idUsuario = token.Split('I')[0];

                string matricula = string.Empty, query = string.Empty;

                if (!string.IsNullOrEmpty(nombreExpediente) && nombreExpediente.Substring(0, 1).ToLower().Equals("d"))
                {
                    query = "select top 1 matricula from matriculas where DNI like '%" + nombreExpediente.Substring(1, nombreExpediente.Length - 1) + "%'";
                    nombreExpediente = DatabaseConnection.executeScalarString(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                }
                else
                    matricula = nombreExpediente;

                query = @"select idExpediente as ID, descripcion as Descripcion from expedientes where 
                    exists (select * from expedientesUsuarios where expedientes.idExpediente = ExpedientesUsuarios.idexpediente and ExpedientesUsuarios.idusuario = " + idUsuario + ")";

                if (!string.IsNullOrEmpty(nombreExpediente)) query += " and descripcion like '%" + matricula + "%'";


                dtExp = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

            }
            dtExp.TableName = "Expedientes";
            return dtExp;
        }
        [WebMethod]
        public  List<string> CargarExpedientes(string IdUsuario, string Cadena)
        {
            string SQL = "select * from expedientes where Descripcion Like '%" + Cadena + "%' and idusuario=" + IdUsuario;            
            System.Data.DataTable DT = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
            List<string> Resul = new List<string>();
            if (DT.Rows.Count > 0)
            {
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    Resul.Add(DT.Rows[i]["Descripcion"].ToString());                   
                }
            }
            return Resul;
        }

        [WebMethod]
        public DataTable IndexacionCargarDocumentos(string token, string idDocumento, string documentosActivos, bool tieneCentro, string idCentro,
            string idTipoDocumento, bool estaIndexado, bool sinExpediente, string idExp, bool esActualizacion, bool ordecacionDescripcion)
        {
            token = Utilities.tratarParam(token);
            idDocumento = Utilities.tratarParam(idDocumento);
            idCentro = Utilities.tratarParam(idCentro);
            idTipoDocumento = Utilities.tratarParam(idTipoDocumento);
            idExp = Utilities.tratarParam(idExp);

            DataTable dtDocs = new DataTable();

            if (Utilities.comprobarValidezUsuario(token) > 0)
            {
                if (!string.IsNullOrEmpty(idDocumento))
                    IndexacionQuitarBloqueo(token, idDocumento);

                //if (consultarSinIndexar) noIndexados = true;
                //else noIndexados = false;
                string idUsuario = token.Split('I')[0];

                string conExpediente = string.Empty, filtroActivos = string.Empty;
                string query = string.Empty, sinIndexar = string.Empty, consultaIdTipo = string.Empty, centro = string.Empty;

                // FILTRO ACTIVOS / INACTIVOS
                if (!documentosActivos.Equals("-1"))
                    filtroActivos = " and Documentos.activo = " + documentosActivos;

                // FILTRO CENTRO
                centro = consultaPorCentro(tieneCentro, idCentro);

                // FILTRO TIPO DOCUMENTO
                consultaIdTipo = consultaPorTipoDocumento(idTipoDocumento);

                // FILTRO POR INDEXADOS
                sinIndexar = consultaPorIndexado(estaIndexado);

                // FILTRO POR EXPEDIENTE
                conExpediente = consultaPorExpediente(idExp, sinExpediente, sinIndexar, consultaIdTipo);

                // BÚSQUEDA DE DOCUMENTOS DE UN EXPEDIENTE, NO SE TIENE EN CUENTA EL TIPO DE DOCUMENTO.
                if (!sinExpediente && !string.IsNullOrEmpty(idTipoDocumento))
                {
                    query = consultaPorExpedienteSinTipo(idExp);
                }
                else
                {
                    // Si el tipo seleccionado es '-2' seleccionamos todos los documentos que no tienen tipo
                    if (!string.IsNullOrEmpty(idTipoDocumento) && idTipoDocumento.Equals("-2"))
                    {
                        query = "select ROW_NUMBER() OVER (ORDER BY iddocumento asc) AS Índice, iddocumento, docOriginal from documentos where exists (select iddocumento from DocumentosUsuarios where documentosUsuarios.idusuario = " + idUsuario + " and documentosUsuarios.iddocumento =  documentos.iddocumento and not exists (select tiposdocumento.iddoc from tiposdocumento where tiposdocumento.iddoc = DocumentosUsuarios.iddocumento))";
                    }
                    else
                    {
                        if (estaIndexado) //TiposDocumento.idMetadato!=-1 and
                        {
                            // Obtenemos TODOS los documentos SIN INDEXAR, pertenezcan o no a un expediente, de un tipo concreto (-1 si son todos)
                            // Queremos los que tienen idMetadato diferente -1 es por que ya tienen los metadatos de matrícula, dni y fecha de inclusión, sino los descartamos
                            // hasta que Luís pase su proceso para renombrar y extraer esos metadatos.
                            query = "select ROW_NUMBER() OVER (ORDER BY iddocumento asc) AS Índice, idDocumento,docOriginal from Documentos where exists " +
                                        " (select * from DocumentosUsuarios where idUsuario = " + idUsuario + conExpediente +
                                        " and Documentos.idDocumento=DocumentosUsuarios.iddocumento)" + centro + filtroActivos;
                        }
                        else
                        { //TiposDocumento.idMetadato!=-1 and 
                            // Obtenemos TODOS los documentos, pertenezcan o no a un expediente, de un tipo concreto (-1 si son todos)
                            query = "select ROW_NUMBER() OVER (ORDER BY iddocumento asc) AS Índice, idDocumento,docOriginal from Documentos where exists " +
                                        " (select * from DocumentosUsuarios where idUsuario = " + idUsuario + conExpediente +
                                        " and Documentos.idDocumento=DocumentosUsuarios.iddocumento)" + centro + filtroActivos;
                        }
                    }
                }

                if (esActualizacion)
                {
                    query += " and EsActualizacion=1";
                }

                query += " order by ";

                if (ordecacionDescripcion)
                {
                    query += "docOriginal ";
                }
                else
                {
                    query += "idDocumento ";
                }
                query += "asc";

                dtDocs = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
            }
            dtDocs.TableName = "Documentos";
            return dtDocs;
        }

        [WebMethod]
        public string IndexacionQuitarBloqueo(string token, string idDocumento)
        {
            try
            {
                idDocumento = Utilities.tratarParam(idDocumento);
                token = Utilities.tratarParam(token);

                if (Utilities.comprobarValidezUsuario(token) > 0)
                {
                    string query = "delete from BLOQUEODOCUMENTO where idDocumento = " + idDocumento;
                    DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                }
                return "1";
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("reasignarTipoSeparacion", "Indexacion", -1, e.ToString(), e.Message);
                return e.Message;
            }

        }

        [WebMethod]
        public bool IndexacionPonerBloqueo(string token, string idDocumento)
        {
            try
            {
                idDocumento = Utilities.tratarParam(idDocumento);
                token = Utilities.tratarParam(token);

                if (Utilities.comprobarValidezUsuario(token) > 0)
                {
                    string query = "insert into BLOQUEODOCUMENTO (idDocumento) values (" + idDocumento + ")";
                    return DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider) > 0;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("reasignarTipoSeparacion", "Indexacion", -1, e.ToString(), e.Message);
                return false;
            }
        }



        private string consultaPorExpediente(string idExp, bool sinExpediente, string sinIndexar, string consultaIdTipo)
        {
            if (!string.IsNullOrEmpty(idExp) && !sinExpediente)
                return @" and exists 
                                    (select * from DocumentosExpedientes where exists
                                  (select * from TiposDocumento where TiposDocumento.activo=1 " + sinIndexar + consultaIdTipo + @"
                                  and TiposDocumento.idDoc=DocumentosExpedientes.iddocumento) and DocumentosExpedientes.idExpediente=" + idExp + @"
                                 and Documentos.iddocumento=DocumentosExpedientes.iddocumento) ";
            else
                return " and exists (select * from TiposDocumento where TiposDocumento.activo=1 " + sinIndexar + consultaIdTipo +
                                 " and TiposDocumento.idDoc=DocumentosUsuarios.iddocumento) ";
        }

        private string consultaPorExpedienteSinTipo(string idExp)
        {
            return @"select ROW_NUMBER() OVER (ORDER BY iddocumento asc) AS Índice, iddocumento, docOriginal from documentos 
    where exists (select de.iddocumento from documentosExpedientes de 
    where de.iddocumento = documentos.iddocumento and de.idExpediente = " + idExp + @")
    and exists (select tiposdocumento.iddoc from tiposdocumento where tiposdocumento.iddoc = documentos.iddocumento and tiposdocumento.activo = 1)";
        }

        private string consultaPorIndexado(bool estaIndexado)
        {
            if (estaIndexado)
                return " and TiposDocumento.indexado = 0 ";
            else
                return string.Empty;
        }

        private string consultaPorCentro(bool tieneCentro, string idCentro)
        {
            if (tieneCentro)
                return " and (idCentro" + (idCentro.Equals("-1") ? " is null or idCentro = -1)" : " = " + idCentro + ")");
            else
                return string.Empty;
        }

        private string consultaPorTipoDocumento(string idTipoDocumento)
        {
            string consultaIdTipo = string.Empty;

            if (!string.IsNullOrEmpty(idTipoDocumento))
            {
                if (idTipoDocumento.ToString().Equals("-2")) return string.Empty;
                else
                {
                    string consulta = "select padre from tipoDocumento where idTipoDocumento=" + idTipoDocumento;
                    int padre = DatabaseConnection.executeScalarInt(consulta, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                    if (padre > 0)
                        consultaIdTipo = " and TiposDocumento.idTipoDocumento=" + idTipoDocumento;
                    else
                        consultaIdTipo = " and exists " +
                               "(select * from tipoDocumento where  tipoDocumento.padre = " + padre + " and tipoDocumento.idTipoDocumento=TiposDocumento.idTipoDocumento)" +
                               " and TiposDocumento.idTipoDocumento=" + idTipoDocumento;
                }
            }
            return consultaIdTipo;
        }

        [WebMethod]
        public string IndexacionDevolverIdTipoDocumento(string token, string idDocumento)
        {
            idDocumento = Utilities.tratarParam(idDocumento);
            token = Utilities.tratarParam(token);

            if (Utilities.comprobarValidezUsuario(token) > 0)
            {
                string tipo = string.Empty;
                if (!string.IsNullOrEmpty(idDocumento))
                {
                    string query = "select top 1 idTipoDocumento from tiposDocumento where activo = 1 and idDoc = " + idDocumento + " order by idTiposDocumento asc ";
                    return DatabaseConnection.executeScalarString(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                }
            }

            return "";
        }

        [WebMethod]
        public DataTable IndexacionCargarTiposDocumento(string token)
        {
            token = Utilities.tratarParam(token);
            //DataSet dtTipos = new DataSet("Tipos");
            DataTable dtTipos = new DataTable();
            if (Utilities.comprobarValidezUsuario(token) > 0)
            {
                string query = "select idtipodocumento, descripcion, Padre from TipoDocumento where (idusuario = " + token.Split('I')[0] + " or idusuario=" + IdUsuarioFormacion + ") and activo = 1 order by descripcion asc ";

                //dtTipos.Tables.Add(DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider));
                dtTipos = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
            }
            dtTipos.TableName = "Tipos";
            return dtTipos;

        }

        [WebMethod]
        public DataTable IndexacionCargarTiposExpediente(string token)
        {
            token = Utilities.tratarParam(token);
            //DataSet dtTipos = new DataSet("Tipos");
            DataTable dtTipos = new DataTable();
            if (Utilities.comprobarValidezUsuario(token) > 0)
            {
                string query = "select idtipoExpediente, descripcion, Padre from TipoExpediente where idusuario = " + token.Split('I')[0] + " and activo = 1 order by descripcion asc ";

                //dtTipos.Tables.Add(DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider));
                dtTipos = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
            }
            dtTipos.TableName = "Tipos";
            return dtTipos;
        }

        [WebMethod]
        public DataTable IndexacionCargarCentros(string token)
        {
            token = Utilities.tratarParam(token);
            DataTable dtCentros = new DataTable();
            if (Utilities.comprobarValidezUsuario(token) > 0)
            {
                string query = "select idCentro, Descripcion from centro order by descripcion asc";
                dtCentros = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
            }
            dtCentros.TableName = "Centros";
            return dtCentros;
        }

        private DataTable devolverMetadatosExpedienteCarrefour(string idDocumento)
        {
            string query = @"select c.descripcion, m.matricula, m.dni,d.FechaCreacion as [fecha de recepción], d.docOriginal, e.descripcion from documentos d
left join DocumentosExpedientes de on d.idDocumento = de.iddocumento
left join expedientes e on de.idexpediente = e.idexpediente
left join matriculas m on e.descripcion = m.MATRICULA
left join centro c on c.codCentroCarrefour = m.[codigo centro]
where  d.idDocumento = " + idDocumento;

            return DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
        }

        [WebMethod]
        public bool IndexacionAsignarTipo(string token, string idDocumento, string nuevoTipo, string viejoTipo)
        {
            // Cuando reasignamos tipo puede ser que;
            //
            // 1. Ya tiene tipo 
            //  1.1. Tiene un tipo con metadatos
            //  1.2. Pertenece a un tipo padre
            //
            // 2. No tiene tipo por alguna circunstancia

            // Recuperamos el tipo y el idMetadato actuales


            if (Utilities.comprobarValidezUsuario(token) > 0)
            {
                string matricula = string.Empty, dni = string.Empty, fecha = string.Empty, centro = string.Empty, idCentro = string.Empty;
                string idUsuario = token.Split('I')[0];
                string tabla = "c00000000";
                string query = string.Empty;
                DataTable dtCampos = new DataTable();
                // 1. YA TIENE TIPO
                if (!string.IsNullOrEmpty(viejoTipo))
                {
                    tabla = tabla.Substring(0, tabla.Length - viejoTipo.Length) + viejoTipo;

                    query = "select * from " + tabla + " where exists (select * from TiposDocumento where idDoc = " + idDocumento + " and activo = 1 and TiposDocumento.idMetadato = " + tabla + ".IdMetaDato)";

                    dtCampos = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                    string nuevoIdMetadato = string.Empty;


                    // 1.1 TIENE TIPO CON METADATOS
                    if (dtCampos.Rows.Count > 0)
                    {
                        if (idUsuario.Equals("409"))
                        {
                            dni = dtCampos.Rows[0]["dni"].ToString();
                            fecha = dtCampos.Rows[0]["fecha"].ToString();
                        }
                        else if (idUsuario.Equals(IdUsuarioMaestro))
                        {
                            dni = dtCampos.Rows[0]["dni"].ToString();
                            matricula = dtCampos.Rows[0]["matricula"].ToString();
                            fecha = dtCampos.Rows[0]["fecha de recepción"].ToString();
                            centro = dtCampos.Rows[0]["centro"].ToString();
                        }
                    }
                }

                if (string.IsNullOrEmpty(viejoTipo) || dtCampos.Rows.Count > 0)
                {
                    if (idUsuario.Equals("409"))
                    {
                        dtCampos = devolverMetadatosExpedienteCarrefour(idDocumento);
                        dni = dtCampos.Rows[0]["dni"].ToString();
                        matricula = dtCampos.Rows[0]["matricula"].ToString();
                        fecha = dtCampos.Rows[0]["fecha de recepción"].ToString();
                        //SI ES W2M NO SE MIRA EL CENTRO
                    }
                    else if (idUsuario.Equals(IdUsuarioMaestro))
                    {
                        dtCampos = devolverMetadatosExpedienteCarrefour(idDocumento);
                        dni = dtCampos.Rows[0]["dni"].ToString();
                        matricula = dtCampos.Rows[0]["matricula"].ToString();
                        fecha = dtCampos.Rows[0]["fecha de recepción"].ToString();
                        centro = dtCampos.Rows[0]["centro"].ToString();
                    }
                }
                else
                {

                    // NO DEBERÍA ENTRAR NUNCA CON EL USUARIO 409
                    // No tiene tipo, por tano tenemos que buscar la matrícula del nombre del expediente, para poder encontrar los datos en la tabla de matrículas

                    //query = "select descripcion from expedientes where exists (select idExpediente from documentosExpedientes where expedientes.idExpediente = documentosExpedientes.idExpediente and documentosExpedientes.idDocumento = " + idDocumento + ")";

                    //string nombreExpediente = DatabaseConnection.executeScalarString(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                    //if (!string.IsNullOrEmpty(nombreExpediente))
                    //{
                    //    string[] dniMatricula = null;

                    //    if (!string.IsNullOrEmpty(nombreExpediente))
                    //    {

                    //        if (nombreExpediente.Contains("-")) dniMatricula = nombreExpediente.Split('-');
                    //        else dniMatricula = nombreExpediente.Split('_');


                    //        // matricula - dni - nombre
                    //        if (dniMatricula.Length == 3)
                    //        {
                    //            matricula = dniMatricula[0];
                    //            dni = dniMatricula[1];
                    //            query = @"select matriculas.matricula, matriculas.centro, centro.idCentro from matriculas 
                    //                        inner join centro on centro.descripcion = matriculas.centro
                    //                     where matriculas.dni = '" + dni.Replace(" ", "") + "' and matriculas.matricula = '" + matricula + "'";
                    //        }
                    //        else if (dniMatricula.Length == 1)
                    //        {
                    //            dni = dniMatricula[0];
                    //            query = @"select matriculas.matricula, matriculas.centro, centro.idCentro from matriculas 
                    //                        inner join centro on centro.descripcion = matriculas.centro
                    //                     where matriculas.dni = '" + dni.Replace(" ", "") + "'";
                    //        }


                    //        dtDatosViejos = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                    //        if (dtDatosViejos.Rows.Count > 0)
                    //        {
                    //            matricula = dtDatosViejos.Rows[0]["matricula"].ToString();
                    //            centro = dtDatosViejos.Rows[0]["centro"].ToString();
                    //            idCentro = dtDatosViejos.Rows[0]["idCentro"].ToString();
                    //        }

                    //        query = "select fechaCreacion from documentos where iddocumento = " + idDocumento;
                    //        fecha = DatabaseConnection.executeScalarString(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                    //        // Tenemos que actualizar la tabla documentos con el idCentro
                    //        if (!string.IsNullOrEmpty(idCentro))
                    //        {
                    //            query = "update documentos set idCentro = " + idCentro + " where idDocumento = " + idDocumento + " and idUsuario = " + idUsuario;
                    //            int res = DatabaseConnection.executeScalarInt(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                    //        }
                    //    }
                    //}
                    //else
                    //{
                    //    return false;
                    //    //MessageBox.Show("No se han encontrado datos de este documento. Se ha reclasificado sin asignarle ningún tipo de metadatos.");
                    //}
                }

                tabla = "c00000000";
                tabla = tabla.Substring(0, tabla.Length - nuevoTipo.Length) + nuevoTipo;

                //Primero creamos la fila en la tabla TiposDocumento (si pasamos idMetadato actualiza)
                ActualizarDocumentosTipos(nuevoTipo, idDocumento, "", "Documentos");
                ActualizarDocumentosTipos(nuevoTipo, idDocumento, "", "Documentos");

                if (((!string.IsNullOrEmpty(dni) || !string.IsNullOrEmpty(fecha)) && idUsuario.Equals("409")) ||
                    ((!string.IsNullOrEmpty(dni) || !string.IsNullOrEmpty(matricula) || !string.IsNullOrEmpty(fecha) || !string.IsNullOrEmpty(centro)) && idUsuario.Equals(IdUsuarioMaestro)))
                {
                    if (idUsuario.Equals(IdUsuarioMaestro))
                    {
                        query = "insert into " + tabla + " (dni,matricula, [fecha de recepción], centro) values " +
                        "('" + dni.Replace(" ", "") + "','" + matricula.Replace(" ", "") + "',";
                        if (!string.IsNullOrEmpty(fecha))
                            query += "'" + fecha + "',";
                        else
                            query += " null,";
                        query += "'" + centro + "'); select SCOPE_IDENTITY();";
                    }
                    else if (idUsuario.Equals("409"))
                    {
                        query = "insert into " + tabla + " (dni, fecha) values " +
                        "('" + dni.Replace(" ", "") + "',";
                        if (!string.IsNullOrEmpty(fecha))
                            query += "'" + fecha + "'";
                        else
                            query += " null";
                        query += "); select SCOPE_IDENTITY();";
                    }

                    string nuevoIdMetadato = DatabaseConnection.executeScalarInt(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider).ToString();


                    //Una vez tenemos la fila, al añadir el metadato y el tipo nos actualiza la fila que nos ha creado antes.
                    ActualizarDocumentosTipos(nuevoTipo, idDocumento, nuevoIdMetadato, "Documentos");
                    //ws.RenombrarDocumento(idDocumento);

                    query = "update documentos set fechaUltimaModificacion='" + DateTime.Now + "' where idDocumento = " + idDocumento;
                    string res = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider).ToString();
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        //[WebMethod]
        //public Boolean TieneHijos(TreeNode Raiz, string token, string IdEmpresa)
        //{
        //    if (!string.IsNullOrEmpty(token))
        //    {
        //        if (Raiz.Tag != null)
        //        {
        //            string SQL = " Select * from tipodocumento where padre=" + Raiz.Tag + " and (idusuario=" + token.Split('I')[0] + " or idempresa=" + IdEmpresa + ")";
        //            DataTable DT = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
        //            if (DT.Rows.Count > 0)
        //                return true;
        //            else
        //                return false;
        //        }
        //        else
        //            return true;
        //    }
        //    else
        //        return false;
        //}
        [WebMethod]
        public string IndexacionDevolverIdCentro(string token, string idDocumento)
        {
            string query = "select top 1 idCentro from documentos where idDocumento = " + idDocumento;

            return DatabaseConnection.executeScalarString(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
        }

        [WebMethod]
        public string IndexacionReasignarTipoSeparacion(string idDocumentoAntiguo, string idDocumento, string idTipoDocumentoNuevo)
        {
            idDocumentoAntiguo = Utilities.tratarParam(idDocumentoAntiguo);
            idDocumento = Utilities.tratarParam(idDocumento);
            idTipoDocumentoNuevo = Utilities.tratarParam(idTipoDocumentoNuevo);
            try
            {
                // Recuperamos el tipo y el idMetadato actuales
                string query = "select idTipoDocumento,idMetadato from tiposDocumento where activo = 1 and idDoc = " + idDocumentoAntiguo;
                DataTable dtDatosViejos = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                DataTable dtCampos = null;
                string nuevoIdMetadato = string.Empty;
                if (dtDatosViejos.Rows.Count > 0)
                {
                    string tabla = "c00000000";
                    tabla = tabla.Substring(0, tabla.Length - dtDatosViejos.Rows[0]["idTipoDocumento"].ToString().Length) + dtDatosViejos.Rows[0]["idTipoDocumento"];
                    query = "select * from " + tabla + " where idMetadato = " + dtDatosViejos.Rows[0]["idMetadato"].ToString();
                    dtCampos = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                    if (dtCampos.Rows.Count > 0)
                    {
                        tabla = "c00000000";
                        tabla = tabla.Substring(0, tabla.Length - idTipoDocumentoNuevo.Length) + idTipoDocumentoNuevo;

                        query = "insert into " + tabla + " (dni,matricula, [fecha de recepción], centro) values " +
                                "('" + dtCampos.Rows[0]["dni"].ToString() + "','" + dtCampos.Rows[0]["matricula"].ToString() + "',";

                        if (!string.IsNullOrEmpty(dtCampos.Rows[0]["fecha de recepción"].ToString()))
                            query += "'" + dtCampos.Rows[0]["fecha de recepción"].ToString() + "',";
                        else
                            query += " null,";

                        query += "'" + dtCampos.Rows[0]["centro"].ToString() + "'); select SCOPE_IDENTITY();";

                        nuevoIdMetadato = DatabaseConnection.executeScalarInt(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider).ToString();

                    }
                }
                //Primero creamos la fila en la tabla TiposDocumento (si pasamos idMetadato actualiza)
                ActualizarDocumentosTipos(idTipoDocumentoNuevo, idDocumento, "", "Documentos");
                //Una vez tenemos la fila, al añadir el metadato y el tipo nos actualiza la fila que nos ha creado antes.
                ActualizarDocumentosTipos(idTipoDocumentoNuevo, idDocumento, nuevoIdMetadato, "Documentos");
                //ws.RenombrarDocumento(idDocumento);

                query = "update documentos set fechaUltimaModificacion='" + DateTime.Now + "' where idDocumento = " + idDocumento;
                string res = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider).ToString();

                return idDocumento;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("reasignarTipoSeparacion", "Indexacion", -1, e.ToString(), e.Message);
                return "";
            }
        }

        [WebMethod]
        public DataTable IndexacionDevolverTipoMetadatos(string token, string idDocumento)
        {
            token = Utilities.tratarParam(token);
            idDocumento = Utilities.tratarParam(idDocumento);
            DataTable dtTipo = new DataTable();
            if (Utilities.comprobarValidezUsuario(token) > 0)
            {
                string query = "select top 1 idMetadato,idTipoDocumento from tiposDocumento where idDoc = " + idDocumento + " and activo=1";
                dtTipo = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
            }
            dtTipo.TableName = "TipoMetadato";
            return dtTipo;
        }

        [WebMethod]
        public bool IndexacionGuardarMetadatos(string token, string idDocumento, string idTipoDocumento, string idMetadato, bool obligarIndexacion, DataTable dtMetadatos)
        {
            token = Utilities.tratarParam(token);
            idDocumento = Utilities.tratarParam(idDocumento);
            idTipoDocumento = Utilities.tratarParam(idTipoDocumento);
            idMetadato = Utilities.tratarParam(idMetadato);
            idTipoDocumento = Utilities.tratarParam(idTipoDocumento);

            string query = string.Empty;
            if (Utilities.comprobarValidezUsuario(token) > 0)
            {
                string idUsuario = token.Split('I')[0];
                string nombreTabla = "c00000000";
                bool error = false;
                nombreTabla = nombreTabla.Substring(0, nombreTabla.Length - idTipoDocumento.Length) + idTipoDocumento;
                query = "update " + nombreTabla + " set ";
                bool esNif = false, esValido = false, comprobado = false;
                string valorCampos = string.Empty, nombres = string.Empty, valores = string.Empty;
                bool indexado = false;
                for (int i = 0; i < dtMetadatos.Rows.Count; i++)
                {
                    string campo = dtMetadatos.Rows[i]["Nombre"].ToString(),
                        tipoCampo = dtMetadatos.Rows[i]["Tipo"].ToString(),
                        valor = dtMetadatos.Rows[i]["Valor"].ToString();

                    valores = valor + ",";
                    nombres += "[" + campo + "],";

                    if ((!string.IsNullOrEmpty(valor) && !comprobado && (campo.ToLower().Contains("nif") || campo.ToLower().Contains("dni") || campo.ToLower().Contains("nie")
                        || campo.ToLower().Contains("cif"))) && idUsuario != IdUsuarioMaestro)
                    {
                        esNif = true; comprobado = true;
                        esValido = (new ValidacionDocumento(valor)).EsCorrecto;
                        esValido = true;
                        valor = "'" + valor + "'";
                    }
                    else
                    {
                        switch (tipoCampo)
                        {
                            case "datetime":
                            case "date":
                                if (string.IsNullOrEmpty(valor)) valor = "null";
                                else
                                {
                                    if (!valor.Contains("/"))
                                        valor = valor.Substring(0, 4) + "/" + valor.Substring(4, 2) + "/" + valor.Substring(6, 2);
                                    DateTime temp;
                                    if (DateTime.TryParse(valor, out temp))
                                        valor = "'" + temp.Date + "'";
                                    else
                                        error = true;
                                }
                                break;
                            case "int":
                            case "numeric":
                            case "float":
                            case "double":
                                if (string.IsNullOrEmpty(valor)) valor = "null";
                                break;
                            default:
                                if (!tipoCampo.Equals("int")) valor = "'" + valor + "'";
                                break;
                        }
                    }

                    valorCampos += "[" + campo + "]=" + valor + ",";

                    // si para algún campo que no sea de los fijos, este contiene valor, es que se ha indexado.
                    if (!campo.ToLower().Equals("dni") && !campo.ToLower().Equals("matricula") && !campo.ToLower().Equals("fecha de recepción") && !campo.ToLower().Equals("centro")
                        && !string.IsNullOrEmpty(valor) && !valor.Equals("null") && !valor.Equals("''"))
                    {
                        indexado = true;
                    }
                }

                if (!string.IsNullOrEmpty(idMetadato))
                {
                    if (!string.IsNullOrEmpty(valorCampos))
                    {
                        if (valorCampos.EndsWith(","))
                            valorCampos = valorCampos.Substring(0, valorCampos.Length - 1);

                        query += valorCampos + " where idMetadato = " + idMetadato;
                        if ((!esNif || (esNif && esValido)) && !error)
                        {
                            int update = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                            if (indexado || obligarIndexacion)
                            {
                                query = "update tiposDocumento set indexado=1 where activo=1 and idDoc=" + idDocumento;
                                update = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                                if (update < 0) error = true;
                                else
                                {
                                    query = "update documentos set fechaUltimaModificacion='" + DateTime.Now + "' where idDocumento = " + idDocumento;
                                    update = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                                }
                                activacionDocumentoYExpediente(idDocumento);
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else
                            return false;
                    }
                    else
                        return false;
                }
                else
                {
                    query = "insert into " + nombreTabla + " (" + nombres + ") VALUES (" + valores + "); select SCOPE_IDENTITY();";
                    string nuevoIdMetadato = DatabaseConnection.executeScalarInt(query, CommandType.Text, "").ToString();
                    if (!string.IsNullOrEmpty(nuevoIdMetadato))
                    {
                        query = "update TiposDocumento set idMetadato=" + nuevoIdMetadato + " where idDoc=" + idDocumento + " and activo=1";
                        string res = DatabaseConnection.executeScalarInt(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider).ToString();
                        if (!string.IsNullOrEmpty(res))
                        {
                            if ((!esNif || (esNif && esValido)) && !error)
                            {
                                if (indexado || obligarIndexacion)
                                {
                                    query = "update tiposDocumento set indexado=1 where activo=1 and idDoc=" + idDocumento + " and idMetadato = " + nuevoIdMetadato;
                                    int update = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                                    if (update < 0) error = true;
                                    else
                                    {
                                        query = "update documentos set fechaUltimaModificacion='" + DateTime.Now + "' where idDocumento = " + idDocumento;
                                        update = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                                    }
                                    activacionDocumentoYExpediente(idDocumento);
                                }
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }
        }

        private static string activacionDocumentoYExpediente(string idDoc)
        {
            string res = string.Empty;
            try
            {
                string query = @"select documentos.idDocumento, documentos.activo as dActivo, Expedientes.activo as eActivo, Expedientes.idexpediente from documentos 
                          left join DocumentosExpedientes on DocumentosExpedientes.idDocumento = documentos.idDocumento
                          left join Expedientes on Expedientes.idexpediente = DocumentosExpedientes.idexpediente
                          where documentos.idDocumento = " + idDoc;

                DataTable dtExpediente = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                if (dtExpediente.Rows.Count > 0)
                {
                    bool expedienteActivo = (!Convert.IsDBNull(dtExpediente.Rows[0]["eActivo"]) ? dtExpediente.Rows[0]["eActivo"].ToString().Equals("True") : false);

                    if (!expedienteActivo)
                    {
                        query = "update expedientes set activo = 1 where idExpediente = " + dtExpediente.Rows[0]["idExpediente"].ToString();
                        int resUpdate = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                    }
                }

                if (dtExpediente.Rows.Count > 0)
                {
                    bool documentoActivo = (!Convert.IsDBNull(dtExpediente.Rows[0]["dActivo"]) ? dtExpediente.Rows[0]["dActivo"].ToString().Equals("True") : false);

                    if (!documentoActivo)
                    {
                        query = "update documentos set activo = 1, fechaUltimaModificacion='" + DateTime.Now + "' where idDocumento = " + idDoc;
                        res = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider).ToString();
                    }
                }

                return res;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("activacionDocumentoYExpediente", "Documentos", -1, e.ToString(), e.Message);
                return "-1";
            }
        }

        [WebMethod]
        public DataSet IndexacionDevolverTiposCamposDatos(string token, string idDocumento)
        {
            token = Utilities.tratarParam(token);
            idDocumento = Utilities.tratarParam(idDocumento);

            DataSet dsTablas = new DataSet();
            string query = "select idTipoDocumento, indexado from TiposDocumento where idDoc = " + idDocumento + " and activo=1";

            dsTablas.Tables.Add(DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider));

            if (dsTablas.Tables[0].Rows.Count > 0)
            {
                string idTipo = dsTablas.Tables[0].Rows[0]["idTipoDocumento"].ToString();
                string nombreTabla = "c00000000";
                nombreTabla = nombreTabla.Substring(0, nombreTabla.Length - idTipo.Length) + idTipo;
                query = "exec sys.sp_columns[" + nombreTabla + "]";
                dsTablas.Tables.Add(DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider));

                query = "select * from " + nombreTabla + " c where exists (select idMetadato from TiposDocumento ts where ts.activo=1 " +
                                   "and ts.idDoc=" + idDocumento + " and ts.idMetadato=c.IdMetaDato)";
                dsTablas.Tables.Add(DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider));
            }

            dsTablas.DataSetName = "Tablas";
            return dsTablas;
        }

        [WebMethod]
        public DataTable IndexacionDevolverDatosEmpresaUsuario(string token)
        {
            token = Utilities.tratarParam(token);

            DataTable dtDatos = new DataTable();
            if (Utilities.comprobarValidezUsuario(token) > 0)
            {
                string idUsuario = token.Split('I')[0];
                string query = @"select top 1 ue.idEmpresa, u.email, u.password from UsuarioEmpresa ue
                              inner join usuarios u on u.idUsuario = ue.idUsuario
                              where ue.idUsuario = " + idUsuario;
                dtDatos = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
            }
            dtDatos.TableName = "Datos";
            return dtDatos;

        }

        [WebMethod]
        public string IndexacionDevolverIdExpedienteDocumento(string token, string idDocumento)
        {
            token = Utilities.tratarParam(token);
            idDocumento = Utilities.tratarParam(idDocumento);

            DataTable dtDatos = new DataTable();
            if (Utilities.comprobarValidezUsuario(token) > 0)
            {
                return DatabaseConnection.executeScalarString("select top 1 idExpediente from documentosExpedientes where idDocumento = " + idDocumento,
                        CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
            }
            else
                return string.Empty;
        }

        [WebMethod]
        public string IndexacionEliminarDocumento(string token, string idDocumento)
        {
            token = Utilities.tratarParam(token);
            idDocumento = Utilities.tratarParam(idDocumento);

            string resp = "-1";
            if (Utilities.comprobarValidezUsuario(token) > 0)
            {
                string idUsuario = token.Split('I')[0];
                string query = "update documentos set activo = 0 where activo = 1 and idUsuario = " + idUsuario + " and idDocumento = " + idDocumento;
                resp = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider).ToString();

                query = "update tiposDocumento set activo = 0 where activo = 1 and idDoc = " + idDocumento;
                resp = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider).ToString();
            }
            return resp;
        }

        [WebMethod]
        public int IndexacionActualizarDocumentoSubido(string token, string idDocumento, string idDocumentoAntiguo, string idCentro, string idTipoDocumento)
        {
            token = Utilities.tratarParam(token);
            idDocumento = Utilities.tratarParam(idDocumento);
            idDocumentoAntiguo = Utilities.tratarParam(idDocumentoAntiguo);
            idCentro = Utilities.tratarParam(idCentro);
            idTipoDocumento = Utilities.tratarParam(idTipoDocumento);

            int updateRes = -1;
            string query = string.Empty;
            if (Utilities.comprobarValidezUsuario(token) > 0)
            {
                if (!string.IsNullOrEmpty(idCentro))
                {
                    query = "update documentos set idCentro = " + idCentro + "where iddocumento = " + idDocumento;
                    updateRes = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                }
                // Copiamos sus metadatos ya introducidos al nuevo tipo
                IndexacionReasignarTipoSeparacion(idDocumentoAntiguo, idDocumento, idTipoDocumento);

                query = "update documentos set fechaUltimaModificacion='" + DateTime.Now + "' where iddocumento = " + idDocumento;
                updateRes = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
            }
            return updateRes;
        }

        [WebMethod]
        public DataSet IndexacionDevolverPendientesTipos(string token, string idCentro)
        {

            token = Utilities.tratarParam(token);
            idCentro = Utilities.tratarParam(idCentro);

            DataSet dsPendTipos = new DataSet();
            string query = string.Empty, conCentro = string.Empty;

            if (Utilities.comprobarValidezUsuario(token) > 0)
            {
                string idUsuario = token.Split('I')[0];
                if (idCentro.Equals("0"))
                    conCentro = "";
                else if (idCentro.Equals("-1"))
                    conCentro = "and (idCentro is null)";
                else
                    conCentro = "and (idCentro = " + idCentro + ")";

                query = @"select documentos.idDocumento, TipoDocumento.Descripcion from Documentos 
                                    left join TiposDocumento on TiposDocumento.iddoc = documentos.idDocumento and TiposDocumento.Activo=1
                                    left join TipoDocumento on TipoDocumento.idTipoDocumento = TiposDocumento.idTipoDocumento
                                    where exists  (select * from DocumentosUsuarios where idUsuario = " + idUsuario + @" and exists 
                                      (select * from TiposDocumento where TiposDocumento.activo=1  and TiposDocumento.indexado = 0  
                                      and TiposDocumento.idDoc=DocumentosUsuarios.iddocumento)  
                                    and Documentos.idDocumento=DocumentosUsuarios.iddocumento) " + conCentro + " order by documentos.iddocumento asc";

                dsPendTipos.Tables.Add(DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider));

                query = "select * from tipoDocumento where (Padre = 280 or Padre = 281) and idUsuario = " + idUsuario + " and activo = 1 order by Descripcion asc";
                dsPendTipos.Tables.Add(DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider));
            }

            dsPendTipos.DataSetName = "PendTipos";
            return dsPendTipos;
        }

        [WebMethod]
        public string IndexacionDevolverRutaVisualizacionDocumento(string token, string idDocumento)
        {
            token = Utilities.tratarParam(token);
            idDocumento = Utilities.tratarParam(idDocumento);
            if (Utilities.comprobarValidezUsuario(token) > 0)
            {
                idDocumento = Utilities.tratarParam(idDocumento);
                string path = WebConfigurationManager.AppSettings["Ruta"].ToString().ToLower();
                path = path.Replace("/iis/c4/signespro/", "https://carrefour-bizlayer.com/zdocs-pro"); //FNMT CARREFOUR
                path = path.Replace("c:/inetpub/wwwroot/zdocs w2m/zdocs/", "https://w2m.signes30.com/zdocs");//W2M LOCAL
                path = path.Replace("c:/inetpub/wwwroot/signespro/", "https://demos.signes30.com/SignesPro"); //LOCAL
                return path + "/visor/web/verDocumento.ashx?idDoc=" + idDocumento + "&Tipo=Documentos&token=" + token;
            }
            else
            {
                return "No tiene permisos.";
            }
        }

        [WebMethod]
        public byte[] IndexacionDevolverRutaDescargaDocumento(string token, string idDocumento)
        {
            token = Utilities.tratarParam(token);
            if (Utilities.comprobarValidezUsuario(token) > 0)
            {
                idDocumento = Utilities.tratarParam(idDocumento);
                string pathDoc = "00000000";
                pathDoc = pathDoc.Substring(0, pathDoc.Length - idDocumento.Length) + idDocumento;

                string path = WebConfigurationManager.AppSettings["RutaVolumenes"] +
                    pathDoc.Substring(0, 2) + @"\" + pathDoc.Substring(2, 2) + @"\" + pathDoc.Substring(4, 2) + @"\" + pathDoc.Substring(6, 2) + ".pdf";
                try
                {
                    if (File.Exists(path))
                        return File.ReadAllBytes(path);
                    else
                        return null;
                }
                catch (Exception e)
                {
                    Trazas t = new Trazas("IndexacionDevolverRutaDescargaDocumento", "WSZFK", Convert.ToInt32(token.Split('I')[0]), e.ToString(), e.Message);
                    return null;
                }

            }
            else
            {
                return null;
            }
        }

        [WebMethod]
        public DataTable IndexacionInfoDocumento(string token, string idDocumento)
        {
            token = Utilities.tratarParam(token);
            idDocumento = Utilities.tratarParam(idDocumento);
            DataTable dtTipo = new DataTable();

            if (Utilities.comprobarValidezUsuario(token) > 0)
            {
                string query = @"select d.docOriginal, d.fechaCreacion, d.fechaUltimaModificacion, d.activo as dActivo
, c.Descripcion as cDesc, c.idCentro as idCentroCentro
, e.descripcion as exDesc, e.Activo as exActivo, e.IdCentro as exIdCentro
, td.idTipoDocumento, td.Descripcion as tdDesc
, tsd.idTiposDocumento, tsd.Activo as tsdActivo, tsd.indexado, tsd.idMetadato
  from documentos d
  left join TiposDocumento tsd on tsd.idDoc = d.iddocumento
  left join TipoDocumento td on td.idTipoDocumento = tsd.idTipoDocumento
  left join centro c on c.idCentro = d.idCentro
  left join DocumentosExpedientes de on de.iddocumento = d.idDocumento
  left join Expedientes e on e.idexpediente = de.idexpediente
  where d.idDocumento = " + idDocumento + " order by d.idDocumento asc";

                dtTipo = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
            }
            dtTipo.TableName = "Tipo";
            return dtTipo;
        }

        [WebMethod]
        public DataSet IndexacionInfoCamposValoresTipo(string token, string idTipoDocumento)
        {
            token = Utilities.tratarParam(token);
            idTipoDocumento = Utilities.tratarParam(idTipoDocumento);
            DataSet dsCamposValor = new DataSet();

            string query = string.Empty;
            if (Utilities.comprobarValidezUsuario(token) > 0)
            {
                string tabla = "c00000000";
                tabla = tabla.Substring(0, tabla.Length - idTipoDocumento.Length) + idTipoDocumento.ToString();
                query = "exec sys.sp_columns[" + tabla + "]";

                dsCamposValor.Tables.Add(DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider));

                query = "select *  from " + tabla + " where idMetadato=" + idTipoDocumento;
                dsCamposValor.Tables.Add(DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider));

            }

            dsCamposValor.DataSetName = "CamposValor";
            return dsCamposValor;
        }

        [WebMethod]
        public string IndexacionInsertarDocumentoExpediente(string token, string idDocumento, string idExpediente)
        {
            token = Utilities.tratarParam(token);

            if (Utilities.comprobarValidezUsuario(token) > 0)
            {
                idDocumento = Utilities.tratarParam(idDocumento);
                idExpediente = Utilities.tratarParam(idExpediente);
                try
                {
                    string Consulta = "insert into documentosexpedientes (iddocumento,idexpediente) values ('" + idDocumento + "','" + idExpediente + "')";
                    return DatabaseConnection.executeNonQueryInt(Consulta, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider).ToString();
                }
                catch (Exception e)
                {
                    Trazas t = new Trazas("InsertarDocumentoExpediente", "WSZFK", -1, e.ToString(), e.Message);
                    return "-1";
                }
            }

            return "-1";
        }

        [WebMethod]
        public string IndexacionSubirFicheroZDocsPro(string token, Byte[] Fichero, string NomFichero, string idExpediente, string idempresa)
        {
            string resultado = string.Empty;
            token = Utilities.tratarParam(token);

            if (Utilities.comprobarValidezUsuario(token) > 0)
            {
                NomFichero = Utilities.tratarParam(NomFichero);
                idExpediente = Utilities.tratarParam(idExpediente);
                idempresa = Utilities.tratarParam(idempresa);

                string idUsuario = token.Split('I')[0];
                try
                {
                    string Directorio = WebConfigurationManager.AppSettings["RutaVolumenes"];
                    if (Fichero != null)
                    {
                        String Fec = DateTime.Today.Year.ToString() + DateTime.Today.Month.ToString() + DateTime.Today.Day.ToString();
                        string Fecha = string.Format("{0:yyyyMMdd}", DateTime.Today);

                        string ObligadaLectura = "False";
                        byte[] kk = null;
                        String[] aux = NomFichero.Split('.');
                        String formato = aux[aux.Length - 1];
                        string err = "";

                        string idDoc = insertarDocEnBBDD(NomFichero, idUsuario, formato, idempresa, "", "");

                        string Ruta = "00000000";
                        Ruta = Ruta.Substring(0, 8 - idDoc.Length) + idDoc;
                        string pathString = Directorio + Ruta.Substring(0, 2) + "/" + Ruta.Substring(2, 2) + "/" + Ruta.Substring(4, 2) + "/";
                        System.IO.Directory.CreateDirectory(pathString);

                        File.WriteAllBytes(pathString + Ruta.Substring(6, 2) + "." + formato, Fichero);

                        if (!ExisteDocumentoUsuario(idDoc, idUsuario))
                            err = InsertarDocumentoenDocumentosUsuario(idDoc, idUsuario, "Documentos Propios", idUsuario, ObligadaLectura, "", "", "", "", idempresa);

                        if (!string.IsNullOrEmpty(idExpediente))
                        {
                            if (!ExisteDocumentoExpediente(idDoc, idExpediente))
                                InsertarDocumentoenExpediente(idDoc, idExpediente);

                            CompartirDocumentoaUsuariosExpediente(idDoc, idExpediente, idUsuario, ObligadaLectura, idempresa);
                        }
                        if (File.Exists(pathString + Ruta.Substring(6, 2) + "." + formato))
                            return idDoc;
                        else
                        {
                            return "-1";
                        }

                    }
                    return resultado;
                }
                catch (Exception e)
                {
                    Trazas t = new Trazas("IndexacionSubirFicheroZDocsPro", "WSZFK", Convert.ToInt32(idUsuario), e.ToString(), e.Message);
                    return "-1";
                }
            }

            return "-1";
        }

        [WebMethod]
        public int IndexacionDevolverNodoPadre(string token, int Padre)
        {

            token = Utilities.tratarParam(token);

            if (Utilities.comprobarValidezUsuario(token) > 0)
            {
                string idUsuario = token.Split('I')[0];
                int UltimoPadre = Padre;
                // Buscamos el nodo del padre para obtenerlo y subir por el arbol


                string query = "select top 1 Padre from TipoDocumento where idUsuario=" + idUsuario + " and idTipoDocumento=" + Padre;

                DataTable dt = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                if (dt.Rows.Count > 0)
                {
                    Padre = Convert.ToInt32(dt.Rows[0]["Padre"].ToString());

                    if (Padre != -1)
                    {
                        UltimoPadre = Padre;
                        return IndexacionDevolverNodoPadre(token, Padre);
                    }
                }
                return UltimoPadre;
            }
            else
            {
                return -1;
            }
        }

        [WebMethod]
        public DataTable IndexacionCargarTipoDocumentoAsociado(string token, string idDocumento)
        {

            token = Utilities.tratarParam(token);

            DataTable dt = new DataTable();

            if (Utilities.comprobarValidezUsuario(token) > 0)
            {
                string query = @"select top 1 idTipoDocumento, Descripcion from TipoDocumento
where exists (select idTiposDocumento from tiposdocumento where tipodocumento.idtipodocumento = tiposdocumento.idTipoDocumento and tiposdocumento.activo=1 and tiposdocumento.idDoc=" + idDocumento + ")";

                dt = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
            }

            dt.TableName = "Tipos";
            return dt;
        }

        [WebMethod]
        public int IndexaxionSustituirDocumento(string token, string idDocumento, byte[] bDocOld, byte[] bDocNew)
        {
            token = Utilities.tratarParam(token);
            string pathNew = string.Empty;
            string pathOld = string.Empty;
            Trazas t2 = new Trazas("PATHOLD = '" + pathOld + "'", "WSZFK", Convert.ToInt32(token.Split('I')[0]), "", "");

            if (Utilities.comprobarValidezUsuario(token) > 0)
            {
                try
                {
                    idDocumento = Utilities.tratarParam(idDocumento);

                    string pathDoc = "00000000";
                    pathDoc = pathDoc.Substring(0, pathDoc.Length - idDocumento.Length) + idDocumento;

                    // Reemplazamos el fichero original por el nuevo "machacamos"
                    pathNew = WebConfigurationManager.AppSettings["RutaVolumenes"] +
                        pathDoc.Substring(0, 2) + @"\" + pathDoc.Substring(2, 2) + @"\" + pathDoc.Substring(4, 2) + @"\" + pathDoc.Substring(6, 2) + ".pdf";

                    Trazas t = new Trazas("PATHNEW = " + pathNew, "WSZFK", Convert.ToInt32(token.Split('I')[0]), "", "");

                    File.WriteAllBytes(pathNew, bDocNew);


                    // Ahora guardamos una copia del antiguo para poder deshacer el cambio más adelante
                    pathOld = WebConfigurationManager.AppSettings["RutaVolumenes"] +
                        pathDoc.Substring(0, 2) + @"\" + pathDoc.Substring(2, 2) + @"\" + pathDoc.Substring(4, 2) + @"\" + pathDoc.Substring(6, 2) + "-copia.pdf";

                    t = new Trazas("PATHOLD = " + pathOld, "WSZFK", Convert.ToInt32(token.Split('I')[0]), "", "");

                    File.WriteAllBytes(pathNew, bDocOld);
                }
                catch (Exception e)
                {
                    Trazas t = new Trazas("IndexaxionSustituirDocumento", "WSZFK", Convert.ToInt32(token.Split('I')[0]), "nuevoDoc=" + pathNew + " ; viejoDoc=" + pathOld + " ; " + e.ToString(), e.Message);
                    return -1;
                }
            }

            return 1;
        }

        [WebMethod]
        public string IndexacionDevolverDocumentosTipo(string token, string idTipoDocumento, string inicio, string fin, string ordenacion, DataTable dtFiltro)
        {
            token = Utilities.tratarParam(token);
            string res = string.Empty;
            DataTable dt = new DataTable();
            if (Utilities.comprobarValidezUsuario(token) > 0)
            {
                string idUsuario = token.Split('I')[0];

                if (string.IsNullOrEmpty(ordenacion)) ordenacion = "asc";

                try
                {

                    if (dtFiltro == null)
                    {
                        string query = @";WITH temporal AS ( SELECT idDocumento, docOriginal, fechaCreacion, rowNum = ROW_NUMBER() OVER (ORDER BY idDocumento " + ordenacion + ") from documentos ";

                        if (!string.IsNullOrEmpty(idTipoDocumento))
                        {
                            query += "where activo = 1 and exists (select idDoc from tiposDocumento where documentos.idDocumento = tiposdocumento.idDoc and idTipoDocumento = " + idTipoDocumento + @" 
                                        and exists (select idUsuario from documentosUsuarios where documentosUsuarios.idDocumento = tiposDocumento.idDoc and idUsuario = " + idUsuario + ")))";
                        }
                        else
                        {
                            query += "where activo = 1 and exists (select idUsuario from documentosUsuarios where documentosUsuarios.idDocumento = documentos.idDocumento and idUsuario = " + idUsuario + "))";
                        }

                        query += "SELECT idDocumento, docOriginal, fechaCreacion from temporal where rowNum between " + inicio + " AND " + fin;

                        dt = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                    }
                    else
                    {
                        dt = dtFiltro;
                    }


                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        res += dt.Rows[i]["idDocumento"] + "^" + dt.Rows[i]["docOriginal"] + "^" + dt.Rows[i]["fechaCreacion"];
                        if (i < dt.Rows.Count - 1) res += "|";
                    }
                }
                catch (Exception e)
                {
                    Trazas t = new Trazas("IndexaxionDevolverDocumentoTipo", "WSZFK", Convert.ToInt32(token.Split('I')[0]), e.ToString(), e.Message);
                    return "-1";
                }
            }
            return res;
        }
        [WebMethod]
        public string DevolverPadre(double Nodo)
        {
            try
            {
                string SQL = "select padre from tipodocumento where idtipodocumento=" + Nodo;
                string res = DatabaseConnection.executeScalarString(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                return res.ToString();
            }
            catch (Exception e)
            {
                return "-1";
            }
        }
        [WebMethod]
        public string DevolverIdUsuarioPorEmail(string Email, string DescripcionEmpresa)
        {
            try
            {
                string SQL = "select usu.Idusuario from usuarios usu inner join usuarioempresa usuemp on usu.idusuario=usuemp.idusuario inner join Empresa Emp on emp.idempresa=usuemp.idempresa where usu.email='" + Email + "' and emp.descripcion='" + DescripcionEmpresa + "'";
                string res = DatabaseConnection.executeScalarString(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                return res.ToString();
            }
            catch (Exception e)
            {
                return "-1";
            }
        }
        [WebMethod]
        public string IndexacionDevolverExpedientesTipo(string token, string idTipoExpediente, string inicio, string fin, string ordenacion)
        {
            token = Utilities.tratarParam(token);
            string res = string.Empty;
            DataTable dt = new DataTable();
            if (Utilities.comprobarValidezUsuario(token) > 0)
            {
                string idUsuario = token.Split('I')[0];

                if (string.IsNullOrEmpty(ordenacion)) ordenacion = "asc";

                try
                {
                    string query = @";WITH temporal AS ( SELECT idExpediente, descripcion, fecha, rowNum = ROW_NUMBER() OVER (ORDER BY idExpediente " + ordenacion + ") from expedientes ";

                    if (!string.IsNullOrEmpty(idTipoExpediente))
                    {
                        query += "where activo = 1 and exists (select idExpediente from tiposExpediente where expedientes.idExpediente = tiposExpediente.idExp and idTipoExpediente = " + idTipoExpediente + @" 
                                    and exists (select idUsuario from ExpedientesUsuarios where expedientesUsuarios.idExpediente = tiposExpediente.idExp and idUsuario = " + idUsuario + ")))";
                    }
                    else
                    {
                        query += "where activo = 1 and exists (select idUsuario from ExpedientesUsuarios where ExpedientesUsuarios.idExpediente = Expedientes.idExpediente and idUsuario = " + idUsuario + "))";
                    }

                    query += "SELECT idExpediente, descripcion, fecha from temporal where rowNum between " + inicio + " AND " + fin;

                    dt = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        res += dt.Rows[i]["idExpediente"] + "^" + dt.Rows[i]["descripcion"] + "^" + dt.Rows[i]["fecha"];
                        if (i < dt.Rows.Count - 1) res += "|";
                    }
                }
                catch (Exception e)
                {
                    Trazas t = new Trazas("IndexaxionDevolverDocumentoTipo", "WSZFK", Convert.ToInt32(token.Split('I')[0]), e.ToString(), e.Message);
                    return "-1";
                }
            }
            return res;
        }

        [WebMethod]
        public string IndexacionDevolverCampoTablaMatriculas(string Matricula, string CampoBuscado)
        {
            string SQL = "Select " + CampoBuscado + " from Matriculas where Matricula='" + Matricula + "'";

            System.Data.DataTable DT = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
            string Resul = "";
            if (DT.Rows.Count > 0)
            {
                Resul = DT.Rows[0]["" + CampoBuscado + ""].ToString();
            }
            return Resul;
        }

        [WebMethod]
        public double IndexacionDevolverIdMetadatodeTipoActivo(Double IdDocumento)
        {
            string SQL = "select * from TiposDocumento where  Activo=1 and idDoc= " + IdDocumento;

            System.Data.DataTable DT = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
            Double Resul = 0;
            if (DT.Rows.Count > 0)
            {
                Resul = Convert.ToDouble(DT.Rows[0]["IdMetadato"]);
            }
            return Resul;
        }

        [WebMethod]
        public int IndexacionActualizarMetadatos(Double IdDocumento, string Tabla, Double IdMetaDato, string Matricula, string Dni)
        {
            string SQL = "select * from " + Tabla + " where idmetadato=" + IdMetaDato;
            SQL = "Update " + Tabla + " set Matricula='" + Matricula + "',Dni='" + Dni + "' where Idmetadato=" + IdMetaDato;
            int res = DatabaseConnection.executeNonQueryInt(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
            // System.Data.DataTable DT = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, CadenaConexionAnywhereSinProvider);


            return res;
        }

        [WebMethod]
        public string IndexacionDevolverMetadatosTipo(string token, string idTipo, string repositorio)
        {
            token = Utilities.tratarParam(token);
            idTipo = Utilities.tratarParam(idTipo);
            repositorio = Utilities.tratarParam(repositorio);

            string res = string.Empty;

            if (Utilities.comprobarValidezUsuario(token) > 0 && !string.IsNullOrEmpty(repositorio))
            {
                string idUsuario = token.Split('I')[0];
                string query = string.Empty;
                try
                {
                    string tabla = (repositorio.ToLower().Equals("documentos") ? "c00000000" : "e00000000");

                    tabla = tabla.Substring(0, tabla.Length - idTipo.Length) + idTipo;

                    query = "exec sp_columns[" + tabla + "]";
                    DataTable dt = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                    for (int i = 1; i < dt.Rows.Count; i++)
                    {
                        res += dt.Rows[i]["column_name"] + "^" + dt.Rows[i]["type_name"];
                        if (i < (dt.Rows.Count - 1))
                            res += "|";
                    }
                }
                catch (Exception e)
                {
                    Trazas t = new Trazas("IndexacionDevolverMetadatosTipo", "WSZFK", Convert.ToInt32(idUsuario), e.ToString(), e.Message);
                    return "-1";
                }
            }
            else
                return "-1";

            return res;
        }

        [WebMethod]
        public string IndexacionDevolverDocumentosTipoFiltro(string token, string idTipo, string inicio, string fin, string ordenacion, string repositorio, string filtrosGenerales, string filtrosMetadatos)
        {
            token = Utilities.tratarParam(token);
            idTipo = Utilities.tratarParam(idTipo);
            repositorio = Utilities.tratarParam(repositorio);

            string res = string.Empty;

            if (Utilities.comprobarValidezUsuario(token) > 0 && !string.IsNullOrEmpty(repositorio))
            {
                string idUsuario = token.Split('I')[0];
                string idEmpresa = DevolverIdEmpresa(idUsuario);
                string query = string.Empty;
                try
                {
                    string[] parametros = filtrosGenerales.Split('|');
                    string resultadoFiltroNombreDoc = string.Empty, resultadoFiltrosGenerales = string.Empty;

                    for (int i = 0; i < parametros.Length; i++)
                    {
                        switch (parametros[i].Split('^')[0])
                        {
                            case "filtroNombreDoc":
                                resultadoFiltroNombreDoc = tratarParametrosBusquedaAvanzada(idUsuario, idEmpresa, parametros[i], idTipo);
                                break;
                            default:
                                resultadoFiltrosGenerales += tratarParametrosBusquedaAvanzada(idUsuario, idEmpresa, parametros[i], idTipo);
                                break;
                        }
                    }

                    string resultadoFiltrosMetadatos = string.Empty;
                    if (!string.IsNullOrEmpty(filtrosMetadatos))
                    {
                        parametros = filtrosMetadatos.Split('|');
                        for (int i = 0; i < parametros.Length; i++)
                        {
                            resultadoFiltrosMetadatos += tratarParametrosBusquedaAvanzada(idUsuario, idEmpresa, parametros[i], idTipo);
                        }
                    }

                    string innerTipoDocumento = string.Empty, condicionTipoDocumento = string.Empty;

                    if (!idTipo.Equals("undefined") && !string.IsNullOrEmpty(idTipo))
                    {
                        innerTipoDocumento = " INNER JOIN TiposDocumento on d.idDocumento=TiposDocumento.idDoc";
                        condicionTipoDocumento = " and idTipoDocumento='" + idTipo + "' ";
                    }

                    if (string.IsNullOrEmpty(ordenacion)) ordenacion = "asc";

                    query = @";WITH temporal AS ( select d.idDocumento, d.docOriginal, d.FechaCreacion, d.idusuariopropietario, d.Compartido,
                                 e.Nombre,
                                 du.hasta,du.desde,du.visualizado,du.LecturaObligatoria, 
                                rowNum = ROW_NUMBER() OVER (ORDER BY d.idDocumento " + ordenacion + @")
                          from documentos d 
                              inner join documentosUsuarios du on du.iddocumento = d.iddocumento
                              inner join empresa e on e.idEmpresa = du.idempresausuariocomparte " +
                              innerTipoDocumento + " where d.activo = 1 " + condicionTipoDocumento + " " + resultadoFiltroNombreDoc + resultadoFiltrosGenerales + @"
                          " + resultadoFiltrosMetadatos;
                    query += @" ) select idDocumento, docOriginal, FechaCreacion, idusuariopropietario, Compartido,
                                 Nombre,
                                 hasta, desde, visualizado, LecturaObligatoria from temporal where rowNum between " + inicio + " AND " + fin;

                    DataTable dt = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);




                    return IndexacionDevolverDocumentosTipo(token, idTipo, inicio, fin, ordenacion, dt);
                }
                catch (Exception e)
                {
                    Trazas t = new Trazas("IndexacionDevolverMetadatosTipo", "WSZFK", Convert.ToInt32(idUsuario), e.ToString(), e.Message);
                    return "-1";
                }
            }
            else
                return "-1";
        }

        private static string tratarParametrosBusquedaAvanzada(string idUsuario, string idEmpresa, string parametrosFiltro, string numTipo)
        {
            /*
             * Parametros generales:
             *      - 0: nombre del filtro
             *      - 1: valor del filtro
             *      - 2: condición --> and, or
             *      - 3: operador  --> >,<, like, <=...
             *      
             *  Parámetros Metadatos
             *      - 0: nombre del filtro
             *      - 1: valor del filtro
             *      - 2: condición --> and, or 
             *      - 3: operador  --> >,<, like, <=...
             *      - 4: tipo del metadato, el nombre del campo
             */
            string[] param = null;
            string query = string.Empty;
            try
            {
                WSZFK.Service1 wsz = new WSZFK.Service1();

                if (!string.IsNullOrEmpty(parametrosFiltro))
                {
                    param = parametrosFiltro.Split('^');

                    switch (Utilities.tratarParam(param[0]))
                    {
                        case "filtroComparte":
                            query += " and exists (select iddocumento from documentosUsuarios du where du.idDocumento = d.idDocumento and du.idUsuarioComparte " +
                                tratarOperador(Utilities.tratarParam(param[2]), Utilities.tratarParam(param[1])) + ")";
                            break;
                        case "filtroCompartido":
                            // Si el usuario no ha indicado a nadie como usuario que ha compartido el documento, le ponemos a el como usuario que comparte
                            // Si no saldrán documentos que otros usuarios han compartido
                            if (!parametrosFiltro.Contains("filtroComparte"))
                            {
                                query += " and exists (select du.iddocumento from documentosUsuarios du where du.idDocumento = d.idDocumento and du.idUsuarioComparte = " + idUsuario + ")";
                            }
                            query += " and exists (select du.iddocumento from documentosUsuarios du where du.idDocumento = d.idDocumento and du.idUsuario " +
                                tratarOperador(Utilities.tratarParam(param[2]), Utilities.tratarParam(param[1])) + ")";
                            break;
                        case "filtroNombreDoc":
                            query += " and d.docOriginal " + tratarOperador(Utilities.tratarParam(param[2]), Utilities.tratarParam(param[1])) + " ";
                            break;
                        case "filtroFechaCreacion":
                            query += " and d.fechaCreacion = " + tratarOperador(Utilities.tratarParam(param[2]), Utilities.tratarParam(param[1])) + " ";
                            break;
                        case "filtroDesde1":
                            query += " and exists (select du.iddocumento from documentosUsuarios du where du.iddocumento = d.iddocumento and du.desde is not null and du.desde " +
                                tratarOperador(Utilities.tratarParam(param[2]), Utilities.tratarParam(param[1])) + ") ";
                            break;
                        case "filtroHasta1":
                            query += " and exists (select du.iddocumento from documentosUsuarios du where du.iddocumento = d.iddocumento and du.hasta is not null and du.hasta " +
                                tratarOperador(Utilities.tratarParam(param[2]), Utilities.tratarParam(param[1])) + ") ";
                            break;
                        case "filtroMisDocumentos":
                            query += " and d.idUsuarioPropietario = " + idUsuario + " ";
                            break;
                        case "filtroTextoLibre":
                            int utilizarCatalogo = Convert.ToInt32(WebConfigurationManager.AppSettings["UtilizarCatalogos"]);
                            if (utilizarCatalogo == 0)
                            {
                                query += " and exists (select do.iddocumento from documentosOCR do where do.iddocumento = d.iddocumento and do.textoOCR LIKE '%" + Utilities.tratarParam(param[1]) + "%') ";
                            }
                            else
                            {
                                query += " and exists (select do.iddocumento from documentosOCR do where do.iddocumento = d.iddocumento and CONTAINS(do.textoOCR,'" + Utilities.tratarParam(param[1]) + "')) ";
                            }
                            break;
                        case "filtroMetadatos":
                            string idTipoDocumento = Utilities.tratarParam(numTipo);

                            // Filtro cuando se ha seleccionado un tipo de documento concreto
                            if (!string.IsNullOrEmpty(idTipoDocumento))
                            {
                                string nombreTabla = "c00000000";
                                nombreTabla = nombreTabla.Substring(0, nombreTabla.Length - idTipoDocumento.Length) + idTipoDocumento;


                                query += @"and exists (select tsd.iddoc from tiposdocumento tsd where tsd.activo = 1 and tsd.iddoc = d.idDocumento and tsd.idTipoDocumento = " + idTipoDocumento +
                                               " and exists (select c.idmetadato from " + nombreTabla + " c where c.IdMetaDato = tsd.idMetadato ";
                                string operador = Utilities.tratarParam(param[3]);
                                if (string.IsNullOrEmpty(operador))
                                    operador = "and";
                                query += " " + operador + " c.[" + Utilities.tratarParam(param[4]) + "] " + tratarOperador(Utilities.tratarParam(param[2]), Utilities.tratarParam(param[1]));
                                query += "))";
                            }
                            else
                            {

                                string queryTables = "select * from sys.tables where name like '%c0%'  order by name asc";
                                DataTable dtTables = DatabaseConnection.executeNonQueryDT(queryTables, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                                // Si no tiene un tipo seleccionado pero sí que hay algún filtro por metadatos, está filtrando por los metadatos comunes
                                string tipos = wsz.DevolverListaTipos(idUsuario, idEmpresa, "-1");

                                //73^01.DOCUMENTOS PERSONALES^E|2^02.CONTRATOS DE TRABAJO Y DOCUMENTACION ADJUNTA^E|13^03.SEGURIDAD SOCIAL / MUTUA LABORAL^E|21^04.FISCALES^E|26^05.MOVIMIENTOS DEL TRABAJADOR EN LA EMPRESA^E|32^06.DATOS ECONÓMICOS^E|45^07.PERMISOS Y AUSENCIAS^E|56^08.DOCUMENTACIÓN DE BAJA EN LA EMPRESA^E|65^09.FORMACIÓN DEL TRABAJADOR^E|172^10.VARIOS^E|
                                if (!string.IsNullOrEmpty(tipos) && !tipos.Equals("-1"))
                                {
                                    if (tipos.EndsWith("|")) tipos = tipos.Substring(0, tipos.Length - 1);
                                    string todosLosTipos = string.Empty;
                                    string[] listaTipos = tipos.Split('|');
                                    for (int i = 0; i < listaTipos.Length; i++)
                                    {
                                        string idTipo = listaTipos[i].Split('^')[0];
                                        todosLosTipos += tratarHijosTipoDocumento(idUsuario, idEmpresa, idTipo);
                                    }
                                    string[] idTipos = todosLosTipos.Split('^');
                                    bool primerTipo = true;
                                    string[,] idTiposConFijos = devolverTiposConMetadatoFijo(idTipos, param[4]);
                                    //Tenemos todos los tipos y todas las tablas de la base de datos
                                    for (int i = 0; i < idTiposConFijos.Length / 2; i++)
                                    {
                                        string nombreTabla = "c00000000";
                                        nombreTabla = nombreTabla.Substring(0, nombreTabla.Length - idTiposConFijos[i, 0].Length) + idTiposConFijos[i, 0];

                                        // Para cada tipo de documento que tiene esa empresa/usuario, miramos si tiene una tabla con metadatos
                                        // Si la tiene añadimos el filtro de ese tipo, si no la ignoramos pq no existe la tabla
                                        if (tablaExiste(dtTables, nombreTabla) && Convert.ToBoolean(idTiposConFijos[i, 1]))
                                        {
                                            if (primerTipo)
                                            {
                                                query += "and (";
                                                primerTipo = false;
                                            }
                                            else
                                            {
                                                query += " or ";
                                            }

                                            query += @" exists (select tsd.iddoc from tiposdocumento tsd where tsd.activo = 1 and tsd.iddoc = d.idDocumento and tsd.idTipoDocumento = " +
                                                idTipos[i] + " and exists (select c.idmetadato from " + nombreTabla + " c where c.IdMetaDato = tsd.idMetadato ";

                                            string operador = Utilities.tratarParam(param[3]);
                                            if (string.IsNullOrEmpty(operador))
                                                operador = "and";
                                            query += " " + operador + " c.[" + Utilities.tratarParam(param[4]) + "] " + tratarOperador(Utilities.tratarParam(param[2]), Utilities.tratarParam(param[1]));
                                            query += @"))
                                                         ";
                                        }
                                    }
                                    query += ")";
                                }

                            }
                            break;
                    }
                }

                return query;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("tratarParametrosBusquedaAvanzada", "Documentos.svc", -1, e.ToString(), e.Message);
                return "";
            }
        }
        private static string[,] devolverTiposConMetadatoFijo(string[] idTipos, string nombreColumna)
        {
            try
            {
                string[,] tiposConMetadatosFijos = new string[idTipos.Length, 2];
                var query = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.COLUMNS where COLUMN_NAME='" + nombreColumna + "' AND TABLE_NAME LIKE 'C0%' ORDER by TABLE_NAME ASC";
                var dt = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);
                var c = dt.Rows.OfType<DataRow>().Select(dr => dr.Field<string>("TABLE_NAME")).ToList();
                var nameIdTipo = string.Empty;
                for (int i = 0; i < idTipos.Length; i++)
                {
                    string nombreTabla = "c00000000";
                    nameIdTipo = nombreTabla.Substring(0, nombreTabla.Length - idTipos[i].Length) + idTipos[i];

                    tiposConMetadatosFijos[i, 0] = nameIdTipo;
                    if (c.IndexOf(nameIdTipo) != -1)
                    {
                        tiposConMetadatosFijos[i, 1] = "true";
                    }
                    else
                    {
                        tiposConMetadatosFijos[i, 1] = "false";
                    }

                }

                return tiposConMetadatosFijos;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("devolverTiposConMetadatoFijo", "documentosNuevos", -1, e.Message, e.ToString());
                return null;
            }
        }

        private static bool tablaExiste(DataTable dtTables, string nombreTabla)
        {
            bool encontrada = false;
            try
            {
                if (dtTables != null && dtTables.Rows.Count > 0)
                {
                    for (int i = 0; i < dtTables.Rows.Count; i++)
                    {
                        if (dtTables.Rows[i]["name"].ToString().Equals(nombreTabla)) encontrada = true;
                    }
                }
                return encontrada;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("existeTabla", "documentosNuevos.aspx.cs", -1, e.ToString(), e.Message);
                return false;
            }
        }

        private static string tratarHijosTipoDocumento(string idUsuario, string idEmpresa, string idTipoDocumento)
        {
            WSZFK.Service1 wsz = new WSZFK.Service1();

            string respuesta = string.Empty;
            try
            {
                string tipos = wsz.DevolverListaTipos(idUsuario, idEmpresa, idTipoDocumento);
                if (!string.IsNullOrEmpty(tipos) && !tipos.Equals("-1"))
                {
                    string[] listaHijos = tipos.Split('|');
                    for (int i = 0; i < listaHijos.Length; i++)
                    {
                        string idTipo = listaHijos[i].Split('^')[0];
                        respuesta += tratarHijosTipoDocumento(idUsuario, idEmpresa, idTipo);
                        if (i < listaHijos.Length - 1) respuesta += "^";
                    }
                    respuesta += idTipoDocumento + "^";
                }
                else
                {
                    respuesta += idTipoDocumento;
                }

                return respuesta;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("tratarHijosTiposDocumento", "documentosNuevos.aspx.cs", -1, e.ToString(), e.Message);
                return respuesta;
            }
        }

        private static string tratarOperador(string operador, string valor)
        {
            string resutlado = string.Empty;
            if (operador.ToLower().Contains("like"))
                return (operador + " '%" + valor + "%'");
            else
                return (operador + " '" + valor + "'");
        }
        #endregion

        #region SubirFicheros
        [WebMethod]
        public string DevolverDescripcionExpediente(string IdExpediente)
        {
            try
            {
                string Consulta = "select descripcion from expedientes where idexpediente=" + IdExpediente;
                string Descripcion = DatabaseConnection.executeScalarString(Consulta, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);


                if (!string.IsNullOrEmpty(Descripcion))
                    return Descripcion;
                else
                    return "";
            }
            catch { return "error"; }
        }
        [WebMethod]
        public string ProcesarLineaCarrefourFormacion(byte[] Fichero, string NombreOriginalFichero, string NombreExpediente, string SQLMetadatos, Double IdUsuarioMaestro, string IdTipoDocumento, string IdCentro, Double IdEmpresa, int Activo)
        {
            try
            {

                if (string.IsNullOrEmpty(NombreExpediente))
                    NombreExpediente = "VARIOS-FORMACION";
                string IdExpediente = ExisteExpedienteUsuarioPorNombre(IdUsuarioMaestro.ToString(), NombreExpediente);//   .ExisteExpedienteUsuarioPorNombre(IdUsuarioMaestro.ToString(), NombreExpediente);
                if (string.IsNullOrEmpty(IdExpediente))
                {
                    IdExpediente = CrearNuevoExpedienteZDocs(NombreExpediente, IdUsuarioMaestro.ToString(), "");
                    CrearNuevoExpedienteUsuarioZDocs(IdExpediente, IdUsuarioMaestro.ToString());
                }
                string IdDocumento = existeDocumentoExpedientePorNombre(IdExpediente, NombreExpediente);
                if (string.IsNullOrEmpty(IdDocumento))
                {
                    IdDocumento = SubirFicheroZDocsPro(Fichero, NombreOriginalFichero, IdUsuarioMaestro.ToString(), IdExpediente, IdEmpresa.ToString());
                    string NombreFichero = DevolverDescripcionExpediente(IdExpediente);
                    string query = "update documentos set activo = " + Activo + ", docOriginal = '" + NombreFichero + "', nombreOriginal = '" + NombreOriginalFichero + "', idCentro = '" + IdCentro + "' where iddocumento = " + IdDocumento + " and idUsuario = " + IdUsuarioMaestro;
                    int res = DatabaseConnection.executeScalarInt(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                    if (!string.IsNullOrEmpty(SQLMetadatos))
                    {
                        var IdMetadato = DatabaseConnection.executeScalar(SQLMetadatos, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                        ActualizarDocumentosTipos(IdTipoDocumento, IdDocumento, "", "Documentos");
                        ActualizarDocumentosTipos(IdTipoDocumento, IdDocumento, IdMetadato.ToString(), "Documentos");
                    }
                    else
                    {
                        ActualizarDocumentosTipos(IdTipoDocumento, IdDocumento, "", "Documentos");
                        ActualizarDocumentosTipos(IdTipoDocumento, IdDocumento, "", "Documentos");
                    }
                    return "Documento CREADO, IdDocumento: " + IdDocumento;
                }
                else
                    return "Documento ya existente, IdDocumento: " + IdDocumento;
            }
            catch (Exception ex)
            {
                return "Error procesando la linea: " + ex.Message;
            }

        }

        [WebMethod]
        public string comprobarCentro(string matricula, string dni)
        {
            string datosCentro = "-1";
            try
            {
                if (!string.IsNullOrEmpty(dni))
                {
                    //ANTIGUA QUERY HACIENDO EN INNER POR EL NOMBRE DEL CENTRO
                    //string query = "select c.idCentro, c.Descripcion as nombreCentro from matriculas m inner join centro c on m.centro = c.descripcion where m.matricula = '" + matricula + "' and m.dni = '" + dni + "'";

                    //CAMBIO LA QUERY PARA HACER EL INNER POR EL CODIGO DE CENTRO DE CARREFOUR
                    string query = "select c.idCentro, c.Descripcion as nombreCentro from matriculas m inner join centro c on m.[CODIGO CENTRO] = c.CodCentroCarrefour where m.matricula = '" + matricula + "' and m.dni = '" + dni + "'";
                    DataTable dtCentro = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                    if (dtCentro.Rows.Count > 0)
                        datosCentro = dtCentro.Rows[0]["idCentro"].ToString() + "^" + dtCentro.Rows[0]["nombreCentro"].ToString();

                    // Si no obtenemos idCentro es pq no existe la fila en la tabla de matriculas y no podemos hacer nada pq se gestiona con un programa a parte
                }

                return datosCentro;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("comprobarExpediente", "SubirFicheroMetadatos/Form1", -1, e.ToString(), e.Message);
                return datosCentro;
            }
        }

        [WebMethod]
        public string comprobarExpediente(string matricula, string idCentro)
        {
            string idExpediente = "-1";
            try
            {
                if (!string.IsNullOrEmpty(matricula))
                {
                    string query = "select idExpediente from expedientes where descripcion = '" + matricula + "'";
                    idExpediente = DatabaseConnection.executeScalarString(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                    // Si no existe el expediente lo crearemos, de lo contrario lo devolvemos
                    if (string.IsNullOrEmpty(idExpediente) && !idExpediente.Equals("-1"))
                    {
                        query = "insert into Expedientes (descripcion, idUsuario, activo, fecha) values ('" + matricula + "', " + IdUsuarioMaestro + ", 'False','" + DateTime.Now + "'); select SCOPE_IDENTITY();";
                        idExpediente = DatabaseConnection.executeScalarInt(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider).ToString();

                        query = "insert into ExpedientesUsuarios (idExpediente, idUsuario) values ('" + idExpediente + "', " + IdUsuarioMaestro + "); select SCOPE_IDENTITY();";
                        string idExpUsu = DatabaseConnection.executeScalarInt(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider).ToString();

                        //compartirExpediente(idExpediente, idCentro);
                    }
                    else
                    {
                        //txtResultado.Text = txtResultado.Text + Environment.NewLine + Environment.NewLine + "---------------  Expediente ENCONTRADO: idExpediente: " + idExpediente;
                        //Application.DoEvents();
                    }
                }

                return idExpediente;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("comprobarExpediente", "SubirFicheroMetadatos/Form1", -1, e.ToString(), e.Message);
                return idExpediente;
            }
        }
        [WebMethod]
        public string existeDocumentoExpedientePorNombre(string idExpediente, string nombreOriginal)
        {
            try
            {
                string query = "select * from documentos where exists (select * from DocumentosExpedientes where DocumentosExpedientes.idexpediente = " + idExpediente +
                    " and DocumentosExpedientes.idDocumento = documentos.idDocumento) and NombreOriginal = '" + nombreOriginal + @"'";
                string idDocumento = DatabaseConnection.executeScalarString(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                return idDocumento;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("existeDocumentoExpediente", "SubirFicherosMetadatos/Form1", -1, e.ToString(), e.Message);
                return string.Empty;
            }
        }
        [WebMethod]
        public string comprobarDocumento(string nombreOriginal, string idExpediente, string matricula, string idTipoDocumento, string descripcionTipo, string idCentro, string ruta, string IdUsuario, int Activo, int Actualizacion)
        {
            string idDocumento = "-1";
            string TextoAMostrar = string.Empty;
            try
            {
                idDocumento = existeDocumentoExpedientePorNombre(idExpediente, nombreOriginal);
                if (string.IsNullOrEmpty(idDocumento))
                {

                    byte[] b = File.ReadAllBytes(ruta);
                    string NomDocumento = string.Empty;

                    // idDocumento = SubirFicheroZDocsPro(b, ".pdf", IdUsuario.ToString(), idExpediente,  IdEmpresaCarrefour.ToString());
                    if (IdUsuario.ToString() == IdUsuarioMaestro) //es RRHH
                        NomDocumento = matricula + "-" + descripcionTipo;
                    else
                        //es FORMACION,LE PONEMOS EL MISMO NOMBRE QUE TIENE EL EXPEDIENTE
                        NomDocumento = DevolverDescripcionExpediente(idExpediente); // nombreOriginal;


                    string query = "update documentos set activo = " + Activo + ", docOriginal = '" + NomDocumento + "', nombreOriginal = '" + nombreOriginal + "', idCentro = '" + idCentro + "', EsActualizacion='" + Actualizacion + "' where iddocumento = " + idDocumento + " and idUsuario = " + IdUsuario;
                    int res = DatabaseConnection.executeScalarInt(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                    TextoAMostrar = "Documento CREADO:" + idDocumento;

                }
                else
                    TextoAMostrar = "Documento ENCONTRADO:" + idDocumento;


                return TextoAMostrar;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("existeDocumento", "SubirFicheroMetadatos", -1, e.ToString(), e.Message);
                return idDocumento;
            }
        }

        //[WebMethod]
        //private string comprobarTiposDocumento(bool sinMetadatos, string idDocumento, string dni, string matricula, string nombreCentro, string idTipoDocumento, string idExpediente, string[] campos)
        //{
        //    string idMetadato = "-1";
        //    bool obligarIndexacion = false;
        //    try
        //    {
        //        string query = "select idMetadato from tiposdocumento where iddoc = " + idDocumento + " and idTipoDocumento = " + idTipoDocumento;
        //        idMetadato = DatabaseConnection.executeScalarString(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

        //        // si no existe ninguna relación entre el documento y el TipoDocumento la creamos insertando los metadatos y luego los en la tabla tiposDocumento
        //        if (string.IsNullOrEmpty(idMetadato) || idMetadato.Equals("-1"))
        //        {
        //            //wsz.ActualizarDocumentosTipos(idTipoDocumento, idDocumento, "", "Documentos");

        //            idMetadato = insertarMetadatos(sinMetadatos, dni, matricula, nombreCentro, idTipoDocumento, campos);

        //            // Sólo en caso de haber insertado los metadatos, comprobamos si es de los que se tienen que marcar como indexados obligatoriamente
        //            // De lo contrario se marcará por defecto sin indexar para que puedan revisarlo más adelante
        //            if (!string.IsNullOrEmpty(idMetadato) && !idMetadato.Equals("-1"))
        //            {
        //                obligarIndexacion = obligarIndexado(idTipoDocumento);
        //                //wsz.ActualizarDocumentosTipos(idTipoDocumento, idDocumento, idMetadato, "Documentos");
        //            }

        //            // Este insert sólo lo hará cuando previamente no haya encontrado ningún documento con este tipo
        //            // No debería existir ninguna fila con este idDoc ya que lo acabamos de crear. Por tanto no hay que desactivar ningun tiposDocumento anterior
        //            // En caso de que haya ido mal la inserción de metadatos, lo podrán ver igualmente sin datos
        //            query = "insert into tiposDocumento (idDoc, idTipoDocumento, idMetadato, indexado) " +
        //                    "values (" + idDocumento + ", " + idTipoDocumento + ", " + idMetadato + ", " + (obligarIndexacion ? "1" : "0") + "); select SCOPE_IDENTITY();";

        //            int res = DatabaseConnection.executeScalarInt(query, CommandType.Text, ConnectionString.strConnection);

        //            // si encontramos un documento del expediente que esté indexado activamos el expediente para que el usuario desde la web pueda ver ese documento buscando por el expediente
        //            if (obligarIndexacion)
        //            {
        //                query = "update expedientes set activo = 1 where idExpediente = '" + idExpediente + "' and idUsuario =" + IdUsuarioMaestro;
        //                int res2 = DatabaseConnection.executeScalarInt(query, CommandType.Text, ConnectionString.strConnection);
        //            }

        //            txtResultado.Text = txtResultado.Text + Environment.NewLine + "- TiposDocumento CREADO: tipo: " + idTipoDocumento + " - idTipos: " + res + " - idMetadato: " + idMetadato + " - indexado: " + obligarIndexacion;

        //            if (obligarIndexacion) txtResultado.Text += " - EXPEDIENTE ACTIVADO";

        //            Application.DoEvents();
        //        }
        //        else
        //        {
        //            txtResultado.Text = txtResultado.Text + Environment.NewLine + "- TiposDocumento ENCONTRADO: tipo: " + idTipoDocumento + " - idMetadato: " + idMetadato + " - indexado: " + obligarIndexacion;
        //            Application.DoEvents();
        //        }
        //        return idMetadato;
        //    }
        //    catch (Exception e)
        //    {
        //        Trazas t = new Trazas("comprobarTiposDocumento", "SubirFicheroMetadatos", -1, e.ToString(), e.Message);
        //        return idMetadato;
        //    }
        //}

        [WebMethod]
        public string insertarMetadatos(bool sinMetadatos, string dni, string matricula, string nombreCentro, string idTipoDocumento, string[] campos, int Formacion)
        {
            string idMetadato = "-1";
            string resul = string.Empty;
            try
            {
                string tabla = "c00000000";
                tabla = tabla.Substring(0, tabla.Length - idTipoDocumento.Length) + idTipoDocumento;

                string query = "exec sp_columns[" + tabla + "]";
                DataTable dtColumnas = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                int contador = 5;
                string nombres = string.Empty, valores = string.Empty;
                bool datoIntroducido = false;
                if (dtColumnas.Rows.Count > 0)
                {
                    string insert = "insert into " + tabla + " (";
                    // los dos primeros valores y los 3 últimos no son metadatos del array campos
                    for (int i = 1; i < dtColumnas.Rows.Count; i++)
                    {
                        // Así evitamos que coja valores que no son metadatos de los campos que nos pasan en el txt
                        if (contador < campos.Length - 3)
                        {
                            if (!campos[contador].ToLower().Equals("vacio") && !string.IsNullOrEmpty(campos[contador]))
                            {
                                switch (dtColumnas.Rows[i]["TYPE_NAME"].ToString().ToLower())
                                {
                                    case "int":
                                    case "numeric":
                                        valores += campos[contador];
                                        break;
                                    case "date":
                                    case "datetime":
                                        //if (idTipoDocumento.Equals("22") || idTipoDocumento.Equals("89") || idTipoDocumento.Equals("68") || idTipoDocumento.Equals("100") || idTipoDocumento.Equals("9"))
                                        //    valores += "'" + transformarFecha(campos[contador]) + "'";
                                        //else
                                        valores += "'" + campos[contador] + "'";
                                        break;
                                    default:
                                        valores += "'" + campos[contador] + "'";
                                        break;
                                }
                            }
                            else
                            {
                                if (dtColumnas.Rows[i]["TYPE_NAME"].ToString().ToLower().Equals("datetime") || dtColumnas.Rows[i]["TYPE_NAME"].ToString().ToLower().Equals("date"))
                                    valores += "null";
                                else
                                    valores += "''";
                            }
                            datoIntroducido = true;
                        }
                        else
                        {
                            if (Formacion == 1)
                            { }
                            else
                            {
                                switch (dtColumnas.Rows[i]["COLUMN_NAME"].ToString().ToLower())
                                {
                                    case "dni":
                                        valores += "'" + dni + "'"; datoIntroducido = true;
                                        break;
                                    case "matricula":
                                        valores += "'" + matricula + "'"; datoIntroducido = true;
                                        break;
                                    case "centro":
                                        valores += "'" + nombreCentro + "'"; datoIntroducido = true;
                                        break;
                                    case "fecha de recepción":
                                        valores += "'" + DateTime.Now + "'"; datoIntroducido = true;
                                        break;
                                }
                            }

                        }
                        // Si existe un dato tando de los que no son fijos o de los que sí entonces metemos el nombre de ese campo.
                        if (datoIntroducido)
                        {
                            nombres += "[" + dtColumnas.Rows[i]["COLUMN_NAME"].ToString() + "]";
                            if (i < dtColumnas.Rows.Count - 1)
                            {
                                nombres += ",";
                                valores += ",";
                            }
                            contador += 2;
                            datoIntroducido = false;
                        }


                    }
                    insert += nombres + ") values (" + valores + "); select SCOPE_IDENTITY()";
                    idMetadato = DatabaseConnection.executeScalarInt(insert, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider).ToString();

                    if (string.IsNullOrEmpty(idMetadato) || idMetadato.Equals("-1"))
                    {
                        idMetadato = guardarCamposFijos(dni, matricula, nombreCentro, idTipoDocumento);
                        if (string.IsNullOrEmpty(idMetadato) || idMetadato.Equals("-1"))
                            resul = Environment.NewLine + "- Error Metadatos NO SE HA GUARDADO NINGÚN METADATO. IdExpediente:" + idMetadato;
                        else
                            resul = Environment.NewLine + "- Error Metadatos SOLO GUARDADOS METADATOS FIJOS. IdExpediente:" + idMetadato;

                    }
                    else
                        resul = idMetadato;
                }

                return resul;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("insertarMetadatos", "SubirFicheroMetadatos/Form1", -1, e.ToString(), e.Message);
                return idMetadato;
            }
        }
        private string guardarCamposFijos(string dni, string matricula, string nombreCentro, string idTipoDocumento)
        {
            string idMetadato = "-1";
            try
            {
                string tabla = "c00000000";
                tabla = tabla.Substring(0, tabla.Length - idTipoDocumento.Length) + idTipoDocumento;

                string insert = "insert into " + tabla + " ([Dni],[matricula],[Fecha de recepción],[centro]) values ('" + dni + "', '" + matricula + "', '" + DateTime.Now + "', '" + nombreCentro + "'); select SCOPE_IDENTITY();";

                idMetadato = DatabaseConnection.executeScalarInt(insert, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider).ToString();

                return idMetadato;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("insertarMetadatos", "SubirFicheroMetadatos/Form1", -1, e.ToString(), e.Message);
                return idMetadato;
            }
        }
        [WebMethod]
        public string DesactivarDocumento(string IdDocumento)
        {
            try
            {
                int resultado;
                string Consulta = "update documentos set activo = 0 where iddocumento = " + IdDocumento;
                resultado = DatabaseConnection.executeScalarInt(Consulta, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                return resultado.ToString();
            }
            catch { return "error"; }
        }

        [WebMethod]
        public Double ExisteDocTipo(Double IdDocumento, int IdTipoDocumento)
        {
            string query = "select idMetadato from tiposdocumento where iddoc = " + IdDocumento + " and idTipoDocumento = " + IdTipoDocumento;
            string resul = DatabaseConnection.executeScalarString(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
            Double IdMetadato = Convert.ToDouble(string.IsNullOrEmpty(resul) ? "0" : resul);
            return IdMetadato;
        }
        [WebMethod]
        public string ActualizarDocumento(string IdDocumento, string IdUsuario, int Activo, string NombreDocumento, string NombreOriginal, string IdCentro, int EsActualizacion)
        {
            try
            {
                int resultado;
                string Consulta = "update documentos set activo = " + Activo + ", docOriginal = '" + NombreDocumento + "', nombreOriginal = '" + NombreOriginal + "', idCentro = '" + IdCentro + "', EsActualizacion='" + EsActualizacion + "' where iddocumento = " + IdDocumento + " and idUsuario = " + IdUsuario;
                resultado = DatabaseConnection.executeScalarInt(Consulta, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                return resultado.ToString();
            }
            catch { return "error"; }
        }
        [WebMethod]
        public string ActualizarPadre(string IdTipo, string Tabla, string IdNuevoPadre)
        {
            try
            {
                int resultado;
                string Consulta = string.Empty;
                if (Tabla.ToUpper() == "DOCUMENTOS")
                    Consulta = "update tipodocumento set padre = " + IdNuevoPadre + " where idtipoDocumento=" + IdTipo;
                else
                {
                    if (Tabla.ToUpper() == "EXPEDIENTES")
                    {
                        Consulta = "update tipoexpediente set padre = " + IdNuevoPadre + " where idtipoExpediente=" + IdTipo;
                    }
                }

                resultado = DatabaseConnection.executeScalarInt(Consulta, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                return resultado.ToString();
            }
            catch { return "error"; }
        }
        [WebMethod]
        public Boolean ExisteDocPorNombre(string NombreOriginal, int IdUsuario)
        {
            string query = "select IdDocumento from Documentos where NombreOriginal = '" + NombreOriginal + "' and idUsuario = '" + IdUsuario + "'";
            string resul = DatabaseConnection.executeScalarString(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider).ToString();
            Boolean Existe = (string.IsNullOrEmpty(resul) ? false : true);
            return Existe;
        }
        [WebMethod]
        public string DevolverIdDocumentoPorNombre(string NombreOriginal, int IdEmpresa)
        {
            string resul = string.Empty;
            if (!string.IsNullOrEmpty(NombreOriginal))
            {
                string query = "select IdDocumento from Documentos where NombreOriginal = '" + NombreOriginal + "' and idEmpresa = " + IdEmpresa;
                resul = DatabaseConnection.executeScalarString(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider).ToString();
                //Boolean Existe = (string.IsNullOrEmpty(resul) ? false : true);
            }
            return resul;
        }
        #endregion
        #region Exportacion
        [WebMethod]
        public int RenombrarExpediente(string IdExpediente, string NombreNUevo)
        {
            string SQL = "Update Expedientes set Descripcion='" + NombreNUevo + "' where IdExpediente=" + IdExpediente;
            int res = DatabaseConnection.executeScalarInt(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
            return res;
        }
        [WebMethod]
        public int DesactivarTipo(string IdTipo, string Tabla, int Activo)
        {
            string SQL = string.Empty;
            if (Tabla.ToUpper() == "DOCUMENTOS")
                SQL = "Update TipoDocumento set Activo=" + Activo + " where IdTipoDocumento=" + IdTipo;
            else
            {
                if (Tabla.ToUpper() == "EXPEDIENTES")
                    SQL = "Update TipoExpediente set Activo=" + Activo + " where IdTipoExpediente=" + IdTipo;
            }
            int res = DatabaseConnection.executeScalarInt(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
            return res;
        }
        [WebMethod]
        public int RenombrarTipoDocumento(string IdTipo, string NombreNUevo)
        {
            string SQL = "Update TipoDocumento set Descripcion='" + NombreNUevo + "' where IdTipoDocumento=" + IdTipo;
            int res = DatabaseConnection.executeScalarInt(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
            return res;
        }
        [WebMethod]
        public string ExisteTablaTipos(string NomTabla)
        {
            string SQl = "if exists(select * from sys.tables where name = '" + NomTabla + "')  select top 1 '1' as res from usuarios ELSE  select top 1 '0' as res from usuarios";
            //"SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_NAME = @c000000749";
            //"IF EXISTS (SELECT * FROM sysobjects WHERE type = 'U' AND name = '" + NomTabla + "') BEGIN PRINT 'Existe'  END ELSE  BEGIN PRINT 'no existe'  END";

            string res = DatabaseConnection.executeScalarString(SQl, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
            return res;
        }
        public string IndexacionInsertarLineaCampos(string fields, string Tabla, string IdTipoDocumento, string IdDocumento, string Tipo)
        {//
            try
            {

                string resultado;
                OleDbConnection vCon = new OleDbConnection(ConnectionString.CadenaConexionZDocs);
                string Consulta = "insert into " + Tabla;
                Boolean Primero = true;
                string sqlnombres = "", sqlvalores = "";
                string[] Metadatos = fields.Split('^');
                for (int i = 0; i < Metadatos.Count(); i++)
                {
                    if (!string.IsNullOrEmpty(Metadatos[i].Split('|')[0]))
                    {
                        if (!Primero)
                        {
                            sqlnombres += ",";
                            sqlvalores += ",";
                        }

                        sqlnombres += "[" + Metadatos[i].Split('|')[0] + "]";
                        if (!string.IsNullOrEmpty(Metadatos[i].Split('|')[3]))
                            sqlvalores += "'" + Metadatos[i].Split('|')[3] + "'";
                        else
                            sqlvalores += "Null";
                        //Consulta += "[" + field.Key + "]=" + "'" + field.Value + "'";
                        Primero = false;
                    }
                }
                Consulta += " (" + sqlnombres + ") values (" + sqlvalores + "); select SCOPE_IDENTITY()";
                OleDbCommand vComando = new OleDbCommand(Consulta, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                da.SelectCommand = vComando;
                resultado = vComando.ExecuteScalar().ToString();
                if (Tipo.ToUpper() == "EXPEDIENTES")
                    ActualizarDocumentosTipos(IdTipoDocumento, IdDocumento, resultado, "Expedientes");
                else
                    if (Tipo.ToUpper() == "DOCUMENTOS")
                    ActualizarDocumentosTipos(IdTipoDocumento, IdDocumento, resultado, "Documentos");
                else
                        if (Tipo.ToUpper() == "FORMULARIOS")
                    ActualizarDocumentosTipos(IdTipoDocumento, IdDocumento, resultado, "Formularios");

                vCon.Close();
                return "1";
            }
            catch { return "-1"; }
        }
        [WebMethod]
        public string IndexarDocumento(Documento Doc, string DocOriginal, string NombreOriginal, string EmailUsuario, string IdEmpresa, string IdExpediente, string DescripcionExpediente, string IdTipo, string DescripcionTipo, byte[] Fichero, string DescripcionTipoPadre, string IdTipoPadre, string Formato, string Fields, string ListaNodos, string DescripcionEmpresa, Centro Cen, Matricula Mat)
        {
            string res = string.Empty;
            //COMPROBAMOS EL EXPEDIENTE
            string IdUsuario = DevolverIdUsuarioPorEmail(EmailUsuario, DescripcionEmpresa); //METODO PASANDO IDEMPRESA
            IdEmpresa= DevolverIdEmpresa(IdUsuario);

            //COMENTO LA PARTE DE ACTUALIZACION DE MATRICULAS POR RENDIEMIENTO
            //if (!Centro.ExisteCentro(Cen) && !string.IsNullOrEmpty(Cen.Descripcion))
            //    Centro.GuardarCentroenBBDD(Cen);
            //if (!Matricula.ExisteMatricula(Mat) && !string.IsNullOrEmpty(Mat.Descripcion))
            //    Matricula.GuardarMatriculaenBBDD(Mat);

            if (!string.IsNullOrEmpty(IdUsuario))
            {
                string IdExpedienteLocal = string.Empty; string DescripcionTipoLocal = string.Empty; string DescripcionExpedienteLocal = string.Empty;


                // PODRIAMOS MIRAR SI EXISTE EL IDEXP QUE VIENE COMO PARAMETRO PARA ESE USUARIO, PERO AL SER LA MAYORIA DE EXPEDIENTES DE CARRREFOR, TAMPOCO NOS SERVIRÍA CASI DE NADA SABER QUE ESE IDEXPEDIENTE YA EXISTE 
                // PARA ESE USUARIO       ExisteExpedienteUsuario(IdExpediente, IdUsuario);

                //if (!Matricula.ExisteMatricula(Mat))  
                //    Matricula.GuardarMatriculaenBBDD(Mat);
                //if (!Centro.ExisteCentro(Cen))
                //    Centro.GuardarCentroenBBDD(Cen);

                if (!string.IsNullOrEmpty(IdExpediente))  //SI SON NULOS ES QUE NO TIENE EXPEDIENTE
                {
                    IdExpedienteLocal = ExisteExpedienteUsuarioPorNombre(IdUsuario, DescripcionExpediente);
                    DescripcionExpedienteLocal = DevolverDescripcionExpediente(IdExpedienteLocal);
                    //if (IdExpedienteLocal == IdExpediente)
                    //{
                    //    if (DescripcionExpediente != DescripcionExpedienteLocal)  //NUNCA ENTRARÁ
                    //    {
                    //        RenombrarExpediente(IdExpedienteLocal, DescripcionExpediente);
                    //    }
                    //    //SINO ES QUE TODO ESTA OK
                    //}
                    //else
                    // {
                    if (string.IsNullOrEmpty(IdExpedienteLocal))
                    { //LO CREAMOS PORQUE NO EXISTE
                        IdExpedienteLocal = CrearNuevoExpedienteZDocs(DescripcionExpediente, IdUsuario, "");
                        res += CrearNuevoExpedienteUsuarioZDocs(IdExpedienteLocal, IdUsuario);
                    }
                    else
                    {  //EL EXPEDIENTE SI QUE EXISTE PERO PERO CON OTRO ID, COMO USAMOS EL IDEXPEDIENTELOCAL DA IGUAL

                    }
                    //}
                }
                //AHORA LOS TIPOS
                string IdTipoLocal = string.Empty; string IdTipoPadreLocal = string.Empty; string DescripcionTipoPadreLocal = string.Empty;
                if (!string.IsNullOrEmpty(IdTipo))
                {//SI FUESE NULO ES QUE EL DOCUMENTO QUE SE INTENTA IMPORTAR NO TIENE TIPO ASIGNADO
                    IdTipoLocal = ExisteTipoDocumentoZDocs(IdUsuario, DescripcionTipo, IdEmpresa);
                    DescripcionTipoLocal = string.Empty;
                    if (!string.IsNullOrEmpty(IdTipoLocal))
                        DescripcionTipoLocal = DevolverDescripcionTipo(IdTipoLocal, IdEmpresa, IdUsuario);
                    if (string.IsNullOrEmpty(IdTipoLocal))// (IdTipoLocal == IdTipo)
                    {
                        //    if (DescripcionTipo != DescripcionTipoLocal)
                        //    {
                        //        RenombrarTipoDocumento(IdTipoLocal, DescripcionTipo);
                        //    }
                        //    //SINO ES QUE TODO ESTA OK
                        //}
                        //else
                        //{
                        IdTipoPadreLocal = ExisteTipoDocumentoZDocs(IdUsuario, DescripcionTipoPadre, IdEmpresa);
                        if (string.IsNullOrEmpty(IdTipoPadreLocal))
                        { //NO EXISTE NI EL TIPO, NI EL PADRE, HAY QUE CREEAR EL ARBOL
                            res += CrearArboldeTipos(ListaNodos, IdEmpresa, IdUsuario, Fields);
                            IdTipoLocal = ExisteTipoDocumentoZDocs(IdUsuario, DescripcionTipo, IdEmpresa);
                            // return "No existe el Nodo Padre del Tipo de Documento al que pertenece este Documento";
                            //MONTAR QUE CREE TODO EL ARBOL SI ES NECESARIO
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(IdTipoLocal))
                            { // LO CREAMOS PORQUE NO EXISTE
                                IdTipoLocal = CrearTipoDocumento(DescripcionTipo, IdEmpresa, false, IdUsuario, IdTipoPadreLocal, "Documentos");
                            }
                            //SI ES QUE NO, ES QUE EL TIPO EXISTE PERO CON OTRO ID, NO PASA NADA USAMOS EL IDLOCAL
                        }
                    }
                }
                // AHORA EL DOCUMENTO 

                string IdDocumentoActual = DevolverIdDocumentoPorNombre(NombreOriginal, Convert.ToInt32(IdEmpresa));
                if (string.IsNullOrEmpty(IdDocumentoActual))
                {
                    IdDocumentoActual = SubirFicheroZDocsPro(Fichero, DocOriginal, IdUsuario, IdExpedienteLocal, IdEmpresa);
                    if (IdDocumentoActual == "-1")
                        //{
                        //    if (!string.IsNullOrEmpty(NombreOriginal) && (NombreOriginal != DocOriginal))
                        //        ActualizarNombreDocumento(NombreOriginal, IdUsuario, IdDocumentoActual);
                        //}
                        //else
                        res += "Error subiendo el fichero: " + DocOriginal + ", Id de documento:" + Doc.Id;
                }
                else
                {
                    string Ruta = "00000000";
                    Ruta = Ruta.Substring(0, 8 - IdDocumentoActual.Length) + IdDocumentoActual;
                    string pathString = Directorio + Ruta.Substring(0, 2) + "/" + Ruta.Substring(2, 2) + "/" + Ruta.Substring(4, 2) + "/";// +Ruta.Substring(6, 2) + "/";

                    System.IO.Directory.CreateDirectory(pathString);
                    File.WriteAllBytes(pathString + Ruta.Substring(6, 2) + "." + Formato, Fichero);
                }
                string IdMetadato = string.Empty;
                Documento DocActualFNMT = Documento.DevolverDocumento(IdDocumentoActual,"","",IdUsuario);
                //SI EL DOCUMENTO QUE HAY ACTUALMENTE EN LA FNMT TIENE UN NOMBRE DISTINTO AL QUE SUBIMOS LO RENOMBRAMOS
                if (DocActualFNMT.Nombre != DocOriginal)
                    ActualizarNombreDocumento(NombreOriginal, IdUsuario, IdDocumentoActual);


                if (!string.IsNullOrEmpty(IdTipo))  //QUE EL DOCUMENTO QUE SE QUIERE IMPORTAR TENGA TIPO ASIGNADO
                {
                    //PREPARAMOS LA LINEA DE METADATOS
                    if (!string.IsNullOrEmpty(Fields))
                    {
                        // Dictionary<string, string> _fields = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(Fields);
                        // string[] MatrizMetadatos = Fields.Split('|');
                        // string MetadatosaInsertar = string.Empty; string Columnas = string.Empty; string Valores = string.Empty;
                        //for (int i = 0; i < MatrizMetadatos.Count(); i++)
                        //{
                        //    Columnas += MatrizMetadatos[i].Split('^')[0];
                        //    Valores += "'" + MatrizMetadatos[i].Split('^')[1] + "'";
                        //}
                        string NomTabla = "c00000000";
                        NomTabla = NomTabla.Substring(0, 9 - IdTipoLocal.Length) + IdTipoLocal;
                        if (ExisteTablaTipos(NomTabla) == "0") //NO EXISTE LA TABLA C00000
                        {
                            //     string[] _Campos = (JsonConvert.DeserializeObject(Fields, typeof(string[])) as string[]);
                            string[] _Campos = Fields.Split('^');
                            res += CrearTablaCamposTipo(IdTipoLocal, DescripcionTipoLocal, _Campos, "DOCUMENTOS");
                        }
                        ActualizarDocumentosTipos(IdTipoLocal, IdDocumentoActual, IdMetadato, "Documentos");
                        IdMetadato = IndexacionInsertarLineaCampos(Fields, NomTabla, IdTipoLocal, IdDocumentoActual, "Documentos"); // InsertarLineaCampos(Fields, NomTabla, IdTipoLocal, IdDocumentoActual, "Documentos");
                    }

                }

                if (!string.IsNullOrEmpty(IdExpedienteLocal) && !string.IsNullOrEmpty(IdDocumentoActual))
                {
                    //ACTUALIZAR DOCUMENTOEXPEDIENTE---CREAR METODO

                    if (!ExisteDocumentoExpediente(IdDocumentoActual, IdExpedienteLocal))
                        InsertarDocumentoenExpediente(IdDocumentoActual, IdExpedienteLocal);
                    //if (ExisteExpedienteUsuario(IdExpedienteLocal, IdUsuario) != "")
                    //    res = CrearNuevoExpedienteUsuarioZDocs(IdExpedienteLocal, IdUsuario);
                }
                IndexacionActualizarDatosDocumento(IdDocumentoActual, Doc, IdUsuario);
                if (!string.IsNullOrEmpty(res))
                    return res;
                else
                    return IdDocumentoActual;
                ////CUIDADO, SI EL EXPEDIENTE NO EXISTE LO CREA PERO PARA EL USUARIO MAESTRO
                //string query = "select IdDocumento from Documentos where DocOriginal = '" + DocOriginal + "' and idUsuario = '" + IdUsuario + "'";
                //string resul = DatabaseConnection.executeScalarString(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider).ToString();
                //Boolean Existe = (string.IsNullOrEmpty(resul) ? false : true);
                //return Existe;
            }
            else
                return "El Usuario no existe en el Destino";
        }

        [WebMethod]
        public string SustituirDocumento(string IdDocumento, string DocOriginal, string NombreOriginal, string EmailUsuario, string IdEmpresa, byte[] Fichero, string Formato, string DescripcionEmpresa)
        {//UNICAMENTE PARA ACTUALIZAR EL FICHERO FÍSICO
            string res = string.Empty;
            string IdUsuario = DevolverIdUsuarioPorEmail(EmailUsuario, DescripcionEmpresa); //METODO PASANDO IDEMPRESA


            if (!string.IsNullOrEmpty(IdUsuario))
            {

                if (!string.IsNullOrEmpty(IdDocumento))
                {
                    string Ruta = "00000000";
                    Ruta = Ruta.Substring(0, 8 - IdDocumento.Length) + IdDocumento;
                    string pathString = Directorio + Ruta.Substring(0, 2) + "/" + Ruta.Substring(2, 2) + "/" + Ruta.Substring(4, 2) + "/";// +Ruta.Substring(6, 2) + "/";

                    System.IO.Directory.CreateDirectory(pathString);
                    File.WriteAllBytes(pathString + Ruta.Substring(6, 2) + "." + Formato, Fichero);
                    if (!File.Exists(pathString + Ruta.Substring(6, 2) + "." + Formato))
                        res = "Fichero no guardado";

                    if (!string.IsNullOrEmpty(res))
                        return res;
                    else
                        return IdDocumento;

                }
                return IdDocumento;
            }
            else
                return "El Usuario no existe en el Destino";
        }
        [WebMethod]
        public string DevolverEmailUsuario(string IdUsuario)
        {
            string query = "select Email from usuarios where idUsuario = " + IdUsuario;

            return DatabaseConnection.executeScalarString(query, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
        }
        [WebMethod]
        public string CrearTokenPassword(string IdUsuario,Boolean Inicializar)
        {
            try {
                int resultado;

                Guid Idtoken = Guid.NewGuid();
                string Consulta = "";
                if (Inicializar)
                    Consulta = "update usuarios set tokenPassword='' where IdUsuario='" + IdUsuario + "'";
                else
                    Consulta = "update usuarios set tokenPassword='" + Idtoken + "' where IdUsuario='" + IdUsuario + "'";


                resultado = DatabaseConnection.executeScalarInt(Consulta, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                return Idtoken.ToString();
            }
            catch (Exception e)
            { 
                Trazas t= new Trazas("Crear Token Password", "WSZFK", -1, e.ToString() , e.InnerException + "");
                return "-1";
            }
        }
        #endregion
        [WebMethod]
        public static string ActualizarMetadatoDocumento(string IdDocumento, string Campo, string Valor)
        {
            try
            {
                string SQL = string.Empty;
                if (Campo.ToLower().Contains("fecha"))
                {
                    if (Valor.Contains(' '))
                    {
                        Valor = Valor.Split(' ')[0];
                    }
                    // Valor = Valor.Split('/')[2] + "/" + Valor.Split('/')[1].PadLeft(2, '0') + "/" + Valor.Split('/')[0].PadLeft(2, '0');
                    SQL = "Update Documentos set " + Campo + "='" + Valor + "' where iddocumento=" + IdDocumento;
                }
                else
                    SQL = "Update Documentos set " + Campo + "='" + Valor + "' where iddocumento=" + IdDocumento;
                int res = DatabaseConnection.executeNonQueryInt(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                return res.ToString();
            }
#pragma warning disable CS0168 // The variable 'e' is declared but never used
            catch (Exception e)
#pragma warning restore CS0168 // The variable 'e' is declared but never used
            {
                return "";
            }
        }
        #region ProgramaIcaibJulio

        [WebMethod]
        public string RellenarComboUsuarios(string Empresa, string Criterio)
        {
            string SQL = string.Empty;
            string res = string.Empty;
            string IdEmpresa = DevolverIdEmpresaporDescripcion(Empresa);
            if (IdEmpresa == "9")  //SI ES CARREFOUR
            {
                SQL = "select * from usuarios usu inner join usuarioempresa ue on ue.idusuario=usu.idusuario where ue.idempresa=" + IdEmpresa;
                if (!string.IsNullOrEmpty(Criterio))
                {
                    SQL += " and usu.email like '%" + Criterio + "%'";
                }
                SQL += " order by usu.email";
            }
            else
            {
                if (IdEmpresa == "1999")  //SI ES COAIB
                {
                    SQL = "select * from usuarios where iddistribuidor=" + IdEmpresa;
                    if (!string.IsNullOrEmpty(Criterio))
                    {
                        SQL += " and email like '%" + Criterio + "%'";
                    }
                    SQL += " order by email";
                }
            }
            DataTable DT = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

            if (DT.Rows.Count > 0)
            {
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    res += DT.Rows[i]["Email"] + "|";
                }
            }
            return res;
        }
        [WebMethod]
        public List<string> DevolverDocumentosUsuario(string Idusuario, string NombreDoc)
        {
            string SQL = "select doc.docOriginal from documentos doc inner join documentosusuarios du on du.iddocumento=doc.iddocumento where du.idusuario=" + Idusuario;
            if (!string.IsNullOrEmpty(NombreDoc))
                SQL += " and doc.docoriginal like '%" + NombreDoc + "%'";

            SQL += " order by doc.FechaCreacion desc, doc.docoriginal";
            System.Data.DataTable DT = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
            List<string> Resul = new List<string>();
            if (DT.Rows.Count > 0)
            {
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    Resul.Add(DT.Rows[i]["DocOriginal"].ToString());
                    //Resul +=DT.Rows[i]["DocOriginal"] + Environment.NewLine;
                }
            }
            return Resul;
        }
        #endregion
        #region ControlUsuarios
        [WebMethod]
        public string RellenarComboCentros(string IdEmpresa)
        {
            string SQL = string.Empty;
            string res = string.Empty;

            SQL = "select * from centro where idempresa=" + IdEmpresa + " order by CodCentroCarrefour";

            DataTable DT = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

            if (DT.Rows.Count > 0)
            {
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    res += DT.Rows[i]["Descripcion"] + "|";
                }
            }
            return res;
        }
        [WebMethod]
        public string DevolverIdsCentros(string IdEmpresa, string CentroaExcluir)
        {
            string SQL = string.Empty;
            string res = string.Empty;

            SQL = "select * from centro where idempresa=" + IdEmpresa + " and idcentro<>" + CentroaExcluir + " order by CodCentroCarrefour";

            DataTable DT = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

            if (DT.Rows.Count > 0)
            {
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    res += DT.Rows[i]["IdCentro"] + "|";
                }
            }
            return res;
        }
        [WebMethod]
        public string DevolverIdCentro(string DescripcionCentro, string IdEmpresa)
        {
            try
            {
                string SQL = "select IdCentro from centro where descripcion='" + DescripcionCentro + "' and IdEmpresa=" + IdEmpresa;
                string res = DatabaseConnection.executeScalarString(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
                if (!string.IsNullOrEmpty(res))
                    return res;
                else
                    return "";
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        [WebMethod]
        public double DevolverIdCentroPorCodigo(string Descripcion, string CodigoCentro)
        {
            string SQL = "Select * from centro where Descripcion like '%" + Descripcion + "%' and codCentroCarrefour='" + CodigoCentro + "'";

            System.Data.DataTable DT = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
            Double Resul = 0;

            if (DT.Rows.Count > 0)
            {
                Resul = Convert.ToDouble(DT.Rows[0]["IdCentro"]);
            }
            return Resul;
        }
        [WebMethod]
        public int AsignarCentro(string IdUsuario, string IdCentro)
        {
            string SQL = "insert into UsuariosCentros (Idusuario,Idcentro) values (" + IdUsuario + "," + IdCentro + ")";
            int res = DatabaseConnection.executeNonQueryInt(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
            return res;
        }
        [WebMethod]
        public Boolean ExisteAsignacionCentro(string IdUsuario, string IdCentro)
        {

            string SQL = "select * from usuarioscentros where idcentro=" + IdCentro + " and Idusuario=" + IdUsuario;
            DataTable DT = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
            if (DT.Rows.Count > 0)
                return true;
            else
                return false;

        }
        [WebMethod]
        public string DevolverCentrosVisiblesporUsuario(string EmailUsuario, string DescripcionEmpresa)
        {
            try
            {
                string IdUsuario = DevolverIdUsuarioPorEmail(EmailUsuario, DescripcionEmpresa);
                string res = string.Empty;
                string SQL = "select cen.descripcion,cen.codcentrocarrefour from centro cen inner join usuarioscentros uc on cen.idcentro=uc.idcentro where uc.idusuario=" + IdUsuario;
                DataTable DT = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);

                if (DT.Rows.Count > 0)
                {
                    for (int i = 0; i < DT.Rows.Count; i++)
                    {
                        res += DT.Rows[i]["Descripcion"] + " - " + DT.Rows[i]["CodcentroCarrefour"] + "|";
                    }
                }
                return res;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        [WebMethod]
        public double DevolverIdExpediente(string Descripcion, string IdUsuario)
        {

            string SQL = "Select * from expedientes exp inner join expedientesusuarios expusu on exp.idexpediente=expusu.idexpediente where exp.Descripcion='" + Descripcion + "' and expusu.idusuario=" + IdUsuario;

            System.Data.DataTable DT = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
            Double Resul = 0;
            if (DT.Rows.Count > 0)
            {
                Resul = Convert.ToDouble(DT.Rows[0]["IdExpediente"]);
            }
            return Resul;
        }
        [WebMethod]
        public string DevolverCampoTablaMatriculas(string Matricula, string CampoBuscado)
        {
            string SQL = "Select " + CampoBuscado + " from Matriculas where Matricula='" + Matricula + "'";

            System.Data.DataTable DT = DatabaseConnection.executeNonQueryDT(SQL, CommandType.Text, ConnectionString.CadenaConexionZDocsSinProvider);
            string Resul = "";
            if (DT.Rows.Count > 0)
            {
                Resul = DT.Rows[0]["" + CampoBuscado + ""].ToString();
            }
            return Resul;
        }
        #endregion
    }

}

