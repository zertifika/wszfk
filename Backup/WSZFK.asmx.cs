﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Data.OleDb;
using System.Data;
using WS = WSZFK.GetMyCompanyUsers;
using WSAlta.Encryption;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace WSZFK
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]

    public class Service1 : System.Web.Services.WebService
    {
        string CadenaConexion = "Provider=SQLNCLI10;Server=tcp:wtpmh0e15c.database.windows.net;Database=zertifika;Uid=ZertifikaAdmin@wtpmh0e15c.database.windows.net;Pwd=zdbB!NCTjl$Lp3NPMWqZ;Encrypt=True;Connection Timeout=30";

        //private static String GetTimestamp(this DateTime value)
        //{
        //    return value.ToString("yyyyMMddHHmmssffff");
        //}

        //[WebMethod]
        //private string CrearUsuario(string Nombre, string Apellido, string Email,string Password,string Dni)
        //{
        //    try
        //    {
        //        OleDbConnection vCon = new OleDbConnection(CadenaConexion);

        //        var Id = new Guid();

        //        string SQL = "insert into users (UniqueId,CreationDate,SchemaVersion,Name,LastName,CompleteName,Email,Disabled,Banned,IsGettingStarted,ChatEnabled,PasswordEncrypted,CompanyUniqueId,DNI)";
        //        SQL += "values ('" + Id + "','" + DateTime.Now + "','1.0','" + Nombre + "','" + Apellido + "','" + Nombre + " " + Apellido + "','" + Email + "','False','False','False','False','" + Password + "','" + CiaId + "','" + Dni + "' ) ";
        //        OleDbCommand vComando = new OleDbCommand(SQL, vCon);
        //        vCon.Open();
        //        vComando.ExecuteNonQuery();
        //        return "";
        //    }
        //    catch (OleDbException ex)
        //    {
        //        return ex.ToString();
        //    }
        //}

        [WebMethod]
        public string ExisteUsuario(string Usuario, string Password)
        {
            WS.UserInfo _info = new WS.UserInfo();
            try
            {

                _info.Mail = Usuario;
                _info.Pwd = Password;


                WS.WSAccessPoint _cliente = new WS.WSAccessPoint();
                string kk = _cliente.GetCategories(_info);
                

            }
            catch (Exception ex)
            {
                string error = ex.Message;
                if (error == "Usuario/Password Incorrectos")
                {
                    return "Usuario/Password Incorrectos";
                }

                if (error == "Usuario no encontrado")
                {
                    return "Usuario no encontrado";
                }


                return idUsuario(Usuario);
            }

            return idUsuario(Usuario);

        }

        [WebMethod]
        public byte[] DescargaDoc(string Usuario, string Password, string Documento)
        {
            WS.UserInfo _info = new WS.UserInfo();
            try
            {

                _info.Mail = Usuario;
                _info.Pwd = Password;


                WS.WSAccessPoint _cliente = new WS.WSAccessPoint();
                byte[] kk = _cliente.DownloadDocument(_info, Documento);

                // Aqui tenemos que generar el evento de documento visualizador por el usuario del correo.
                string vUsuario = idUsuario(Usuario);

                string leido = GenerarEventoLectura(vUsuario, Documento);



                return kk;


            }
            catch (Exception ex)
            {

                return null;
            }

            

        }

        [WebMethod]
        public string ExisteUsuarioDNI(string DNI)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select * from users where DNI='" + DNI + "'";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return "True"; // ds.Tables[0].Rows[0].ItemArray[7].ToString();


                }
                else
                {
                    return "False";
                }


            }
            catch (OleDbException ex)
            {
                return ex.ToString();
            }


        }

        [WebMethod]
        public string ExisteUsuarioMail(string Mail)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select * from users where eMail='" + Mail + "'";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return "True"; // ds.Tables[0].Rows[0].ItemArray[36].ToString();


                }
                else
                {
                    return "False";
                }


            }
            catch (OleDbException ex)
            {
                return ex.ToString();
            }


        }

        [WebMethod]
        public string EmpleadosEmpresaCIF(string CIF)
        {
            try
            {

                // Primero buscamos idEmpresa
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select * from Companies where IdentifierValue='" + CIF + "'";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {

                    string IdCompañia = ds.Tables[0].Rows[0].ItemArray[0].ToString();

                    SQL = "select * from users where CompanyUniqueId='" + IdCompañia + "'";
                    vComando.CommandText = SQL;

                    ds.Tables.Clear();

                    da.Fill(ds);

                    string Usuarios = "";


                    for (int i = 0; i < ds.Tables[0].Rows.Count - 1; i++)
                    {
                        Usuarios += "|" + ds.Tables[0].Rows[i].ItemArray[36].ToString();
                    }
                    return Usuarios;

                }
                else
                {
                    return "";
                }


            }
            catch (OleDbException ex)
            {
                return ex.ToString();
            }


        }

        [WebMethod]
        public String JerarquiaEtiquetas(string Padre,string Usuario)
        {


            
            Padre = idEtiqueta(Usuario, Padre);

            if (Padre == "")
            {
                return "";
            }

            OleDbConnection vCon = new OleDbConnection(CadenaConexion);


            string SQL = "SELECT Name from Tags where exists (select * from TagsJerarquia where idpadre='" + Padre + "' and idHijo=UniqueId) and  not exists (select * from TagsJerarquia where idPadre=UniqueId) order by name";

            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Etiquetas = "";

            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                Etiquetas += "|" + ds.Tables[0].Rows[i].ItemArray[0].ToString();

            }
            return Etiquetas;


        }

        [WebMethod]
        public String JerarquiaEtiquetasConHijos(string Padre, string Usuario)
        {



            Padre = idEtiqueta(Usuario, Padre);

            if (Padre == "")
            {
                return "";
            }

            OleDbConnection vCon = new OleDbConnection(CadenaConexion);

            string SQL = "SELECT Name from Tags where exists (select * from TagsJerarquia where idpadre='" + Padre + "' and idHijo=UniqueId) and  exists (select * from TagsJerarquia where idPadre=UniqueId) order by name";

            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Etiquetas = "";

            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                Etiquetas += "|" + ds.Tables[0].Rows[i].ItemArray[0].ToString();

            }
            return Etiquetas;


        }

        private String idEtiqueta(string Usuario,string Etiqueta)
        {


            Usuario = idUsuario(Usuario);

            OleDbConnection vCon = new OleDbConnection(CadenaConexion);


            string SQL = "SELECT * from Tags where useruniqueid='" + Usuario + "' and name='" + Etiqueta + "'";

            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Etiquetas = "";

            if (ds.Tables[0].Rows.Count > 0)
            {
                Etiquetas = ds.Tables[0].Rows[0].ItemArray[0].ToString();
            }
            else
            {
                Etiquetas = "";
            }
                
            return Etiquetas;

        }

        [WebMethod]
        public String EtiquetasRaiz(string Usuario)
        {


            Usuario = idUsuario(Usuario);

            OleDbConnection vCon = new OleDbConnection(CadenaConexion);


            string SQL = "SELECT Name from Tags where useruniqueid='" + Usuario + "' and not exists (select * from TagsJerarquia where idhijo=UniqueId) and not exists (select * from TagsJerarquia where idPadre=UniqueId) order by name desc";

            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Etiquetas = "";

            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                Etiquetas += "|" + ds.Tables[0].Rows[i].ItemArray[0].ToString();

            }
            return Etiquetas;


        }

        [WebMethod]
        public String EtiquetasRaizConHijos(string Usuario)
        {


            Usuario = idUsuario(Usuario);

            OleDbConnection vCon = new OleDbConnection(CadenaConexion);


            string SQL = "SELECT Name from Tags where useruniqueid='" + Usuario + "' and not exists (select * from TagsJerarquia where idhijo=UniqueId) and  exists (select * from TagsJerarquia where idPadre=UniqueId) order by name desc";

            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Etiquetas = "";

            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                Etiquetas += "|" + ds.Tables[0].Rows[i].ItemArray[0].ToString();

            }
            return Etiquetas;


        }



        [WebMethod]
        public String EtiquetasDocumento(string Documento)
        {
            OleDbConnection vCon = new OleDbConnection(CadenaConexion);


            string SQL = "SELECT Name,UniqueId from Tags where exists (select * from DocumentTags where documentuniqueid='" + Documento + "' and TagUniqueId=UniqueId)";



            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Etiquetas = "";

            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                Etiquetas += "|" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[1].ToString();

            }
            return Etiquetas;


        }


        [WebMethod]
        public string DocumentosUsuarioEtiqueta(string Usuario, string Etiqueta)
        {
            OleDbConnection vCon = new OleDbConnection(CadenaConexion);

            string vEtiqueta = idEtiquetaUsuario(Usuario, Etiqueta);

            string SQL = "SELECT     Name,CreationDate,PreviewMimeType, UniqueId FROM  Documents AS doc WHERE  (EXISTS  (SELECT     UniqueId, Timestamp, CreationDate, SchemaVersion, LastModify, DocumentId, UserId, CompanyId, CreatorId, UserIdLastModify     FROM          DocShareds        WHERE      (doc.UniqueId = DocumentId) AND (UserId = '" + Usuario + "')) or UserUniqueId='" + Usuario + "') and exists (select * from DocumentTags where DocumentTags.TagUniqueid='" + vEtiqueta + "' and DocumentTags.DocumentUniqueId=UniqueId)"  ;


            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Documentos = "";

            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
              //  if (DocumentoTieneEtiqueta(ds.Tables[0].Rows[i].ItemArray[3].ToString(), vEtiqueta) == "True")
              //  {
                    Documentos += "|" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[1].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[2].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[3].ToString();
               // }
            }
            return Documentos;


        }


        // Mirar el tag de la etiqueta de documentos compartiro por empresa.
        private string TagEtiqueta(string Usuario,string Tipo)
        {
            string SQL = "select * from tags where UserUniqueID='" + Usuario + "' and Name='" + Tipo + "'";

            OleDbConnection vCon = new OleDbConnection(CadenaConexion);

            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                return ds.Tables[0].Rows[0]["UniqueId"].ToString();  
            } else
               return "";
        }

        private string NombreUsuario(string Usuario,ref string Compañia)
        {
            string SQL = "select * from users where uniqueid='" + Usuario + "'";

            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);

                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    Compañia = ds.Tables[0].Rows[0]["CompanyUniqueId"].ToString();
                    return ds.Tables[0].Rows[0]["CompleteName"].ToString();
                }
                else
                {
                    return "";
                }

            }
            catch (OleDbException ex)
            {
                return "";
            }


        }

        public string DescripcionDocumento(string Documento)
        {
            string SQL = "select * from documents where uniqueid='" + Documento + "'";

            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);

                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].Rows[0]["Name"].ToString();
                }
                else
                {
                    return "";
                }

            }
            catch (OleDbException ex)
            {
                return "";
            }

        }

        public string DescripcionCompañia(string Compañia)
        {
            string SQL = "select * from companies where uniqueid='" + Compañia + "'";

            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);

                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].Rows[0]["Name"].ToString();
                }
                else
                {
                    return "";
                }

            }
            catch (OleDbException ex)
            {
                return "";
            }

        }


        private string GenerarEventoLectura(string Usuario, string Documento)
        {

            // Buscamos los datos necesarios para hacer el insert en el socialevents
            // SocialEvents: UniqueId, TimeStamp = '634834758123100104', CreationDate, SchemaVersion='1.0', Type='6', Source='2', ActionUserUniqueId, ActionUserDescription, ActionCompanyUniqueId, ActionCompanyDescription, DocumentUniqueId, DocumentDescription

            string Compañia = "";
            string Nombre = NombreUsuario(Usuario,ref Compañia);

            Guid valueid = Guid.NewGuid();

            string SQL = "insert into Socialevents (UniqueId, TimeStamp, CreationDate, SchemaVersion, Type, Source, ActionUserUniqueId, ActionUserDescription, ActionCompanyUniqueId,ActionCompanyDescription,  DocumentUniqueId, DocumentDescription)";
            SQL+= " values ('" + valueid + "','" +  DevolverUltimoTimestamp() + "','" + DateTime.Now.Month + "/" + DateTime.Now.Day + "/" + DateTime.Now.Year + " " + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second + "','1.0','6','2','";
            SQL += Usuario + "','" +  Nombre + "','" + Compañia + "','" + DescripcionCompañia(Compañia) + "','" +  Documento +"','" + DescripcionDocumento(Documento) + "') ";

            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                vComando.ExecuteNonQuery();
                return "OK";
            }
            catch (OleDbException ex)
            {
                return ex.Message;
            }

        }

        
        public string DevolverUltimoTimestamp()
       {
           try
           {
               OleDbConnection vCon = new OleDbConnection(CadenaConexion);
               string Sql = "select top(10) Timestamp from Socialevents order by Creationdate desc ";
               OleDbCommand vComando = new OleDbCommand(Sql, vCon);
               vCon.Open();
               OleDbDataAdapter da = new OleDbDataAdapter();
               DataSet ds = new DataSet();

               da.SelectCommand = vComando;
               da.Fill(ds);

               if (ds.Tables[0].Rows.Count > 0)
               {
                   return ds.Tables[0].Rows[0].ItemArray[0].ToString();
               }
               else
               {
                   return "";
               }

           }
           catch (OleDbException ex)
           {
               return "";
           }
       }


        private string BuscarEtiquetaUsuario(string Usuario, string Etiqueta)
        {
           OleDbConnection vCon = new OleDbConnection(CadenaConexion);

            string SQL = "select * from TAGS where useruniqueid='" + Usuario + "' and name='" + Etiqueta + "'";

            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                return ds.Tables[0].Rows[0]["UniqueId"].ToString();

            }
            else
            {
                return "";
            }

        }

       
        private int NumeroEtiquetasDocumento(string Documento)
        {
            string SQL = "select max(DocumentTagsId) as vTotal from DocumentTags where DocumentUniqueId='" + Documento + "'";
            OleDbConnection vCon = new OleDbConnection(CadenaConexion);
            Guid UniqueId = new Guid(Documento);
 
            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();
            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                try
                {
                    int Total = Convert.ToInt32(ds.Tables[0].Rows[0]["vTotal"].ToString());
                    Total += 1;
                    return Total;
                }
                catch (Exception ex)
                {
                    return 0;
                }


            }
            else
            {
                return 0;
            }

        }

        [WebMethod]
        public string AnadirEtiquetaDocumento(string Usuario, string Documento,string Etiqueta)
        {
            Usuario = idUsuario(Usuario);

            string vidEtiqueta = BuscarEtiquetaUsuario(Usuario, Etiqueta);
            if (vidEtiqueta != "")
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                int vNumEtiquetas = NumeroEtiquetasDocumento(Documento);

                string SQL = "insert into DocumentTags (DocumentUniqueID,TagUniqueId,DocumentTagsId) values (";
                SQL += "'" + Documento + "','" + vidEtiqueta + "'," + vNumEtiquetas + ")";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                vComando.ExecuteNonQuery();



            }

            return "";
        }

        [WebMethod]
        public string DocumentosNuevosCompartidos2(string Usuario)
        {
            OleDbConnection vCon = new OleDbConnection(CadenaConexion);

            Usuario = idUsuario(Usuario);

            string vTagEtiquetaEmpresa = TagEtiqueta(Usuario,"Documentos de empresa");
           // string vTagEtiquetaObsoletos = TagEtiqueta(Usuario, "Documentos obsoletos");
            string vTagEtiquetaPapelera = TagEtiqueta(Usuario, "Papelera");




            string SQL = "SELECT  Name,CreationDate, PreviewMimeType, UniqueId  FROM Documents docs WHERE docs.name<>'NotificationsManual' and docs.uploader<>'" + Usuario + "' and (exists";
            SQL += " (SELECT DossierUniqueId FROM DossierCompanyDatas WHERE dossieruniqueid=docs.dossieruniqueid and (UserUniqueId ='" + Usuario + "') )) and not exists (select *";
            SQL +=" from tags,documenttags where documenttags.documentuniqueid=docs.uniqueid and documenttags.taguniqueid=tags.uniqueid and tags.name='Documentos Obsoletos' ) and not exists (select * from SocialEvents where";
            SQL += " ActionUserUniqueId='" + Usuario + "' and documentuniqueid=docs.uniqueid and (type=6 or type=16)) and not exists (select * FROM DossierDocNotShareds where userid='" + Usuario + "' and";
            SQL += " DossierDocNotShareds.documentid=docs.uniqueid) AND ( exists (SELECT t.UniqueId FROM Tags AS t INNER JOIN DocumentTags AS d ON t.UniqueId = d.TagUniqueId WHERE d.documentuniqueid=docs.uniqueid and t.useruniqueid='" + Usuario + "' and (t.uniqueid =";
            SQL += "'" + vTagEtiquetaEmpresa + "'   )) )and not exists (select * from tags,documenttags where tags.useruniqueid='" + Usuario + "' and documenttags.documentuniqueid=docs.uniqueid and documenttags.taguniqueid=tags.uniqueid and";
            SQL += " tags.uniqueid='" + vTagEtiquetaPapelera + "' )and not exists (select * from tags,documenttags where tags.useruniqueid=docs.useruniqueid and documenttags.documentuniqueid=docs.uniqueid and documenttags.taguniqueid=tags.uniqueid and tags.Name='Papelera' ) UNION ";

            SQL += " (select Name,docs.CreationDate, PreviewMimeType, docs.UniqueId  from DocShareds sha,documents docs where docs.uniqueid=sha.documentid and not exists (select dtag.documentuniqueid from Documenttags dtag,tags where dtag.documentuniqueid=docs.uniqueid and dtag.taguniqueid=tags.uniqueid and tags.name='Documentos Obsoletos') and";
            SQL += " docs.uploader<>'" + Usuario + "' and sha.userid='" + Usuario + "' and not exists (select * from socialevents where (type=6 or type=16) and documentuniqueid=sha.documentid and actionuseruniqueid='" + Usuario + "') and not";
            SQL += " exists (select * FROM DossierDocNotShareds where userid='" + Usuario + "' and DossierDocNotShareds.documentid=docs.uniqueid) AND ( exists (SELECT t.UniqueId FROM Tags AS t INNER JOIN DocumentTags AS d ON t.UniqueId = d.TagUniqueId WHERE d.documentuniqueid=docs.uniqueid and";
            SQL += " t.useruniqueid='" + Usuario + "' and (t.uniqueid ='" + vTagEtiquetaEmpresa + "'   )) )and not exists (select dtag.documentuniqueid from Documenttags dtag,tags where dtag.taguniqueid=tags.uniqueid and dtag.documentuniqueid=docs.uniqueid and tags.useruniqueid='" + Usuario + "' and";
            SQL += " tags.uniqueid='" + vTagEtiquetaPapelera + "')and not exists (select dtag.documentuniqueid from Documenttags dtag,tags where dtag.taguniqueid=tags.uniqueid and dtag.documentuniqueid=docs.uniqueid and tags.useruniqueid=docs.useruniqueid and tags.Name='Papelera')) order by docs.creationdate desc";
                     

            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Documentos = "";

            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                Documentos += "|" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[1].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[2].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[3].ToString();

            }



            return Documentos;

  
        }
        
        [WebMethod]
        public string DocumentosNuevosCompartidosNombre2(string Usuario,string Nombre)
        {
            OleDbConnection vCon = new OleDbConnection(CadenaConexion);

            Usuario = idUsuario(Usuario);

            string vTagEtiquetaEmpresa = TagEtiqueta(Usuario, "Documentos de empresa");
            // string vTagEtiquetaObsoletos = TagEtiqueta(Usuario, "Documentos obsoletos");
            string vTagEtiquetaPapelera = TagEtiqueta(Usuario, "Papelera");




            string SQL = "SELECT  Name,CreationDate, PreviewMimeType, UniqueId  FROM Documents docs WHERE docs.name like '%" + Nombre + "%' and docs.uploader<>'" + Usuario + "' and (exists";
            SQL += " (SELECT DossierUniqueId FROM DossierCompanyDatas WHERE dossieruniqueid=docs.dossieruniqueid and (UserUniqueId ='" + Usuario + "') )) and not exists (select *";
            SQL += " from tags,documenttags where documenttags.documentuniqueid=docs.uniqueid and documenttags.taguniqueid=tags.uniqueid and tags.name='Documentos Obsoletos' ) and not exists (select * from SocialEvents where";
            SQL += " ActionUserUniqueId='" + Usuario + "' and documentuniqueid=docs.uniqueid and (type=6 or type=16)) and not exists (select * FROM DossierDocNotShareds where userid='" + Usuario + "' and";
            SQL += " DossierDocNotShareds.documentid=docs.uniqueid) AND ( exists (SELECT t.UniqueId FROM Tags AS t INNER JOIN DocumentTags AS d ON t.UniqueId = d.TagUniqueId WHERE d.documentuniqueid=docs.uniqueid and t.useruniqueid='" + Usuario + "' and (t.uniqueid =";
            SQL += "'" + vTagEtiquetaEmpresa + "'   )) )and not exists (select * from tags,documenttags where tags.useruniqueid='" + Usuario + "' and documenttags.documentuniqueid=docs.uniqueid and documenttags.taguniqueid=tags.uniqueid and";
            SQL += " tags.uniqueid='" + vTagEtiquetaPapelera + "' )and not exists (select * from tags,documenttags where tags.useruniqueid=docs.useruniqueid and documenttags.documentuniqueid=docs.uniqueid and documenttags.taguniqueid=tags.uniqueid and tags.Name='Papelera' ) UNION ";

            SQL += " (select Name,docs.CreationDate, PreviewMimeType, docs.UniqueId  from DocShareds sha,documents docs where docs.name like '%" + Nombre + "%' and docs.uniqueid=sha.documentid and not exists (select dtag.documentuniqueid from Documenttags dtag,tags where dtag.documentuniqueid=docs.uniqueid and dtag.taguniqueid=tags.uniqueid and tags.name='Documentos Obsoletos') and";
            SQL += " docs.uploader<>'" + Usuario + "' and sha.userid='" + Usuario + "' and not exists (select * from socialevents where (type=6 or type=16) and documentuniqueid=sha.documentid and actionuseruniqueid='" + Usuario + "') and not";
            SQL += " exists (select * FROM DossierDocNotShareds where userid='" + Usuario + "' and DossierDocNotShareds.documentid=docs.uniqueid) AND ( exists (SELECT t.UniqueId FROM Tags AS t INNER JOIN DocumentTags AS d ON t.UniqueId = d.TagUniqueId WHERE d.documentuniqueid=docs.uniqueid and";
            SQL += " t.useruniqueid='" + Usuario + "' and (t.uniqueid ='" + vTagEtiquetaEmpresa + "'   )) )and not exists (select dtag.documentuniqueid from Documenttags dtag,tags where dtag.taguniqueid=tags.uniqueid and dtag.documentuniqueid=docs.uniqueid and tags.useruniqueid='" + Usuario + "' and";
            SQL += " tags.uniqueid='" + vTagEtiquetaPapelera + "')and not exists (select dtag.documentuniqueid from Documenttags dtag,tags where dtag.taguniqueid=tags.uniqueid and dtag.documentuniqueid=docs.uniqueid and tags.useruniqueid=docs.useruniqueid and tags.Name='Papelera')) order by docs.creationdate desc";


            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Documentos = "";

            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                Documentos += "|" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[1].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[2].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[3].ToString();

            }



            return Documentos;


        }

        [WebMethod]
        public string DocumentosNombre2(string Usuario, string Pass, string Nombre)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                //Usuario = idUsuario(Usuario);
                Usuario = LoginCorrecto(Usuario, Pass);
                if (Usuario == "-1")
                {
                    return "-1";
                }

                string vTagEtiquetaEmpresa = TagEtiqueta(Usuario, "Documentos de empresa");
                // string vTagEtiquetaObsoletos = TagEtiqueta(Usuario, "Documentos obsoletos");
                string vTagEtiquetaPapelera = TagEtiqueta(Usuario, "Papelera");

                string SQL = "SELECT  Name,CreationDate, PreviewMimeType, UniqueId, DossierUniqueId FROM Documents docs WHERE docs.name like '%" + Nombre + "%' and docs.uploader<>'" + Usuario + "' and (exists";
                SQL += " (SELECT DossierUniqueId FROM DossierCompanyDatas WHERE dossieruniqueid=docs.dossieruniqueid and (UserUniqueId ='" + Usuario + "') )) and not exists (select *";
                SQL += " from tags,documenttags where documenttags.documentuniqueid=docs.uniqueid and documenttags.taguniqueid=tags.uniqueid and tags.name='Documentos Obsoletos' ) and not exists (select * FROM DossierDocNotShareds where userid='" + Usuario + "' and";
                SQL += " DossierDocNotShareds.documentid=docs.uniqueid) AND ( exists (SELECT t.UniqueId FROM Tags AS t INNER JOIN DocumentTags AS d ON t.UniqueId = d.TagUniqueId WHERE d.documentuniqueid=docs.uniqueid and t.useruniqueid='" + Usuario + "' and (t.uniqueid =";
                SQL += "'" + vTagEtiquetaEmpresa + "'   )) )and not exists (select * from tags,documenttags where tags.useruniqueid='" + Usuario + "' and documenttags.documentuniqueid=docs.uniqueid and documenttags.taguniqueid=tags.uniqueid and";
                SQL += " tags.uniqueid='" + vTagEtiquetaPapelera + "' )and not exists (select * from tags,documenttags where tags.useruniqueid=docs.useruniqueid and documenttags.documentuniqueid=docs.uniqueid and documenttags.taguniqueid=tags.uniqueid and tags.Name='Papelera' ) UNION ";

                SQL += " (select Name,docs.CreationDate, PreviewMimeType, docs.UniqueId, docs.DossierUniqueId from DocShareds sha,documents docs where docs.name like '%" + Nombre + "%' and docs.uniqueid=sha.documentid and not exists (select dtag.documentuniqueid from Documenttags dtag,tags where dtag.documentuniqueid=docs.uniqueid and dtag.taguniqueid=tags.uniqueid and tags.name='Documentos Obsoletos') and";
                SQL += " docs.uploader<>'" + Usuario + "' and sha.userid='" + Usuario + "' and not";
                SQL += " exists (select * FROM DossierDocNotShareds where userid='" + Usuario + "' and DossierDocNotShareds.documentid=docs.uniqueid) AND ( exists (SELECT t.UniqueId FROM Tags AS t INNER JOIN DocumentTags AS d ON t.UniqueId = d.TagUniqueId WHERE d.documentuniqueid=docs.uniqueid and";
                SQL += " t.useruniqueid='" + Usuario + "' and (t.uniqueid ='" + vTagEtiquetaEmpresa + "'   )) )and not exists (select dtag.documentuniqueid from Documenttags dtag,tags where dtag.taguniqueid=tags.uniqueid and dtag.documentuniqueid=docs.uniqueid and tags.useruniqueid='" + Usuario + "' and";
                SQL += " tags.uniqueid='" + vTagEtiquetaPapelera + "')and not exists (select dtag.documentuniqueid from Documenttags dtag,tags where dtag.taguniqueid=tags.uniqueid and dtag.documentuniqueid=docs.uniqueid and tags.useruniqueid=docs.useruniqueid and tags.Name='Papelera'))";

                SQL += " union (SELECT  Name,CreationDate, PreviewMimeType, UniqueId, DossierUniqueId FROM Documents docs WHERE docs.name like '%" + Nombre + "%' and docs.uploader='" + Usuario + "' and not exists (select *";
                SQL += " from tags,documenttags where documenttags.documentuniqueid=docs.uniqueid and documenttags.taguniqueid=tags.uniqueid and tags.name='Documentos Obsoletos' ))";

                SQL += " order by docs.creationdate desc";


                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                string Documentos = "";

                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    Documentos += "|" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[1].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[2].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[3].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[4].ToString();

                }

                return Documentos;
            }
            catch (Exception e)
            {
                return -1 + "";
            }

        }

        [WebMethod]
        public string DocumentosUsuarioNuevos(string Usuario, string Desde, string Hasta)
        {
            OleDbConnection vCon = new OleDbConnection(CadenaConexion);


            string SQL = "SELECT     Name,CreationDate, PreviewMimeType, UniqueId FROM  Documents AS doc WHERE not exists (select * from SocialEvents where ActionUserUniqueId='" + Usuario + "' and Type=6 and SocialEvents.DocumentUniqueId=Doc.UniqueId) and  EXISTS  (SELECT     UniqueId, Timestamp, CreationDate, SchemaVersion, LastModify, DocumentId, UserId, CompanyId, CreatorId, UserIdLastModify     FROM          DocShareds        WHERE      (doc.UniqueId = DocumentId) AND (UserId = '" + Usuario + "')) or UserUniqueId='" + Usuario + "'";


            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Documentos = "";

            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                Documentos += "|" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[1].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[2].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[3].ToString();

            }
            return Documentos;


        }

        [WebMethod]
        public string DocumentosNuevosCompartidos(string Usuario)
        {
            OleDbConnection vCon = new OleDbConnection(CadenaConexion);


            //string SQL = "SELECT     Name,CreationDate, PreviewMimeType, UniqueId FROM  Documents AS doc WHERE not exists (select * from SocialEvents where ActionUserUniqueId='" + Usuario + "' and Type=6 and SocialEvents.DocumentUniqueId=Doc.UniqueId) and  EXISTS  (SELECT     UniqueId, Timestamp, CreationDate, SchemaVersion, LastModify, DocumentId, UserId, CompanyId, CreatorId, UserIdLastModify     FROM          DocShareds        WHERE      (doc.UniqueId = DocumentId) AND (UserId = '" + Usuario + "')) or UserUniqueId='" + Usuario + "'";
            string SQL="SELECT    Name,CreationDate, PreviewMimeType, UniqueId from   documents as doc where exists (select * from docshareds as docs where doc.uniqueid=docs.documentid and docs.userid='" + Usuario + "')";

            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Documentos = "";

            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                Documentos += "|" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[1].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[2].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[3].ToString();

            }
            return Documentos;


        }


        [WebMethod]
        public string NumeroDocumentosNuevosUsuario(string Usuario, string Desde, string Hasta)
        {
            OleDbConnection vCon = new OleDbConnection(CadenaConexion);

            Usuario = idUsuario(Usuario);

            string SQL = "SELECT    count(*) as Documentos FROM  Documents AS doc WHERE not exists (select * from SocialEvents where ActionUserUniqueId='" + Usuario + "' and Type=6 and SocialEvents.DocumentUniqueId=Doc.UniqueId) and  EXISTS  (SELECT     UniqueId, Timestamp, CreationDate, SchemaVersion, LastModify, DocumentId, UserId, CompanyId, CreatorId, UserIdLastModify     FROM          DocShareds        WHERE      (doc.UniqueId = DocumentId) AND (UserId = '" + Usuario + "')) or UserUniqueId='" + Usuario + "'";


            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Documentos = "";

            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                Documentos += ds.Tables[0].Rows[i].ItemArray[0].ToString();

            }
            return Documentos;


        }


        [WebMethod]
        public string DocumentosUsuario(string Usuario,string Desde,string Hasta)
        {
            OleDbConnection vCon = new OleDbConnection(CadenaConexion);


            string SQL = "SELECT     Name,CreationDate, PreviewMimeType, UniqueId FROM  Documents AS doc WHERE  EXISTS  (SELECT     UniqueId, Timestamp, CreationDate, SchemaVersion, LastModify, DocumentId, UserId, CompanyId, CreatorId, UserIdLastModify     FROM          DocShareds        WHERE      (doc.UniqueId = DocumentId) AND (UserId = '" + Usuario + "')) or UserUniqueId='" + Usuario + "'";


            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Documentos = "";

            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                Documentos += "|" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[1].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[2].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[3].ToString();

            }
            return Documentos;


        }

        [WebMethod]
        public string NumDocumentosUsuarioCompartidos(string Usuario)
        {
            OleDbConnection vCon = new OleDbConnection(CadenaConexion);

            Usuario = idUsuario(Usuario);

            string SQL = "SELECT count(*) from docshareds where UserId ='" + Usuario + "'";


            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Documentos = "";

            Documentos += ds.Tables[0].Rows[0].ItemArray[0].ToString();


            return Documentos;


        }

        [WebMethod]
        public string SolicitudesPendientesContestar(string Usuario)
        {
            OleDbConnection vCon = new OleDbConnection(CadenaConexion);

            Usuario = idUsuario(Usuario);

            string SQL = "SELECT count(*) from UserConnectionInvitations where ConnectionUniqueId ='" + Usuario + "'";


            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Documentos = "";

            Documentos += ds.Tables[0].Rows[0].ItemArray[0].ToString();


            return Documentos;


        }

        [WebMethod]
        public string ContactosPendientesContestar(string Usuario)
        {
            OleDbConnection vCon = new OleDbConnection(CadenaConexion);

            Usuario = idUsuario(Usuario);

            string SQL = "SELECT count(*) from UserConnectionInvitations where UserUniqueId ='" + Usuario + "'";


            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Documentos = "";

            Documentos += ds.Tables[0].Rows[0].ItemArray[0].ToString();


            return Documentos;


        }

        [WebMethod]
        public string ContactosUsuario(string Usuario)
        {
            OleDbConnection vCon = new OleDbConnection(CadenaConexion);

            Usuario = idUsuario(Usuario);

            string SQL = "SELECT count(*) from UserConnections where UserUniqueId ='" + Usuario + "'";


            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Documentos = "";

            Documentos += ds.Tables[0].Rows[0].ItemArray[0].ToString();


            return Documentos;


        }

        [WebMethod]
        public string DatosUsuario(string Usuario,string Password)
        {

            if (ExisteUsuario(Usuario, Password) == "Usuario/Password Incorrectos")
            {
                return "Usuario/Password Incorrectos";
            }

            if (ExisteUsuario(Usuario, Password) == "Usuario no encontrado")
            {
                return "Usuario no encontrado";
            }
            
            
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);




                Usuario = idUsuario(Usuario);


                string SQL = "SELECT * from users where UniqueId ='" + Usuario + "'";


                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                string Documentos = "";

                Documentos += ds.Tables[0].Rows[0].ItemArray[6].ToString() + "|" + ds.Tables[0].Rows[0].ItemArray[7].ToString() + "|" + ds.Tables[0].Rows[0].ItemArray[36].ToString();

                return Documentos;
            

        }

        [WebMethod]
        public string getEmpresaUsuario(string Usuario, string Pass)
        {
            OleDbConnection vCon = new OleDbConnection(CadenaConexion);

            Usuario = LoginCorrecto(Usuario, Pass);

            string SQL = "SELECT comp.UniqueId, comp.Name, comp.Logo FROM Users AS us INNER JOIN "+
                  "Companies AS comp ON comp.UniqueId = us.CompanyUniqueId " +
                  "WHERE  (us.UniqueId = '" + Usuario + "')";

            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Documentos = "";

            Documentos += ds.Tables[0].Rows[0].ItemArray[0].ToString() + "^" + ds.Tables[0].Rows[0].ItemArray[1].ToString();
            if (Convert.IsDBNull(ds.Tables[0].Rows[0].ItemArray[2]))
            {
                Documentos += "^";
            }else{
                Documentos += "^https://www.zertifikaanywhere.com/Services/Avatar.aspx?companyId=" + ds.Tables[0].Rows[0].ItemArray[0].ToString();
                
            }


            return Documentos;

        }

        private string CompañiaUsuario(string Usuario)
        {
            OleDbConnection vCon = new OleDbConnection(CadenaConexion);

            Usuario = idUsuario(Usuario);

            string SQL = "SELECT * from users where UniqueId ='" + Usuario + "'";


            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Documentos = "";

            Documentos += ds.Tables[0].Rows[0].ItemArray[34].ToString();


            return Documentos;

        }


        [WebMethod]
        public string EmpresasContactadas(string Usuario)
        {
            OleDbConnection vCon = new OleDbConnection(CadenaConexion);

            string Compañia = CompañiaUsuario(Usuario);

            Usuario = idUsuario(Usuario);

            string SQL = "SELECT count(*) from CompanyConnections where ConnectionUniqueId ='" + Compañia + "'";


            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Documentos = "";

            Documentos += ds.Tables[0].Rows[0].ItemArray[0].ToString();


            return Documentos;


        }

        [WebMethod]
        public string getContactosUsuario(string Usuario, string Pass)
        {
            string resultado = "";
            string query = "";
            try
            {
                Usuario = LoginCorrecto(Usuario, Pass);
                if (Usuario.Equals("-1"))
                {
                    return "-1";
                }

                query = "SELECT usAmigo.UniqueId AS idAmigo, usAmigo.CompleteName AS nombreAmigo, usAmigo.Email AS mailAmigo, " +
                    "comp.Name AS cia, up.Thumbnail, comp.UniqueId " +
                  "FROM Users AS us INNER JOIN UserConnections AS uc ON us.UniqueId = uc.UserUniqueId " +
                  "INNER JOIN Users AS usAmigo ON usAmigo.UniqueId = uc.FriendUniqueId " +
                  "INNER JOIN Companies AS comp ON comp.UniqueId = usAmigo.CompanyUniqueId " +
                  "LEFT OUTER JOIN UserProfiles AS up ON up.UserUniqueId = usAmigo.UniqueId " +
                  "WHERE  (us.UniqueId = ?)";

                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                OleDbCommand vComando = new OleDbCommand(query, vCon);
                vComando.Parameters.Add("@Usuario", OleDbType.VarChar, 50).Value = Usuario;
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    String imagen = "";
                    if(ds.Tables[0].Rows[i].ItemArray[4].ToString().Length > 0){
                        imagen = "https://www.zertifikaanywhere.com/Services/Avatar.aspx?id=" + ds.Tables[0].Rows[i].ItemArray[0].ToString(); 
                    }
                    
                    resultado += "|" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[1].ToString() + "^" +
                        ds.Tables[0].Rows[i].ItemArray[2].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[3].ToString() + "^" + imagen + "^" + 
                        ds.Tables[0].Rows[i].ItemArray[5].ToString();

                }

                return resultado;
            }
            catch (Exception e)
            {
                return "Error.";
            }
        }

        [WebMethod]
        public string MensajesRecibidos(string Usuario)
        {
            OleDbConnection vCon = new OleDbConnection(CadenaConexion);
     

            Usuario = idUsuario(Usuario);

            string SQL = "SELECT count(*) from MessageRecipients where RecipientUniqueId ='" + Usuario + "'";


            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Documentos = "";

            Documentos += ds.Tables[0].Rows[0].ItemArray[0].ToString();


            return Documentos;


        }

        [WebMethod]
        public string MensajesEnviados(string Usuario)
        {
            OleDbConnection vCon = new OleDbConnection(CadenaConexion);

           
            Usuario = idUsuario(Usuario);

            string SQL = "SELECT count(*) from Messages where SentByUniqueId ='" + Usuario + "'";


            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Documentos = "";

            Documentos += ds.Tables[0].Rows[0].ItemArray[0].ToString();


            return Documentos;


        }

        [WebMethod]
        public string NumDocumentosUsuarioCompartidosNoLeidos(string Usuario)
        {
            OleDbConnection vCon = new OleDbConnection(CadenaConexion);

            Usuario = idUsuario(Usuario);

            string SQL = "SELECT count(*) from docshareds where UserId ='" + Usuario + "' and (isread is null or isread='N')";


            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Documentos = "";

            Documentos += ds.Tables[0].Rows[0].ItemArray[0].ToString();


            return Documentos;


        }

        [WebMethod]
        public string NumDocumentosUsuario(string Usuario)
        {
            OleDbConnection vCon = new OleDbConnection(CadenaConexion);

            Usuario = idUsuario(Usuario);

            string SQL = "SELECT     count(*) FROM  Documents where UserUniqueId='" + Usuario + "'";


            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Documentos = "";


            Documentos = ds.Tables[0].Rows[0].ItemArray[0].ToString();
            
            return Documentos;


        }

        [WebMethod]
        private string EmpleadosEmpresaTipo(string CIF, string Tipo)
        {
            try
            {

                // Primero buscamos idEmpresa
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select * from Companies where IdentifierValue='" + CIF + "'";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {

                    string IdCompañia = ds.Tables[0].Rows[0].ItemArray[0].ToString();

                    SQL = "select * from users where CompanyUniqueId='" + IdCompañia + "' and JobTitle='" + Tipo + "'";
                    vComando.CommandText = SQL;

                    ds.Tables.Clear();

                    da.Fill(ds);

                    string Usuarios = "";


                    for (int i = 0; i < ds.Tables[0].Rows.Count - 1; i++)
                    {
                        Usuarios += "|" + ds.Tables[0].Rows[i].ItemArray[36].ToString();
                    }
                    return Usuarios;

                }
                else
                {
                    return "";
                }


            }
            catch (OleDbException ex)
            {
                return ex.ToString();
            }


        }


        [WebMethod]
        private string EmpleadosEmpresaAdmin(string CIF)
        {
            try
            {

                // Primero buscamos idEmpresa
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select * from Companies where IdentifierValue='" + CIF + "'";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {

                    string IdCompañia = ds.Tables[0].Rows[0].ItemArray[0].ToString();

                    SQL = "select * from users where CompanyUniqueId='" + IdCompañia + "' and IsCompanyAdmin='True'";
                    vComando.CommandText = SQL;

                    ds.Tables.Clear();

                    da.Fill(ds);

                    string Usuarios = "";


                    for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                    {
                        Usuarios += "|" + ds.Tables[0].Rows[i].ItemArray[36].ToString();
                    }
                    return Usuarios;

                }
                else
                {
                    return "";
                }


            }
            catch (OleDbException ex)
            {
                return ex.ToString();
            }


        }

        [WebMethod]
        private string idEmpresa(string CIF)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select UniqueId from Companies where IdentifierValue='" + CIF + "'";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].Rows[0].ItemArray[0].ToString();

                }
                else
                {
                    return "False";
                }
            }
            catch (OleDbException ex)
            {
                return ex.ToString();
            }
        }

        [WebMethod]
        private string HacerContactoEmpresa(string CIF1, string CIF2)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                //        OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                //        var Id = new Guid();

                //        string SQL = "insert into users (UniqueId,CreationDate,SchemaVersion,Name,LastName,CompleteName,Email,Disabled,Banned,IsGettingStarted,ChatEnabled,PasswordEncrypted,CompanyUniqueId,DNI)";
                //        SQL += "values ('" + Id + "','" + DateTime.Now + "','1.0','" + Nombre + "','" + Apellido + "','" + Nombre + " " + Apellido + "','" + Email + "','False','False','False','False','" + Password + "','" + CiaId + "','" + Dni + "' ) ";
                //        OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                //        vCon.Open();
                //        vComando.ExecuteNonQuery();

                // Ponemos el contacto en direccion cif1->cif2
                string SQL = "insert into CompanyConnections (CreationDate,CompanyUniqueId,ConnectionUniqueId) values ('" + DateTime.Now.ToString() + "','" + CIF1 + "','" + CIF2 + "')";
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();
                vComando.ExecuteNonQuery();

                // Ponemos el contacto en direccion cif2->cif1
                string SQL2 = "insert into CompanyConnections (CreationDate,CompanyUniqueId,ConnectionUniqueId) values ('" + DateTime.Now.ToString() + "','" + CIF2 + "','" + CIF1 + "')";
                OleDbCommand vComando2 = new OleDbCommand(SQL, vCon);
                vCon.Open();
                vComando2.ExecuteNonQuery();

                return "OK";

            }
            catch (OleDbException ex)
            {
                return ex.ToString();
            }
        }

        [WebMethod]
        private string SonContactoEmpresa(string CIF1, string CIF2)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select * from CompaniesConnections where CompanyUniqueId='" + CIF1 + "' and ConnectionUniqueId='" + CIF2 + "'";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].Rows[0].ItemArray[0].ToString();

                }
                else
                {
                    return "False";
                }
            }
            catch (OleDbException ex)
            {
                return ex.ToString();
            }
        }

        [WebMethod]
        private string SonContactoUsuario(string idUsuario1, string idUsuario2)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select * from UserConnections where UserUniqueId='" + idUsuario1 + "' and FriendUniqueId='" + idUsuario2 + "'";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].Rows[0].ItemArray[0].ToString();

                }
                else
                {
                    return "False";
                }
            }
            catch (OleDbException ex)
            {
                return ex.ToString();
            }
        }

        private  byte[] StrToByteArray(string str)
        {
            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            return encoding.GetBytes(str);
        }

        [WebMethod]
        public byte[] ThumbUsuario(string Usuario)
        {
            try
            {

                 Usuario =  idUsuario(Usuario);

                OleDbConnection vCon = new OleDbConnection(CadenaConexion);


                string SQL = "select Thumbnail from UserProfiles where useruniqueid='" + Usuario + "'";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    string thumb = ds.Tables[0].Rows[0].ItemArray[0].ToString();
                    return StrToByteArray(thumb);

                }
                else
                {
                    return null;
                }
            }
            catch (OleDbException ex)
            {
                return null;
            }
        }
     
        private string idUsuario(string Correo)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select UniqueId from Users where Email='" + Correo + "'";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].Rows[0].ItemArray[0].ToString();

                }
                else
                {
                    return "False";
                }
            }
            catch (OleDbException ex)
            {
                return ex.ToString();
            }
        }

        [WebMethod]
        private string idEmpresaUsuario(string idUsuario)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select CompanyUniqueId from Users where UniqueId='" + idUsuario + "'";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].Rows[0].ItemArray[0].ToString();

                }
                else
                {
                    return "False";
                }
            }
            catch (OleDbException ex)
            {
                return ex.ToString();
            }
        }

        [WebMethod]
        private string HacerContactoUsuario(string idUsuario1, string idUsuario2)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                //        OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                //        var Id = new Guid();

                //        string SQL = "insert into users (UniqueId,CreationDate,SchemaVersion,Name,LastName,CompleteName,Email,Disabled,Banned,IsGettingStarted,ChatEnabled,PasswordEncrypted,CompanyUniqueId,DNI)";
                //        SQL += "values ('" + Id + "','" + DateTime.Now + "','1.0','" + Nombre + "','" + Apellido + "','" + Nombre + " " + Apellido + "','" + Email + "','False','False','False','False','" + Password + "','" + CiaId + "','" + Dni + "' ) ";
                //        OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                //        vCon.Open();
                //        vComando.ExecuteNonQuery();

                // Ponemos el contacto en direccion cif1->cif2
                string SQL = "insert into UserConnections (CreationDate,UserUniqueId,FriendUniqueId) values ('" + DateTime.Now.ToString() + "','" + idUsuario1 + "','" + idUsuario2 + "')";
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();
                vComando.ExecuteNonQuery();

                // Ponemos el contacto en direccion cif2->cif1
                string SQL2 = "insert into UserConnections (CreationDate,UserUniqueId,FriendUniqueId) values ('" + DateTime.Now.ToString() + "','" + idUsuario2 + "','" + idUsuario1 + "')";
                OleDbCommand vComando2 = new OleDbCommand(SQL, vCon);
                vCon.Open();
                vComando2.ExecuteNonQuery();

                return "OK";

            }
            catch (OleDbException ex)
            {
                return ex.ToString();
            }
        }


        [WebMethod]
        public string CompartirDocumento(string DocumentId, string UserId)
        {
            try
            {
                // Miramos la compañia del usuario
                string CompanyId = idEmpresaUsuario(UserId);
                Guid UniqueId = Guid.NewGuid() ;
                string CreationDate = DateTime.Now.ToString();
                string LastModify = CreationDate;
                //string UserIdLastModify = CreationDate;

                string CreatorId = CreadorDocumento(DocumentId);

                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "insert into DocShareds (UniqueId,Timestamp,CreationDate,SchemaVersion,LastModify,DocumentId,UserId,CompanyId,CreatorId,UserIdLastModify) values ('" + UniqueId + "'," + TimeStampDocumento(DocumentId) + ", GETDATE() , '" + "v1.0" + "', GETDATE() ,'" + DocumentId + "','" + UserId + "','" + CompanyId + "','" + CreatorId + "','" + CreatorId + "')";
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();
                vComando.ExecuteNonQuery();

                return "OK";

            }
            catch (OleDbException ex)
            {
                return "-1";
            }
        }

        private string getUserCreatorId(string idExpediente)
        {
            try
            {
                String CompanyId = "-1";
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);
                String SQLGetCompanyId = "SELECT UserCreator FROM DossierCompanyDatas WHERE (DossierUniqueId = '" + idExpediente + "')";

                OleDbCommand vComando = new OleDbCommand(SQLGetCompanyId, vCon);

                vComando = new OleDbCommand(SQLGetCompanyId, vCon);
                vCon.Open();
                CompanyId = vComando.ExecuteScalar().ToString();
                vCon.Close();
                return CompanyId;
            }
            catch (Exception e)
            {
                return "-1";
            }
        }
        
        [WebMethod]
        private string TimeStampDocumento(string idDocumento)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select Timestamp from Documents where UniqueId='" + idDocumento + "'";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].Rows[0].ItemArray[0].ToString();

                }
                else
                {
                    return "False";
                }
            }
            catch (OleDbException ex)
            {
                return ex.ToString();
            }
        }

        [WebMethod]
        private string CreadorDocumento(string idDocumento)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select Useruniqueid from Documents where UniqueId='" + idDocumento + "'";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].Rows[0].ItemArray[0].ToString();
                }
                else
                {
                    return "False";
                }
            }
            catch (OleDbException ex)
            {
                return ex.ToString();
            }
        }

        private string CompañiasDistribuidor(string idDistribuidor,string Desde,string Hasta)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);
                string SQL = "select CompanyUniqueId from Users where cod_distribuidor='" + idDistribuidor + "' and  creationdate>='" + string.Format("{0:yyyy-MM-dd H:mm:ss}",DateTime.Parse( Desde)) + "' and creationdate<='" + string.Format("{0:yyyy-MM-dd H:mm:ss}",DateTime.Parse( Hasta)) + "'  group by CompanyUniqueId ";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);


                return ds.Tables[0].Rows.Count.ToString();
                
            }
            catch
            {
                return "0";
            }
 
                    
        

        }

        private string UsuariosDistribuidor(string idDistribuidor,string Desde,string Hasta)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);
                string SQL = "select CompanyUniqueId from Users where cod_distribuidor='" + idDistribuidor + "' and creationdate>='" + string.Format("{0:yyyy-MM-dd H:mm:ss}",DateTime.Parse( Desde)) + "' and creationdate<='" + string.Format("{0:yyyy-MM-dd H:mm:ss}",DateTime.Parse( Hasta)) +"'";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);


                return ds.Tables[0].Rows.Count.ToString();

            }
            catch
            {
                return "0";
            }
 
        }

        private string DocumentosDistribuidor(string idDistribuidor,string Desde,string Hasta)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);
                string SQL = "select * from Documents where uploader in (select uniqueid from users where cod_distribuidor='" + idDistribuidor + "')";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);


                return ds.Tables[0].Rows.Count.ToString();

            }
            catch
            {
                return "0";
            }

        }
        private string TotalDocs()
        {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select count(*) as TotalDocs from Documents";


                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].Rows[0].ItemArray[0].ToString();
                } else
                    {
                    return "0";
                }
  

        }

        private string TotalUsuarios()
        {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select count(*) as TotalUsuarios from users";


                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].Rows[0].ItemArray[0].ToString();
                } else
                    {
                    return "0";
                }
  

        }


        private string TotalCompanies()
        {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select count(*) as TotalEmpresas from companies";


                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].Rows[0].ItemArray[0].ToString();
                } else
                    {
                    return "0";
                }
  

        }


        private string TotalAccesosUsuarios(string Desde, string Hasta)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                Desde += " 00:00:00";
                Hasta += " 23:59:59";
                string SQL = "select usermail as TotalAccesos from audits where creationdate>='" + string.Format("{0:yyyy-MM-dd H:mm:ss}", DateTime.Parse(Desde)) + "' and creationdate<='" + string.Format("{0:yyyy-MM-dd H:mm:ss}", DateTime.Parse(Hasta)) + "' and Message='User Logged. Common membership' group by usermail";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].Rows.Count.ToString();

                }
            }
            catch (Exception ex)
            {
                return "-1";
            }

            return "-1";


        }



        private string TotalAccesos(string Desde, string Hasta)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);
                Desde += " 00:00:00";
                Hasta += " 23:59:59";

                string SQL = "select count(*) as TotalAccesos from audits where creationdate>='" + string.Format("{0:yyyy-MM-dd H:mm:ss}", DateTime.Parse(Desde)) + "' and creationdate<='" + string.Format("{0:yyyy-MM-dd H:mm:ss}", DateTime.Parse(Hasta)) + "' and Message='User Logged. Common membership'";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].Rows[0].ItemArray[0].ToString();
                }
            }
            catch (Exception ex)
            {
                return "-1";
            }

            return "-1";
    

        }


        [WebMethod]
        public string Accesos(string Desde, string Hasta)
        {

            string vAccesos = "" + TotalAccesosUsuarios(Desde, Hasta) + "|" + TotalAccesos(Desde, Hasta);

            return vAccesos;
        }

        private string DocsMail(string Desde, string Hasta)
        {

            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select count(*) as TotalDocsLeido from SocialEvents where type= 17 and creationdate>='" + string.Format("{0:yyyy-MM-dd H:mm:ss}", DateTime.Parse(Desde)) + "' and creationdate<='" + string.Format("{0:yyyy-MM-dd H:mm:ss}", DateTime.Parse(Hasta)) + "'";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].Rows[0].ItemArray[0].ToString();
                }
            }
            catch (Exception ex)
            {
                return "-1";
            }

            return "-1";


        }


        private string DocsLeidos(string Desde, string Hasta)
        {

            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select count(*) as TotalDocsLeido from SocialEvents where type= 6 and creationdate>='" + string.Format("{0:yyyy-MM-dd H:mm:ss}", DateTime.Parse(Desde)) + "' and creationdate<='" + string.Format("{0:yyyy-MM-dd H:mm:ss}", DateTime.Parse(Hasta)) + "'";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].Rows[0].ItemArray[0].ToString();
                }
            }
            catch (Exception ex)
            {
                return "-1";
            }

            return "-1";


        }

        private string DocsDescargados(string Desde, string Hasta)
        {

            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select count(*) as TotalDocsLeido from SocialEvents where type = 16 and creationdate>='" + string.Format("{0:yyyy-MM-dd H:mm:ss}", DateTime.Parse(Desde)) + "' and creationdate<='" + string.Format("{0:yyyy-MM-dd H:mm:ss}", DateTime.Parse(Hasta)) + "'";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].Rows[0].ItemArray[0].ToString();
                }
            }
            catch (Exception ex)
            {
                return "-1";
            }

            return "-1";


        }



        [WebMethod]
        private string AccesoDocumentos(string Desde, string Hasta)
        {

            string vAccesos = "" + DocsLeidos(Desde + " 00:00:00", Hasta + " 23:59:59") + "|" + DocsDescargados(Desde + " 00:00:00", Hasta + " 23:59:59") + "|" + DocsMail(Desde + " 00:00:00", Hasta + " 23:59:59");
            return vAccesos;
        }


        [WebMethod]
        private string AccesoUsuarioCentro(string Centro,string Usuario,string Empresa)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select * from usuarioscentros where cif='" + Empresa + "' and DNI='" + Usuario + "'";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    SQL = "select * from usuarioscentros where cif='" + Empresa + "' and DNI='" + Usuario + "' and centro='" + Centro + "'";
                    OleDbCommand vComando2 = new OleDbCommand(SQL, vCon);
                   

                    OleDbDataAdapter da2 = new OleDbDataAdapter();
                    DataSet ds2 = new DataSet();

                    da2.SelectCommand = vComando;
                    da2.Fill(ds2);
                    if (ds2.Tables[0].Rows.Count > 0)
                    {
                        return "True"; // ds.Tables[0].Rows[0].ItemArray[36].ToString();
                    }
                    else
                    {
                        return "False";
                    }



                }
                else
                {
                    return "True";
                }
            }
            catch (OleDbException ex)
            {
                return ex.ToString();
            }

        }


        [WebMethod]
        private string AltaUsuarioCentro(string Centro, string Usuario, string idUsuario,string Empresa)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

               // string SQL = "select * from usuarioscentros where cif='" + Empresa + "' and DNI='" + Usuario + "'";

                Guid Id = Guid.NewGuid();
                string vId = Id.ToString();

               
                //add time clocked in/out in clock table
                string insertStatement = "INSERT INTO UsuariosCentros "
                                     + "(UniqueId,Cif, Dni, Centro,UserUniqueId) "
                                     + "VALUES ('" + vId + "','" + Empresa + "','" + Usuario + "','" + Centro + "','" + idUsuario + "')";

                OleDbCommand insertCommand = new OleDbCommand(insertStatement, vCon);

              /*  insertCommand.Parameters.Add("Cif", OleDbType.VarChar).Value = Empresa;
                insertCommand.Parameters.Add("Dni", OleDbType.VarChar).Value = Usuario;
                insertCommand.Parameters.Add("Centro", OleDbType.VarChar).Value = Centro;
                insertCommand.Parameters.Add("UniqueId", OleDbType.VarChar).Value = vId;
                insertCommand.Parameters.Add("UserUniqueId", OleDbType.VarChar).Value = vId; */

                vCon.Open();

                try
                {
                    int count = insertCommand.ExecuteNonQuery();
                }
                catch (OleDbException ex)
                {
                    return ex.Message;
                }
                return "True";

            }
            catch (OleDbException ex)
            {
                return ex.ToString();
            }

        }


        [WebMethod]
        private string BajaUsuarioCentro(string Centro, string Usuario, string idUsuario, string Empresa)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                // string SQL = "select * from usuarioscentros where cif='" + Empresa + "' and DNI='" + Usuario + "'";

                Guid Id = Guid.NewGuid();
                string vId = Id.ToString();


                //add time clocked in/out in clock table
                string deleteStatement = "delete from UsuariosCentros "    + "where  (CIF='" + Empresa + "' and dni='" + Usuario + "' and Centro='" + Centro + "')";

                vCon.Open();
                OleDbCommand deleteCommand = vCon.CreateCommand();     // new OleDbCommand(deleteCommand , vCon);
                deleteCommand.CommandText = deleteStatement;
                


                try
                {
                    int count = deleteCommand.ExecuteNonQuery();
                }
                catch (OleDbException ex)
                {
                    return ex.Message;
                }
                return "True";

            }
            catch (OleDbException ex)
            {
                return ex.ToString();
            }

        }

        public string BuscarNombreEtiqueta(string Etiqueta,string Documento)
        {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select * from tags where uniqueid='" +Etiqueta + "'";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    string NombreEtiqueta=ds.Tables[0].Rows[0].ItemArray[4].ToString();
                    string vEtiqueta = BuscarCamposEtiqueta(Etiqueta,Documento);



                    return NombreEtiqueta + "|"+vEtiqueta;


                }

            return "";
        }

        public string BuscarValorCampo(string Campo,string Documento)
        {
            string ValorCampo ="";
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select * from FieldValues where fielduniqueid='" + Campo + "' and documentuniqueid='" + Documento + "'";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {

                    return ds.Tables[0].Rows[0].ItemArray[4].ToString();

                }
            } catch
            {

                return "";

            }


            return "";
        }


        public string BuscarCamposEtiqueta(string Etiqueta,string Documento)
        {
            string CampoEtiqueta ="";
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select * from fields where taguniqueid='" +Etiqueta + "' order by Position";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                 {
                    for (int i =0;i<ds.Tables[0].Rows.Count;i++)
                    {
                        CampoEtiqueta +=  BuscarValorCampo(ds.Tables[0].Rows[i].ItemArray[0].ToString(),Documento) + "|";


                    }

                    return CampoEtiqueta;
                }
            } catch
             {
                 return "";
             }

                

            return "";


        }

        [WebMethod]
        public string CamposEtiqueta(string Documento)
        {

            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select * from documenttags where documentuniqueid='" + Documento + "'";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    string Etiqueta = BuscarNombreEtiqueta(ds.Tables[0].Rows[0].ItemArray[1].ToString(),Documento);

                    return Etiqueta;



                }
                ;
            } catch                     
                    {
                        return "";

                    }


            return "";
        
        }

        

        [WebMethod]
        private DataSet DocumentosPrevis(string Empresa,string Desde,string Hasta)
        {

            string DesdeFecha = Desde;
            string HastaFecha=Hasta;

            if (Desde!="")
            {
                DesdeFecha += " 00:00:00";

            }

            if (Hasta!="")
            {
                HastaFecha += " 23:59:59";

            }


            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select UniqueId,CreationDate,Name from documents where uploader='" + "6954a08f-2f52-4a08-960c-a0a000b2b0f5' ";
                

                    
                if (Empresa!="")
                {
                    SQL += "and name like '" + Empresa + "%' " ;
                }
                

                if (DesdeFecha!="")
                {
                    SQL += "and CreationDate>='" + string.Format("{0:yyyy-MM-dd H:mm:ss}", DateTime.Parse(DesdeFecha)) + "'";
                }
                if (HastaFecha!="")
                {

                    SQL += "and CreationDate<='" + string.Format("{0:yyyy-MM-dd H:mm:ss}",DateTime.Parse( HastaFecha)) + "'";

                }


                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                return ds;


            }
            catch (OleDbException ex)
            {
                return null;
            }

        }



        [WebMethod]
        private string UsuariosCIF(string Empresa)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select users.*,* from users,recibirdocumentosempresa where cifempresa='" + Empresa + "' and users.uniqueid<>'6954a08f-2f52-4a08-960c-a0a000b2b0f5' and users.uniqueid=recibirdocumentosempresa.useruniqueid and users.email not like '%mc@zertifika.com'";

 

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    string vUsuarios = "";
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        vUsuarios += ds.Tables[0].Rows[i].ItemArray[0] + "^" + ds.Tables[0].Rows[i].ItemArray[6] + "^" + ds.Tables[0].Rows[i].ItemArray[36] + "|";

                    return vUsuarios;
                }
                else
                {
                    return "";
                }

            }
            catch (OleDbException ex)
            {
                return ex.ToString();
            }

        }

        [WebMethod]
        private string UsuariosCentros(string idUsuario)
        {
            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);

                string SQL = "select * from usuarioscentros  where useruniqueid='" + idUsuario + "' order by Centro";




                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    string vUsuarios = "";
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        vUsuarios += ds.Tables[0].Rows[i].ItemArray[3] + "|";

                    return vUsuarios;
                }
                else
                {
                    return "";
                }

            }
            catch (OleDbException ex)
            {
                return ex.ToString();
            }

        }

        [WebMethod]
        private string PosicionGlobal()
        {

            return TotalCompanies() + "|" + TotalUsuarios() + "|" + TotalDocs();


        }


        [WebMethod]
        private string Distribuidores(string Desde, string Hasta)
        {

            try
            {
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);
                Desde += " 00:00:00";
                Hasta += " 23:59:59";
                string SQL = "select * from companies where exists (select * from users where cod_distribuidor=companies.uniqueid and creationdate>='" + string.Format("{0:yyyy-MM-dd H:mm:ss}",DateTime.Parse( Desde)) + "' and creationdate<='" + string.Format("{0:yyyy-MM-dd H:mm:ss}",DateTime.Parse( Hasta)) + "')";

                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vCon.Open();

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    string Distribuidores = "";
                    for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                    {
                        Distribuidores += ds.Tables[0].Rows[i].ItemArray[15].ToString();
                        // Miramos cuantas empresas tiene y cuantos usuarios
                        Distribuidores += "?" + CompañiasDistribuidor(ds.Tables[0].Rows[i].ItemArray[0].ToString(),Desde,Hasta);
                        Distribuidores += "?" + UsuariosDistribuidor(ds.Tables[0].Rows[i].ItemArray[0].ToString(),Desde,Hasta);
                        Distribuidores += "?" + DocumentosDistribuidor(ds.Tables[0].Rows[i].ItemArray[0].ToString(),Desde,Hasta);
                        
                        Distribuidores += "|";

                    }

                    return Distribuidores;

                }
            }
            catch (OleDbException ex)
            {
                return ex.ToString();
            }

            return "";


        }

        [WebMethod]
        public string EtiquetasUsuario(string Usuario)
        {

            OleDbConnection vCon = new OleDbConnection(CadenaConexion);


            string SQL = "SELECT  uniqueid,name from Tags where UserUniqueId='" + Usuario + "'";


            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Etiquetas = "";

            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                Etiquetas += "|" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[1].ToString();

            }
            return Etiquetas;


        }

        private string idEtiquetaUsuario(string Usuario,string Etiqueta)
        {

            OleDbConnection vCon = new OleDbConnection(CadenaConexion);


            string SQL = "SELECT   * from Tags where UserUniqueId='" + Usuario + "' and Name='" + Etiqueta + "'";


            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                return ds.Tables[0].Rows[0].ItemArray[0].ToString();
            }
            else
            {
                return "";
            }

        }

        private string DocumentoTieneEtiqueta(string Documento, string Etiqueta)
        {

            OleDbConnection vCon = new OleDbConnection(CadenaConexion);


            string SQL = "SELECT   * from DocumentTags where DocumentUniqueId='" + Documento + "' and TagUniqueId='" + Etiqueta + "'";


            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                return "True";
            }
            else
            {
                return "False";
            }

       

        }

        [WebMethod]
        public string DocumentosUsuarioNombre(string Usuario,string NombreDocumento)
        {

            OleDbConnection vCon = new OleDbConnection(CadenaConexion);


            string SQL = "SELECT     Name,CreationDate, PreviewMimeType, UniqueId FROM  Documents AS doc WHERE name like '%" + NombreDocumento + "%' and  (EXISTS  (SELECT     UniqueId, Timestamp, CreationDate, SchemaVersion, LastModify, DocumentId, UserId, CompanyId, CreatorId, UserIdLastModify     FROM          DocShareds        WHERE      (doc.UniqueId = DocumentId) AND (UserId = '" + Usuario + "')) or UserUniqueId='" + Usuario + "')";


            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Documentos = "";

            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                Documentos += "|" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[1].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[2].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[3].ToString();

            }
            return Documentos;

        }


        [WebMethod]
        public string DocumentosUsuarioNombreEtiqueta(string Usuario, string NombreDocumento,string Etiqueta)
        {

            OleDbConnection vCon = new OleDbConnection(CadenaConexion);

            string vEtiqueta = idEtiquetaUsuario(Usuario, Etiqueta);


            string SQL = "SELECT     Name,CreationDate, PreviewMimeType, UniqueId FROM  Documents AS doc WHERE name like '%" + NombreDocumento + "%' and  ((EXISTS  (SELECT     UniqueId, Timestamp, CreationDate, SchemaVersion, LastModify, DocumentId, UserId, CompanyId, CreatorId, UserIdLastModify     FROM          DocShareds        WHERE      (doc.UniqueId = DocumentId) AND (UserId = '" + Usuario + "')) or UserUniqueId='" + Usuario + "')) and exists (select * from DocumentTags where DocumentTags.TagUniqueid='" + vEtiqueta + "' and DocumentTags.DocumentUniqueId=UniqueId)";


            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Documentos = "";

            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                Documentos += "|" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[1].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[2].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[3].ToString();

            }
            return Documentos;

        }

        [WebMethod]
        public string DocumentosExpediente(string Usuario, string Expediente)
        {

            Usuario = idUsuario(Usuario);

            OleDbConnection vCon = new OleDbConnection(CadenaConexion);

            string SQL = "SELECT  Name,CreationDate, PreviewMimeType, UniqueId from Documents where dossieruniqueid='" + Expediente + "' and not exists (select * from dossierdocnotshareds where dossierdocnotshareds.userid='" + Usuario +  "' and documentid=documents.uniqueid)";


            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Documentos = "";

            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                Documentos += "|" + ds.Tables[0].Rows[i].ItemArray[0].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[1].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[2].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[3].ToString();

            }
            return Documentos;

        }


        [WebMethod]
        public string ExpedientesUsuario(string Usuario, string NombreExpediente, string Etiqueta)
        {

            Usuario=idUsuario(Usuario);

            OleDbConnection vCon = new OleDbConnection(CadenaConexion);

            string vEtiqueta = idEtiquetaUsuario(Usuario, Etiqueta);


            string SQL = "SELECT * from DossierCompanyDatas where useruniqueid='" + Usuario + "'";

            if (NombreExpediente != "")
            {
                SQL += " and DossierName like '%" + NombreExpediente + "%'";
            }



            OleDbCommand vComando = new OleDbCommand(SQL, vCon);
            vCon.Open();

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            da.SelectCommand = vComando;
            da.Fill(ds);

            string Documentos = "";

            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                Documentos += "|" + ds.Tables[0].Rows[i].ItemArray[7].ToString() + "^" + ds.Tables[0].Rows[i].ItemArray[5].ToString();

            }
            return Documentos;

        }

        #region Métodos traidos de otro webservice

        [WebMethod]
        public string LoginCorrecto(string Usuario, string Clave)
        {
            try
            {

                Encrypter Cripto = new Encrypter();
                string vAnterior = Cripto.AESEncrypt(Clave);

                OleDbConnection vCon = new OleDbConnection(CadenaConexion);
                vCon.Open();
                string SQL = "select UniqueId from users where email=? and PasswordEncrypted=?";
                OleDbCommand vComando = new OleDbCommand(SQL, vCon);
                vComando.Parameters.Add("@Usuario", OleDbType.VarChar, 50).Value = Usuario;
                vComando.Parameters.Add("@vAnterior", OleDbType.VarChar, 50).Value = vAnterior;

                OleDbDataAdapter da = new OleDbDataAdapter();
                DataSet ds = new DataSet();

                da.SelectCommand = vComando;
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].Rows[0].ItemArray[0].ToString();
                }
                else
                {
                    return "-1";
                }

            }
            catch (Exception e)
            {
                return "-1";
            }
        }


        [WebMethod]
        public String AltaExpediente(String nombreExpediente, int nivelComparticion, String tokenUsuario)
        {

            try
            {

                string DossierUniqueId = Guid.NewGuid().ToString();

                String SQLInsertDossier = "INSERT INTO Dossiers (UniqueId, Timestamp, CreationDate, SchemaVersion, LevelGrantPrivileges, UserUniqueId) VALUES ('" +
                    DossierUniqueId + "','" + DevolverUltimoTimestamp() + "' , GETDATE(), '1.0', 0,'" + tokenUsuario + "');";

                OleDbConnection vCon = new OleDbConnection(CadenaConexion);
                OleDbCommand vComando = new OleDbCommand(SQLInsertDossier, vCon);

                vCon.Open();
                int resp = vComando.ExecuteNonQuery();
                if (resp < 1) return "-1";
                vCon.Close();


                String CompanyId = getCompanyId(tokenUsuario);


                String DossierCompUniqueId = Guid.NewGuid().ToString();

                String SQLInsertDossierCompanyDatas = "INSERT INTO DossierCompanyDatas (LastModify, ShareLevel, isRead, isArchived, DossierName, " +
                    "CompanyUniqueId, DossierUniqueId, UserUniqueId, UserCreator, UserLastModify) VALUES" +
                    "(GETDATE(),'" + nivelComparticion + "','False','False','" + nombreExpediente + "','" +
                    CompanyId + "','" + DossierUniqueId + "','" + tokenUsuario + "','" + tokenUsuario + "','" + tokenUsuario + "');";

                vComando = new OleDbCommand(SQLInsertDossierCompanyDatas, vCon);
                vCon.Open();
                resp = vComando.ExecuteNonQuery();
                vCon.Close();
                if (resp < 1) return "-1";
                else return DossierUniqueId;


            }
            catch (Exception e)
            {
                return "-1";
            }
        }

        [WebMethod]
        public int CompartirExpediente(String idExpediente, String tokenUsuario)
        {
            try
            {

                String nombreExpediente = getNombreExpediente(idExpediente);
                int nivelComparticion = getNivelComparticionExpediente(idExpediente);
                String CompanyId = getCompanyId(tokenUsuario);
                String idUserCreator = getUserCreatorId(idExpediente);

                //string DossierUniqueId = Guid.NewGuid().ToString();
                String SQLInsertDossierCompanyDatas = "INSERT INTO DossierCompanyDatas (LastModify, ShareLevel, isRead, isArchived, DossierName, " +
                 "CompanyUniqueId, DossierUniqueId, UserUniqueId, UserCreator, UserLastModify) VALUES" +
                 "(GETDATE(),'" + nivelComparticion + "','False','False','" + nombreExpediente + "','" +
                 CompanyId + "','" + idExpediente + "','" + tokenUsuario + "','" + idUserCreator + "','" + tokenUsuario + "');";

                OleDbConnection vCon = new OleDbConnection(CadenaConexion);
                OleDbCommand vComando = new OleDbCommand(SQLInsertDossierCompanyDatas, vCon);

                vCon.Open();
                int resp = vComando.ExecuteNonQuery();
                if (resp < 1) return -1;
                vCon.Close();
                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        private int getNivelComparticionExpediente(string idExpediente)
        {
            try
            {
                String nivelComparticion = "-1";
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);
                String getNivelComparticion = "SELECT ShareLevel FROM DossierCompanyDatas WHERE (DossierUniqueId = '" + idExpediente + "')";

                OleDbCommand vComando = new OleDbCommand(getNivelComparticion, vCon);

                vCon.Open();
                nivelComparticion = vComando.ExecuteScalar().ToString();
                vCon.Close();
                return Convert.ToInt32(nivelComparticion);
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        private string getNombreExpediente(string idExpediente)
        {
            try
            {
                String NombreExpediente = "-1";
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);
                String getNombreExpediente = "SELECT DossierName FROM DossierCompanyDatas WHERE (DossierUniqueId = '" + idExpediente + "')";

                OleDbCommand vComando = new OleDbCommand(getNombreExpediente, vCon);

                vCon.Open();
                NombreExpediente = vComando.ExecuteScalar().ToString();
                vCon.Close();
                return NombreExpediente;
            }
            catch (Exception e)
            {
                return "-1";
            }
        }

        private string getCompanyId(string tokenUsuario)
        {
            try
            {
                String CompanyId = "-1";
                OleDbConnection vCon = new OleDbConnection(CadenaConexion);
                String SQLGetCompanyId = "SELECT CompanyUniqueId FROM Users WHERE (UniqueId = '" + tokenUsuario + "')";

                OleDbCommand vComando = new OleDbCommand(SQLGetCompanyId, vCon);

                vComando = new OleDbCommand(SQLGetCompanyId, vCon);
                vCon.Open();
                CompanyId = vComando.ExecuteScalar().ToString();
                vCon.Close();
                return CompanyId;
            }
            catch (Exception e)
            {
                return "-1";
            }
        }

        #endregion

    }


        
}
